FROM ubuntu:18.04
LABEL maintainer="@fernandohillebrand <fer.hill@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
    && apt install -y --no-install-recommends apt-utils \
    && apt install -y software-properties-common \
    && apt update \
    && apt -y install nano \
    && apt -y install composer

RUN add-apt-repository "deb http://archive.ubuntu.com/ubuntu bionic main"
RUN apt update

#Basic
RUN apt install -y php && \
    apt install -y php-mbstring && \
    apt install -y php-mysql && \
    apt install -y php-xml && \
    # apt install -y php-gd && \
    apt install -y php-curl && \
    apt install -y php-bcmath && \
    apt install -y php-zip && \
    apt install -y php-opcache && \
    apt install -y php-amqplib && \
    apt install -y php-xdebug && \
    apt install -y php-pgsql && \
    apt install -y php-tokenizer && \
    apt install -y php-ctype && \
    apt install -y php-json && \
    apt install -y curl && \
    apt install -y php-redis && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash  && \
    apt install -y nodejs && \
    apt install -y php-font-lib

#MONGO
RUN apt install -y php-pear && \
    apt install -y php-dev && \
    pecl channel-update pecl.php.net && \
    pecl install mongodb


RUN echo "extension=mongodb.so" >> /etc/php/7.2/cli/php.ini


#Symfony
WORKDIR '/usr/local/bin/symfony'
RUN apt install -y wget
RUN wget https://get.symfony.com/cli/installer -O - | bash

RUN echo alias reApache='/etc/init.d/apache2 reload' >> ~/.bashrc
RUN a2enmod rewrite
ENV APACHE_RUN_USER    1000
ENV APACHE_RUN_GROUP   www-data
ENV APACHE_PID_FILE    /var/run/apache2.pid
ENV APACHE_RUN_DIR     /var/run/apache2
ENV APACHE_LOCK_DIR    /var/lock/apache2
ENV APACHE_LOG_DIR     /var/log/apache2
CMD []

COPY ./dockerfiles/php/apache/site.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data /var/www/
RUN rm -R /var/www/html

# Install wkhtmltopdf
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb
RUN apt install -y ./wkhtmltox_0.12.5-1.bionic_amd64.deb

WORKDIR '/var/www/html'
COPY ./microcreditos/composer.json composer.json
COPY ./microcreditos/symfony.lock symfony.lock
ADD ./microcreditos/bin ./bin/
ADD ./microcreditos/config ./config
ADD ./microcreditos/var ./var

# Argunment to create a dockerFile
ARG MYSQL_DATABASE
ARG MYSQL_USER
ARG MYSQL_PASSWORD
ARG MYSQL_HOST
ARG MYSQL_PORT
# Create a file to store the environment variables
RUN touch .env
RUN echo 'APP_ENV=dev' >> .env
RUN echo 'APP_SECRET=4c5e472ce4a619cf203554c804b6328a' >> .env
RUN echo "DATABASE_URL=mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_HOST}:${MYSQL_PORT}/${MYSQL_DATABASE}" >> .env
RUN echo 'MAILER_URL=http://localhost' >> .env

ENTRYPOINT ["apachectl","-DFOREGROUND"]
EXPOSE 80
