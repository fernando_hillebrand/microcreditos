#!/bin/sh

docker-compose up -d 

# wait for the docker to be ready
echo "Wait for the docker to be ready"
while ! curl http://localhost:9090; do
    sleep 1;
done;
echo "Docker is up and running"

# install dependencies
docker exec microcreditos_server composer install