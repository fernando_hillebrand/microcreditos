<?php
namespace App\Service;

use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class RolesHelper
{
    private $rolesHierarchy;

    public function __construct(RoleHierarchyInterface $rolesHierarchy)
    {
        $this->rolesHierarchy = $rolesHierarchy;
    }

    /**
     * Return roles.
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = array();

        array_walk_recursive($this->rolesHierarchy, function($val) use (&$roles) {
            $roles[$val] = $val;
        });

        return array_unique($roles);
    }
}