<?php
namespace App\Entity;
use App\Controller\DefaultController;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository as _em;
use Entity\Cuota;
/**
 * App\Entity\Credito
 *
 * @ORM\Table(name="credito")
 * @ORM\Entity(repositoryClass="App\Repository\CreditoRepository")
 */
class Credito extends BaseClass
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="nro_credito", type="string",length=6)
     */
    protected $nroCredito;
    
    /**
     * @ORM\Column(name="nro_carpeta", type="string", length=6, nullable=true)
     */
    protected $nroCarpeta;
    
    /**
     * @ORM\Column(name="nro_renovacion", type="integer", nullable=true)
     */
    protected $nroRenovacion;
    
    /**
     * @ORM\Column(name="fecha_credito", type="date")
     */
    protected $fechaCredito;

    /**
     * @ORM\Column(name="fecha_solicitud_credito", type="date")
     */
    protected $fechaSolicitudCredito;
    
    /**
     * @ORM\Column(name="monto_credito", type="integer")
     */
    protected $montoCredito;
    
    /**
     * @ORM\Column(name="cant_cuotas", type="integer")
     */
    protected $cantCuotas;
    
    /**
     * @ORM\Column(name="periodo", type="integer")
     */
    protected $periodo;
 
    /**
     * @ORM\Column(name="porc_interes", type="decimal", scale=2)
     */
    protected $porcInteres;
    
    /**
     * @ORM\Column(name="porc_punitorio", type="decimal", scale=2)
     */
    protected $porcPunitorio;
    
    /**
     * @ORM\Column(name="gasto_administrativo", type="decimal", scale=2)
     */
    protected $gastoAdministrativo;

    /**
     * @ORM\Column(name="aprobado", type="boolean")
     */
    protected $aprobado; 
    
    /**
     * @ORM\Column(name="incobrable", type="boolean", nullable=true)
     */
    protected $incobrable; 
    /**
     * @ORM\Column(name="fecha_incobrable", type="datetime", nullable=true)
     */
    protected $fechaIncobrable;     
    
    /**
     * @ORM\Column(name="sin_intereses", type="boolean")
     */
    protected $sinIntereses; 
    
    /**
     * @ORM\Column(name="sin_gastos", type="boolean")
     */
    protected $sinGastos; 
    
    /**
     * @ORM\Column(name="sin_punitorios", type="boolean")
     */
    protected $sinPunitorios; 
    
    /**
     * @ORM\Column(name="fecha_aprobacion", type="datetime", nullable=true)
     */
    protected $fechaAprobacion; 

    /**
     * @ORM\Column(name="cancelado", type="boolean")
     */
    protected $cancelado; 
    
    /**
     * @ORM\Column(name="fecha_cancelacion", type="datetime", nullable=true)
     */
    protected $fechaCancelacion; 
    
    /**
     * @ORM\Column(name="fecha_vto_cuota1", type="datetime", nullable=true)
     */
    protected $fechaVtoCuota1; 
    
    /**
     * @ORM\Column(name="fecha_entrega", type="datetime", nullable=true)
     */
    protected $fechaEntrega; 
    
    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta; 
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="creditos")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;   
    
    /**
     *@ORM\ManyToOne(targetEntity="Grupo", inversedBy="creditos")
     *@ORM\JoinColumn(name="grupo_id", referencedColumnName="id") 
     */
    protected $grupo;   
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="creditos")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $asesor;
    
    /**
    * @ORM\OneToMany(targetEntity="CreditoEmprendedor", mappedBy="credito",cascade={"persist","remove"})
    */
    protected $emprendedores;
    
    /**
    * @ORM\OneToMany(targetEntity="Cuota", mappedBy="credito",orphanRemoval=true,cascade={"persist","remove"})
    */
    protected $cuotas;
    
     /**
    * @ORM\OneToMany(targetEntity="CreditoRefinanciacion", mappedBy="credito")
    * @ORM\OrderBy({"fechaRefinanciacion" = "DESC"})
    */
    protected $refinanciaciones;
    
    protected $primerVtoImpago;
    protected $ultimaCuotaPaga;
    protected $ultimoPago;
    protected $renovado;
    protected $deuda;
            
    public function __construct()
    {
        $this->emprendedores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->periodo = 7;
        $this->nroRenovacion = 0;
        $this->cancelado = 0;
        $this->renovado = 0;
        $this->aprobado = 0;
        $this->incobrable = 0;
        $this->fechaAlta = new \DateTime();      
    }
    public function __toString() {
        return $this->nroCredito.'-'.$this->nroRenovacion;
    }
    public function getTitle(){
        return '" '.$this->nroCredito.' - '.$this->nroRenovacion.' "';
    }    
    
    public function getTituloCompleto(){
        $nombre='';
        if($this->grupo){
            $nombre .= ' Grupo:'. $this->grupo->getNombre();
        }else{
            $nombre .=" Emprendedor: ";
            foreach($this->emprendedores as $emprendedoresCredito){
                $emprendor = $emprendedoresCredito->getEmprendedor();
                $nombre .= ' '. $emprendor->getApellido().', '.$emprendor->getNombres();
            }
        }
        return $this->nroCredito.'/'.$this->nroRenovacion . ' Programa:'.$this->programa->getSigla().' '.$nombre;
    }
   
//    public function getPrimerVtoImpago(){
//        return $this->primerVtoImpago;
//    }
    public function setPrimerVtoImpago($cuota){
        $this->primerVtoImpago = $cuota;
    }
    public function getUltimaCuotaPaga(){
        $this->ultimaCuotaPaga = 0;
        foreach ($this->getCuotas() as $cuota) {
            if($cuota->getCancelado()){
                if($this->ultimaCuotaPaga < $cuota->getNroCuota()){
                    $this->ultimaCuotaPaga = $cuota->getNroCuota();
                }
            }
        }
        return $this->ultimaCuotaPaga;
    }
    public function getCantidadCuotaCredito(){
        $ultimaCuota= 0;
        foreach ($this->getCuotas() as $cuota) {
            if($ultimaCuota < $cuota->getNroCuota()){
                $ultimaCuota= $cuota->getNroCuota();
            }
        }
        return $ultimaCuota;
    }
    public function getPrimerVtoImpago(){
        foreach ($this->getCuotas() as $cuota) {
            if(!$cuota->getCancelado()){
                return $cuota->getFechaVencimiento();
            }
        }
        return null;
    }
    public function getUltimoPago(){
        $fechaPago=null;
        foreach ($this->getCuotas() as $cuota) {
            foreach($cuota->getPagos() as $pago) {
                if (is_null($fechaPago)) {
                    $fechaPago = $pago->getFecha();
                }else{
                    if($fechaPago < $pago->getFecha()){
                        $fechaPago = $pago->getFecha();
                    }
                }
            }
        }
        return $fechaPago;
    }
    public function setUltimaCuotaPaga($cuota){
        $this->ultimaCuotaPaga = $cuota;
    }
    public function setUltimoPago($fecha){
        $this->ultimoPago = $fecha;
    }
    public function getDeuda(){
        return $this->deuda;
    }
    public function setDeuda($deuda){
        $this->deuda = $deuda;
    }
    /* marca de credito que fue renovado antes de cancelarse */
    public function getRenovado(){
        return $this->renovado;
    }
    public function setRenovado($renovado){
        $this->renovado = $renovado;
    }
    
    public function textoPunitorioParaContrato(){
        if( $this->getPrograma()->getMontoPunitorio()>0 ){
            return '$'.$this->getPrograma()->getMontoPunitorio().' por día por cuota adeudada';
        }else{
            return  $this->getPrograma()->getPorcPunitorio(). ' % mensual de la tasa compensatoria sobre capital e interés adeudados';
        }
    }
    
    /**
     * Get emprendedor individual
     *
     * @return \App\Entity\CreditoEmprendedor
     */
    public function getEmprendedor()
    {
        return $this->emprendedores[0];
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nroCredito
     *
     * @param string $nroCredito
     */
    public function setNroCredito($nroCredito)
    {
        $this->nroCredito = $nroCredito;
    }

    /**
     * Get nroCredito
     *
     * @return string 
     */
    public function getNroCredito()
    {
        return $this->nroCredito;
    }

    /**
     * Set nroRenovacion
     *
     * @param integer $nroRenovacion
     */
    public function setNroRenovacion($nroRenovacion)
    {
        $this->nroRenovacion = $nroRenovacion;
    }

    /**
     * Get nroRenovacion
     *
     * @return integer 
     */
    public function getNroRenovacion()
    {
        return $this->nroRenovacion;
    }

    /**
     * Set fechaCredito
     *
     * @param date $fechaCredito
     */
    public function setFechaSolicitudCredito($fechaSolicitudCredito)
    {
        $this->fechaSolicitudCredito = $fechaSolicitudCredito;
    }

    /**
     * Get fechaCredito
     *
     * @return date
     */
    public function getFechaSolicitudCredito()
    {
        return $this->fechaSolicitudCredito;
    }

    /**
     * Set fechaCredito
     *
     * @param date $fechaCredito
     */
    public function setFechaCredito($fechaCredito)
    {
        $this->fechaCredito = $fechaCredito;
    }

    /**
     * Get fechaCredito
     *
     * @return date 
     */
    public function getFechaCredito()
    {
        return $this->fechaCredito;
    }

    /**
     * Set montoCredito
     *
     * @param integer $montoCredito
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    }

    /**
     * Get montoCredito
     *
     * @return integer 
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set cantCuotas
     *
     * @param integer $cantCuotas
     */
    public function setCantCuotas($cantCuotas)
    {
        $this->cantCuotas = $cantCuotas;
    }

    /**
     * Get cantCuotas
     *
     * @return integer 
     */
    public function getCantCuotas()
    {
        return $this->cantCuotas;
    }

    /**
     * Set Periodo
     *
     * @param integer $periodo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;
    }

    /**
     * Get Periodo
     *
     * @return integer 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set porcInteres
     *
     * @param decimal $porcInteres
     */
    public function setPorcInteres($porcInteres)
    {
        $this->porcInteres = $porcInteres;
    }

    /**
     * Get porcInteres
     *
     * @return decimal 
     */
    public function getPorcInteres()
    {
        return $this->porcInteres;
    }

    /**
     * Set porcPunitorio
     *
     * @param decimal $porcPunitorio
     */
    public function setPorcPunitorio($porcPunitorio)
    {
        $this->porcPunitorio = $porcPunitorio;
    }

    /**
     * Get porcPunitorio
     *
     * @return decimal 
     */
    public function getPorcPunitorio()
    {
        return $this->porcPunitorio;
    }

    /**
     * Set gastoAdministrativo
     *
     * @param decimal $gastoAdministrativo
     */
    public function setGastoAdministrativo($gastoAdministrativo)
    {
        $this->gastoAdministrativo = $gastoAdministrativo;
    }

    /**
     * Get gastoAdministrativo
     *
     * @return decimal 
     */
    public function getGastoAdministrativo()
    {
        return $this->gastoAdministrativo;
    }

    /**
     * Set aprobado
     *
     * @param boolean $aprobado
     */
    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;
    }

    /**
     * Get aprobado
     *
     * @return boolean 
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }    

    /**
     * Set fechaAprobacion
     *
     * @param datetime $fechaAprobacion
     */
    public function setFechaAprobacion($fechaAprobacion)
    {
        $this->fechaAprobacion = $fechaAprobacion;
    }

    /**
     * Get fechaAprobacion
     *
     * @return datetime 
     */
    public function getFechaAprobacion()
    {
        return $this->fechaAprobacion;
    }
    
    /**
     * Get incobrable
     *
     * @return boolean 
     */
    public function getIncobrable()
    {
        return $this->incobrable;
    }
    
    /**
     * Set incobrable
     *
     * @param boolean $incobrable
     */
    public function setIncobrable($incobrable)
    {
        $this->incobrable = $incobrable;
    }
        
    /**
     * Set fechaIncobrable
     *
     * @param datetime $fechaIncobrable
     */
    public function setFechaIncobrable($fechaIncobrable)
    {
        $this->fechaIncobrable = $fechaIncobrable;
    }

    /**
     * Get fechaIncobrable
     *
     * @return datetime 
     */
    public function getFechaIncobrable()
    {
        return $this->fechaIncobrable;
    }    

    /**
     * Set cancelado
     *
     * @param boolean $cancelado
     */
    public function setCancelado($cancelado)
    {
        $this->cancelado = $cancelado;
    }

    /**
     * Get cancelado
     *
     * @return boolean 
     */
    public function getCancelado()
    {
        return $this->cancelado;
    }

    /**
     * Set fechaCancelacion
     *
     * @param datetime $fechaCancelacion
     */
    public function setFechaCancelacion($fechaCancelacion)
    {
        $this->fechaCancelacion = $fechaCancelacion;
    }

    /**
     * Get fechaCancelacion
     *
     * @return datetime 
     */
    public function getFechaCancelacion()
    {
        return $this->fechaCancelacion;
    }

    /**
     * Set fechaVtoCuota1
     *
     * @param datetime $fechaVtoCuota1
     */
    public function setFechaVtoCuota1($fechaVtoCuota1)
    {
        $this->fechaVtoCuota1 = $fechaVtoCuota1;
    }

    /**
     * Get fechaVtoCuota1
     *
     * @return datetime 
     */
    public function getFechaVtoCuota1()
    {
        return $this->fechaVtoCuota1;
    }

    /**
     * Set fechaEntrega
     *
     * @param datetime $fechaEntrega
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;
    }

    /**
     * Get fechaEntrega
     *
     * @return datetime 
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set grupo
     *
     * @param \App\Entity\Grupo $grupo
     */
    public function setGrupo( $grupo)
    {
        $this->grupo = $grupo;
    }

    /**
     * Get grupo
     *
     * @return \App\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set asesor
     *
     * @param \App\Entity\User $asesor
     */
    public function setAsesor(\App\Entity\User $asesor)
    {
        $this->asesor = $asesor;
    }

    /**
     * Get asesor
     *
     * @return \App\Entity\User
     */
    public function getAsesor()
    {
        return $this->asesor;
    }

    /**
     * Add emprendedores
     *
     * @param \App\Entity\CreditoEmprendedor $emprendedores
     */
    public function addCreditoEmprendedor(\App\Entity\CreditoEmprendedor $emprendedores)
    {
        $this->emprendedores[] = $emprendedores;
    }

    /**
     * Get emprendedores
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEmprendedores()
    {
        return $this->emprendedores;
    }

    /**
     * Add cuotas
     *
     * @param \App\Entity\Cuota $cuotas
     */
    public function addCuota(\App\Entity\Cuota $cuotas)
    {
        $this->cuotas[] = $cuotas;
    }

    /**
     * Get cuotas
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCuotas()
    {
        return $this->cuotas;
    }
    /**
     * unset Cuotas
     */
  /*  public function unsetCuotas()
    {
        $this->cuotas[]->clear() ;
    }*/

    /**
     * Set nroCarpeta
     *
     * @param string $nroCarpeta
     */
    public function setNroCarpeta($nroCarpeta)
    {
        $this->nroCarpeta = $nroCarpeta;
    }

    /**
     * Get nroCarpeta
     *
     * @return string 
     */
    public function getNroCarpeta()
    {
        return $this->nroCarpeta;
    }

    /**
     * Add refinanciaciones
     *
     * @param \App\Entity\CreditoRefinanciacion $refinanciaciones
     */
    public function addCreditoRefinanciacion(\App\Entity\CreditoRefinanciacion $refinanciaciones)
    {
        $this->refinanciaciones[] = $refinanciaciones;
    }

    /**
     * Get refinanciaciones
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRefinanciaciones()
    {
        return $this->refinanciaciones;
    }

    /**
     * Set sinIntereses
     *
     * @param boolean $sinIntereses
     */
    public function setSinIntereses($sinIntereses)
    {
        $this->sinIntereses = $sinIntereses;
    }

    /**
     * Get sinIntereses
     *
     * @return boolean 
     */
    public function getSinIntereses()
    {
        return $this->sinIntereses;
    }

    /**
     * Set sinGastos
     *
     * @param boolean $sinGastos
     */
    public function setSinGastos($sinGastos)
    {
        $this->sinGastos = $sinGastos;
    }

    /**
     * Get sinGastos
     *
     * @return boolean 
     */
    public function getSinGastos()
    {
        return $this->sinGastos;
    }
    /**
     * Set sinPunitorios
     *
     * @param boolean $sinPunitorios
     */
    public function setSinPunitorios($sinPunitorios)
    {
        $this->sinPunitorios = $sinPunitorios;
    }

    /**
     * Get sinPunitorios
     *
     * @return boolean 
     */
    public function getSinPunitorios()
    {
        return $this->sinPunitorios;
    }
    
    public function getRecomendador(){
        if($this->grupo){
            return $this->grupo->getRecomendador();
        }else{
            $emp = $this->emprendedores[0];
            return $emp->getEmprendedor()->getRecomendador();
        }
    }
    
    public function getMontoIncobrable(){
        $capital = 0;
        foreach ($this->getCuotas() as $cuota) {
            if ($cuota->getSaldo() > 0) {
                if($cuota->getPagado()>0){
                    /*con pago parcial*/
                    $porcCapital = ($cuota->getMontoCapital() * 100) / $cuota->getMontoTotal();
                    $capital += ($cuota->getSaldo() * $porcCapital) / 100;                    
                }else{
                    /*cuota impaga*/
                    $capital += $cuota->getMontoCapital();
                }                
            }
        }
        return (int)$capital;
    }

    public function getStatus(){
        if(!$this->getAprobado()){
            return 'Solicitud';
        }

        if($this->cancelado == true ){
            return 'Cancelado';
        }

        if($this->checkMoroso()){
            return 'Moroso';
        }

        if($this->getActivo() AND $this->sinPago()){
            return 'Solicitud Aprobada sin pago';
        }

//        if($this->getActivo()){
//            return 'Solicitud Borrado';
//        }

        return 'Al dia';
    }

    public function sinPago(){
        $pago = 0;
        foreach($this->getCuotas() as $cuota){
            $pago +=$cuota->getPagado();
        }
        return $pago == 0;
    }

    public function checkMoroso(){
       if( $this->getSinPunitorios()== true){
           return false;
       }

       if($this->cancelado == true){
           return false;
       }

       foreach($this->getCuotas() as $cuota){
            if($cuota->getFechaVencimiento() < new \DateTime()){
                if($cuota->getSaldo() > 0){
                    return true;
                }
            }
       }
       return false;
    }

    public function getMora($feriados){
        $mora = array();
        $fecha = new \Datetime ;
        $fechaHoy = $fecha->format('d-m-Y');
        $tasa = $this->getPrograma()->getPorcPunitorio()/30;
        $montoPun = $this->getPrograma()->getMontoPunitorio();
        foreach ($this->getCuotas() as $cuota) {
            $montoPunitorios = 0;
            $vencimiento = $cuota->getFechaVencimiento();
            while ( in_array($vencimiento->format('Y-m-d'), $feriados ) || in_array( $vencimiento->format('N'),[6,7])  ){
                $vencimiento->modify("+1 days");
            }
            if($cuota->getPagado()>0)
                $fechaCuota = ($vencimiento>$cuota->getFechaCancelacion())
                    ? $vencimiento : $cuota->getFechaCancelacion();
            //$fechaCuota = $cuota->getFechaCancelacion();
            else  $fechaCuota = $vencimiento;

            $dias = DefaultController::dateDiff($fechaCuota->format('d-m-Y'), $fechaHoy);
            if( !$cuota->getCancelado() && $dias>0 && $cuota->getSaldo()>0){
                if($montoPun>0)
                    $montoPunitorios = $montoPun * $dias ;
                else
                    $montoPunitorios = round((($cuota->getSaldo() * $tasa / 100) * $dias ));
            }
            array_push($mora, $montoPunitorios);
        }
        return $mora;
    }

    public function getTotalCredito(){
        $total = 0;
        foreach ($this->getCuotas() as $cuota) {
            $total += $cuota->getMontoTotal();
        }
        return $total;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' nroCredito: '.$this->nroCredito .' | ';
        $str .=' nroRenovacion: '.$this->nroRenovacion .' | ';
        $str .=' fechaSolicitudCredito: '.$this->fechaSolicitudCredito->format('d-m-Y H:i:s') .' | ';
        $str .=' fechaCredito: '.$this->fechaCredito->format('d-m-Y H:i:s') .' | ';
        $str .=' montoCredito: '.$this->montoCredito .' | ';
        $str .=' cantCuotas: '.$this->cantCuotas .' | ';
        $str .=' periodo: '.$this->periodo .' | ';
        $str .=' porcInteres: '.$this->porcInteres .' | ';
        $str .=' porcPunitorio: '.$this->porcPunitorio .' | ';
        $str .=' gastoAdministrativo: '.$this->gastoAdministrativo .' | ';
        $str .=' aprobado: '.$this->aprobado .' | ';
        $str .=' fechaAprobacion: '.$this->fechaAprobacion->format('d-m-Y H:i:s') .' | ';
        $str .=' fechaEntrega: '.$this->fechaEntrega->format('d-m-Y H:i:s') .' | ';
        $str .=' Estatus: '.$this->getStatus() .' | ';
        $str .=' emprendedores: '.$this->emprendedores .' | ';
        $str .=' grupo: '.$this->grupo .' | ';
        $str .= parent::getDataLog();
        return $str;
    }

}