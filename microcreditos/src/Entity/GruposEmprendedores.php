<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Emprendedor
 *
 * @ORM\Table(name="grupos_emprendedores")
 * @ORM\Entity(repositoryClass="App\Repository\GruposEmprendedoresRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class GruposEmprendedores extends BaseClass
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     *@ORM\ManyToOne(targetEntity="Grupo", inversedBy="gruposEmprendedores")
     *@ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     */
    protected $grupo;

    /**
     *@ORM\ManyToOne(targetEntity="Emprendedor", inversedBy="gruposEmprendedores")
     *@ORM\JoinColumn(name="emprendedor_id", referencedColumnName="id")
     */
    protected $emprendedor;


    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * @param mixed $grupo
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;
    }

    /**
     * @return mixed
     */
    public function getEmprendedor()
    {
        return $this->emprendedor;
    }

    /**
     * @param mixed $emprendedor
     */
    public function setEmprendedor($emprendedor)
    {
        $this->emprendedor = $emprendedor;
    }

    public function __toString()
    {
        $string = $this->getGrupo()->__toString();
        return $string;
    }


}