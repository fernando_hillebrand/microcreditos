<?php
// src/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    protected $nombre;

    /**
     * @var string $apellido
     *
     * @ORM\Column(name="apellido", type="string", length=100)
     */
    protected $apellido;


    public function getId(){
        return $this->id;
    }

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    public function getTitle(){
        return '" '.$this->getUsername().' - '.$this->getEmail().' "';
    }


    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setApellido($apellido){
        $this->apellido = $apellido;
    }

    public function getApellido(){
        return $this->apellido;
    }

    public function getNombreCompleto(){
        return $this->nombre . ' '. $this->apellido;
    }

    /**
     * Returns whether or not the given user is equivalent to *this* user.
     *
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     *
     * @return Boolean
     */
    public function equals(UserInterface $user)
    {
        if($user->getId() == $this->id){
            return true;
        }
        return false;
    }

    public function getrolesIndex(){
        $string = '';
        $items = $this->getRoles();
        foreach($items as $index => $item){
            if($index == 0){
                $string .=$item;
            }else{
                $string .=', '.$item;
            }
        }
        return $string;
    }

    public function setActivo($status){
        $this->enabled = $status;
    }

    public function getActivo(){
        return  $this->enabled;
    }


}