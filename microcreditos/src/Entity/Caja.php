<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\ProgramaIngreso
 *
 * @ORM\Table(name="caja")
 * @ORM\Entity(repositoryClass="App\Repository\CajaRepository")
 */

class Caja extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="total", type="decimal", scale=2)
     */
    protected $total;

    /**
     * @ORM\Column(name="ingreso", type="decimal", scale=2)
     */
    protected $ingreso;

    /**
     * @ORM\Column(name="egreso", type="decimal", scale=2)
     */
    protected $egreso;


    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="caja")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id",nullable=true)
     */
    protected $programa;

    /**
     *@ORM\ManyToOne(targetEntity="Gastos", inversedBy="caja")
     *@ORM\JoinColumn(name="gasto_id", referencedColumnName="id",nullable=true)
     */
    protected $gasto;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="programaIngresos")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="concepto", referencedColumnName="id",nullable=true)
     **/
    protected $concepto;

    /**
     * @ORM\Column(name="obserbacion", type="string", nullable=true)
     */
    protected $observacion;

    /**
     *@ORM\ManyToOne(targetEntity="Credito", inversedBy="caja")
     *@ORM\JoinColumn(name="credito_id", referencedColumnName="id",nullable=true)
     */
    protected $credito;

    /**
     *@ORM\ManyToOne(targetEntity="Pago", inversedBy="caja")
     *@ORM\JoinColumn(name="pago_id", referencedColumnName="id",nullable=true)
     */
    protected $pago;
    
    public function __toString() {
        return $this->total;
    }

    public function __construct()
    {
        $this->fecha = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param decimal $monto
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * Get total
     *
     * @return decimal 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set ingreso
     *
     * @param decimal $monto
     */
    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;
    }

    /**
     * Get ingreso
     *
     * @return decimal
     */
    public function getIngreso()
    {
        return $this->ingreso;
    }

    /**
     * Set egreso
     *
     * @param decimal $monto
     */
    public function setEgreso($egreso)
    {
        $this->egreso = $egreso;
    }

    /**
     * Get egreso
     *
     * @return decimal
     */
    public function getEgreso()
    {
        return $this->egreso;
    }


    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setGastos(\App\Entity\Gastos $gasto)
    {
        $this->gasto = $gasto;
    }

    /**
     * Get gasto
     *
     * @return \App\Entity\Gastos
     */
    public function getGastos()
    {
        return $this->gasto;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set concepto
     *
     * @param \App\Entity\Parametro $concepto
     */
    public function setConcepto(\App\Entity\Parametro $concepto)
    {
        $this->concepto = $concepto;
    }

    /**
     * Get concepto
     *
     * @return \App\Entity\Parametro
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set observacion
     *
     * @param text $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * Get observacion
     *
     * @return text
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    public function getTitle(){
        return $this->concepto;
    }

    /**
     * @return mixed
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * @param mixed $gasto
     */
    public function setGasto($gasto)
    {
        $this->gasto = $gasto;
    }

    /**
     * @return mixed
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * @param mixed $credito
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;
    }

    /**
     * @return mixed
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * @param mixed $pago
     */
    public function setPago($pago)
    {
        $this->pago = $pago;
    }

    public function getDataLog(){
        $str =' total: '.$this->total .' | ';
        $str .=' ingreso: '.$this->ingreso .' | ';
        $str .=' egreso: '.$this->egreso .' | ';
        $str .=' concepto: '.$this->concepto .' | ';
        $str .=' programa: '.$this->programa .' | ';
        $str .=' credito: '.$this->credito .' | ';
        $str .= parent::getDataLog();
        return $str;
    }

}