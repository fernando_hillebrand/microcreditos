<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 *
 * @ORM\Table(name="agenda",uniqueConstraints={@ORM\UniqueConstraint(name="agenda_idx", columns={"fecha_inicio", "asesor_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\AgendaRepository")
 * @DoctrineAssert\UniqueEntity(fields={"fecha_inicio","asesor_id"})
 */

class Agenda extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="fecha_inicio", type="datetime")
     */
    protected $fechaInicio;
    /**
     * @ORM\Column(name="fecha_fin", type="datetime")
     */
    protected $fechaFin;

    /**
     * @var string $fecha_asignado
     *
     * @ORM\Column(name="fecha_asignado", type="datetime", nullable=true)
     */
    protected $fecha_asignado;
    
    /**
     * @var string $fecha_cancelado
     *
     * @ORM\Column(name="fecha_cancelado", type="datetime", nullable=true)
     */
    protected $fecha_cancelado;

    /**
     * @ORM\Column(name="asistio", type="boolean", nullable=true)
     */
    protected $asistio; 

    /**
     * @ORM\Column(name="nuevo_emprendedor", type="string", length=100, nullable=true)
     */
    protected $nuevoEmprendedor; 

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="turnos")
     */
    protected $asesor;

    /**
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     */
    protected $observaciones;

     /**
     *@ORM\ManyToOne(targetEntity="Grupo", inversedBy="turnos")
     *@ORM\JoinColumn(name="grupo_id", referencedColumnName="id") 
     */
    protected $grupo; 

    /**
     *@ORM\ManyToOne(targetEntity="Emprendedor", inversedBy="turnos")
     *@ORM\JoinColumn(name="emprendedor_id", referencedColumnName="id") 
     */
    protected $emprendedor; 


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param datetime $fechaInicio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
    }

    /**
     * Get fechaInicio
     *
     * @return datetime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param datetime $fechaFin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    /**
     * Get fechaFin
     *
     * @return datetime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set fecha_asignado
     *
     * @param datetime $fechaAsignado
     */
    public function setFechaAsignado($fechaAsignado)
    {
        $this->fecha_asignado = $fechaAsignado;
    }

    /**
     * Get fecha_asignado
     *
     * @return datetime 
     */
    public function getFechaAsignado()
    {
        return $this->fecha_asignado;
    }

    /**
     * Set fecha_cancelado
     *
     * @param datetime $fechaCancelado
     */
    public function setFechaCancelado($fechaCancelado)
    {
        $this->fecha_cancelado = $fechaCancelado;
    }

    /**
     * Get fecha_cancelado
     *
     * @return datetime 
     */
    public function getFechaCancelado()
    {
        return $this->fecha_cancelado;
    }

    /**
     * Set asistio
     *
     * @param boolean $asistio
     */
    public function setAsistio($asistio)
    {
        $this->asistio = $asistio;
    }

    /**
     * Get asistio
     *
     * @return boolean 
     */
    public function getAsistio()
    {
        return $this->asistio;
    }

    /**
     * Set asesor
     *
     * @param \App\Entity\User $asesor
     */
    public function setAsesor(User $asesor)
    {
        $this->asesor = $asesor;
    }

    /**
     * Get asesor
     *
     * @return \App\Entity\User
     */
    public function getAsesor()
    {
        return $this->asesor;
    }

    /**
     * Set grupo
     *
     * @param \App\Entity\Grupo $grupo
     */
    public function setGrupo(Grupo $grupo)
    {
        $this->grupo = $grupo;
    }

    /**
     * Get grupo
     *
     * @return \App\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set emprendedor
     *
     * @param \App\Entity\Emprendedor $emprendedor
     */
    public function setEmprendedor(Emprendedor $emprendedor)
    {
        $this->emprendedor = $emprendedor;
    }

    /**
     * Get emprendedor
     *
     * @return \App\Entity\Emprendedor
     */
    public function getEmprendedor()
    {
        return $this->emprendedor;
    }

    /**
     * Set observaciones
     *
     * @param text $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

    /**
     * Get observaciones
     *
     * @return text 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }
    
    public function getNombre(){
        if($this->getGrupo())
                return $this->getGrupo()->getNombre();
        elseif($this->getEmprendedor())
                return $this->getEmprendedor()->__toString();
        else    return $this->getNuevoEmprendedor();
    }

    /**
     * Set nuevoEmprendedor
     *
     * @param string $nuevoEmprendedor
     */
    public function setNuevoEmprendedor($nuevoEmprendedor)
    {
        $this->nuevoEmprendedor = $nuevoEmprendedor;
    }

    /**
     * Get nuevoEmprendedor
     *
     * @return string 
     */
    public function getNuevoEmprendedor()
    {
        return $this->nuevoEmprendedor;
    }
    
    /*
     * Unset Emprendedor
     */
    public function unsetEmprendedor(){
        unset($this->emprendedor);
    } 
    
    /*
     * Unset Grupo
     */
    public function unsetGrupo(){
        unset($this->grupo);
    }

    public function getDataLog(){
        $str =' fechaInicio: '.$this->fechaInicio->format('d-m-Y H:i:s') .' | ';
        $str .=' fechaFin: '.$this->fechaFin->format('d-m-Y H:i:s') .' | ';
        $str .=' nombre: '.$this->getNombre() .' | ';
        $str .=' asistio: '.$this->asistio .' | ';
        $str .=' fecha_cancelado: '.$this->fecha_cancelado->format('d-m-Y H:i:s') .' | ';
        $str .= parent::getDataLog();
        return $str;
    }
}