<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\EmprendedorDatosAdicionales
 *
 * @ORM\Table(name="emprendedor_datos_adicionales")
 * @ORM\Entity
 */

class EmprendedorDatosAdicionales extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="actividad", type="string", length=200, nullable=true)
     */
    protected $actividad;

    /**
     * @ORM\Column(name="experiencia", type="string", length=50, nullable=true)
     */
    protected $experiencia;

    /**
     * @ORM\Column(name="fecha_inicio_actividad", type="date", nullable=true)
     */
    protected $fechaInicioActividad;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="tipo_actividad", referencedColumnName="id")
     **/  
    protected $tipoActividad;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="lugar_actividad", referencedColumnName="id")
     **/  
    protected $lugarActividad;

    /**
     * @ORM\Column(name="dia_hora_atencion", type="string", length=200, nullable=true)
     */
    protected $diaHoraAtencion;

    /**
     * @ORM\Column(name="ventas_semanal", type="string", length=200, nullable=true)
     */
    protected $ventasSemanal;

    /**
     * @ORM\Column(name="nro_empleados", type="integer", nullable=true)
     */
    protected $nroEmpleados = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="tipo_inmueble_negocio", referencedColumnName="id")
     **/  
    protected $tipoInmuebleNegocio;

     /**
     * @ORM\Column(name="direccion_negocio", type="text", nullable=true)
     */
    protected $direccionNegocio;
    
    /**
     * @ORM\Column(name="ubicacion_negocio", type="string", length=200, nullable=true)
     */
    protected $ubicacionNegocio;
    
    /**
     * @ORM\Column(name="telefono_negocio", type="string", nullable=true)
     */
    protected $telefonoNegocio;
        
    /**
     * @ORM\Column(name="zona_negocio", type="string", nullable=true)
     */
    protected $zonaNegocio;
        
    /**
     * @ORM\Column(name="actividad_secundaria", type="string", length=200, nullable=true)
     */
    protected $actividadSecundaria;
    
    /**
     * @ORM\Column(name="trabajos_anteriores", type="string", length=200, nullable=true)
     */
    protected $trabajosAnteriores;
    
    /**
     * @ORM\Column(name="nombres_conyuge", type="string", length=100, nullable=true)
     */
    protected $nombresConyuge;
    
    /**
     * @ORM\Column(name="apellido_conyuge", type="string", length=100, nullable=true)
     */
    protected $apellidoConyuge;
    
    /**
     * @ORM\Column(name="sobrenombre_conyuge", type="string", length=100, nullable=true)
     */
    protected $sobrenombreConyuge;
    
    /**
     * @ORM\Column(name="dni_conyuge", type="string", length=8, nullable=true)
     */
    protected $dniConyuge;    
    
    /**
     * @ORM\Column(name="fecha_nacimiento_conyuge", type="date", nullable=true)
     */
    protected $fechaNacimientoConyuge;   

    /**
     * @ORM\Column(name="lugar_nacimiento_conyuge", type="string", length=100, nullable=true)
     */
    protected $lugarNacimientoConyuge;

     /**
     * @ORM\Column(name="actividad_conyuge", type="string", length=200, nullable=true)
     */
    protected $actividadConyuge;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="nivel_educativo_conyuge", referencedColumnName="id")
     **/       
    protected $nivelEducativoConyuge;
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actividad
     *
     * @param string $actividad
     */
    public function setActividad($actividad)
    {
        $this->actividad = $actividad;
    }

    /**
     * Get actividad
     *
     * @return string 
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set experiencia
     *
     * @param string $experiencia
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;
    }

    /**
     * Get experiencia
     *
     * @return string 
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Set fechaInicioActividad
     *
     * @param date $fechaInicioActividad
     */
    public function setFechaInicioActividad($fechaInicioActividad)
    {
        $this->fechaInicioActividad = $fechaInicioActividad;
    }

    /**
     * Get fechaInicioActividad
     *
     * @return date 
     */
    public function getFechaInicioActividad()
    {
        return $this->fechaInicioActividad;
    }

    /**
     * Set diaHoraAtencion
     *
     * @param string $diaHoraAtencion
     */
    public function setDiaHoraAtencion($diaHoraAtencion)
    {
        $this->diaHoraAtencion = $diaHoraAtencion;
    }

    /**
     * Get diaHoraAtencion
     *
     * @return string 
     */
    public function getDiaHoraAtencion()
    {
        return $this->diaHoraAtencion;
    }

    /**
     * Set ventasSemanal
     *
     * @param string $ventasSemanal
     */
    public function setVentasSemanal($ventasSemanal)
    {
        $this->ventasSemanal = $ventasSemanal;
    }

    /**
     * Get ventasSemanal
     *
     * @return string 
     */
    public function getVentasSemanal()
    {
        return $this->ventasSemanal;
    }

    /**
     * Set nroEmpleados
     *
     * @param integer $nroEmpleados
     */
    public function setNroEmpleados($nroEmpleados)
    {
        $this->nroEmpleados = $nroEmpleados;
    }

    /**
     * Get nroEmpleados
     *
     * @return integer 
     */
    public function getNroEmpleados()
    {
        return $this->nroEmpleados;
    }

    /**
     * Set direccionNegocio
     *
     * @param text $direccionNegocio
     */
    public function setDireccionNegocio($direccionNegocio)
    {
        $this->direccionNegocio = $direccionNegocio;
    }

    /**
     * Get direccionNegocio
     *
     * @return text 
     */
    public function getDireccionNegocio()
    {
        return $this->direccionNegocio;
    }

    /**
     * Set ubicacionNegocio
     *
     * @param string $ubicacionNegocio
     */
    public function setUbicacionNegocio($ubicacionNegocio)
    {
        $this->ubicacionNegocio = $ubicacionNegocio;
    }

    /**
     * Get ubicacionNegocio
     *
     * @return string 
     */
    public function getUbicacionNegocio()
    {
        return $this->ubicacionNegocio;
    }

    /**
     * Set telefonoNegocio
     *
     * @param string $telefonoNegocio
     */
    public function setTelefonoNegocio($telefonoNegocio)
    {
        $this->telefonoNegocio = $telefonoNegocio;
    }

    /**
     * Get telefonoNegocio
     *
     * @return string 
     */
    public function getTelefonoNegocio()
    {
        return $this->telefonoNegocio;
    }

    /**
     * Set zonaNegocio
     *
     * @param string $zonaNegocio
     */
    public function setZonaNegocio($zonaNegocio)
    {
        $this->zonaNegocio = $zonaNegocio;
    }

    /**
     * Get zonaNegocio
     *
     * @return string 
     */
    public function getZonaNegocio()
    {
        return $this->zonaNegocio;
    }

    /**
     * Set actividadSecundaria
     *
     * @param string $actividadSecundaria
     */
    public function setActividadSecundaria($actividadSecundaria)
    {
        $this->actividadSecundaria = $actividadSecundaria;
    }

    /**
     * Get actividadSecundaria
     *
     * @return string 
     */
    public function getActividadSecundaria()
    {
        return $this->actividadSecundaria;
    }

    /**
     * Set trabajosAnteriores
     *
     * @param string $trabajosAnteriores
     */
    public function setTrabajosAnteriores($trabajosAnteriores)
    {
        $this->trabajosAnteriores = $trabajosAnteriores;
    }

    /**
     * Get trabajosAnteriores
     *
     * @return string 
     */
    public function getTrabajosAnteriores()
    {
        return $this->trabajosAnteriores;
    }

    /**
     * Set nombresConyuge
     *
     * @param string $nombresConyuge
     */
    public function setNombresConyuge($nombresConyuge)
    {
        $this->nombresConyuge = $nombresConyuge;
    }

    /**
     * Get nombresConyuge
     *
     * @return string 
     */
    public function getNombresConyuge()
    {
        return $this->nombresConyuge;
    }

    /**
     * Set apellidoConyuge
     *
     * @param string $apellidoConyuge
     */
    public function setApellidoConyuge($apellidoConyuge)
    {
        $this->apellidoConyuge = $apellidoConyuge;
    }

    /**
     * Get apellidoConyuge
     *
     * @return string 
     */
    public function getApellidoConyuge()
    {
        return $this->apellidoConyuge;
    }

    /**
     * Set sobrenombreConyuge
     *
     * @param string $sobrenombreConyuge
     */
    public function setSobrenombreConyuge($sobrenombreConyuge)
    {
        $this->sobrenombreConyuge = $sobrenombreConyuge;
    }

    /**
     * Get sobrenombreConyuge
     *
     * @return string 
     */
    public function getSobrenombreConyuge()
    {
        return $this->sobrenombreConyuge;
    }

    /**
     * Set dniConyuge
     *
     * @param string $dniConyuge
     */
    public function setDniConyuge($dniConyuge)
    {
        $this->dniConyuge = $dniConyuge;
    }

    /**
     * Get dniConyuge
     *
     * @return string 
     */
    public function getDniConyuge()
    {
        return $this->dniConyuge;
    }

    /**
     * Set fechaNacimientoConyuge
     *
     * @param date $fechaNacimientoConyuge
     */
    public function setFechaNacimientoConyuge($fechaNacimientoConyuge)
    {
        $this->fechaNacimientoConyuge = $fechaNacimientoConyuge;
    }

    /**
     * Get fechaNacimientoConyuge
     *
     * @return date 
     */
    public function getFechaNacimientoConyuge()
    {
        return $this->fechaNacimientoConyuge;
    }

    /**
     * Set lugarNacimientoConyuge
     *
     * @param string $lugarNacimientoConyuge
     */
    public function setLugarNacimientoConyuge($lugarNacimientoConyuge)
    {
        $this->lugarNacimientoConyuge = $lugarNacimientoConyuge;
    }

    /**
     * Get lugarNacimientoConyuge
     *
     * @return string 
     */
    public function getLugarNacimientoConyuge()
    {
        return $this->lugarNacimientoConyuge;
    }

    /**
     * Set actividadConyuge
     *
     * @param string $actividadConyuge
     */
    public function setActividadConyuge($actividadConyuge)
    {
        $this->actividadConyuge = $actividadConyuge;
    }

    /**
     * Get actividadConyuge
     *
     * @return string 
     */
    public function getActividadConyuge()
    {
        return $this->actividadConyuge;
    }

    /**
     * Set tipoActividad
     *
     * @param \App\Entity\Parametro $tipoActividad
     */
    public function setTipoActividad(\App\Entity\Parametro $tipoActividad)
    {
        $this->tipoActividad = $tipoActividad;
    }

    /**
     * Get tipoActividad
     *
     * @return \App\Entity\Parametro
     */
    public function getTipoActividad()
    {
        return $this->tipoActividad;
    }

    /**
     * Set lugarActividad
     *
     * @param \App\Entity\Parametro $lugarActividad
     */
    public function setLugarActividad(\App\Entity\Parametro $lugarActividad)
    {
        $this->lugarActividad = $lugarActividad;
    }

    /**
     * Get lugarActividad
     *
     * @return \App\Entity\Parametro
     */
    public function getLugarActividad()
    {
        return $this->lugarActividad;
    }

    /**
     * Set tipoInmuebleNegocio
     *
     * @param \App\Entity\Parametro $tipoInmuebleNegocio
     */
    public function setTipoInmuebleNegocio(\App\Entity\Parametro $tipoInmuebleNegocio)
    {
        $this->tipoInmuebleNegocio = $tipoInmuebleNegocio;
    }

    /**
     * Get tipoInmuebleNegocio
     *
     * @return \App\Entity\Parametro
     */
    public function getTipoInmuebleNegocio()
    {
        return $this->tipoInmuebleNegocio;
    }

    /**
     * Set nivelEducativoConyuge
     *
     * @param \App\Entity\Parametro $nivelEducativoConyuge
     */
    public function setNivelEducativoConyuge(\App\Entity\Parametro $nivelEducativoConyuge)
    {
        $this->nivelEducativoConyuge = $nivelEducativoConyuge;
    }

    /**
     * Get nivelEducativoConyuge
     *
     * @return \App\Entity\Parametro
     */
    public function getNivelEducativoConyuge()
    {
        return $this->nivelEducativoConyuge;
    }
}