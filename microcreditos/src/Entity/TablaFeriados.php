<?php
namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\TablaFeriados
 *
 * @ORM\Table(name="tabla_feriados")
 * @ORM\Entity(repositoryClass="App\Repository\ParametroRepository")
 */

class TablaFeriados  extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    /**
     * @ORM\Column(name="concepto", type="string")
     */
    protected $concepto;
    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;

    /**
     *@ORM\ManyToOne(targetEntity="User")
     *@ORM\JoinColumn(name="created_by", referencedColumnName="id") 
     */
    protected $createdBy;    
    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo = true; 
        
    /**
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;     
    /**
     *@ORM\ManyToOne(targetEntity="User")
     *@ORM\JoinColumn(name="updated_by", referencedColumnName="id") 
     */
    protected $updatedBy;   
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param string $concepto
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;
    }

    /**
     * Get concepto
     *
     * @return string 
     */
    public function getConcepto()
    {
        return $this->concepto;
    }    
    
    /**
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }    
    
    /**
     * @param \App\Entity\User $createdBy
     */
    public function setCreatedBy(\App\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \App\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }    
    
    /**
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }     
    /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }    
    
    /**
     * @param \App\Entity\User $updatedBy
     */
    public function setUpdatedBy(\App\Entity\User $updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return \App\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }    
    /**
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }     
    
    public function getTitle(){
        return $this->concepto;
    }
}