<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Exclusiones
 *
 * @ORM\Table(name="exclusiones")
 * @ORM\Entity(repositoryClass="App\Repository\ExclusionesRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Exclusiones extends BaseClass
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="fecha_desde", type="datetime", nullable=true)
     */
    protected $fechaDesde;

    /**
     * @ORM\Column(name="fecha_hasta", type="datetime", nullable=true)
     */
    protected $fechaHasta;

    /**
     * @ORM\Column(name="sin_gastos", type="boolean")
     */
    protected $sinMora;

    /**
     * @ORM\OneToMany(targetEntity="Pago", mappedBy="cuota")
     */
    protected $pagos;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * @param mixed $fechaDesde
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;
    }

    /**
     * @return mixed
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * @param mixed $fechaHasta
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;
    }

    /**
     * @return mixed
     */
    public function getSinMora()
    {
        return $this->sinMora;
    }

    /**
     * @param mixed $sinMora
     */
    public function setSinMora($sinMora)
    {
        $this->sinMora = $sinMora;
    }

    /**
     * @return mixed
     */
    public function getPagos()
    {
        return $this->pagos;
    }

    /**
     * @param mixed $pagos
     */
    public function setPagos($pagos)
    {
        $this->pagos = $pagos;
    }


}