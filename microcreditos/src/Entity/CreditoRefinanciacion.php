<?php
namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entity\CreditoRefinanciacion
 *
 * @ORM\Table(name="credito_refinanciacion")
 * @ORM\Entity(repositoryClass="App\Repository\CreditoRepository")
 */
class CreditoRefinanciacion extends BaseClass
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="fecha_refinanciacion", type="datetime")
     */
    protected $fechaRefinanciacion; 
    
    /**
     * @ORM\Column(name="monto_refinanciado", type="integer")
     */
    protected $montoRefinanciado;
    
     /**
     * @ORM\Column(name="punitorios", type="decimal", scale=2, nullable=true)
     */
    protected $punitorios;   
    
    /**
     * @ORM\Column(name="cant_cuotas", type="integer")
     */
    protected $cantCuotas;
    
    /**
     * @ORM\Column(name="periodo", type="integer")
     */
    protected $periodo;
    
     /**
     * @ORM\Column(name="nuevo_vencimiento", type="datetime")
     */
    protected $nuevoVencimiento; 

    /**
     *@ORM\ManyToOne(targetEntity="Credito", inversedBy="refinanciaciones")
     *@ORM\JoinColumn(name="credito_id", referencedColumnName="id") 
     */ 
    protected $credito;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaRefinanciacion
     *
     * @param datetime $fechaRefinanciacion
     */
    public function setFechaRefinanciacion($fechaRefinanciacion)
    {
        $this->fechaRefinanciacion = $fechaRefinanciacion;
    }

    /**
     * Get fechaRefinanciacion
     *
     * @return datetime 
     */
    public function getFechaRefinanciacion()
    {
        return $this->fechaRefinanciacion;
    }

    /**
     * Set montoRefinanciado
     *
     * @param integer $montoRefinanciado
     */
    public function setMontoRefinanciado($montoRefinanciado)
    {
        $this->montoRefinanciado = $montoRefinanciado;
    }

    /**
     * Get montoRefinanciado
     *
     * @return integer 
     */
    public function getMontoRefinanciado()
    {
        return $this->montoRefinanciado;
    }

    /**
     * Set punitorios
     *
     * @param decimal $punitorios
     */
    public function setPunitorios($punitorios)
    {
        $this->punitorios = $punitorios;
    }

    /**
     * Get punitorios
     *
     * @return decimal 
     */
    public function getPunitorios()
    {
        return $this->punitorios;
    }

    /**
     * Set cantCuotas
     *
     * @param integer $cantCuotas
     */
    public function setCantCuotas($cantCuotas)
    {
        $this->cantCuotas = $cantCuotas;
    }

    /**
     * Get cantCuotas
     *
     * @return integer 
     */
    public function getCantCuotas()
    {
        return $this->cantCuotas;
    }

    /**
     * Set periodo
     *
     * @param integer $periodo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;
    }

    /**
     * Get periodo
     *
     * @return integer 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set nuevoVencimiento
     *
     * @param datetime $nuevoVencimiento
     */
    public function setNuevoVencimiento($nuevoVencimiento)
    {
        $this->nuevoVencimiento = $nuevoVencimiento;
    }

    /**
     * Get nuevoVencimiento
     *
     * @return datetime 
     */
    public function getNuevoVencimiento()
    {
        return $this->nuevoVencimiento;
    }

    /**
     * Set credito
     *
     * @param \App\Entity\Credito $credito
     */
    public function setCredito(\App\Entity\Credito $credito)
    {
        $this->credito = $credito;
    }

    /**
     * Get credito
     *
     * @return \App\Entity\Credito
     */
    public function getCredito()
    {
        return $this->credito;
    }

    public function getCantidadCuotaCredito(){
        $ultimaCuota= 0;
        foreach ($this->credito->getCuotas() as $cuota) {
            if($ultimaCuota < $cuota->getNroCuota()){
                $ultimaCuota= $cuota->getNroCuota();
            }
        }
        return $ultimaCuota;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' nuevoVencimiento: '.$this->nuevoVencimiento->format('d-m-Y H:i:s') .' | ';
        $str .=' cantCuotas: '.$this->cantCuotas .' | ';
        $str .=' punitorios: '.$this->punitorios .' | ';
        $str .=' montoRefinanciado: '.$this->montoRefinanciado .' | ';
        $str .= parent::getDataLog();
        return $str;
    }
}