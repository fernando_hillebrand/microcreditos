<?php

namespace App\Entity;

use PHPExcel_IOFactory;

class CreditoFile
{
    protected $file;

    public function getFile()
    {
        return $this->file;
    }


    public function setFile($file)
    {
        $this->file = $file;
    }


    public function getCuotasEmisores(string $fileName, $em): array
    {
        $excel_data = [];
        $cuotasEmisores = [];
        $objPHPExcel = PHPExcel_IOFactory::load('/var/www/html/public/uploads/' . $fileName);
        $worksheet = $objPHPExcel->getActiveSheet();

        // Build an array that have just Importe, CUIT and Estado
        foreach ($worksheet->getRowIterator() as $row) {
            $rowData = array();
            $cellD = $worksheet->getCellByColumnAndRow(3, $row->getRowIndex())->getValue();
            $cellE = $worksheet->getCellByColumnAndRow(4, $row->getRowIndex())->getValue();
            $cellH = $worksheet->getCellByColumnAndRow(7, $row->getRowIndex())->getValue();
            $rowData['importe'] = str_replace(['.', ','], ['', '.'], $cellD);
            $rowData['CUIT'] = $cellE;
            $rowData['estado'] = $cellH;

            // Ignore excel headers and if an int comes, convert it to string
            if (ctype_digit(strval($cellE))) {
                $excel_data[] = $rowData;
            }
        }

        // Get cuotas no pagadas for every CUIT
        // TODO: what if we have 2 active creditos?
        foreach ($excel_data as $row) {
            if (strtolower($row["estado"]) != "pagado") {
                continue;
            }

            $emprendedor = $em->getRepository('App:Emprendedor')->getEmprendedorbyCuit($row["CUIT"]);
            if (is_null($emprendedor) || empty($emprendedor)) {
                continue;
            }

            $cuotas = $em->getRepository('App:Cuota')->getCuotasByCuit($row["CUIT"]);
            if (is_null($cuotas) || empty($cuotas)) {
                continue;
            }

            $cuotasEmisores[] = [
                "emprendedor" => $emprendedor[0],
                "cuotas" => $cuotas,
                "importePagadoExcel" => $row["importe"]
            ];
        }


        return $cuotasEmisores;
    }



}