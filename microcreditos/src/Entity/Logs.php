<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\Entity
 * @ORM\Table(name="logs")
 */
class Logs
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="campo", type="string",length=100)
     */
    protected $campo;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="objeto_id", type="string",length=100)
     */
    protected $idObjeto;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    protected $descripcion;

    /**
     * @ORM\Column(name="borrado", type="boolean")
     */
    protected $borrado;

    /**
     * @var \DateTime $fechaCreacion
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $fechaCreacion;

    /**
     * @var string $creadoPor
     *
     * @Gedmo\Blameable(on="create")
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $creadoPor;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCampo()
    {
        return $this->campo;
    }

    /**
     * @param string $descipcion
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descipcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return string
     */
    public function getIdObjeto()
    {
        return $this->idObjeto;
    }

    /**
     * @param string $descipcion
     */
    public function setIdObjeto($idObjeto)
    {
        $this->idObjeto = $idObjeto;
    }

    /**
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param \DateTime $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**
     * @return string
     */
    public function getCreadoPor()
    {
        return $this->creadoPor;
    }

    /**
     * @param string $creadoPor
     */
    public function setCreadoPor($creadoPor)
    {
        $this->creadoPor = $creadoPor;
    }

    /**
     * @return mixed
     */
    public function getBorrado()
    {
        return $this->borrado;
    }

    /**
     * @param mixed $borrado
     */
    public function setBorrado($borrado)
    {
        $this->borrado = $borrado;
    }

}