<?php

namespace App\Entity;

use App\Repository\GastosTotalesRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\Base\BaseClass;

/**
 * @ORM\Entity(repositoryClass=GastosTotalesRepository::class)
 */
class GastosTotales extends BaseClass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $monto;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha;

    /**
     *@ORM\ManyToOne(targetEntity="ConceptoGastos")
     *@ORM\JoinColumn(name="id_concepto", referencedColumnName="id") 
     */

     private $concepto;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $num_comprobante;

     /**
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonto(): ?string
    {
        return $this->monto;
    }

    public function setMonto(?string $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

     /**
     * Set concepto
     *
     * @param \App\Entity\ConceptoGastos $concepto
     */

    public function setConcepto(\App\Entity\ConceptoGastos $concepto)
    {
        $this->concepto = $concepto;
    }

    public function getConcepto()
    {
        return $this->concepto;
    }

    

    public function getComprobante(): ?string
    {
        return $this->num_comprobante;
    }

    public function setComprobante(string $num_comprobante): self
    {
        $this->num_comprobante = $num_comprobante;

        return $this;
    }
    

    public function getTitle(){
        return '"Comprobante:'.$this->num_comprobante.'"';
    }

    public function getDataLog(){
        $str =' comprobante numero: '.$this->num_comprobante .' | ';
      
        $str .= parent::getDataLog();
        return $str;
    }

      /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

   
}
