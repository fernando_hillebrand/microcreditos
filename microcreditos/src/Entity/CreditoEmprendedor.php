<?php
namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entity\CreditoEmprendedor
 *
 * @ORM\Table(name="credito_emprendedor")
 * @ORM\Entity(repositoryClass="App\Repository\CreditoRepository")
 */
class CreditoEmprendedor  extends BaseClass
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="monto_credito", type="integer")
     */
    protected $montoCredito;

    /**
     * @ORM\Column(name="gasto_administrativo", type="decimal", scale=2)
     */
    protected $gastoAdministrativo;

    /**
     * @ORM\Column(name="monto_cuota", type="decimal", scale=2, nullable=true)
     */
    protected $montoCuota;

    /**
     * @ORM\Column(name="cuota_refinanciada", type="decimal", scale=2, nullable=true)
     */
    protected $cuotaRefinanciada;

    /**
     * @ORM\Column(name="coordinador", type="boolean")
     */
    protected $coordinador = false;

    /**
     * @ORM\Column(name="is_credito", type="boolean")
     */
    protected $isCredito = false;

    /**
     * @ORM\Column(name="garante", type="string", length=100, nullable=true)
     */
    protected $garante;

    /**
     * @ORM\Column(name="garante_dni", type="string", length=8, nullable=true)
     */
    protected $garanteDni;

    /**
     * @ORM\Column(name="garante_direccion", type="text", nullable=true)
     */
    protected $garanteDireccion;
    /**
     * @ORM\Column(name="garante_telefono", type="string", nullable=true)
     */
    protected $garanteTelefono;

    /**
     * @ORM\Column(name="garante_ocupacion", type="string", nullable=true)
     */
    protected $garanteOcupacion;
    /**
     * @ORM\Column(name="garante_lugar_trabajo", type="string", nullable=true)
     */
    protected $garanteLugarTrabajo;

    /**
     * @ORM\Column(name="garante_empleador_nombre", type="string", nullable=true)
     */
    protected $garanteEmpleadorNombre;
    /**
     * @ORM\Column(name="garante_empleador_cuit", type="string", nullable=true)
     */
    protected $garanteEmpleadorCuit;

    /**
     * @ORM\Column(name="garante_ingresos", type="string", nullable=true)
     */
    protected $garanteIngresos;

    /**
     * @ORM\ManyToOne(targetEntity="Credito")
     **/
    protected $credito;

    /**
     * @ORM\ManyToOne(targetEntity="Emprendedor")
     **/
    protected $emprendedor;

    public function __toString() {
        return $this->emprendedor->getApellido().', '.$this->emprendedor->getNombres();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montoCredito
     *
     * @param integer $montoCredito
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    }

    /**
     * Get montoCredito
     *
     * @return integer
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set coordinador
     *
     * @param boolean $coordinador
     */
    public function setCoordinador($coordinador)
    {
        $this->coordinador = $coordinador;
    }

    /**
     * Get coordinador
     *
     * @return boolean
     */
    public function getCoordinador()
    {
        return $this->coordinador;
    }

    /**
     * Set garante
     *
     * @param string $garante
     */
    public function setGarante($garante)
    {
        $this->garante = $garante;
    }

    /**
     * Get garante
     *
     * @return string
     */
    public function getGarante()
    {
        return $this->garante;
    }

    /**
     * Set garanteDni
     *
     * @param string $garanteDni
     */
    public function setGaranteDni($garanteDni)
    {
        $this->garanteDni = $garanteDni;
    }

    /**
     * Get garanteDni
     *
     * @return string
     */
    public function getGaranteDni()
    {
        return $this->garanteDni;
    }

    /**
     * Set garanteDireccion
     *
     * @param text $garanteDireccion
     */
    public function setGaranteDireccion($garanteDireccion)
    {
        $this->garanteDireccion = $garanteDireccion;
    }

    /**
     * Get garanteDireccion
     *
     * @return text
     */
    public function getGaranteDireccion()
    {
        return $this->garanteDireccion;
    }
    /**
     * Set garanteTelefono
     *
     * @param string $garanteTelefono
     */
    public function setGaranteTelefono($garanteTelefono)
    {
        $this->garanteTelefono = $garanteTelefono;
    }

    /**
     * Get garanteTelefono
     *
     * @return string
     */
    public function getGaranteTelefono()
    {
        return $this->garanteTelefono;
    }

    /**
     * Set garanteOcupacion
     *
     * @param string $garanteOcupacion
     */
    public function setGaranteOcupacion($garanteOcupacion)
    {
        $this->garanteOcupacion = $garanteOcupacion;
    }

    /**
     * Get garanteOcupacion
     *
     * @return string
     */
    public function getGaranteOcupacion()
    {
        return $this->garanteOcupacion;
    }

    /**
     * Set garanteLugarTrabajo
     *
     * @param string $garanteLugarTrabajo
     */
    public function setGaranteLugarTrabajo($garanteLugarTrabajo)
    {
        $this->garanteLugarTrabajo = $garanteLugarTrabajo;
    }

    /**
     * Get garanteLugarTrabajo
     *
     * @return string
     */
    public function getGaranteLugarTrabajo()
    {
        return $this->garanteLugarTrabajo;
    }

    /**
     * Set credito
     *
     * @param \App\Entity\Credito $credito
     */
    public function setCredito(\App\Entity\Credito $credito)
    {
        $this->credito = $credito;
    }

    /**
     * Get credito
     *
     * @return \App\Entity\Credito
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * Set emprendedor
     *
     * @param \App\Entity\Emprendedor $emprendedor
     */
    public function setEmprendedor(\App\Entity\Emprendedor $emprendedor)
    {
        $this->emprendedor = $emprendedor;
    }

    /**
     * Get emprendedor
     *
     * @return \App\Entity\Emprendedor
     */
    public function getEmprendedor()
    {
        return $this->emprendedor;
    }

    /**
     * Set isCredito
     *
     * @param boolean $isCredito
     */
    public function setIsCredito($isCredito)
    {
        $this->isCredito = $isCredito;
    }

    /**
     * Get isCredito
     *
     * @return boolean 
     */
    public function getIsCredito()
    {
        return $this->isCredito;
    }   

    /**
     * Set gastoAdministrativo
     *
     * @param decimal $gastoAdministrativo
     */
    public function setGastoAdministrativo($gastoAdministrativo)
    {
        $this->gastoAdministrativo = $gastoAdministrativo;
    }

    /**
     * Get gastoAdministrativo
     *
     * @return decimal
     */
    public function getGastoAdministrativo()
    {
        return $this->gastoAdministrativo;
    }

    /**
     * Set montoCuota
     *
     * @param decimal $montoCuota
     */
    public function setMontoCuota($montoCuota)
    {
        $this->montoCuota = $montoCuota;
    }

    /**
     * Get montoCuota
     *
     * @return decimal
     */
    public function getMontoCuota()
    {
        return $this->montoCuota;
    }

    /**
     * Set cuotaRefinanciada
     *
     * @param decimal $cuotaRefinanciada
     */
    public function setCuotaRefinanciada($cuotaRefinanciada)
    {
        $this->cuotaRefinanciada = $cuotaRefinanciada;
    }

    /**
     * Get cuotaRefinanciada
     *
     * @return decimal
     */
    public function getCuotaRefinanciada()
    {
        return $this->cuotaRefinanciada;
    }

    /**
     * Set garanteEmpleadorNombre
     *
     * @param String $garanteEmpleadorNombre
     */
    public function setGaranteEmpleadorNombre($garanteEmpleadorNombre)
    {
        $this->garanteEmpleadorNombre = $garanteEmpleadorNombre;
    }

    /**
     * Get garanteEmpleadorNombre
     *
     * @return String
     */
    public function getGaranteEmpleadorNombre()
    {
        return $this->garanteEmpleadorNombre;
    }

    /**
     * Set garanteEmpleadorCuit
     *
     * @param String $garanteEmpleadorCuit
     */
    public function setGaranteEmpleadorCuit($garanteEmpleadorCuit)
    {
        $this->garanteEmpleadorCuit = $garanteEmpleadorCuit;
    }

    /**
     * Get garanteEmpleadorCuit
     *
     * @return String
     */
    public function getGaranteEmpleadorCuit()
    {
        return $this->garanteEmpleadorCuit;
    }

    /**
     * Set garanteIngresos
     *
     * @param String $garanteIngresos
     */
    public function setGaranteIngresos($garanteIngresos)
    {
        $this->garanteIngresos = $garanteIngresos;
    }

    /**
     * Get garanteEmpleadorCuit
     *
     * @return String
     */
    public function getGaranteIngresos()
    {
        return $this->garanteIngresos;
    }


    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' montoCredito: '.$this->montoCredito .' | ';
        $str .=' garante: '.$this->garante .' | ';
        $str .=' garanteDni: '.$this->garanteDni .' | ';
        $str .=' montoCuota: '.$this->montoCuota .' | ';
        $str .=' gastoAdministrativo: '.$this->gastoAdministrativo .' | ';
        $str .=' credito: '.$this->credito .' | ';
        $str .= parent::getDataLog();
        return $str;
    }
}