<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Pago
 *
 * @ORM\Table(name="pago")
 * @ORM\Entity(repositoryClass="App\Repository\PagoRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Pago extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;
    
    /**
     * @ORM\Column(name="capital", type="decimal", scale=2)
     */
    protected $capital;

    /**
     * @ORM\Column(name="intereses", type="decimal", scale=2)
     */
    protected $intereses;

    /**
     * @ORM\Column(name="refin_punitorios", type="decimal", scale=2, nullable=true)
     */
    protected $refinPunitorios;

    /**
     * @ORM\Column(name="punitorios", type="decimal", scale=2, nullable=true)
     */
    protected $punitorios;
    
    /**
     * @ORM\Column(name="gasto_administrativo", type="decimal", scale=2, nullable=true)
     */
    protected $gastoAdministrativo;
    
   /**
     * @ORM\Column(name="total", type="decimal", scale=2)
     */
    protected $total;

    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="gastosCargados")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;

    /**
     *@ORM\ManyToOne(targetEntity="Cuota", inversedBy="pagos")
     *@ORM\JoinColumn(name="cuota_id", referencedColumnName="id") 
     */
    protected $cuota;
    
    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;  

    /**
     * @ORM\Column(name="observacion", type="string", length=100, nullable=true)
     */
    protected $observacion;  
    
    /**
     *@ORM\ManyToOne(targetEntity="ReporteMensual", inversedBy="pagosReporte")
     *@ORM\JoinColumn(name="reporte_mensual_id", referencedColumnName="id") 
     */
    protected $reporte;

    /**
     *@ORM\ManyToOne(targetEntity="Exclusiones", inversedBy="pagos")
     *@ORM\JoinColumn(name="exclusion_id", referencedColumnName="id")
     */
    protected $exclusion;

    /**
     * @ORM\Column(name="exclusion_monto", type="decimal", scale=2,nullable=true)
     */
    protected $montoExclusion;

    /**
     * @ORM\OneToMany(targetEntity="PagoBancario", mappedBy="pago",cascade={"persist","remove"})
     */
    protected $pagosBancario;

    /**
     * @ORM\PrePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
    }       


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return 'Pago # '.$this->getId(). ' de la Cuota '.$this->getCuota()->getNroCuota();
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set capital
     *
     * @param decimal $capital
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;
    }

    /**
     * Get capital
     *
     * @return decimal 
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set intereses
     *
     * @param decimal $intereses
     */
    public function setIntereses($intereses)
    {
        $this->intereses = $intereses;
    }

    /**
     * Get intereses
     *
     * @return decimal 
     */
    public function getIntereses()
    {
        return $this->intereses;
    }

    /**
     * Set punitorios
     *
     * @param decimal $punitorios
     */
    public function setPunitorios($punitorios)
    {
        $this->punitorios = $punitorios;
    }

    /**
     * Get punitorios
     *
     * @return decimal 
     */
    public function getPunitorios()
    {
        return $this->punitorios;
    }

    /**
     * Set gastoAdministrativo
     *
     * @param decimal $gastoAdministrativo
     */
    public function setGastoAdministrativo($gastoAdministrativo)
    {
        $this->gastoAdministrativo = $gastoAdministrativo;
    }

    /**
     * Get gastoAdministrativo
     *
     * @return decimal 
     */
    public function getGastoAdministrativo()
    {
        return $this->gastoAdministrativo;
    }

    /**
     * Set total
     *
     * @param decimal $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * Get total
     *
     * @return decimal 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }
    
    /**
     * Set observacion
     *
     * @param datetime $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set cuota
     *
     * @param \App\Entity\Cuota $cuota
     */
    public function setCuota(\App\Entity\Cuota $cuota)
    {
        $this->cuota = $cuota;
    }

    /**
     * Get cuota
     *
     * @return \App\Entity\Cuota
     */
    public function getCuota()
    {
        return $this->cuota;
    }

    /**
     * Set refinPunitorios
     *
     * @param decimal $refinPunitorios
     */
    public function setRefinPunitorios($refinPunitorios)
    {
        $this->refinPunitorios = $refinPunitorios;
    }

    /**
     * Get refinPunitorios
     *
     * @return decimal 
     */
    public function getRefinPunitorios()
    {
        return $this->refinPunitorios;
    }


    /**
     * Set reporte
     *
     * @param \App\Entity\ReporteMensual $reporte
     */
    public function setReporte(\App\Entity\ReporteMensual $reporte)
    {
        $this->reporte = $reporte;
    }

    /**
     * Get reporte
     *
     * @return \App\Entity\ReporteMensual
     */
    public function getReporte()
    {
        return $this->reporte;
    }
    
    /*
     * Unset Reporte
     */
    public function unsetReporte(){
        unset($this->reporte);
    }

    /**
     * @return mixed
     */
    public function getExclusion()
    {
        return $this->exclusion;
    }

    /**
     * @param mixed $exclusion
     */
    public function setExclusion($exclusion)
    {
        $this->exclusion = $exclusion;
    }

    /**
     * @return mixed
     */
    public function getMontoExclusion()
    {
        return $this->montoExclusion;
    }

    /**
     * @param mixed $montoExclision
     */
    public function setMontoExclusion($montoExclusion)
    {
        $this->montoExclusion = $montoExclusion;
    }

    /**
     * Get pagos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPagosBancario()
    {
        return $this->pagosBancario;
    }

    /**
     * Add pagos
     *
     * @param \App\Entity\Pago $pagos
     */
    public function addPago(\App\Entity\PagoBancario $pagosBancario)
    {
        $this->pagosBancario[] = $pagosBancario;
    }

    public function isPagoBancario(){
        if(count($this->pagosBancario) > 0){
            return true;
        }
        return false;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' pago: '.$this->__toString() .' | ';
        $str .=' fecha: '.$this->fecha->format('d-m-Y H:i:s') .' | ';
        $str .=' capital: '.$this->capital .' | ';
        $str .=' intereses: '.$this->intereses .' | ';
        $str .=' punitorios: '.$this->punitorios .' | ';
        $str .=' gastoAdministrativo: '.$this->gastoAdministrativo .' | ';
        $str .=' total: '.$this->total .' | ';
        $str .=' usuario: '.$this->usuario .' | ';
        $str .=' observacion: '.$this->observacion .' | ';
        $str .= parent::getDataLog();
        return $str;
    }


}