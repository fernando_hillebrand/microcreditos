<?php

namespace App\Entity;

use App\Repository\ConceptoGastosRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Base\BaseClass;

/**
 * @ORM\Entity(repositoryClass=ConceptoGastosRepository::class)
 */
class ConceptoGastos extends BaseClass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private  $nombre;

     /**
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function __toString(){
        return $this->nombre;
    }

    public function getTitle(){
        return '"Concepto:'.$this->nombre.'"';
    }

    public function getDataLog(){
        $str =' concepto:  '.$this->nombre .' | ';
      
        $str .= parent::getDataLog();
        return $str;
    }


         /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
}
