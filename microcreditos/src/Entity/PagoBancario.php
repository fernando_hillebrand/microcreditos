<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\ProgramaIngreso
 *
 * @ORM\Table(name="pagos_bancarios")
 * @ORM\Entity(repositoryClass="App\Repository\PagoBancarioRepository")
 */

class PagoBancario extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    /**
     * @ORM\Column(name="ingreso", type="decimal", scale=2)
     */
    protected $ingreso;


    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="caja")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id",nullable=true)
     */
    protected $programa;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="programaIngresos")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="concepto", referencedColumnName="id",nullable=true)
     **/
    protected $concepto;

    /**
     * @ORM\Column(name="obserbacion", type="string", nullable=true)
     */
    protected $observacion;

    /**
     *@ORM\ManyToOne(targetEntity="Credito", inversedBy="caja")
     *@ORM\JoinColumn(name="credito_id", referencedColumnName="id",nullable=true)
     */
    protected $credito;

    /**
     *@ORM\ManyToOne(targetEntity="Pago", inversedBy="caja")
     *@ORM\JoinColumn(name="pago_id", referencedColumnName="id",nullable=true)
     */
    protected $pago;
    
    public function __toString() {
        return $this->ingreso;
    }

    public function __construct()
    {
        $this->fecha = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIngreso()
    {
        return $this->ingreso;
    }

    /**
     * @param mixed $ingreso
     */
    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * @param mixed $programa
     */
    public function setPrograma($programa)
    {
        $this->programa = $programa;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * @param mixed $concepto
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;
    }

    /**
     * @return mixed
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * @param mixed $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * @return mixed
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * @param mixed $credito
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;
    }

    /**
     * @return mixed
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * @param mixed $pago
     */
    public function setPago($pago)
    {
        $this->pago = $pago;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' ingreso: '.$this->ingreso .' | ';
        $str .=' fecha: '.$this->fecha->format('d-m-Y H:i:s') .' | ';
        $str .=' programa: '.$this->programa .' | ';
        $str .=' concepto: '.$this->concepto .' | ';
        $str .=' usuario: '.$this->usuario .' | ';
        $str .=' credito: '.$this->credito .' | ';
        $str .=' pago: '.$this->pago .' | ';
        $str .= parent::getDataLog();
        return $str;
    }

}