<?php
namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;

/**
 * \App\Entity\Cuota
 *
 * @ORM\Table(name="cuota")
 * @ORM\Entity(repositoryClass="App\Repository\CuotaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Cuota extends BaseClass
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
     * @ORM\Column(name="nro_cuota", type="integer")
     */
    protected $nroCuota;
    
    /**
     * @ORM\Column(name="fecha_vencimiento", type="date")
     */
    protected $fechaVencimiento;
    
    /**
     * @ORM\Column(name="monto_capital", type="decimal", scale=2)
     */
    protected $montoCapital;

    /**
     * @ORM\Column(name="monto_intereses", type="decimal", scale=2)
     */
    protected $montoIntereses;

    /**
     * @ORM\Column(name="monto_punitorios", type="decimal", scale=2, nullable=true)
     */
    protected $montoPunitorios;
    
    /**
     * @ORM\Column(name="gasto_administrativo", type="decimal", scale=2, nullable=true)
     */
    protected $gastoAdministrativo;
    
   /**
     * @ORM\Column(name="monto_total", type="decimal", scale=2)
     */
    protected $montoTotal;
    
   /**
     * @ORM\Column(name="pagado", type="decimal", scale=2)
     */
    protected $pagado;
    
   /**
     * @ORM\Column(name="saldo", type="decimal", scale=2)
     */
    protected $saldo;
    
    /**
     * @ORM\Column(name="saldo_punitorios", type="decimal", scale=2, nullable=true)
     */
    protected $saldoPunitorios;
    
    /**
     * @ORM\Column(name="cancelado", type="boolean")
     */
    protected $cancelado;
    
    /**
     * @ORM\Column(name="refinanciada", type="boolean")
     */
    protected $refinanciada;
    
    /**
     * @ORM\Column(name="fecha_cancelacion", type="date", nullable=true)
     */
    //Fecha de pago
    protected $fechaCancelacion;

    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;     
    
    /**
     *@ORM\ManyToOne(targetEntity="Credito", inversedBy="cuotas")
     *@ORM\JoinColumn(name="credito_id", referencedColumnName="id") 
     */
    protected $credito;    
    
    /**
    * @ORM\OneToMany(targetEntity="Pago", mappedBy="cuota",cascade={"persist","remove"})
    */
    protected $pagos;

    /**
     * @ORM\PrePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
        /*$this->cancelado = false;
        $this->pagado = 0;*/
    }       

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nroCuota
     *
     * @param integer $nroCuota
     */
    public function setNroCuota($nroCuota)
    {
        $this->nroCuota = $nroCuota;
    }

    /**
     * Get nroCuota
     *
     * @return integer 
     */
    public function getNroCuota()
    {
        return $this->nroCuota;
    }

    /**
     * Set fechaVencimiento
     *
     * @param date $fechaVencimiento
     */
    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;
    }

    /**
     * Get fechaVencimiento
     *
     * @return date 
     */
    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    /**
     * Set montoCapital
     *
     * @param decimal $montoCapital
     */
    public function setMontoCapital($montoCapital)
    {
        $this->montoCapital = $montoCapital;
    }

    /**
     * Get montoCapital
     *
     * @return decimal 
     */
    public function getMontoCapital()
    {
        return $this->montoCapital;
    }

    /**
     * Set montoIntereses
     *
     * @param decimal $montoIntereses
     */
    public function setMontoIntereses($montoIntereses)
    {
        $this->montoIntereses = $montoIntereses;
    }

    /**
     * Get montoIntereses
     *
     * @return decimal 
     */
    public function getMontoIntereses()
    {
        return $this->montoIntereses;
    }

    /**
     * Set montoPunitorios
     *
     * @param decimal $montoPunitorios
     */
    public function setMontoPunitorios($montoPunitorios)
    {
        $this->montoPunitorios = $montoPunitorios;
    }

    /**
     * Get montoPunitorios
     *
     * @return decimal 
     */
    public function getMontoPunitorios()
    {
        return $this->montoPunitorios;
    }

    /**
     * Set gastoAdministrativo
     *
     * @param decimal $gastoAdministrativo
     */
    public function setGastoAdministrativo($gastoAdministrativo)
    {
        $this->gastoAdministrativo = $gastoAdministrativo;
    }

    /**
     * Get gastoAdministrativo
     *
     * @return decimal 
     */
    public function getGastoAdministrativo()
    {
        return $this->gastoAdministrativo;
    }

    /**
     * Set montoTotal
     *
     * @param decimal $montoTotal
     */
    public function setMontoTotal($montoTotal)
    {
        $this->montoTotal = $montoTotal;
    }

    /**
     * Get montoTotal
     *
     * @return decimal 
     */
    public function getMontoTotal()
    {
        return $this->montoTotal;
    }

    /**
     * Set pagado
     *
     * @param decimal $pagado
     */
    public function setPagado($pagado)
    {
        $this->pagado = $pagado;
    }

    /**
     * Get pagado
     *
     * @return decimal 
     */
    public function getPagado()
    {
        return $this->pagado;
    }

    /**
     * Set saldo
     *
     * @param decimal $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * Get saldo
     *
     * @return decimal 
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * Set cancelado
     *
     * @param boolean $cancelado
     */
    public function setCancelado($cancelado)
    {
        $this->cancelado = $cancelado;
    }

    /**
     * Get cancelado
     *
     * @return boolean 
     */
    public function getCancelado()
    {
        return $this->cancelado;
    }

    /**
     * Set fechaCancelacion
     *
     * @param date $fechaCancelacion
     */
    public function setFechaCancelacion($fechaCancelacion)
    {
        $this->fechaCancelacion = $fechaCancelacion;
    }

    /**
     * Get fechaCancelacion
     *
     * @return date 
     */
    public function getFechaCancelacion()
    {
        return $this->fechaCancelacion;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set Credito
     *
     * @param \App\Entity\Credito $credito
     */
    public function setCredito(\App\Entity\Credito $credito)
    {
        $this->credito = $credito;
    }

    /**
     * Get Credito
     *
     * @return \App\Entity\Credito
     */
    public function getCredito()
    {
        return $this->credito;
    }
    public function __construct()
    {
        $this->pagos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cancelado = false;
        $this->pagado = 0;
        $this->refinanciada = 0;
        
    }
    
    /**
     * Add pagos
     *
     * @param \App\Entity\Pago $pagos
     */
    public function addPagos(\App\Entity\Pago $pagos)
    {
        $this->pagos[] = $pagos;
    }

    /**
     * Get pagos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPagos()
    {
        return $this->pagos;
    }

    /**
     * Add pagos
     *
     * @param \App\Entity\Pago $pagos
     */
    public function addPago(\App\Entity\Pago $pagos)
    {
        $this->pagos[] = $pagos;
    }

    /**
     * Set refinanciada
     *
     * @param boolean $refinanciada
     */
    public function setRefinanciada($refinanciada)
    {
        $this->refinanciada = $refinanciada;
    }

    /**
     * Get refinanciada
     *
     * @return boolean 
     */
    public function getRefinanciada()
    {
        return $this->refinanciada;
    }

    /**
     * Set saldoPunitorios
     *
     * @param decimal $saldoPunitorios
     */
    public function setSaldoPunitorios($saldoPunitorios)
    {
        $this->saldoPunitorios = $saldoPunitorios;
    }

    /**
     * Get saldoPunitorios
     *
     * @return decimal 
     */
    public function getSaldoPunitorios()
    {
        return $this->saldoPunitorios;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' nroCuota: '.$this->nroCuota .' | ';
        $str .=' fechaVencimiento: '.$this->fechaVencimiento->format('d-m-Y H:i:s') .' | ';

        $str .=' montoCapital: '.$this->montoCapital .' | ';
        $str .=' montoIntereses: '.$this->montoIntereses .' | ';
        $str .=' montoPunitorios: '.$this->montoPunitorios .' | ';
        $str .=' gastoAdministrativo: '.$this->gastoAdministrativo .' | ';
        $str .=' montoTotal: '.$this->montoTotal .' | ';
        $str .=' pagado: '.$this->pagado .' | ';
        $str .=' saldo: '.$this->saldo .' | ';
        $str .=' cancelado: '.$this->cancelado .' | ';
        $str .=' fechaCancelacion: '.$this->fechaCancelacion->format('d-m-Y H:i:s') .' | ';

        $str .=' refinanciada: '.$this->refinanciada .' | ';
        $str .=' saldoPunitorios: '.$this->saldoPunitorios .' | ';
        $str .= parent::getDataLog();
        return $str;
    }
}