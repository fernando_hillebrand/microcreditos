<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\TablaGastos
 *
 * @ORM\Table(name="tabla_gastos")
 * @ORM\Entity
 */

class TablaGastos  extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="periodo", type="integer")
     */
    protected $periodo;
    
    /**
     * @ORM\Column(name="desde",type="decimal",scale=2,nullable=true)
     */
    protected $desde;
    
    /**
     * @ORM\Column(name="hasta",type="decimal",scale=2,nullable=true)
     */
    protected $hasta;
    
    /**
     * @ORM\Column(name="valor",type="decimal",scale=2)
     */
    protected $valor;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getTitle(){
        return '';
    }
    /**
     * Set periodo
     *
     * @param integer $periodo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;
    }

    /**
     * Get periodo
     *
     * @return integer 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set desde
     *
     * @param decimal $desde
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;
    }

    /**
     * Get desde
     *
     * @return decimal 
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * Set hasta
     *
     * @param decimal $hasta
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    /**
     * Get hasta
     *
     * @return decimal 
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * Set valor
     *
     * @param decimal $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * Get valor
     *
     * @return decimal 
     */
    public function getValor()
    {
        return $this->valor;
    }
}