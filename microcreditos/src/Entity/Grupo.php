<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Grupo
 *
 * @ORM\Table(name="grupo")
 * @ORM\Entity(repositoryClass="App\Repository\GrupoRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Grupo extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
     * @var string $codigo
     *
     * @ORM\Column(name="codigo", type="string", length=6)
     */
    protected $codigo;
    
    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    protected $nombre;
    
    /**
     * @var integer $activo
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo;
    
    /**
     * @var integer $fechaAlta
     *
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;

    /**
     * @ORM\OneToMany(targetEntity="GruposEmprendedores", mappedBy="grupo",cascade={"persist","remove"})
     */
    protected $emprendedores;
    
    /**
     *@ORM\OneToMany(targetEntity="Credito", mappedBy="grupo")
     *@ORM\OrderBy({"nroRenovacion" = "DESC"}) 
     */
    protected $creditos;
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="grupos")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;    
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="grupos")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;
    
    /**
     *@ORM\ManyToOne(targetEntity="Emprendedor", inversedBy="recomendado")
     *@ORM\JoinColumn(name="recomendador_id", referencedColumnName="id") 
     */
    protected $recomendador;


    public function __toString() {
        return $this->nombre;
    }
    
    public function getTitle(){
        return '" '.$this->nombre.' "';
    }
    
    public function __construct()
    {
        $this->emprendedores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activo = true;
    }
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add emprendedores
     *
     * @param \App\Entity\GruposEmprendedor $emprendedores
     */
    public function addGruposEmprendedores(\App\Entity\GruposEmprendedores $emprendedores)
    {
        $this->emprendedores[] = $emprendedores;
    }

    /**
     * Get emprendedores
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGruposEmprendedores()
    {
        return $this->emprendedores;
    }

    public function getEmprendedores(){
        $array = array();
        foreach($this->getGruposEmprendedores() as $gruposEmprendedor){
            $array[] = $gruposEmprendedor->getEmprendedor();
        }
        return $array;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
        $this->activo = 1;
    }    

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add creditos
     *
     * @param \App\Entity\Credito $creditos
     */
    public function addCredito(\App\Entity\Credito $creditos)
    {
        $this->creditos[] = $creditos;
    }

    /**
     * Get creditos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    /**
     * Set recomendador
     *
     * @param \App\Entity\Emprendedor $recomendador
     */
    public function setRecomendador($recomendador)
    {
        $this->recomendador = ($recomendador)?$recomendador:null;;
    }

    /**
     * Get recomendador
     *
     * @return \App\Entity\Emprendedor
     */
    public function getRecomendador()
    {
        return $this->recomendador;
    }

    public function getStatus(){
        return 'ok';
    }
}