<?php

namespace App\Entity;

use App\Repository\RegistrosBancariosRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\Base\BaseClass;

/**
 * @ORM\Entity(repositoryClass=RegistrosBancariosRepository::class)
 */
class RegistrosBancarios  extends BaseClass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $monto;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $cuenta;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $banco;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getMonto(): ?string
    {
        return $this->monto;
    }

    public function setMonto(string $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getCuenta(): ?string
    {
        return $this->cuenta;
    }

    public function setCuenta(?string $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(?string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    
    public function getTitle(){
        return '"Registro: '.$this->id.'"';
    }

    public function getDataLog(){
        $str =' registro id: '.$this->id .' | ';
      
        $str .= parent::getDataLog();
        return $str;
    }

      /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
}
