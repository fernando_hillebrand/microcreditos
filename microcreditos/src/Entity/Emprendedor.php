<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Emprendedor
 *
 * @ORM\Table(name="emprendedor")
 * @ORM\Entity(repositoryClass="App\Repository\EmprendedorRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Emprendedor extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="codigo", type="string", length=8)
     */
    protected $codigo;
    
    /**
     * @ORM\Column(name="nombres", type="string", length=100)
     */
    protected $nombres;
    
    /**
     * @ORM\Column(name="apellido", type="string", length=100)
     */
    protected $apellido;

     /**
     * @ORM\Column(name="email", type="string", length=100)
     */
    protected $email;

     /**
     * @ORM\Column(name="cbu", type="string", length=300)
     */
    protected $cbu;

    /**
     * @ORM\Column(name="dni", type="string", length=8)
     */
    protected $dni;    
    /**
     * @ORM\Column(name="cuit", type="string", length=15)
     */
    protected $cuit;    

    /**
     * @ORM\Column(name="sobrenombre", type="string", length=100, nullable=true)
     */
    protected $sobrenombre;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="estado_civil", referencedColumnName="id")
     **/    
    protected $estadoCivil;
    
    /**
     * @ORM\Column(name="fecha_nacimiento", type="string", length=10)
     */
    protected $fechaNacimiento;   

    /**
     * @ORM\Column(name="lugar_nacimiento", type="string", length=100, nullable=true)
     */
    protected $lugarNacimiento;
    
    /**
     *  @ORM\Column(name="sexo", type="string", length=1)
     */
    protected $sexo = 'M';

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="nivel_educativo", referencedColumnName="id")
     **/  
    protected $nivelEducativo;
    
    /**
     * @ORM\Column(name="telefono", type="string", nullable=true)
     */
    protected $telefono;
    
    /**
     * @ORM\Column(name="direccion", type="text", nullable=true)
     */
    protected $direccion;
    
    /**
     * @ORM\Column(name="ubicacion", type="string", length=200, nullable=true)
     */
    protected $ubicacion;
    
    /**
     * @ORM\Column(name="nro_hijos", type="integer", nullable=true)
     */
    protected $nroHijos = 0;

    /**
     * @ORM\Column(name="edades_hijos", type="string", length=30, nullable=true)
     */
    protected $edadesHijos;

    /**
     * @ORM\Column(name="nro_dependientes", type="integer", nullable=true)
     */
    protected $nroDependientes = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="tipo_vivienda", referencedColumnName="id")
     **/  
    protected $tipoVivienda;

    /**
     * @ORM\Column(name="tiempo_residencia", type="string", length=50, nullable=true)
     */
    protected $tiempoResidencia;

    /**
     * @ORM\Column(name="aclaraciones", type="text", nullable=true)
     */
    protected $aclaraciones;

    /**
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo = true;

    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;   

     /**
     * @ORM\OneToOne(targetEntity="EmprendedorDatosAdicionales", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="emprendedor_datos_adicionales_id", referencedColumnName="id")
     */
    protected $datosAdicionales;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="emprendedoresCreados")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;
    
    /**
     * @ORM\OneToMany(targetEntity="Emprendedor", mappedBy="recomendador")
     */
    protected $recomendado;
    
    /**
     *@ORM\ManyToOne(targetEntity="Emprendedor", inversedBy="recomendado")
     *@ORM\JoinColumn(name="recomendador_id", referencedColumnName="id") 
     */
    protected $recomendador;

    /**
     * @ORM\OneToMany(targetEntity="GruposEmprendedores", mappedBy="emprendedor",cascade={"persist","remove"})
     */
    protected $grupo;    

    /**
    * @ORM\OneToMany(targetEntity="CreditoEmprendedor", mappedBy="emprendedor")
    */
    protected $creditos;

     /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="emprendedores")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;


    /**
     *@ORM\ManyToOne(targetEntity="Grupo", inversedBy="emprendedores")
     *@ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     */
    protected $grupoAnterior;


    public function __toString() {
        return $this->apellido.', '.$this->nombres;
    }

    public function getTitle(){
        return '" '.$this->codigo.' - '.$this->apellido.', '.$this->nombres.' "';
    }
    public function getDatos(){
        $telefono = ($this->getTelefono()) ? ' Tel:'.$this->getTelefono() : '';
        $direccion = ($this->getDireccion()) ? ' Dir:'.$this->getDireccion().' '.$this->getUbicacion() : '';
        return 'DNI:'.$this->getDni(). $telefono. $direccion;
    }
    public function __construct()
    {
        $this->activo = true;
        $this->grupo = new \Doctrine\Common\Collections\ArrayCollection();
    }
   
    /**
     * @ORM\prePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
        $this->activo = 1;
    }    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
    }

    /**
     * Get nombres
     *
     * @return string 
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set dni
     *
     * @param string $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }


    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

     /**
     * Set email
     *
     * @param string $cbu
     */
    public function setCbu($cbu)
    {
        $this->cbu = $cbu;
    }

    /**
     * Get cbu
     *
     * @return string 
     */
    public function getCbu()
    {
        return $this->cbu;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set sobrenombre
     *
     * @param string $sobrenombre
     */
    public function setSobrenombre($sobrenombre)
    {
        $this->sobrenombre = $sobrenombre;
    }

    /**
     * Get sobrenombre
     *
     * @return string 
     */
    public function getSobrenombre()
    {
        return $this->sobrenombre;
    }

    /**
     * Set fechaNacimiento
     *
     * @param date $fechaNacimiento
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    /**
     * Get fechaNacimiento
     *
     * @return string 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set lugarNacimiento
     *
     * @param string $lugarNacimiento
     */
    public function setLugarNacimiento($lugarNacimiento)
    {
        $this->lugarNacimiento = $lugarNacimiento;
    }

    /**
     * Get lugarNacimiento
     *
     * @return string 
     */
    public function getLugarNacimiento()
    {
        return $this->lugarNacimiento;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * Get sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccion
     *
     * @param text $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * Get direccion
     *
     * @return text 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set ubicacion
     *
     * @param string $ubicacion
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;
    }

    /**
     * Get ubicacion
     *
     * @return string 
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set nroHijos
     *
     * @param integer $nroHijos
     */
    public function setNroHijos($nroHijos)
    {
        $this->nroHijos = $nroHijos;
    }

    /**
     * Get nroHijos
     *
     * @return integer 
     */
    public function getNroHijos()
    {
        return $this->nroHijos;
    }

    /**
     * Set edadesHijos
     *
     * @param string $edadesHijos
     */
    public function setEdadesHijos($edadesHijos)
    {
        $this->edadesHijos = $edadesHijos;
    }

    /**
     * Get edadesHijos
     *
     * @return string 
     */
    public function getEdadesHijos()
    {
        return $this->edadesHijos;
    }

    /**
     * Set nroDependientes
     *
     * @param integer $nroDependientes
     */
    public function setNroDependientes($nroDependientes)
    {
        $this->nroDependientes = $nroDependientes;
    }

    /**
     * Get nroDependientes
     *
     * @return integer 
     */
    public function getNroDependientes()
    {
        return $this->nroDependientes;
    }

    /**
     * Set tiempoResidencia
     *
     * @param string $tiempoResidencia
     */
    public function setTiempoResidencia($tiempoResidencia)
    {
        $this->tiempoResidencia = $tiempoResidencia;
    }

    /**
     * Get tiempoResidencia
     *
     * @return string 
     */
    public function getTiempoResidencia()
    {
        return $this->tiempoResidencia;
    }

    /**
     * Set aclaraciones
     *
     * @param text $aclaraciones
     */
    public function setAclaraciones($aclaraciones)
    {
        $this->aclaraciones = $aclaraciones;
    }

    /**
     * Get aclaraciones
     *
     * @return text 
     */
    public function getAclaraciones()
    {
        return $this->aclaraciones;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set estadoCivil
     *
     * @param \App\Entity\Parametro $estadoCivil
     */
    public function setEstadoCivil(\App\Entity\Parametro $estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    }

    /**
     * Get estadoCivil
     *
     * @return \App\Entity\Parametro
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set nivelEducativo
     *
     * @param \App\Entity\Parametro $nivelEducativo
     */
    public function setNivelEducativo(\App\Entity\Parametro $nivelEducativo)
    {
        $this->nivelEducativo = $nivelEducativo;
    }

    /**
     * Get nivelEducativo
     *
     * @return \App\Entity\Parametro
     */
    public function getNivelEducativo()
    {
        return $this->nivelEducativo;
    }

    /**
     * Set tipoVivienda
     *
     * @param \App\Entity\Parametro $tipoVivienda
     */
    public function setTipoVivienda(\App\Entity\Parametro $tipoVivienda)
    {
        $this->tipoVivienda = $tipoVivienda;
    }

    /**
     * Get tipoVivienda
     *
     * @return \App\Entity\Parametro
     */
    public function getTipoVivienda()
    {
        return $this->tipoVivienda;
    }

    /**
     * Set datosAdicionales
     *
     * @param \App\Entity\EmprendedorDatosAdicionales $datosAdicionales
     */
    public function setDatosAdicionales(\App\Entity\EmprendedorDatosAdicionales $datosAdicionales)
    {
        $this->datosAdicionales = $datosAdicionales;
    }

    /**
     * Get datosAdicionales
     *
     * @return \App\Entity\EmprendedorDatosAdicionales
     */
    public function getDatosAdicionales()
    {
        return $this->datosAdicionales;
    }

    /**
     * Set datosAdicionales
     *
     * @param \App\Entity\EmprendedorDatosAdicionales $datosAdicionales
     */
    public function setEmprendedorDatosAdicionales(\App\Entity\EmprendedorDatosAdicionales $datosAdicionales)
    {
        $this->datosAdicionales = $datosAdicionales;
    }

    /**
     * Get datosAdicionales
     *
     * @return \App\Entity\EmprendedorDatosAdicionales
     */
    public function getEmprendedorDatosAdicionales()
    {
        return $this->datosAdicionales;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }


    /**
     * Add grupo
     *
     * @param \App\Entity\CreditoEmprendedor $grupo
     */
    public function addGruposEmprendedores(\App\Entity\GruposEmprendedores $grupo)
    {
        $this->grupo[] = $grupo;
    }

    /**
     * Get creditos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getGruposEmprendedores()
    {
        return $this->grupo;
    }



    public function getGrupo(){
        $array = array();
        foreach($this->getGruposEmprendedores() as $gruposEmprendedor){
            $array[] = $gruposEmprendedor->getGrupo();
        }
        return $array;
    }


    /**
     * Add creditos
     *
     * @param \App\Entity\CreditoEmprendedor $creditos
     */
    public function addCreditoEmprendedor(\App\Entity\CreditoEmprendedor $creditos)
    {
        $this->creditos[] = $creditos;
    }

    /**
     * Get creditos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Add recomendado
     *
     * @param \App\Entity\Emprendedor $recomendado
     */
    public function addEmprendedor(\App\Entity\Emprendedor $recomendado)
    {
        $this->recomendado[] = $recomendado;
    }

    /**
     * Get recomendado
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRecomendado()
    {
        return $this->recomendado;
    }

    /**
     * Set recomendador
     *
     * @param \App\Entity\Emprendedor $recomendador
     */
    public function setRecomendador($recomendador)
    {
        $this->recomendador = ($recomendador)?$recomendador:null;
    }

    /**
     * Get recomendador
     *
     * @return \App\Entity\Emprendedor
     */
    public function getRecomendador()
    {
        return $this->recomendador;
    }

    public function getGrupoLegacy(){
        return $this->grupoAnterior;
    }
}