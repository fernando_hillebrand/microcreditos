<?php


namespace App\Entity\Base;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Entity\User;
use Doctrine\ORM\Mapping\EntityListenerResolver;

abstract class BaseClass
{

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo = true;

    /**
     * @var \DateTime $fechaCreacion
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $fechaCreacion;
    /**
     * @var \DateTime $fechaActualizacion
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $fechaActualizacion;

    /**
     * @var string $creadoPor
     *
     * @Gedmo\Blameable(on="create")
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $creadoPor;

    /**
     * @var string $actualizadoPor
     *
     * @Gedmo\Blameable(on="update")
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $actualizadoPor;


    /**
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @return \DateTime
     */
    public function setFechaCreacion($date)
    {
        return $this->fechaCreacion = $date;
    }


    /**
     * @return \DateTime
     */
    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion($date)
    {
        return $this->fechaActualizacion=$date;
    }

    public function getCreadoPor()
    {
        return $this->creadoPor;
    }

    public function setCreadoPor($user)
    {
        return $this->creadoPor = $user;
    }


    public function getActualizadoPor()
    {
        return $this->actualizadoPor;
    }

    public function setActualizadoPor($user)
    {
        return $this->actualizadoPor = $user;
    }


    public function getDataLog(){
        $str =' creadoPor: '.$this->creadoPor .' | ';
        $str.=' actualizadoPor: '.$this->actualizadoPor .' | ';
        if(!is_null($this->fechaCreacion)) $str.=' fechaCreacion: '.$this->fechaCreacion->format('d-m-Y H:i:s') .' | ';
        if(!is_null($this->fechaActualizacion)) $str.=' fechaActualizacion: '.$this->fechaActualizacion->format('d-m-Y H:i:s') .' | ';
        return $str;
    }
}
