<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Programa
 *
 * @ORM\Table(name="programa")
 * @ORM\Entity(repositoryClass="App\Repository\ProgramaRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Programa  extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    protected $nombre;
    
    /**
     * @var string $sigla
     *
     * @ORM\Column(name="sigla", type="string", length=15)
     * @Assert\Length(max = 15, maxMessage = "Las siglas no debe superar los {{ limit }} caracteres")
     */
    protected $sigla;
    
    /**
     * @ORM\Column(name="porc_interes", type="decimal", scale=2)
     */
    protected $porcInteres;
    
    /**
     * @ORM\Column(name="porc_punitorio", type="decimal", scale=2)
     */
    protected $porcPunitorio;

    /**
     * @ORM\Column(name="monto_punitorio", type="decimal", scale=2)
     */
    protected $montoPunitorio;

    /**
     * @ORM\Column(name="caja", type="decimal", scale=2)
     */
    protected $caja;
    
    /**
     * @ORM\Column(name="banco", type="decimal", scale=2)
     */
    protected $banco;
 
    /**
      * @ORM\Column(name="mes", type="integer")
     */
    protected $mes; 
    
    /**
      * @ORM\Column(name="anio", type="integer")
     */
    protected $anio; 
    
    /**
     * @var integer $activo
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    protected $activo;    
    
    /**
     * @var integer $individual
     *
     * @ORM\Column(name="individual", type="boolean")
     */
    protected $individual;    
    
    /**
      * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;    
    
     /**
     *@ORM\OneToMany(targetEntity="Grupo", mappedBy="programa")
     * 
     */
    protected $grupos;

     /**
     *@ORM\OneToMany(targetEntity="Emprendedor", mappedBy="programa")
     * 
     */
    protected $emprendedores;

     /**
     *@ORM\OneToMany(targetEntity="ProgramaIngreso", mappedBy="programa")
     *@ORM\OrderBy({"fechaIngreso" = "ASC"}) 
     */
    protected $ingresos;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="programas")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;
    
    /**
    * @ORM\OneToMany(targetEntity="ReporteMensual", mappedBy="programa")
    */
    protected $resumenes;


    /**
     * @var integer $habilitadoNuevoCredito
     *
     * @ORM\Column(name="habilitado_nuevo_credito", type="boolean")
     */
    protected $habilitadoNuevoCredito;

    /**
     * @ORM\Column(name="monto_tope", type="decimal", scale=2)
     */
    protected $montoTope;

    /**
     * @ORM\Column(name="cuotas_tope_mensual", type="integer")
     */
    protected $cuotaTopeMensual;

    /**
     * @var integer $habilitadoNuevoCredito
     *
     * @ORM\Column(name="cuotas_mensuales", type="boolean")
     */
    protected $cuotasMensuales;

    /**
     * @ORM\Column(name="cuotas_tope_semanales", type="integer")
     */
    protected $cuotaTopeSemanal;
    /**
     * @var integer $habilitadoNuevoCredito
     *
     * @ORM\Column(name="cuotas_semanales", type="boolean")
     */
    protected $cuotasSemanal;

    /**
     * @var integer $habilitadoNuevoCredito
     *
     * @ORM\Column(name="utiliza_caja", type="boolean")
     */
    protected $utilizaCaja;


    /**
     * @var integer $habilitadoNuevoCredito
     *
     * @ORM\Column(name="solo_empleados", type="boolean")
     */
    protected $soloEmpleados;

    /**
     * @var boolean $grupoSolidario
     *
     * @ORM\Column(name="grupo_solidario", type="boolean")
     */
    protected $grupoSolidario;


    /**
     * @ORM\Column(name="porc_gastos_adm", type="decimal", scale=2)
     */
    protected $porcGastosAdm;

    public function __toString() {
        return $this->sigla;
    }
    
    public function getTitle(){
        return '" '.$this->sigla.' "';
    }
    
    public function __construct()
    {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activo = TRUE;
        $this->individual = FALSE;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set sigla
     *
     * @param string $sigla
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;
    }

    /**
     * Get sigla
     *
     * @return string 
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add grupos
     *
     * @param \App\Entity\Grupo $grupos
     */
    public function addGrupo(\App\Entity\Grupo $grupos)
    {
        $this->grupos[] = $grupos;
    }

    /**
     * Get grupos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    /**
     * @ORM\PrePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
        $this->activo = 1;
        $this->individual = 0;
    }
    

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set porcInteres
     *
     * @param decimal $porcInteres
     */
    public function setPorcInteres($porcInteres)
    {
        $this->porcInteres = $porcInteres;
    }

    /**
     * Get porcInteres
     *
     * @return decimal 
     */
    public function getPorcInteres()
    {
        return $this->porcInteres;
    }

    /**
     * Set porcPunitorio
     *
     * @param decimal $porcPunitorio
     */
    public function setPorcPunitorio($porcPunitorio)
    {
        $this->porcPunitorio = $porcPunitorio;
    }

    /**
     * Get porcPunitorio
     *
     * @return decimal 
     */
    public function getPorcPunitorio()
    {
        return $this->porcPunitorio;
    }

    /**
     * Add ingresos
     *
     * @param \App\Entity\ProgramaIngreso $ingresos
     */
    public function addProgramaIngreso(\App\Entity\ProgramaIngreso $ingresos)
    {
        $this->ingresos[] = $ingresos;
    }

    /**
     * Get ingresos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getIngresos()
    {
        return $this->ingresos;
    }

    /**
     * Add emprendedores
     *
     * @param \App\Entity\Emprendedor $emprendedores
     */
    public function addEmprendedor(\App\Entity\Emprendedor $emprendedores)
    {
        $this->emprendedores[] = $emprendedores;
    }

    /**
     * Get emprendedores
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEmprendedores()
    {
        return $this->emprendedores;
    }

    /**
     * Set gastoAdministrativo
     *
     * @param decimal $gastoAdministrativo
     */
    public function setGastoAdministrativo($gastoAdministrativo)
    {
        $this->gastoAdministrativo = $gastoAdministrativo;
    }

    /**
     * Get gastoAdministrativo
     *
     * @return decimal 
     */
    public function getGastoAdministrativo()
    {
        return $this->gastoAdministrativo;
    }

    /**
     * Set individual
     *
     * @param boolean $individual
     */
    public function setIndividual($individual)
    {
        $this->individual = $individual;
    }

    /**
     * Get individual
     *
     * @return boolean 
     */
    public function getIndividual()
    {
        return $this->individual;
    }

    /**
     * Set caja
     *
     * @param decimal $caja
     */
    public function setCaja($caja)
    {
        $this->caja = $caja;
    }

    /**
     * Get caja
     *
     * @return decimal 
     */
    public function getCaja()
    {
        return $this->caja;
    }

    /**
     * Set banco
     *
     * @param decimal $banco
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    /**
     * Get banco
     *
     * @return decimal 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set montoPunitorio
     *
     * @param decimal $montoPunitorio
     */
    public function setMontoPunitorio($montoPunitorio)
    {
        $this->montoPunitorio = $montoPunitorio;
    }

    /**
     * Get montoPunitorio
     *
     * @return decimal 
     */
    public function getMontoPunitorio()
    {
        return $this->montoPunitorio;
    }

    /**
     * Add resumenes
     *
     * @param \App\Entity\ReporteMensual $resumenes
     */
    public function addReporteMensual(\App\Entity\ReporteMensual $resumenes)
    {
        $this->resumenes[] = $resumenes;
    }

    /**
     * Get resumenes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getResumenes()
    {
        return $this->resumenes;
    }

    /**
     * @return int
     */
    public function getHabilitadoNuevoCredito()
    {
        return $this->habilitadoNuevoCredito;
    }

    /**
     * @param int $habilitadoNuevoCredito
     */
    public function setHabilitadoNuevoCredito($habilitadoNuevoCredito)
    {
        $this->habilitadoNuevoCredito = $habilitadoNuevoCredito;
    }

    /**
     * @return mixed
     */
    public function getMontoTope()
    {
        return $this->montoTope;
    }

    /**
     * @param mixed $montoTope
     */
    public function setMontoTope($montoTope)
    {
        $this->montoTope = $montoTope;
    }

    /**
     * @return mixed
     */
    public function getCuotaTopeMensual()
    {
        return $this->cuotaTopeMensual;
    }

    /**
     * @param mixed $cuotaTopeMensual
     */
    public function setCuotaTopeMensual($cuotaTopeMensual)
    {
        $this->cuotaTopeMensual = $cuotaTopeMensual;
    }

    /**
     * @return int
     */
    public function getCuotasMensuales()
    {
        return $this->cuotasMensuales;
    }

    /**
     * @param int $cuotasMensuales
     */
    public function setCuotasMensuales($cuotasMensuales)
    {
        $this->cuotasMensuales = $cuotasMensuales;
    }

    /**
     * @return mixed
     */
    public function getCuotaTopeSemanal()
    {
        return $this->cuotaTopeSemanal;
    }

    /**
     * @param mixed $cuotaTopeSemanal
     */
    public function setCuotaTopeSemanal($cuotaTopeSemanal)
    {
        $this->cuotaTopeSemanal = $cuotaTopeSemanal;
    }

    /**
     * @return int
     */
    public function getCuotasSemanal()
    {
        return $this->cuotasSemanal;
    }

    /**
     * @param int $cuotasSemanal
     */
    public function setCuotasSemanal($cuotasSemanal)
    {
        $this->cuotasSemanal = $cuotasSemanal;
    }

    /**
     * @return int
     */
    public function getUtilizaCaja()
    {
        return $this->utilizaCaja;
    }

    /**
     * @param int $utilizaCaja
     */
    public function setUtilizaCaja($utilizaCaja)
    {
        $this->utilizaCaja = $utilizaCaja;
    }

    /**
     * @return int
     */
    public function getSoloEmpleados()
    {
        return $this->soloEmpleados;
    }

    /**
     * @param int $soloEmpleados
     */
    public function setSoloEmpleados($soloEmpleados)
    {
        $this->soloEmpleados = $soloEmpleados;
    }

    /**
     * @return mixed
     */
    public function getPorcGastosAdm()
    {
        return $this->porcGastosAdm;
    }

    /**
     * @param mixed $porcGastosAdm
     */
    public function setPorcGastosAdm($porcGastosAdm)
    {
        $this->porcGastosAdm = $porcGastosAdm;
    }

    /**
     * @return mixed
     */
    public function getGrupoSolidario()
    {
        return $this->grupoSolidario;
    }

    /**
     * @param mixed $grupoSolidario
     */
    public function setGrupoSolidario($grupoSolidario)
    {
        $this->grupoSolidario = $grupoSolidario;
    }



}