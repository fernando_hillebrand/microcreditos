<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\ProgramaIngreso
 *
 * @ORM\Table(name="programa_ingreso")
 * @ORM\Entity
 */

class ProgramaIngreso extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="monto", type="decimal", scale=2)
     */
    protected $monto;
    
    /**
     * @ORM\Column(name="fecha_ingreso", type="date")
     */
    protected $fechaIngreso;    
    
    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;    
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="ingresos")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;    
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="programaIngresos")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;
    
    public function __toString() {
        return $this->monto;
    }
    public function __construct()
    {
        $this->fechaAlta = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set monto
     *
     * @param decimal $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    /**
     * Get monto
     *
     * @return decimal 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set fechaIngreso
     *
     * @param date $fechaIngreso
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;
    }

    /**
     * Get fechaIngreso
     *
     * @return date 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}