<?php

namespace App\Entity;
use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\Gastos
 *
 * @ORM\Table(name="gastos")
 * @ORM\Entity(repositoryClass="App\Repository\GastosRepository")
 * @ORM\HasLifecycleCallbacks() 
 */

class Gastos extends BaseClass
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
 
    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;
    
    /**
     * @ORM\Column(name="importe", type="decimal", scale=2)
     */
    protected $importe;
    
    /**
     * @ORM\Column(name="obserbacion", type="string", nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\ManyToOne(targetEntity="Parametro")
     * @ORM\JoinColumn(name="concepto", referencedColumnName="id")
     **/  
    protected $concepto;

    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="gastos")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="gastosCargados")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;
    
    /**
     * @ORM\Column(name="fecha_alta", type="datetime")
     */
    protected $fechaAlta;  

    
    /**
     * @ORM\PrePersist
     */
    public function setAlta()
    {
        $this->fechaAlta = new \DateTime();
    }       
    public function getTitle(){
        return '';
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param decimal $importe
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    }

    /**
     * Get importe
     *
     * @return decimal 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set observacion
     *
     * @param text $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * Get observacion
     *
     * @return text 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set fechaAlta
     *
     * @param datetime $fechaAlta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    }

    /**
     * Get fechaAlta
     *
     * @return datetime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set concepto
     *
     * @param \App\Entity\Parametro $concepto
     */
    public function setConcepto(\App\Entity\Parametro $concepto)
    {
        $this->concepto = $concepto;
    }

    /**
     * Get concepto
     *
     * @return \App\Entity\Parametro
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function getDataLog(){
        $str =' id: '.$this->id .' | ';
        $str .=' fecha: '.$this->fecha->format('d-m-Y H:i:s') .' | ';
        $str .=' importe: '.$this->importe .' | ';
        $str .=' observacion: '.$this->observacion .' | ';
        $str .=' concepto: '.$this->concepto .' | ';
        $str .=' programa: '.$this->programa .' | ';
        $str .=' usuario: '.$this->usuario .' | ';
        $str .= parent::getDataLog();
        return $str;
    }
}