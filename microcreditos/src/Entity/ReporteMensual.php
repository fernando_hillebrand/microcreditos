<?php

namespace App\Entity;

use App\Entity\Base\BaseClass;
use Doctrine\ORM\Mapping as ORM;
/**
 * \App\Entity\ReporteMensual
 *
 * @ORM\Table(name="reporte_mensual")
 * @ORM\Entity(repositoryClass="App\Repository\ReporteMensualRepository")
 */

class ReporteMensual extends BaseClass
{
   
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
    
    /**
      * @ORM\Column(name="fecha", type="datetime")
     */
    protected $fecha;    
    
    /**
      * @ORM\Column(name="mes", type="integer")
     */
    protected $mes; 
    
    /**
      * @ORM\Column(name="anio", type="integer")
     */
    protected $anio; 
    
    /**
      * @ORM\Column(name="cartera_activa", type="decimal", scale=2)
     */    
    protected $carteraActiva;

    /**
      * @ORM\Column(name="cartera_nueva", type="decimal", scale=2)
     */    
    protected $carteraNueva;

    /**
      * @ORM\Column(name="renovaciones", type="decimal", scale=2)
     */    
    protected $renovaciones;

    /**
      * @ORM\Column(name="cobranzas", type="decimal", scale=2)
     */    
    protected $cobranzas;

    /**
      * @ORM\Column(name="incobrables", type="decimal", scale=2)
     */    
    protected $incobrables;

    /**
      * @ORM\Column(name="interes_semana1", type="decimal", scale=2)
     */    
    protected $interesSemana1;

    /**
      * @ORM\Column(name="interes_semana2", type="decimal", scale=2)
     */    
    protected $interesSemana2;

    /**
      * @ORM\Column(name="interes_semana3", type="decimal", scale=2)
     */    
    protected $interesSemana3;

    /**
      * @ORM\Column(name="interes_semana4", type="decimal", scale=2)
     */    
    protected $interesSemana4;

    /**
      * @ORM\Column(name="interes_semana5", type="decimal", scale=2)
     */    
    protected $interesSemana5;

    /**
      * @ORM\Column(name="gastos_administrativos", type="decimal", scale=2)
     */    
    protected $gastosAdministrativos;

    /**
      * @ORM\Column(name="punitorios", type="decimal", scale=2)
     */    
    protected $punitorios;

    /**
      * @ORM\Column(name="honorarios", type="decimal", scale=2)
     */    
    protected $honorarios;

    /**
      * @ORM\Column(name="gastos", type="decimal", scale=2)
     */    
    protected $gastos;

    /**
      * @ORM\Column(name="saldo_caja", type="decimal", scale=2)
     */    
    protected $saldoCaja;

    /**
      * @ORM\Column(name="saldo_banco", type="decimal", scale=2)
     */    
    protected $saldoBanco;

    /**
      * @ORM\Column(name="ajuste", type="decimal", scale=2)
     */    
    protected $ajuste;

    /**
     * @ORM\Column(name="observacion", type="string", length=100, nullable=true)
     */
    protected $observacion;
    /**
      * @ORM\Column(name="fecha_carga", type="datetime")
     */
    protected $fechaCarga;    
    
    /**
     *@ORM\ManyToOne(targetEntity="Programa", inversedBy="resumenes")
     *@ORM\JoinColumn(name="programa_id", referencedColumnName="id") 
     */
    protected $programa;    
    
    /**
     *@ORM\ManyToOne(targetEntity="User", inversedBy="resumenes")
     *@ORM\JoinColumn(name="usuario_id", referencedColumnName="id") 
     */
    protected $usuario;

    /**
    * @ORM\OneToMany(targetEntity="Pago", mappedBy="reporte_mensual")
    */
    protected $pagos;
    
    public function __construct()
    {
        $this->fechaCarga = new \DateTime();      
        $this->incobrables = 0;      
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set carteraActiva
     *
     * @param decimal $carteraActiva
     */
    public function setCarteraActiva($carteraActiva)
    {
        $this->carteraActiva = $carteraActiva;
    }

    /**
     * Get carteraActiva
     *
     * @return decimal 
     */
    public function getCarteraActiva()
    {
        return $this->carteraActiva;
    }

    /**
     * Set carteraNueva
     *
     * @param decimal $carteraNueva
     */
    public function setCarteraNueva($carteraNueva)
    {
        $this->carteraNueva = $carteraNueva;
    }

    /**
     * Get carteraNueva
     *
     * @return decimal 
     */
    public function getCarteraNueva()
    {
        return $this->carteraNueva;
    }

    /**
     * Set renovaciones
     *
     * @param decimal $renovaciones
     */
    public function setRenovaciones($renovaciones)
    {
        $this->renovaciones = $renovaciones;
    }

    /**
     * Get renovaciones
     *
     * @return decimal 
     */
    public function getRenovaciones()
    {
        return $this->renovaciones;
    }

    /**
     * Set cobranzas
     *
     * @param decimal $cobranzas
     */
    public function setCobranzas($cobranzas)
    {
        $this->cobranzas = $cobranzas;
    }

    /**
     * Get cobranzas
     *
     * @return decimal 
     */
    public function getCobranzas()
    {
        return $this->cobranzas;
    }
    
    /**
     * Set incobrables
     *
     * @param decimal $incobrables
     */
    public function setIncobrables($incobrables)
    {
        $this->incobrables = $incobrables;
    }

    /**
     * Get incobrables
     *
     * @return decimal 
     */
    public function getIncobrables()
    {
        return $this->incobrables;
    }

    /**
     * Set interesSemana1
     *
     * @param decimal $interesSemana1
     */
    public function setInteresSemana1($interesSemana1)
    {
        $this->interesSemana1 = $interesSemana1;
    }

    /**
     * Get interesSemana1
     *
     * @return decimal 
     */
    public function getInteresSemana1()
    {
        return $this->interesSemana1;
    }

    /**
     * Set interesSemana2
     *
     * @param decimal $interesSemana2
     */
    public function setInteresSemana2($interesSemana2)
    {
        $this->interesSemana2 = $interesSemana2;
    }

    /**
     * Get interesSemana2
     *
     * @return decimal 
     */
    public function getInteresSemana2()
    {
        return $this->interesSemana2;
    }

    /**
     * Set interesSemana3
     *
     * @param decimal $interesSemana3
     */
    public function setInteresSemana3($interesSemana3)
    {
        $this->interesSemana3 = $interesSemana3;
    }

    /**
     * Get interesSemana3
     *
     * @return decimal 
     */
    public function getInteresSemana3()
    {
        return $this->interesSemana3;
    }

    /**
     * Set interesSemana4
     *
     * @param decimal $interesSemana4
     */
    public function setInteresSemana4($interesSemana4)
    {
        $this->interesSemana4 = $interesSemana4;
    }

    /**
     * Get interesSemana4
     *
     * @return decimal 
     */
    public function getInteresSemana4()
    {
        return $this->interesSemana4;
    }

    /**
     * Set interesSemana5
     *
     * @param decimal $interesSemana5
     */
    public function setInteresSemana5($interesSemana5)
    {
        $this->interesSemana5 = $interesSemana5;
    }

    /**
     * Get interesSemana5
     *
     * @return decimal 
     */
    public function getInteresSemana5()
    {
        return $this->interesSemana5;
    }

    /**
     * Set gastos
     *
     * @param decimal $gastos
     */
    public function setGastos($gastos)
    {
        $this->gastos = $gastos;
    }

    /**
     * Get gastos
     *
     * @return decimal 
     */
    public function getGastos()
    {
        return $this->gastos;
    }

    /**
     * Set saldoCaja
     *
     * @param decimal $saldoCaja
     */
    public function setSaldoCaja($saldoCaja)
    {
        $this->saldoCaja = $saldoCaja;
    }

    /**
     * Get saldoCaja
     *
     * @return decimal 
     */
    public function getSaldoCaja()
    {
        return $this->saldoCaja;
    }

    /**
     * Set fechaCarga
     *
     * @param datetime $fechaCarga
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;
    }

    /**
     * Get fechaCarga
     *
     * @return datetime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set programa
     *
     * @param \App\Entity\Programa $programa
     */
    public function setPrograma(\App\Entity\Programa $programa)
    {
        $this->programa = $programa;
    }

    /**
     * Get programa
     *
     * @return \App\Entity\Programa
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set usuario
     *
     * @param \App\Entity\User $usuario
     */
    public function setUsuario(\App\Entity\User $usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get usuario
     *
     * @return \App\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set ajuste
     *
     * @param decimal $ajuste
     */
    public function setAjuste($ajuste)
    {
        $this->ajuste = $ajuste;
    }

    /**
     * Get ajuste
     *
     * @return decimal 
     */
    public function getAjuste()
    {
        return $this->ajuste;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

    /**
     * Set saldoBanco
     *
     * @param decimal $saldoBanco
     */
    public function setSaldoBanco($saldoBanco)
    {
        $this->saldoBanco = $saldoBanco;
    }

    /**
     * Get saldoBanco
     *
     * @return decimal 
     */
    public function getSaldoBanco()
    {
        return $this->saldoBanco;
    }

    /**
     * Set honorarios
     *
     * @param decimal $honorarios
     */
    public function setHonorarios($honorarios)
    {
        $this->honorarios = $honorarios;
    }

    /**
     * Get honorarios
     *
     * @return decimal 
     */
    public function getHonorarios()
    {
        return $this->honorarios;
    }

    /**
     * Set gastosAdministrativos
     *
     * @param decimal $gastosAdministrativos
     */
    public function setGastosAdministrativos($gastosAdministrativos)
    {
        $this->gastosAdministrativos = $gastosAdministrativos;
    }

    /**
     * Get gastosAdministrativos
     *
     * @return decimal 
     */
    public function getGastosAdministrativos()
    {
        return $this->gastosAdministrativos;
    }

    /**
     * Set punitorios
     *
     * @param decimal $punitorios
     */
    public function setPunitorios($punitorios)
    {
        $this->punitorios = $punitorios;
    }

    /**
     * Get punitorios
     *
     * @return decimal 
     */
    public function getPunitorios()
    {
        return $this->punitorios;
    }

    /**
     * Add pagos
     *
     * @param \App\Entity\Pago $pagos
     */
    public function addPago(\App\Entity\Pago $pagos)
    {
        $this->pagos[] = $pagos;
    }

    /**
     * Get pagos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPagos()
    {
        return $this->pagos;
    }

    public function __toString()
    {
        return $this->id.' - '.$this.$this->fechaCarga;
    }
}