<?php

namespace App\EventListener;

use App\Entity\Logs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Monolog\Logger;

class DatabaseActivitySubscriber implements EventSubscriber
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postRemove,
        ];
    }

    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();


        if( ! (get_class($entity) =='App\Entity\User') ) {
            if (!get_class($entity) == 'App\Entity\Logs') {
                $entity->setCreadoPor($this->tokenStorage->getToken()->getUser());
                $entity->setActualizadoPor($this->tokenStorage->getToken()->getUser());
                $fecha = new \DateTime();
                $entity->setFechaCreacion($fecha);
                $entity->setFechaActualizacion($fecha);
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $args){
        $entity = $args->getObject();

        if(!is_null($entity->getId())) {
            if( ! (get_class($entity) =='App\Entity\User') ){
                if( ! (get_class($entity) =='App\Entity\Logs') ){
                    if( ! (get_class($entity) =='App\Entity\ReporteMensual') ){
                        $this->logChangeRow(
                            get_class($args->getObject()),
                            $entity->getId(),
                            $args->getEntityChangeSet(),
                            $args->getEntityManager()
                        );
                    }
                }
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $this->logDeleteRow(
            get_class($args->getObject()),
            $entity,
            $args->getEntityManager()
        );

    }

    public function logChangeRow($tabla,$id,$arrays, $em){
        $str ='';
        $delete=0;
        foreach($arrays as $index => $element){
            $str.=' campo: '.$index.' | ';

            if( is_object($element[0]) AND get_class($element[0]) =='DateTime' ){
                $str.=' Valor Anterior: '.$element[0]->format('d/m/Y').' |';
            }else{
                if(!is_null($element[0])){
                    $str.=' Valor Anterior: '.$element[0].' | ';
                }else{
                    $str.=' Valor Anterior: NULL | ';
                }

            }

            if( is_object($element[1]) AND get_class($element[1]) =='DateTime' ){
                $str.=' Valor Nuevo: '.$element[1]->format('d/m/Y').' @ ';
            }else{
                if(!is_null($element[0])){
                    $str.=' Valor Nuevo: '.$element[1].' @ ';
                }else{
                    $str.=' Valor Nuevo: NULL @ ';
                }

            }
        }
        $fecha = new \DateTime();
        $conn = $em->getConnection();

        $stmt = $conn->prepare(
            'INSERT INTO  logs (creado_por_id,descripcion,fecha_creacion,campo,objeto_id,borrado ) '.
            'VALUES ('.$this->tokenStorage->getToken()->getUser()->getId() . ',"'.$str.'","'.$fecha->format('Y-m-d h:i:s').'","'.$tabla.'","'.$id.'",'.$delete.')'
        );
        $stmt->execute();
    }

    public function logDeleteRow($tabla,$entity, $em){
        $str = $entity->getDataLog();
        $id = $entity->getId();
        $delete=1;
        $fecha = new \DateTime();
        $conn = $em->getConnection();

        $stmt = $conn->prepare(
                  'INSERT INTO  logs (creado_por_id,descripcion,fecha_creacion,campo,objeto_id,borrado ) '.
                  'VALUES ('.$this->tokenStorage->getToken()->getUser()->getId() . ',"'.$str.'","'.$fecha->format('Y-m-d h:i:s').'","'.$tabla.'","'.$id.'",'.$delete.')'
                 );
        $stmt->execute();
    }
}