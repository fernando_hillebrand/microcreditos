<?php

namespace App\EventListener;

use App\Service\SecurityManager;
use App\Service;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;


class FirewallListener{

    protected $container;

    public function __construct(ContainerInterface $container){ // this is @service_container
        $this->container = $container;
    }

    public function __invoke(RequestEvent $event)
    {
        $this->onKernelRequest($event);
    }

    public function onKernelRequest($event){
        try {
            $securityContext = $this->container->get('security.authorization_checker');
            //si el usuario está autenticado, lanzo el control especial.
            if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
                $usuario = $this->container->get('security.token_storage')->getToken()->getUser();
                if (!$usuario->hasRole('ROLE_SUPER_ADMIN')) {
                    $requestStack = $this->container->get('request_stack');
                    $masterRequest = $requestStack->getMasterRequest(); // this is the call that breaks ESI
                    if ($masterRequest) {
                        $ruta = $masterRequest->attributes->get('_route');
                        //si no es la request del debugger de sf o el profiler
                        if (!preg_match("/^(_|root|usuario_acceso_denegado)/", $ruta)) {
                            $rol = $usuario->getRoles();
                            if (!$this->isGranted($rol, $ruta)) {
//                                var_dump($ruta,'usuario_acceso_denegado');exit;
                                $url = $this->container->get('router')->generate('usuario_acceso_denegado');
                                $event->setResponse(new RedirectResponse($url));
                            }
                        }
                    }
                }
            }
        }catch( AuthenticationCredentialsNotFoundException $e){
            //hago el catch de la excepcion para que siga el flujo normal de symfony
        }
    }

    private function isGranted($roles, $ruta)
    {
        $acceso = $this->generateArray();
        $role_hierarchy = $this->container->getParameter('security.role_hierarchy.roles');

        $retorno = false;
        if(!is_null($acceso)) {
            $rolesFind = $this->findAllRolesHierarchy($role_hierarchy,$roles);
            foreach($rolesFind as $role){
                if (isset($acceso[$role][$ruta]) AND $retorno == false) {
                    if ($acceso[$role][$ruta]) {
                        $retorno = $acceso[$role][$ruta];
                    }
                }
            }
        }
        return $retorno;
    }

    private function findAllRolesHierarchy($role_hierarchy,$roles, $rolesArray = array()){
        foreach($roles as $role){
            $rolesArray[$role] =$role;
            if(isset($role_hierarchy[$role]) AND is_array($role_hierarchy[$role])) {
                $rolesArray = $this->findAllRolesHierarchy($role_hierarchy, $role_hierarchy[$role], $rolesArray);
            }
        }
        return $rolesArray;
    }

    private function generateArray(){
        $values = null;
        try {
            $values = Yaml::parse(file_get_contents(dirname(__FILE__).'/../Resources/config/firewall_acl.yml'));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
        return $values['acl'];
    }
}