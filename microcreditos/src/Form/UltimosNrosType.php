<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class UltimosNrosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class)
            ->add('activo',CheckboxType::class)
            ->add('descripcion',TextType::class,array('label'=>'Prefijo:'))
            ->add('numerico',IntegerType::class,array('label'=>'Último Número:'))
            ->add('agrupador',EntityType::class,array(
                'label'=>'Concepto:',
                'class' => Parametro::class,
                'query_builder' => function(\App\Repository\ParametroRepository $em)
                {
                        return $em->createQueryBuilder('d')->where('d.descripcion='."'ultimo-nro'")
                                ->orderBy('d.nombre', 'ASC');
                },))
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_ultimosnrostype';
    }
}
