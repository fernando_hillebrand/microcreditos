<?php

namespace App\Form;

use App\Entity\Parametro;
use App\Entity\Programa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class IngresoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class,array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha:'))
            ->add('ingreso',IntegerType::class, array('label'=>'Importe Ingreso:'))
            ->add('observacion',TextType::class, array('label'=>'Observaciones:','required' => false,))
            ->add('programa', EntityType::class,
                array(  'label' => 'Programa de Fondos:',
                    'required' => false,
                    'class' => Programa::class,
                )
            )
            ->add('concepto',EntityType::class,array(
                    'label'=>'Concepto:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                        $concepto = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=1 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'conceptos-caja')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.agrupador_id = ?1 ')
                                 ->orderBy(' p.nombre ')
                                ->setParameter(1,$concepto);  
                    },)) 
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_gastostype';
    }
}
