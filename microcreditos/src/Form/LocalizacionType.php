<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class LocalizacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agrupador_id')
            ->add('padre_id')
            ->add('nombre')
            ->add('descripcion')
            ->add('numerico')
            ->add('boleano')
            ->add('activo')
            ->add('padre')
            ->add('agrupador')
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_localizaciontype';
    }
}
