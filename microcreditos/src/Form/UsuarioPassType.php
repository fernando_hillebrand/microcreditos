<?php

namespace App\Form;

use App\Entity\User;
use App\Service\RolesHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UsuarioPassType extends AbstractType {

    /**
     * @var RolesHelper
     */
    private $roles;

    /**
     * @param string $class The User class name
     * @param RolesHelper $roles Array or roles.
     */
    public function __construct(RolesHelper $roles)
    {
        $this->roles = $roles;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'Contraseña','attr' => array('placeholder'=>'contraseña','class'=>'form-control')),
                'second_options' => array('label' => 'Repita','attr' => array('placeholder'=>'Repita la contraseña','class'=>'form-control')),
                'invalid_message' => 'Las contraseñas ingresadas no coinciden',
            ))
        ;
    }
    
    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => User::class,
        );
    }

    public function getName() {
        return 'microcreditos_microcreditosbundle_usuariotype';
    }

}
