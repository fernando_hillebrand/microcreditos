<?php

namespace App\Form;

use App\Entity\Emprendedor;
use App\Entity\Grupo;
use App\Entity\GruposEmprendedores;
use App\Entity\Parametro;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Microcreditos\MicrocreditosBundle\Form\Type\GroupSelectorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmprendedorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('programa',null,array('label'=>'Programa de Fondos:'))
//            ->add('grupo',GroupSelectorType::class,array('label'=>'Grupo:','required'=>true))
//            ->add('grupo', EntityType::class,
//                array(  'label' => 'Grupo:',
//                    'required' => true,
//                    'class' => GruposEmprendedores::class,
//                    'multiple' => true
//                )
//            )
            ->add('codigo',TextType::class,array('label'=>'Código:'))
            ->add('nombres',TextType::class,array('label'=>'Nombres:'))
            ->add('apellido',TextType::class,array('label'=>'Apellido:'))
            ->add('sobrenombre',TextType::class,array('label'=>'Conocido/a como:','required'=>false))
                
//            ->add('estado_civil',EntityType::class,array(
//                    'label'=>'Estado Civil:',
//                    'class' => 'App\Entity\Parametro',
//                    'query_builder' => function(ParametroRepository $em)

//                        $estado_civil = $em->createQueryBuilder('g')->select('g.id')
//                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
//                                           ->setParameter(1,'estado-civil')
//                                           ->getQuery()->getSingleScalarResult();
//                         return $em->createQueryBuilder('p')
//                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
//                                ->setParameter(1,$estado_civil);
//                    },))
            ->add('estado_civil', EntityType::class, [
                'class' => Parametro::class,
                'query_builder' => function (\App\Repository\ParametroRepository $er) {
                        $estado_civil = $er->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'estado-civil')
                                           ->getQuery()->getSingleScalarResult();
                         return $er->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$estado_civil);
                },
            ])
                
            ->add('dni',null,array('label'=>'DNI:'))
            ->add('email',null,array('label'=>'Email:'))
            ->add('cbu',null,array('label'=>'CBU:'))
            ->add('cuit',null,array('label'=>'CUIT/CUIL:'))
            ->add('lugar_nacimiento',null,array('label'=>'Lugar de Nacimiento:','required'=>false))
           // ->add('fecha_nacimiento','date',array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha de Nacimiento:'))
            ->add('fecha_nacimiento',TextType::class,array('label'=>'Fecha de Nacimiento:'))
            ->add('sexo',ChoiceType::class, array('label'=>'Sexo:',
                  'choices'   => array('Femenino' => 'F', 'Masculino' => 'M'),'expanded'=>true))

            ->add('nivel_educativo',EntityType::class,array(
                    'label'=>'Nivel Educativo:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository  $em)
                    {
                        $nivel_educativo = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'educacion')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$nivel_educativo);  
                    },)) 
                            
            ->add('telefono',null,array('label'=>'Teléfonos:','required'=>false))
            ->add('direccion',TextType::class,array('label'=>'Dirección:'))
            ->add('ubicacion',null,array('label'=>'Ubicación/Ciudad:','required'=>false))
                            
            ->add('tipo_vivienda',EntityType::class,array(
                    'label'=>'Tipo de Vivienda:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository  $em)
                    {
                        $tipo_vivienda = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'vivienda')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$tipo_vivienda);  
                    },))                             

            ->add('tiempo_residencia',null,array('label'=>'Tiempo de Residencia:','required'=>false))
//            ->add('recomendador','emprendedor_selector',array('label'=>'RECOMENDADO POR:','required'=>false))
            ->add('recomendador', EntityType::class,
                array(  'label' => 'Recomendado por:',
                    'required' => false,
                    'class' => Emprendedor::class,
                )
            )
                            
            ->add('activo',null,array('label'=>'Activo:','required'=>false))
            ->add('aclaraciones',null,array('label'=>'Aclaraciones Adicionales:','required'=>false))
            ->add('DatosAdicionales', EmprendedorDatosAdicionalesType::class)
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Emprendedor::class,
        ]);
    }

    
    public function getName()
    {
        return 'microcreditos_microcreditosbundle_emprendedortype';
    }
}
