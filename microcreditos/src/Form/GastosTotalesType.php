<?php

namespace App\Form;

use App\Entity\ConceptoGastos;
use App\Repository\ConceptoGastosRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class GastosTotalesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
            ->add('monto',IntegerType::class, array('label'=>'Monto:'))
            ->add('fecha',DateType::class,array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha:'))
            ->add('comprobante',TextType::class, array('label'=>'Numero Comprobante:'))
            ->add('concepto',EntityType::class,array(
                'class' => ConceptoGastos::class,
                'query_builder' => function(EntityRepository $em) {
                    return $em->createQueryBuilder('u')
                            // ->where('u.id>1')
                            ->orderBy('u.nombre', 'ASC'); },'label'=>'Concepto:')
            )
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_GastosTotalesType';
    }
}
