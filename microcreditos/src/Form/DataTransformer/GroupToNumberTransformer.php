<?php
namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Microcreditos\MicrocreditosBundle\Entity\Grupo;

class GroupToNumberTransformer //implements DataTransformerInterface
{
//    /**
//     * @var ObjectManager
//     */
//    private $om;
//
//    /**
//     * @param ObjectManager $om
//     */
//    public function __construct(ObjectManager $om)
//    {
//        $this->om = $om;
//    }
//
//    /**
//     * Transforms an object (grupo) to a string (number).
//     *
//     * @param  Grupo|null $grupo
//     * @return string
//     */
//    public function transform($grupo)
//    {
//        if (null === $grupo) {
//            return "";
//        }
//
//        return $grupo->getId();
//    }
//
//    /**
//     * Transforms a string (number) to an object (grupo).
//     *
//     * @param  string $number
//     * @return Grupo|null
//     * @throws TransformationFailedException if object (grupo) is not found.
//     */
//    public function reverseTransform($number)
//    {
//        if (!$number) {
//            return null;
//        }
//
//        $grupo = $this->om
//            ->getRepository('MicrocreditosBundle:Grupo')
//            ->findOneBy(array('id' => $number))
//        ;
//
//        if (null === $grupo) {
//            throw new TransformationFailedException(sprintf(
//                'An issue with number "%s" does not exist!',
//                $number
//            ));
//        }
//
//        return $grupo;
//    }
}