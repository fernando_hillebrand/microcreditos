<?php
namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Microcreditos\MicrocreditosBundle\Entity\Emprendedor;

class EmprendedorToNumberTransformer //implements DataTransformerInterface
{
//    /**
//     * @var ObjectManager
//     */
//    private $om;
//
//    /**
//     * @param ObjectManager $om
//     */
//    public function __construct(ObjectManager $om)
//    {
//        $this->om = $om;
//    }
//
//    /**
//     * Transforms an object (emprendedor) to a string (number).
//     *
//     * @param  Emprendedor|null $emprendedor
//     * @return string
//     */
//    public function transform($emprendedor)
//    {
//        if (null === $emprendedor) {
//            return "";
//        }
//
//        return $emprendedor->getId();
//    }
//
//    /**
//     * Transforms a string (number) to an object (emprendedor).
//     *
//     * @param  string $number
//     * @return $emprendedor|null
//     * @throws TransformationFailedException if object (emprendedor) is not found.
//     */
//    public function reverseTransform($number)
//    {
//        if (!$number) {
//            return null;
//        }
//
//        $emprendedor = $this->om
//            ->getRepository('MicrocreditosBundle:Emprendedor')
//            ->findOneBy(array('id' => $number))
//        ;
//
//        if (null === $emprendedor) {
//            throw new TransformationFailedException(sprintf(
//                'An issue with number "%s" does not exist!',
//                $number
//            ));
//        }
//
//        return $emprendedor;
//    }
}