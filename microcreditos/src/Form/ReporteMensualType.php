<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ReporteMensualType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha',DateType::class,array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha de Cierre:'))
            ->add('programa',null)
            ->add('mes',null)
            ->add('anio',null)
            ->add('carteraActiva',null,array('label'=>'Cartera Activa al inicio del periodo:'))
            ->add('carteraNueva',null,array('label'=>'Cartera Nueva incorporada en el periodo:'))
            ->add('renovaciones',null,array('label'=>'Renovaciones del periodo:'))
            ->add('cobranzas',null,array('label'=>'Cobranzas de cartera del periodo:'))
            ->add('incobrables',null,array('label'=>'Incobrables del periodo:'))
            ->add('interesSemana1',null,array('label'=>'Semana 1:'))
            ->add('interesSemana2',null,array('label'=>'Semana 2:'))
            ->add('interesSemana3',null,array('label'=>'Semana 3:'))
            ->add('interesSemana4',null,array('label'=>'Semana 4:'))
            ->add('interesSemana5',null,array('label'=>'Semana 5:'))
            ->add('gastosAdministrativos',null,array('label'=>'Gastos Administrativos:'))
            ->add('punitorios',null,array('label'=>'Punitorios:'))
            ->add('gastos',null,array('label'=>'Gastos:'))
            ->add('honorarios',null,array('label'=>'Honorarios:'))
            ->add('saldoBanco',null,array('label'=>'Saldo de Cuenta Corriente del Fondo:'))
            ->add('saldoCaja',null,array('label'=>'Saldo en Caja:'))
            ->add('ajuste',null,array('label'=>'Ajuste:'))
            ->add('observacion',null,array('label'=>'Observaciones:'))
            ->add('usuario',null,array('label'=>'Usuario:'))
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_reportemensualtype';
    }
}
