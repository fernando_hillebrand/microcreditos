<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\File;


class CreditoFileAnalyzerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'file',
                FileType::class,
                [
                    'label' => null,
                    'required' => true,
                    'constraints' => [
                        new File([
                            'mimeTypes' => [
                                'application/vnd.ms-excel',
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                'application/vnd.oasis.opendocument.spreadsheet'
                            ],
                            'mimeTypesMessage' => 'Por favor, seleccione un archivo de excel válido (.xls, .xlsx)',
                        ])
                    ],
                ]
            )
            ->add('Subir', SubmitType::class);
    }
}
