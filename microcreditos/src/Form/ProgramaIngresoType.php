<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class ProgramaIngresoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
        {
        $builder
            ->add('monto','text',array('label'=>'Monto:'))
            ->add('fecha_ingreso','date',array('widget' => 'single_text',
                'format'=>'dd-MM-yyyy','label'=>'Fecha Ingreso:'))
        ;
    }
    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Microcreditos\MicrocreditosBundle\Entity\ProgramaIngreso',
        );
    }
    public function getName()
    {
        return 'microcreditos_microcreditosbundle_programaingresotype';
    }
}
