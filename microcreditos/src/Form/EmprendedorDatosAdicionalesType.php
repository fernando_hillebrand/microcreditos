<?php

namespace App\Form;

use App\Entity\EmprendedorDatosAdicionales;
use App\Entity\Parametro;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmprendedorDatosAdicionalesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        /* Datos de Negocio */
            ->add('actividad',null,array('label'=>'Actividad Principal:'))
            ->add('experiencia',null,array('label'=>'Experiencia:'))
         /*   ->add('fecha_inicio_actividad','date',array('widget' => 'single_text',
                'format'=>'dd-MM-yyyy','label'=>'Fecha Inicio Actividad:','required'=>false))*/

            ->add('tipo_actividad',EntityType::class,array(
                    'label'=>'Tipo de Actividad:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                        $tipo_actividad = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'tipo-actividad')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$tipo_actividad);  
                    },))                             
            ->add('lugar_actividad',EntityType::class,array(
                    'label'=>'Su actividad se realiza en:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                        $lugar_actividad = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'lugar-actividad')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$lugar_actividad);  
                    },))                             

            ->add('dia_hora_atencion',null,array('label'=>'Días y Horario de Atención:','required'=>false))    
            ->add('ventas_semanal',null,array('label'=>'Ventas Semanal:','required'=>false))    
       /*     ->add('nro_empleados',null,array('label'=>'N° de Empleados:','required'=>false))  */
                            
            ->add('tipo_inmueble_negocio',EntityType::class,array(
                    'label'=>'Inmueble es:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                        $tipo_inmueble_negocio = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'vivienda')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$tipo_inmueble_negocio);  
                    },))                             
                            
            ->add('telefono_negocio',null,array('label'=>'Teléfonos:','required'=>false))
            ->add('direccion_negocio',TextType::class,array('label'=>'Dirección:','required'=>false))
            ->add('ubicacion_negocio',null,array('label'=>'Ubicación:','required'=>false))
            ->add('zona_negocio',null,array('label'=>'Zona:','required'=>false))
            ->add('actividad_secundaria',null,array('label'=>'Actividad Secundaria:','required'=>false))
            ->add('trabajos_anteriores',null,array('label'=>'Trabajos Anteriores:','required'=>false))
                            
          /* Referencia Comercial */                  
          /* Datos del Conyuge */  
            ->add('nombres_conyuge',TextType::class,array('label'=>'Nombres:','required'=>false))
            ->add('apellido_conyuge',TextType::class,array('label'=>'Apellido:','required'=>false))
           // ->add('sobrenombre_conyuge','text',array('label'=>'Conocido/a como:','required'=>false))
            ->add('dni_conyuge',null,array('label'=>'DNI:','required'=>false))    
            ->add('fecha_nacimiento_conyuge',DateType::class,array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha de Nacimiento:','required'=>false))
            ->add('actividad_conyuge',null,array('label'=>'Actividad:','required'=>false))
            ->add('lugar_nacimiento_conyuge',null,array('label'=>'Lugar de Nacimiento:','required'=>false))
            ->add('nivel_educativo_conyuge',EntityType::class,array(
                    'label'=>'Nivel Educativo:',
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                        $nivel_educativo = $em->createQueryBuilder('g')->select('g.id')
                                           ->where('g.boleano=0 and  g.descripcion = ?1 ')
                                           ->setParameter(1,'educacion')
                                           ->getQuery()->getSingleScalarResult();
                         return $em->createQueryBuilder('p')
                                ->where('p.boleano=0 and  p.agrupador_id = ?1 ')
                                ->setParameter(1,$nivel_educativo);  
                    },))               
    
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmprendedorDatosAdicionales::class,
        ]);
    }
    
    public function getName()
    {
        return 'microcreditos_microcreditosbundle_emprendedordatosadicionalestype';
    }
}
