<?php

namespace App\Form;

use App\Entity\Parametro;
use App\Entity\Programa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class FeriadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('concepto',TextType::class, array('label'=>'Concepto:'))
            ->add('fecha',DateType::class,array('widget' => 'single_text','format'=>'dd-MM-yyyy','label'=>'Fecha:'))
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_feriadotype';
    }
}

