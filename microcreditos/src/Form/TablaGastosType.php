<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TablaGastosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('periodo',ChoiceType::class, array('label'=>'Periodo:',
                  'choices'   => array('7' => '7 días', '14' => '14 días', '28' => '28 días')))
            ->add('desde',IntegerType::class, array('label'=>'Importe desde:'))
            ->add('hasta',IntegerType::class, array('label'=>'Importe hasta:'))
            ->add('valor',NumberType::class, array('label'=>'Monto:'))
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_tablagastostype';
    }
}
