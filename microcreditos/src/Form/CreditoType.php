<?php

namespace App\Form;


use App\Entity\Grupo;
use App\Entity\Programa;
use App\Entity\User;
use App\Form\Type\GroupSelectorType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use App\Entity\UserRepository;
use Symfony\Component\Form\FormBuilderInterface;

class CreditoType extends AbstractType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['data']->getNroRenovacion() == 0) {
            $builder
                    ->add('nro_credito', IntegerType::class, array('label' => 'Crédito N°:', 'required' => false))
                    ->add('nro_renovacion', IntegerType::class, array('label' => 'Renovación N°:', 'required' => false))
                    ->add('nro_carpeta', IntegerType::class, array('label' => 'Carpeta N°:', 'required' => false))
                    ->add('fecha_credito', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha:', 'required' => false))
                    ->add('fecha_vto_cuota1', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha 1° Vencimiento:', 'required' => true))
                    ->add('fecha_entrega', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha Entrega:'))
                    ->add('monto_credito', IntegerType::class, array('label' => 'Monto total:', 'required' => true))
                    ->add('cant_cuotas', IntegerType::class, array('label' => 'Cantidad de Cuotas:', 'required' => true))
                    ->add('periodo', IntegerType::class, array('label' => 'Periodicidad de Pago:', 'required' => true))
                    ->add('programa', EntityType::class,
                        array(  'label' => 'Programa de Fondos:',
                            'required' => true,
                            'class' => Programa::class,
                            'query_builder' => function(EntityRepository $em) {
                                return $em->createQueryBuilder('u')
                                    ->where('u.activo = 1')
                                    ->where('u.habilitadoNuevoCredito = 1')
                                    ->orderBy('u.nombre', 'ASC');
                                }
                        )
                    )
                    ->add('grupo', EntityType::class,
                        array(  'label' => 'Nombre del Grupo',
                            'required' => false,
                            'class' => Grupo::class,
                        )
                    )

                    ->add('porc_interes', TextType::class, array('label' => 'Interés:', 'required' => false))
                    ->add('porc_punitorio', TextType::class, array('label' => 'Punitorio por Mora:', 'required' => FALSE))
                    ->add('gasto_administrativo', TextType::class, array('label' => 'Gasto Administrativo:', 'required' => FALSE))
                    ->add('sin_intereses',CheckboxType::class,array('label'=>'Sin Intereses:','required'=>false))
                    ->add('sin_gastos',CheckboxType::class,array('label'=>'Sin Gastos:','required'=>false))

                    ->add('asesor',EntityType::class,array(
                            'class' => User::class,
                            'query_builder' => function(EntityRepository $em) {
                                return $em->createQueryBuilder('u')->where('u.id>1')
                                        ->orderBy('u.username', 'ASC'); },'label'=>'Asesor:'))
            ;
        } else {
            $programaId = $options['data']->getPrograma()->getId();
            $builder
                    ->add('nro_credito', IntegerType::class, array('label' => 'Crédito N°:', 'required' => false))
                    ->add('nro_renovacion', IntegerType::class, array('label' => 'Renovación N°:', 'required' => false))
                    ->add('nro_carpeta', IntegerType::class, array('label' => 'Carpeta N°:', 'required' => false))
                    ->add('fecha_credito', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha:', 'required' => true))
                    ->add('fecha_vto_cuota1', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha 1° Vencimiento:', 'required' => true))
                    ->add('fecha_entrega', DateType::class, array('widget' => 'single_text', 'format' => 'dd-MM-yyyy', 'label' => 'Fecha Entrega:', 'required' => false))
                    ->add('monto_credito', IntegerType::class, array('label' => 'Monto total:', 'required' => true))
                    ->add('cant_cuotas', IntegerType::class, array('label' => 'Cantidad de Cuotas:', 'required' => true))
                    ->add('periodo', IntegerType::class, array('label' => 'Periodicidad de Pago:', 'required' => true))
                    ->add('programa', EntityType::class,
                        array(  'label' => 'Programa de Fondos:',
                            'required' => true,
                            'class' => Programa::class,
                            'query_builder' => function(EntityRepository $em) use($programaId) {
                                return $em->createQueryBuilder('u')
                                    ->where('u.activo = 1')
                                    ->where('u.habilitadoNuevoCredito = 1')
                                    ->where('u.id = '.$programaId)
                                    ->orderBy('u.nombre', 'ASC'); }
                        )
                    )
                    ->add('grupo', EntityType::class,
                        array(  'label' => 'Nombre del Grupo',
                            'required' => true,
                            'class' => Grupo::class,
                        )
                    )
                    ->add('porc_interes', TextType::class, array('label' => 'Interés:', 'required' => false))
                    ->add('porc_punitorio', TextType::class, array('label' => 'Punitorio por Mora:', 'required' => FALSE))
                    ->add('gasto_administrativo', TextType::class, array('label' => 'Gasto Administrativo:', 'required' => FALSE))
                    ->add('sin_intereses',CheckboxType::class,array('label'=>'Sin Intereses:','required'=>false))
                    ->add('sin_gastos',CheckboxType::class,array('label'=>'Sin Gastos:','required'=>false))
                    ->add('asesor',EntityType::class,array(
                                    'class' => User::class,
                                    'query_builder' => function(EntityRepository $em) {
                                        return $em->createQueryBuilder('u')->where('u.id>1')
                                                ->orderBy('u.username', 'ASC'); },'label'=>'Asesor:')
                    )
            ;
        }
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_creditotype';
    }
}
