<?php

namespace App\Form;

use App\Entity\Emprendedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GrupoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo',TextType::class,array('label'=>'Código:'))
            ->add('nombre',TextType::class,array('label'=>'Nombre:'))
            ->add('activo',CheckboxType::class,array('label'=>'Activo:','required'=>false))
            ->add('programa',null,array('label'=>'Programa de Fondos:'))
//            ->add('recomendador','emprendedor_selector',array('label'=>'RECOMENDADO POR:','required'=>false))
            ->add('recomendador', EntityType::class,
                array(  'label' => 'RECOMENDADO POR:',
                    'required' => false,
                    'class' => Emprendedor::class,
                )
            )
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_grupotype';
    }
}
