<?php

namespace App\Form;

use App\Entity\Parametro;
use Symfony\Component\Form\AbstractType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

class ConceptosCajaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class)
            ->add('activo', CheckboxType::class)
            ->add('numerico',HiddenType::class)
            ->add('agrupador',EntityType::class,
                array(
                    'class' => Parametro::class,
                    'query_builder' => function(\App\Repository\ParametroRepository $em) {
                        return $em->createQueryBuilder('d')->where('d.descripcion='."'conceptos-caja'")
                                ->orderBy('d.nombre', 'ASC'); },
                    )
                )
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_conceptoscajatype';
    }
}
