<?php

namespace App\Form;

use App\Entity\User;
use App\Service\RolesHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UsuarioType extends AbstractType {

    /**
     * @var RolesHelper
     */
    private $roles;

    /**
     * @param string $class The User class name
     * @param RolesHelper $roles Array or roles.
     */
    public function __construct(RolesHelper $roles)
    {
        $this->roles = $roles;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('email', EmailType::class, array('required'=>true))
            ->add('username', TextType::class, array('required'=>true))
            ->add('nombre', TextType::class, array('required'=>true))
            ->add('apellido', TextType::class, array('required'=>true))
            ->add('roles', ChoiceType::class, array(
                'choices' =>  $this->roles->getRoles(),
                'required' => false,
                'multiple'=>true
            ))
        ;
    }
    
    public function getDefaultOptions(array $options) {
        return array(
            'data_class' => User::class,
        );
    }

    public function getName() {
        return 'microcreditos_microcreditosbundle_usuariotype';
    }

}
