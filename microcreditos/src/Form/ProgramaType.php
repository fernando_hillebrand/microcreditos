<?php

namespace App\Form;

use App\Entity\Programa;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ProgramaType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('nombre',TextType::class,array('label'=>'Nombre:'))
            ->add('sigla',TextType::class,array('label'=>'Siglas:'))
            ->add('porc_interes',TextType::class,array('label'=>'Interés:'))
            ->add('porc_punitorio',TextType::class,array('label'=>'Punitorio por Mora: '))
            ->add('monto_punitorio',TextType::class,array('label'=>'Monto fijo '))
            ->add('porc_gastos_adm',TextType::class,array('label'=>'Porcentaje de Gastos Administrativos:'))
            ->add('caja',TextType::class,array('label'=>'Ultimo Saldo en Caja:'))
            ->add('banco',TextType::class,array('label'=>'Ultimo Saldo en Banco:'))
            ->add('individual',CheckboxType::class,array('label'=>'Individual:','required'=>false))
            ->add('activo',CheckboxType::class,array('label'=>'Activo:','required'=>false))
            ->add('mes',HiddenType::class,['data' => date('m')])
            ->add('anio',HiddenType::class,['data' => date('Y')])

            ->add('habilitadoNuevoCredito',CheckboxType::class,array('label'=>'Habilitado para generar Nuevos creditos:','required'=>false))
            ->add('grupoSolidario',CheckboxType::class,array('label'=>'Grupos Solidarios','required'=>false))
            ->add('utilizaCaja',CheckboxType::class,array('label'=>'Utiliza Fondos Propios:','required'=>false))
            ->add('soloEmpleados',CheckboxType::class,array('label'=>'Solo para Empleados:','required'=>false))
            ->add('montoTope',TextType::class,array('label'=>'Monto tope por Integrante:','required'=>false))
            ->add('cuotaTopeMensual',TextType::class,array('label'=>'Cantidad maxima de Cuotas Mensuales:','required'=>false))
            ->add('cuotasMensuales',CheckboxType::class,array('label'=>'Permite generar Cuotas Mensuales:','required'=>false))
            ->add('cuotaTopeSemanal',TextType::class,array('label'=>'Cantidad maxima de Cuotas Semanales','required'=>false))
            ->add('cuotasSemanal',CheckboxType::class,array('label'=>'Permite generar Cuotas Semanales:','required'=>false))
        ;

    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_programatype';
    }


}
