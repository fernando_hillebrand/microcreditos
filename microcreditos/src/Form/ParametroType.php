<?php

namespace App\Form;

use App\Entity\Parametro;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Microcreditos\MicrocreditosBundle\Entity\ParametroRepository;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ParametroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class)
            ->add('activo',CheckboxType::class)
            ->add('agrupador',EntityType::class,array(
                'label'=>'Concepto:',
                'class' => Parametro::class,
                'query_builder' => function(\App\Repository\ParametroRepository $em)
                    {
                    return $em->createQueryBuilder('d')->where('d.agrupador_id is null and d.boleano=0')
                        ->orderBy('d.nombre', 'ASC');
                    },
                ))
        ;
    }

    public function getName()
    {
        return 'microcreditos_microcreditosbundle_parametrotype';
    }
}
