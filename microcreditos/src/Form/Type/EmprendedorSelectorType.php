<?php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Microcreditos\MicrocreditosBundle\Form\DataTransformer\EmprendedorToNumberTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormBuilderInterface;

class EmprendedorSelectorType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

//    /**
//     * @param ObjectManager $om
//     */
//    public function __construct(ObjectManager $om)
//    {
//        $this->om = $om;
//    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EmprendedorToNumberTransformer($this->om);
        $builder->prependNormTransformer($transformer);
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'invalid_message' => 'The selected issue does not exist',
        );
    }

//    public function getParent(array $options)
//    {
//        return 'hidden';
//    }

    public function getName()
    {
        return 'emprendedor_selector';
    }
}