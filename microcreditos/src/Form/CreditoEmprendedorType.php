<?php

namespace App\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class CreditoEmprendedorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('credito',HiddenType::class)
            ->add('emprendedor','emprendedor_selector',array('label'=>'Emprendedor:','required'=>true))
            ->add('monto_credito',TextType::class,array('label'=>'Monto solicitado:'))
            ->add('gasto_administrativo','text',array('label'=>'Gasto Administrativo:'))
            ->add('coordinador',null,array('label'=>'Es coordinador:','required'=>false))
        ;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'App\Entity\CreditoEmprendedor',
        );
    }
    
    public function getName()
    {
        return 'microcreditos_microcreditosbundle_creditoemprendedortype';
    }
}
