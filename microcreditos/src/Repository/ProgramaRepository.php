<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

class ProgramaRepository extends EntityRepository
{
  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL() {
    $query = $this->_em->createQuery('Select p from App\Entity\Programa p' );
    return $query;
  }
  
  public function findByActivosDQL() {
    $query = $this->_em->createQuery('Select p from App\Entity\Programa p
        where p.activo=1' );
    return $query->getResult();
  }
  
  public function findArray($id){
      $query = $this->_em->createQuery('Select p from App\Entity\Programa p
          where p.id='.$id);
      return $query->getOneOrNullResult( 2 );
  }
  
  public function findUltPeriodo(){
     $queryAnio = $this->_em->createQuery('select MAX(p2.anio) from App\Entity\Programa p2 ');
     $query = $this->_em->createQueryBuilder();
     $query->select('MAX(p.mes) as mes,p.anio')
            ->from('App\Entity\Programa', 'p')
            ->where($query->expr()->In(' p.anio ', $queryAnio->getDQL()))
            ->groupBy('p.anio');
   // var_dump($query->getQuery()->getArrayResult());die;
     return $query->getQuery()->getSingleResult();
  }
  
  public function setTasas($int,$porc,$monto){
      $qb = $this->_em->createQueryBuilder() 
              ->update('App\Entity\Programa', 'p')
              ->set('p.porcInteres', $int)
              ->set('p.porcPunitorio', $porc)
              ->set('p.montoPunitorio', $monto);
      $res = $qb->getQuery()->execute();
      return $res;
  }
}
?>
