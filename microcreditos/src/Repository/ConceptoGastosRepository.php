<?php

namespace App\Repository;

use App\Entity\ConceptoGastos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConceptoGastos>
 *
 * @method ConceptoGastos|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConceptoGastos|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConceptoGastos[]    findAll()
 * @method ConceptoGastos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConceptoGastosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConceptoGastos::class);
    }


    public function getAll() {
   


        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\ConceptoGastos', 'c');
                
        $query->select('c.id, c.nombre');
        
        
        
         return $query->getQuery()->getResult();
      }


    

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ConceptoGastos $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ConceptoGastos $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return ConceptoGastos[] Returns an array of ConceptoGastos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConceptoGastos
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
