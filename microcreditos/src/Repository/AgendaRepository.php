<?php

namespace App\Repository;

use App\Controller\DefaultController;
use Doctrine\ORM\EntityRepository;

class AgendaRepository extends EntityRepository
{
   public function findTurnosByFiltro($asesor,$ini,$fin){
        $query = $this->_em->createQueryBuilder();
        $query->select('a')
              ->from('App\Entity\Agenda', 'a');
        if($ini){
          $cadena = " a.fechaInicio >= '".DefaultController::toAnsiDate($ini)." 00:00'";
          $query->andWhere($cadena);
      }
      if($fin){
          $cadena = " a.fechaInicio <= '".DefaultController::toAnsiDate($fin)." 23:59'";
          $query->andWhere($cadena);
      }
      if($asesor){
          $cadena = " a.asesor = ".$asesor;
          $query->andWhere($cadena);
      }
        return $query->getQuery()->getResult();
    }
    public function deleteTurno($id){
        $query = $this->_em->createQuery('DELETE App\Entity\Agenda a WHERE a.id = '.$id);
        $query->execute(); 
    }
    public function findByCriteria($id,$campo){
        $query = $this->_em->createQueryBuilder();
        $query->select('a')
              ->from('App\Entity\Agenda', 'a')
              ->where('a.'.$campo.'='.$id)
              ->orderBy('a.fechaInicio','DESC')  ;
        return $query->getQuery()->getResult();
    }
}
?>
