<?php

namespace App\Repository;

use App\Controller\DefaultController;
use App\Entity\PagoBancario;
use Doctrine\ORM\EntityRepository;

class PagoBancarioRepository extends EntityRepository
{

  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL($filtro=null, $tipoMovimiento = null) {
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
            ->from('App\Entity\PagoBancario','g')
              ->leftJoin('g.concepto', 'co')
              ->leftJoin('g.programa', 'p')
            ->addOrderBy('g.id', 'DESC');

    if($tipoMovimiento){
        if($tipoMovimiento == 'ingreso'){
            $cadena = " g.ingreso > 0 ";
            $query->andWhere($cadena);
        }
    }
    if(isset($filtro['desde']) AND $filtro['desde'] != '' ){
      $cadena = " g.fecha >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['hasta']) AND $filtro['hasta'] != '' ){
      $cadena = " g.fecha <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['concepto']) AND $filtro['concepto'] > 0 ){
      $cadena = " co.id = '".$filtro['concepto']."'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['programa']) AND $filtro['programa'] > 0 ){
      $cadena = " p.id = '".$filtro['programa']."'";
      $query->andWhere($cadena);
    }

    return $query->getQuery();
  }

  public function getByPago($pagoId){
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
          ->from('App\Entity\PagoBancario','g')
          ->where('g.pago = '.$pagoId)
          ->addOrderBy('g.id', 'DESC');
      return $query->getQuery()->getResult();
  }

  public function obtenerTotalIngresos($anio,$mes){
      $fechaUltimoDia = new \DateTime($anio .'-'. $mes .'-'. 1);
      $fechaPrimerDia = new \DateTime($anio .'-'. $mes .'-'. 1);
      $fechaUltimoDia->modify('last day of this month');

      $query = $this->_em->createQueryBuilder();

      $query->select('sum(g.ingreso) AS cantidad')
          ->from('App\Entity\PagoBancario','g')
          ->leftJoin('g.concepto', 'co')
          ->leftJoin('g.programa', 'p')
          ->add('where',
                       '('.
                          $query->expr()->between(
                              'g.fecha',
                              ':fechaInicio',
                              ':fechaFin'
                          ).
                      ') AND (
                          g.concepto = 210 
                      ) AND g.ingreso > 0'
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->addOrderBy('g.id', 'DESC');

      return $query->getQuery()->getResult();
  }


  public function obtenerTotalPagos($filtro){
    $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
    $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
    $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
    $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

    $query = $this->_em->createQueryBuilder();

    $query->select(
            'sum(pago.capital) AS cantidad_capital',
            'sum(pago.intereses) AS cantidad_intereses',
            'sum(pago.refinPunitorios) AS cantidad_refinPunitorios',
            'sum(pago.punitorios) AS cantidad_punitorios',
            'sum(pago.gastoAdministrativo) AS cantidad_gastoAdministrativo'
        )
        ->from('App\Entity\PagoBancario','g')
        ->innerJoin('g.pago', 'pago')
        ->add('where',
            '('.
            $query->expr()->between(
                'g.fecha',
                ':fechaInicio',
                ':fechaFin'
            ).
            ') AND (
                      g.concepto = 210
                  ) AND g.ingreso > 0'
        )
        ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
        ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
        ->addOrderBy('g.id', 'DESC');
        return $query->getQuery()->getResult();
    }

  public function guardarNuevoIngreso($ingreso,$descripcion,$userId,$credito=null,$pago=null,$concepto=null){
        $fecha = new \DateTime();
        $usuario = $this->_em->getRepository('App:User')->find(
            $userId
        );

        $pagoBancario = new PagoBancario();
        $pagoBancario->setIngreso($ingreso);
        $pagoBancario->setFechaCreacion($fecha);
        $pagoBancario->setFechaActualizacion($fecha);
        $pagoBancario->setUsuario($usuario);
        $pagoBancario->setCreadoPor($usuario);
        $pagoBancario->setActualizadoPor($usuario);
        $pagoBancario->setObservacion($descripcion);

        if(!is_null($credito)){
            $pagoBancario->setCredito($credito);
            $pagoBancario->setPrograma($credito->getPrograma());
        }

        if(!is_null($pago)){
            $pagoBancario->setPago($pago);
        }

        if(!is_null($concepto)){
            $pagoBancario->setConcepto($concepto);
        }

        $this->_em->persist($pagoBancario);
        $this->_em->flush();
  }

}
?>
