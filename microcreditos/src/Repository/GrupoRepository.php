<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;
use App\Controller\DefaultController;

class GrupoRepository extends EntityRepository
{
  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL() {
    $query = $this->_em->createQuery('Select g from Grupo g' );
    return $query;
  }
  
  public function findAllByTerm($term,$programa=null){
      $sqlPrograma = '';
      if($programa){
          $sqlPrograma = ' and p.id = '.$programa->getId(); 
      }
      $query = $this->_em->createQuery("Select g.id,g.nombre as label from App\Entity\Grupo g
        LEFT OUTER JOIN g.programa p   
        where g.nombre like '%".$term."%'".$sqlPrograma." and g.activo=1 order by g.nombre");
      return $query->getScalarResult();
  }
  
  public function findByMyFiltroDQL($filtro=null) {
      $DQLFiltro = $this->armarFiltroDQL($filtro);
      $DQL = 'Select g from App\Entity\Grupo g LEFT OUTER JOIN g.programa p ';
      $DQLOrder = ' ORDER BY g.codigo ';
      $DQLFiltro = ($DQLFiltro=='') ? '' : ' WHERE '.$DQLFiltro;
      
      $query = $this->_em->createQuery( $DQL.' '.$DQLFiltro.' '.$DQLOrder );

      return $query ;
  } 
  
  protected function armarFiltroDQL($filtro){
      $cadenaFiltro = '';
      if($filtro['desde']){
          $cadenaFiltro = " g.fechaAlta >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
      }
      if($filtro['hasta']){
          $cadena = " g.fechaAlta <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
          $cadenaFiltro = ($cadenaFiltro=='') ? $cadena : $cadenaFiltro.' AND '.$cadena;
      }
      if($filtro['programa']){
          $cadena = " p.id = '".$filtro['programa']."'";
          $cadenaFiltro = ($cadenaFiltro=='') ? $cadena : $cadenaFiltro.' AND '.$cadena;
      }
      if($filtro['grupoId']){
          $cadena = ' g.id = '.$filtro['grupoId'];
          $cadenaFiltro = ($cadenaFiltro=='') ? $cadena : $cadenaFiltro.' AND '.$cadena;
      }
      return $cadenaFiltro;
  }
    
}
?>