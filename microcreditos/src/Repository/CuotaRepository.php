<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Controller\DefaultController;

class CuotaRepository extends EntityRepository
{
  public function findCuotasByCredito($credito)
  {
    $query = $this->_em->createQueryBuilder();
    $query->select('cu')
      ->from('App\Entity\Cuota', 'cu')
      ->innerJoin('cu.credito', 'c')
      ->leftJoin('cu.pagos', 'p')
      ->where('c.id=' . $credito);
    return $query->getQuery()->getResult();
  }

  public function findCuotaByNro($credito, $nro)
  {
    $query = $this->_em->createQueryBuilder();
    $query->select('cu.nroCuota,cu.montoCapital,cu.montoIntereses, cu.gastoAdministrativo, cu.montoTotal,
                        cu.fechaVencimiento, cu.montoPunitorios, cu.fechaCancelacion, cu.cancelado, cu.pagado,cu.saldo')
      ->from('App\Entity\Cuota', 'cu')
      ->innerJoin('cu.credito', 'c')
      ->where('c.id=' . $credito)
      ->andWhere(' cu.nroCuota= ' . $nro);
    return $query->getQuery()->getOneOrNullResult();
  }

  public function findByCreditoCuota($credito, $nro)
  {
    $query = $this->_em->createQueryBuilder();
    $query->select('cu')
      ->from('App\Entity\Cuota', 'cu')
      ->innerJoin('cu.credito', 'c')
      ->where('c.id=' . $credito)
      ->andWhere(' cu.id= ' . $nro);
    return $query->getQuery()->getOneOrNullResult();
  }

  public function getVenceEstaSemana()
  {
    $hoy = date('w', strtotime("today"));
    $viernes = date("Y-m-d", strtotime("next Friday"));
    if ($hoy == 1)
      $lunes = date('Y-m-d');
    elseif ($hoy > 5)
      $lunes = date("Y-m-d", strtotime("next Monday"));
    else
      $lunes = date("Y-m-d", strtotime("last Monday"));
    /* $lunes = ($hoy==1) ? $lunes = date('Y-m-d') : 
              ($hoy >5) ?  $lunes = date("Y-m-d", strtotime ("next Monday")) 
                      : $lunes = date("Y-m-d", strtotime ("last Monday"));*/
    /*   if( $hoy == 1 )  $lunes = date('Y-m-d'); 
       else {
          if (($hoy>5 )) $lunes = date("Y-m-d", strtotime ("next Monday")); 
          else $lunes = date("Y-m-d", strtotime ("last Monday"));
       }*/

    $query = $this->_em->createQueryBuilder();
    $query->select('c.id as credId, cu.nroCuota, cu.montoTotal,cu.fechaVencimiento')
      ->from('App\Entity\Cuota', 'cu')
      ->innerJoin('cu.credito', 'c')
      ->where('c.cancelado=0')
      ->andWhere("cu.cancelado=0")
      ->andWhere("cu.fechaVencimiento between '" . $lunes . "' and '" . $viernes . "' ");
    return $query->getQuery()->getResult();
  }

  public function delCuotasByCredito($credito)
  {
    $qb = $this->_em->createQueryBuilder()
      ->delete('App\Entity\Cuota', 'cu')
      ->where('cu.credito = ' . $credito->getId());
    $res = $qb->getQuery()->execute();
    return $res;
  }

  public function delCuotasImpagasByCredito($credito)
  {
    $qb = $this->_em->createQueryBuilder()
      ->delete('App\Entity\Cuota', 'cu')
      ->where('cu.saldo>0 and cu.pagado=0 and cu.credito = ' . $credito->getId());
    $res = $qb->getQuery()->execute();
    return $res;
  }

  public function getCuotaCapitalMensual($filtro = null)
  {

    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c');
    if (isset($filtro['desde']) and $filtro['desde'] != '') {
      $cadena = " c.fechaCancelacion >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
      $query->andWhere($cadena);
    }
    if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
      $cadena = " c.fechaCancelacion <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
      $query->andWhere($cadena);
    }
    $query->select('SUM(c.montoCapital) as capital', 'MONTH(c.fechaCancelacion) as mes', 'YEAR(c.fechaCancelacion) as year')
      ->groupBy('year', 'mes')
      ->orderBy('mes', 'ASC')
      ->orderBy('year', 'ASC');


    return $query->getQuery()->getResult();
  }

  public function getCuotaInteresMensual($filtro = null)
  {

    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c');
    if (isset($filtro['desde']) and $filtro['desde'] != '') {
      $cadena = " c.fechaCancelacion >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
      $query->andWhere($cadena);
    }
    if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
      $cadena = " c.fechaCancelacion <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
      $query->andWhere($cadena);
    }
    $query->select('SUM(c.montoIntereses) as interes', 'MONTH(c.fechaCancelacion) as mes', 'YEAR(c.fechaCancelacion) as year')
      ->groupBy('year', 'mes')
      ->orderBy('mes', 'ASC')
      ->orderBy('year', 'ASC');


    return $query->getQuery()->getResult();
  }

  public function getCuotaTotalMensual($filtro = null)
  {

    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c');
    if (isset($filtro['desde']) and $filtro['desde'] != '') {
      $cadena = " c.fechaCancelacion >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
      $query->andWhere($cadena);
    }
    if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
      $cadena = " c.fechaCancelacion <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
      $query->andWhere($cadena);
    }
    $query->select('SUM(c.montoTotal) as total', 'MONTH(c.fechaCancelacion) as mes', 'YEAR(c.fechaCancelacion) as year')
      ->groupBy('year', 'mes')
      ->orderBy('mes', 'ASC')
      ->orderBy('year', 'ASC');


    return $query->getQuery()->getResult();
  }

  public function getCuotaCantidadMensual($filtro = null)
  {

    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c');
    if (isset($filtro['desde']) and $filtro['desde'] != '') {
      $cadena = " c.fechaCancelacion >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
      $query->andWhere($cadena);
    }
    if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
      $cadena = " c.fechaCancelacion <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
      $query->andWhere($cadena);
    }
    $query->select('COUNT(c) as cantidad', 'MONTH(c.fechaCancelacion) as mes', 'YEAR(c.fechaCancelacion) as year')
      ->groupBy('year', 'mes')
      ->orderBy('mes', 'ASC')
      ->orderBy('year', 'ASC');


    return $query->getQuery()->getResult();
  }

  public function getEstadoResultadosCuota($month, $year)
  {



    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c')
      ->select('SUM(c.montoTotal) as total', 'SUM(c.montoIntereses) as intereses', 'SUM(c.montoPunitorios) as punitorios', 'SUM(c.gastoAdministrativo) as gastoAdmin', 'MONTH(c.fechaCancelacion) as mes', 'YEAR(c.fechaCancelacion) as year')
      ->Where(' c.cancelado = 1')
      ->andWhere('MONTH(c.fechaCancelacion)=' . $month . '')
      ->andWhere('YEAR(c.fechaCancelacion) = ' . $year . '')
      ->groupBy('year', 'mes');


    return $query->getQuery()->getResult();
  }

  public function getCapitalCuotas($fecha)
  {



    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c')
      ->select('SUM(c.montoCapital) as capital');
    //->Where(' c.cancelado = 1');
    $cadena = "c.fechaCancelacion <='" . DefaultController::toAnsiDate($fecha) . " 23:59'";
    $query->where($cadena);


    return $query->getQuery()->getResult();
  }

  public function getInteresesCobrar($fecha)
  {


    $query = $this->_em->createQueryBuilder();
    $query->from('App\Entity\Cuota', 'c')
      ->select('SUM(c.montoIntereses) as intereses')
      ->Where(' c.cancelado = 0');
    $cadena = "c.fechaVencimiento <='" . DefaultController::toAnsiDate($fecha) . " 23:59'";
    $query->andWhere($cadena);


    return $query->getQuery()->getResult();
  }

  public function getCuotasByCuit($emprendedorCuit)
  {
    /* 
      SELECT c.nro_cuota, c.saldo, c.pagado, c.cancelado, e.nombres, c2.id  
        FROM emprendedor e 
          INNER JOIN credito_emprendedor ce ON e.id = ce.emprendedor_id 
          INNER JOIN credito c2 ON ce.credito_id = c2.id 
          INNER JOIN cuota c ON c2.id = c.credito_id 
        WHERE e.cuit = 27364116492 and c2.activo = 1 and c.pagado = 0; 
    */

    $queryBuilder = $this->_em->createQueryBuilder();
    $queryBuilder
      ->select(
        'c.id',
        'IDENTITY(c.credito) AS credito',
        'c.nroCuota',
        'c.fechaVencimiento',
        'c.montoCapital',
        'c.montoIntereses',
        'c.gastoAdministrativo',
        'c.montoTotal',
        'c.montoPunitorios',
        'c.saldo'
      )
      ->from('App\Entity\Emprendedor', 'e')
      ->innerJoin('e.creditos', 'ce')
      ->innerJoin('ce.credito', 'c2')
      ->innerJoin('c2.cuotas', 'c')
      ->where($queryBuilder->expr()->eq('e.cuit', ':cuit'))
      ->andWhere($queryBuilder->expr()->eq('c2.activo', ':activo'))
      ->andWhere($queryBuilder->expr()->neq('c.saldo', '0'))
      ->setParameters([
        'cuit' => $emprendedorCuit,
        'activo' => 1,
      ]);

    $cuotas = $queryBuilder->getQuery()->getResult();


    return $cuotas;

  }






}
?>

