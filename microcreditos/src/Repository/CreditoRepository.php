<?php

namespace App\Repository;

use App\Entity\Emprendedor;
use App\Entity\GruposEmprendedores;
use Doctrine\ORM\EntityRepository;
use App\Controller\DefaultController;

class CreditoRepository extends EntityRepository
{

    public function findByMyFiltroDQL($filtro = null, $tipo = 0)
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
          where c2.nroCredito=c.nroCredito and c2.aprobado=' . $tipo);
        /* $queryCuota = $this->_em->createQuery('select MIN(cu.nroCuota) as cuota from App\Entity\Cuota cu
             INNER JOIN cu.creditoEmprendedor cre INNER JOIN cre.credito cr2  where cu.cancelado=0 and cr2.id=c.id ');*/
        $query->select('c')
            // ->addSelect('p')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->andWhere(' c.aprobado=' . $tipo)
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->orderBy('c.fechaEntrega', 'DESC');
        if ($filtro['desde']) {
            $cadena = " c.fechaEntrega >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
            $query->andWhere($cadena);
        }
        if ($filtro['hasta']) {
            $cadena = " c.fechaEntrega <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $query->andWhere($cadena);
        }
        if ($filtro['programa']) {
            $cadena = " p.id = '" . $filtro['programa'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['asesor']) {
            $cadena = " u.id = '" . $filtro['asesor'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['grupoId']) {
            $cadena = ' g.id = ' . $filtro['grupoId'];
            $query->andWhere($cadena);
        }
        if ($filtro['emprendedorId']) {
            $queryEmprendedor = $this->_em->createQuery('select cr.id from App:CreditoEmprendedor ce 
              INNER JOIN ce.emprendedor e INNER JOIN ce.credito cr where e.id =' . $filtro['emprendedorId']);
            $query->andWhere($query->expr()->In(' c.id ', $queryEmprendedor->getDQL()));
        }
        //var_dump($query->getQuery()->getArrayResult()); die;
        //return $query->getQuery();
        return $query->getQuery()->getResult();
    }

    public function findMorososDQL($filtro = null)
    {
        $query = $this->_em->createQueryBuilder();

        $queryRenovacion =
            $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App:Credito c2
          where c2.nroCredito=c.nroCredito ');

        $queryCuotas = $this->_em->createQuery("select 1 from App:Cuota cu
          where cu.credito=c.id and cu.saldo>0 and cu.fechaVencimiento<'" . date("Y-m-d") . "'");

        $query->select('c')
            ->from('App:Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->where('c.aprobado=1')
            ->andWhere('c.incobrable=0')
            //->andWhere('c.sinPunitorios=0')
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->andWhere($query->expr()->exists($queryCuotas->getDQL()))
            ->orderBy('c.fechaEntrega', 'DESC')
            ->orderBy('c.fechaAlta', 'DESC');
        if ($filtro['desde']) {
            $cadena = " c.fechaEntrega >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
            $query->andWhere($cadena);
        }
        if ($filtro['hasta']) {
            $cadena = " c.fechaEntrega <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $query->andWhere($cadena);
        }
        if ($filtro['programa']) {
            $cadena = " p.id = '" . $filtro['programa'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['asesor']) {
            $cadena = " u.id = '" . $filtro['asesor'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['grupoId']) {
            $cadena = ' g.id = ' . $filtro['grupoId'];
            $query->andWhere($cadena);
        }
        if ($filtro['emprendedorId']) {
            $queryEmprendedor = $this->_em->createQuery('select cr.id from App:CreditoEmprendedor ce 
              INNER JOIN ce.emprendedor e INNER JOIN ce.credito cr where e.id =' . $filtro['emprendedorId']);
            $query->andWhere($query->expr()->In(' c.id ', $queryEmprendedor->getDQL()));
        }

        return $query->getQuery();
    }

    public function findIncobrablesDQL($filtro = null)
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
          where c2.nroCredito=c.nroCredito ');
        $queryCuotas = $this->_em->createQuery("select 1 from App\Entity\Cuota cu
          where cu.credito=c.id and cu.saldo>0 and cu.fechaVencimiento<'" . date("Y-m-d") . "'");
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            //        ->andWhere('c.aprobado=1')
            ->where('c.incobrable=1');
        //         ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
        //         ->andWhere($query->expr()->exists($queryCuotas->getDQL()))
        //         ->orderBy('c.fechaEntrega', 'DESC')
        //         ->orderBy('c.fechaAlta', 'DESC');
        if ($filtro['desde']) {
            $cadena = " c.fechaIncobrable >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
            $query->andWhere($cadena);
        }
        if ($filtro['hasta']) {
            $cadena = " c.fechaIncobrable <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $query->andWhere($cadena);
        }
        if ($filtro['programa']) {
            $cadena = " p.id = '" . $filtro['programa'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['asesor']) {
            $cadena = " u.id = '" . $filtro['asesor'] . "'";
            $query->andWhere($cadena);
        }
        if ($filtro['grupoId']) {
            $cadena = ' g.id = ' . $filtro['grupoId'];
            $query->andWhere($cadena);
        }
        if ($filtro['emprendedorId']) {
            $queryEmprendedor = $this->_em->createQuery('select cr.id from App\Entity\CreditoEmprendedor ce 
              INNER JOIN ce.emprendedor e INNER JOIN ce.credito cr where e.id =' . $filtro['emprendedorId']);
            $query->andWhere($query->expr()->In(' c.id ', $queryEmprendedor->getDQL()));
        }
        return $query->getQuery();
    }

    /*public function getUltimaCuotaPaga($credito){
      $queryCuota = $this->_em->createQuery('select MIN(cu.nroCuota) as cuota from App\Entity\Cuota cu
          INNER JOIN cu.creditoEmprendedor cre INNER JOIN cre.credito cr2  where cu.cancelado=1 and cr2.id= '.$credito->getId());
      return $queryCuota->getSingleScalarResult();
  }*/

    public function findHistorico($id)
    {
        $nroCredito = $this->_em->createQuery('select c.nroCredito from App\Entity\Credito c
            where c.id=' . $id)->getSingleScalarResult();
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
              where c2.nroCredito=c.nroCredito and c2.aprobado=1');
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->andWhere(' c.aprobado=1')
            ->andWhere(' c.nroCredito=' . $nroCredito)
            ->andWhere($query->expr()->notIn(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->orderBy('c.nroRenovacion', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function getSolicitudes()
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
              where c2.nroCredito=c.nroCredito ');
        $ts = mktime(0, 0, 0, date("n") - 2, date("d"), date("Y"));
        $fecha = date("Y-m-d H:i:s", $ts);
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->andWhere(' c.aprobado=0')
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->andWhere('c.fechaEntrega >= :fecha')
            ->setParameter('fecha', new \DateTime($fecha), \Doctrine\DBAL\Types\Type::DATETIME)
            ->orderBy('c.fechaEntrega', 'ASC')
            ->orderBy('c.fechaAlta', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function getEntregados()
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2 where c2.nroCredito=c.nroCredito ');
    
        $semana = date("Y-m-d", (strtotime('today') - (86400 * 8)));
        $mañana = date("Y-m-d", (strtotime('today') + (86400 * 1)));
        $cadena = " c.fechaAprobacion <= '" . $mañana . "'";
        $cadena2 = " c.fechaAprobacion >= '" . $semana . "'";
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->leftJoin('App\Entity\Caja', 'caja', 'WITH', 'c.id = caja.credito') // Agrega la relación con la tabla Caja
            ->andWhere(' c.aprobado=1')
            ->andWhere($cadena)
            ->andWhere($cadena2)
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->andWhere('caja.credito IS NULL OR c.id IS NULL') // Ajusta la condición para seleccionar créditos sin relación en la tabla Caja
            ->orderBy('p.nombre', 'ASC')
            ->orderBy('c.fechaAprobacion', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function getEntregadosEfectivo()
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2 where c2.nroCredito=c.nroCredito ');
        
        $semana = date("Y-m-d", (strtotime('today') - (86400 * 8)));
        $mañana = date("Y-m-d", (strtotime('today') + (86400 * 1)));
        $cadena = " c.fechaAprobacion <= '" . $mañana . "'";
        $cadena2 = " c.fechaAprobacion >= '" . $semana . "'";
        
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->innerJoin('App\Entity\Caja', 'caja', 'WITH', 'c.id = caja.credito') // Cambié a innerJoin para asegurarme de que haya una relación en la tabla Caja
            ->andWhere(' c.aprobado=1')
            ->andWhere($cadena)
            ->andWhere($cadena2)
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->orderBy('p.nombre', 'ASC')
            ->orderBy('c.fechaAprobacion', 'DESC');
        
        return $query->getQuery()->getResult();
    }



    public function getRenovaciones()
    {
        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
              where c2.nroCredito=c.nroCredito ');
        $queryUltVto = $this->_em->createQuery('select MAX(cu.fechaVencimiento) from App\Entity\Cuota cu
              where cu.credito=c.id ');
        $queryUltCtaPaga = $this->_em->createQuery('select MAX(cu2.nroCuota) from App\Entity\Cuota cu2
              where cu2.credito=c.id and cu2.cancelado=1 ');
        $query->select('c')
            ->addSelect('(' . $queryUltVto->getDQL() . ') as ultVto')
            ->addSelect('(' . $queryUltCtaPaga->getDQL() . ') as ultCtaPaga')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->innerJoin('c.asesor', 'u')
            ->leftJoin('c.grupo', 'g')
            ->andWhere(' c.aprobado=1')
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->orderBy('c.ultVto', 'ASC')
            ->orderBy('c.fechaAlta', 'DESC');
        return $query->getQuery()->getResult();
    }

    public function getUltimoPago($credito)
    {
        $queryCuota = $this->_em->createQuery('select MAX(cu.nroCuota) as cuota from App\Entity\Cuota cu
          INNER JOIN cu.credito cr2  where cu.cancelado=1 and cr2.id= ' . $credito->getId());
        $queryPago = $this->_em->createQueryBuilder();
        $queryPago->select('MAX(cu.fechaCancelacion)')
            ->from('App\Entity\Cuota', 'cu')
            ->innerJoin('cu.credito', 'cr2')
            ->andWhere($queryPago->expr()->isNotNull('cu.fechaCancelacion'))
            ->andWhere('cr2.id=' . $credito->getId());
        $pago = $queryPago->getQuery()->getSingleScalarResult();
        if ($pago) {
            // buscar vto de primera cuota imaga
            $queryVto = $this->_em->createQueryBuilder();
            $queryVto->select('MIN(cu.fechaVencimiento)')
                ->from('App\Entity\Cuota', 'cu')
                ->innerJoin('cu.credito', 'cr2')
                ->where('cu.saldo>0')
                ->andWhere('cr2.id=' . $credito->getId());
            $vto = new \DateTime($queryVto->getQuery()->getSingleScalarResult());
        } else {
            // buscar primer vto del credito - nunca se pagó
            $vto = $credito->getFechaVtoCuota1();
        }

        $queryRenov = $this->_em->createQuery("select c.id from App\Entity\Credito c
          where c.nroCredito= " . $credito->getNroCredito() . " and c.nroRenovacion> " . $credito->getNroRenovacion() .
            " and c.aprobado=0 ");
        if ($queryRenov->getResult())
            $renov = $queryRenov->getSingleScalarResult();
        else
            $renov = 0;

        return array('vto' => $vto, 'cuota' => $queryCuota->getSingleScalarResult(), 'pago' => $pago, 'renovado' => $renov);
    }

    public function esRenovacion($credito)
    {
        $queryRenov = $this->_em->createQuery("select 1 from App\Entity\Credito c
          where c.nroCredito= " . $credito->getNroCredito() . " and c.nroRenovacion< " . $credito->getNroRenovacion() .
            " and c.cancelado=0 ");
        return $queryRenov->getOneOrNullResult();
    }

    public function esMoroso($credito)
    {
        $sinpunitorios = ($credito->getSinPunitorios()) ? '1' : '0';
        $queryMor = $this->_em->createQuery("select count(cu.id) from App\Entity\Cuota cu
          where cu.credito= " . $credito->getId() . " and cu.fechaVencimiento<'" . date("Y-m-d") . "' and cu.saldo>0 "
            . "and 0=" . $sinpunitorios);
        return $queryMor->getSingleScalarResult();
    }

    public function esValidoParaCredito($tipo, $id, $renov, $credId)
    {
        $cadena = '';
        // $auxCantCtas=$this->_em->createQuery("select count(cc.id) from App\Entity\Cuota cc
        //              where cc.cancelado=0 and cc.credito=c.id");
        // $fechaMax = date('Y-m-d', (strtotime('today') + (86400*7) ));
        if ($credId > 0) {
            $cadena = ' AND c.id!= ' . $credId . ' ';
        }

        if ($tipo == 'G') {
            /* Es grupal */
            if ($renov == 0) {
                /* No puede existir un credito grupal */
                $query = $this->_em->createQuery('Select 1 from App\Entity\Credito c
                inner join c.grupo g where g.id=' . $id . $cadena . " AND c.aprobado=1 AND c.cancelado = 0");
            } else {
                /* no puede tener otro credito grupal sin cancelar */
                /*  NO PUEDE TENER OTRA SOLICITUD HECHA */
                $query = $this->_em->createQuery("SELECT 1 FROM App\Entity\Credito c
                INNER JOIN c.grupo g WHERE g.id=" . $id . $cadena . " AND c.aprobado=1 AND c.cancelado = 0");

                /* $query = $this->_em->createQuery("SELECT 1 FROM App\Entity\Credito c
                 INNER JOIN c.grupo g INNER JOIN c.cuotas cu WHERE cu.nroCuota=c.cantCuotas AND
                 g.id=" . $id . $cadena ." AND c.cancelado=0 AND (".$auxCantCtas->getDQL().") > 1 ");*/

                //   g.id=" . $id . $cadena." AND c.cancelado=0 AND ( (".$auxCantCtas->getDQL().") > 1 or cu.fechaVencimiento>='". $fechaMax."')");
            }
        } else {
            /* Es individual */
            if ($renov == 0)
                /* no puede tener otro credito individual */
                $query = $this->_em->createQuery('Select 1 from App\Entity\CreditoEmprendedor ce
            inner join ce.emprendedor e inner join ce.credito c where e.id=' . $id[0] . ' and c.grupo is null ' . $cadena . " AND c.aprobado=1 AND c.cancelado = 0");
            else
                /*  NO PUEDE TENER OTRA SOLICITUD HECHA */
                /* no puede tener otro credito individual sin cancelar */
                /*                $query = $this->_em->createQuery('Select 1 from App\Entity\CreditoEmprendedor ce
                        inner join ce.emprendedor e inner join ce.credito c where c.cancelado=0 and e.id=' . $id[0] . ' and c.grupo is null ' . $cadena);*/
                $query = $this->_em->createQuery("SELECT 1 FROM App\Entity\CreditoEmprendedor ce
                INNER JOIN ce.emprendedor e INNER JOIN ce.credito c WHERE e.id=" . $id[0] . " AND c.grupo IS NULL " . $cadena . " AND c.aprobado=1 AND c.cancelado = 0");
            /*                $query = $this->_em->createQuery("SELECT 1 FROM App\Entity\CreditoEmprendedor ce
                        INNER JOIN ce.emprendedor e INNER JOIN ce.credito c INNER JOIN c.cuotas cu WHERE cu.nroCuota=c.cantCuotas AND
                        e.id=". $id[0] ." AND c.grupo IS NULL ". $cadena );*/
            //." AND c.cancelado=0 AND (".$auxCantCtas->getDQL().") > 1 ");
        }
        //x
        if ($query->getResult())
            return FALSE;
        else
            return TRUE;
    }

    public function esValidoParaCargaCredito($tipo, $id)
    {
        if ($tipo == 'G') {
            $query = $this->_em->createQuery('Select 1 from App\Entity\Credito c
                inner join c.grupo g where g.id=' . $id);
        } else {
            $query = $this->_em->createQuery('Select 1 from App\Entity\CreditoEmprendedor ce
            inner join ce.emprendedor e inner join ce.credito c where e.id=' . $id[0] . ' and c.grupo is null ');
        }

        //var_dump($query->getSQL());die;
        if ($query->getResult())
            return FALSE;
        else
            return TRUE;
    }

    /*public function esValidoNroCredito($nro,$renov){
      $query = $this->_em->createQuery('Select 1 from App\Entity\Credito c
            where c.nroCredito='.$nro.' AND c.nroRenovacion= '.$renov );
      if($query->getResult()) return TRUE;
      else return FALSE;
    }*/
    public function esValidoNroCarpeta($nro)
    {
        $query = $this->_em->createQuery('Select 1 from App\Entity\Credito c
            where c.nroCarpeta=' . $nro);
        if ($query->getResult())
            return TRUE;
        else
            return FALSE;
    }

    public function getGastosAdministrativos($periodo = 7, $monto = 0)
    {
        $query = $this->_em->createQuery('Select t from App\Entity\TablaGastos t
          where t.desde <=' . $monto . '
                AND t.hasta >=' . $monto);
        return $query->getResult();
    }

    public function getMinGastoAdministrativo()
    {
        $query = $this->_em->createQuery('Select MIN(t.valor) from App\Entity\TablaGastos t');
        return $query->getSingleScalarResult();
    }

    public function estaPagado($id)
    {
        $query = $this->_em->createQuery('Select c from App\Entity\Credito c
            inner join c.cuotas cu  where cu.cancelado=0 and c.id=' . $id);
        if ($query->getResult())
            return FALSE;
        else
            return TRUE;
    }

    public function getRefinanciaciones($id)
    {
        $query = $this->_em->createQuery('Select cr from App\Entity\CreditoRefinanciacion cr
            where cr.credito=' . $id);
        return $query->getResult();
    }

    public function getListadoClientePaginador($filtro)
    {
        $query = $this->_em->createQueryBuilder();
        $query->select('g')
            ->from('App:Grupo', 'g');
        return $query->getQuery();
    }

    public function getListadoClintes($filtro = null, $limit = null, $offset = null)
    {
        if ($filtro['programa']) {
            $programa = " WHERE p.id = '" . $filtro['programa'] . "'";
        }

        $conn = $this->_em->getConnection();

        $arrayResult = array();

        $sql = 'SELECT g.id,g.codigo, g.nombre
                 FROM grupo g
                 ORDER BY g.codigo';
        if (!is_null($limit)) {
            if (is_null($offset))
                $offset = 1;

            $offset = ($offset - 1) * $limit;
            $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        $result = $conn->query($sql)->fetchAll();

        foreach ($result as $element) {

            $arrayEmprendedor = [
                'codigo' => $element['codigo'],
                'nombre' => $element['nombre']
            ];

            $query = $this->_em->createQueryBuilder();
            $query->select('c')
                ->from('App\Entity\Credito', 'c')
                ->where('c.aprobado=1')
                ->andWhere('c.incobrable=0')
                ->andWhere('c.grupo = ' . $element['id'])
                ->orderBy('c.fechaEntrega', 'DESC');

            $creditos = $query->getQuery()->getResult();

            $cantidadActual = 0;
            $cantidadCancelados = 0;
            $sumaCantidaDias = 0;
            $promedioCuotaCapital = 0;
            $diasDefase = 0;
            $diasDesfaseContador = 0;
            foreach ($creditos as $index => $creditos) {
                if ($index == 0) {
                    $arrayEmprendedor['fecha_entraga'] = $creditos->getFechaAprobacion();
                    $arrayEmprendedor['fecha_ultimo_pago'] = $creditos->getUltimoPago();
                    $arrayEmprendedor['cuota_actual'] = $creditos->getUltimaCuotaPaga();
                    $arrayEmprendedor['cuota_cantidad'] = $creditos->getCantCuotas();
                    $arrayEmprendedor['ko'] = $creditos->getMontoCredito();

                    $programa = $creditos->getPrograma();
                    if ($programa->getHabilitadoNuevoCredito() == false) {
                        $arrayEmprendedor['id'] = $creditos->getId() . ' - ' . $programa->getSigla();
                    } else {
                        $arrayEmprendedor['id'] = $creditos->getId();
                    }

                    $arrayEmprendedor['emprendor_nombre'] = '';
                    $arrayEmprendedor['emprendor_telefono'] = '';
                    $arrayEmprendedor['garante_nombre'] = '';
                    $arrayEmprendedor['garante_telefono'] = '';
                    foreach ($creditos->getEmprendedores() as $creditoEmprendores) {

                        if ($creditoEmprendores->getGarante() != '')
                            $arrayEmprendedor['garante_nombre'] = $creditoEmprendores->getGarante();

                        if ($creditoEmprendores->getGaranteTelefono() != '')
                            $arrayEmprendedor['garante_telefono'] = $creditoEmprendores->getGaranteTelefono();

                        $emprendedor = $creditoEmprendores->getEmprendedor();
                        $nombres = $emprendedor->getApellido() . ' ' . $emprendedor->getNombres() . ' | ';
                        $arrayEmprendedor['emprendor_nombre'] = $arrayEmprendedor['emprendor_nombre'] . $nombres;
                        if ($emprendedor->getTelefono()) {
                            $arrayEmprendedor['emprendor_telefono'] =
                                $arrayEmprendedor['emprendor_telefono'] . $emprendedor->getTelefono() . ' | ';
                        } else {
                            $arrayEmprendedor['emprendor_telefono'] =
                                $arrayEmprendedor['emprendor_telefono'] . ' - | ';
                        }
                    }
                }
                $promedioCuotaCapital += 100 - (($creditos->getMontoCredito() / $creditos->getTotalCredito()) * 100);
                $status = $creditos->getStatus();
                $sumaCantidaDias += $creditos->getPeriodo();
                if ($status == "Moroso" or $status == "Al dia") {
                    $cantidadActual++;
                }
                if ($status == "Cancelado") {
                    $cantidadCancelados++;
                }

                foreach ($creditos->getCuotas() as $cuota) {
                    if (!is_null($cuota->getFechaCancelacion())) {
                        $dias = DefaultController::getDiasRetrasoCuotaCanccelada(
                            $this->_em,
                            $cuota,
                            $cuota->getFechaCancelacion(),
                            $creditos->getPrograma(),
                            true
                        );
                        $diasDefase += $dias;
                        $diasDesfaseContador++;
                    }
                }

            }
            $arrayEmprendedor['creditos_activos'] = $cantidadActual;
            $arrayEmprendedor['creditos_cancelados'] = $cantidadCancelados;
            if ($cantidadActual + $cantidadCancelados == 0) {
                $total = 1;
            } else {
                $total = $cantidadActual + $cantidadCancelados;
            }
            $arrayEmprendedor['cuotas_frecuencias'] = intval($sumaCantidaDias / $total);
            $arrayEmprendedor['cuota_porcentaje'] = $promedioCuotaCapital / $total;
            if ($diasDesfaseContador == 0)
                $diasDesfaseContador = 1;
            if ($diasDesfaseContador == 0)
                $diasDesfaseContador = 1;
            $arrayEmprendedor['cuota_dias_desfase'] = $diasDefase;

            $arrayResult[] = $arrayEmprendedor;

        }

        return $arrayResult;
    }

    public function getListadoClientePaginadorEmprendedores($filtro)
    {
        $query = $this->_em->createQueryBuilder();
        $query->select('g')
            ->from('App:Emprendedor', 'g')
            ->innerJoin('g.creditos', 'ce')
            ->innerJoin('ce.credito', 'c')
            ->innerJoin('c.programa', 'p')
            ->where('p.individual=1')
        ;
        return $query->getQuery();
    }

    public function getListadoClintesEmprendedores($filtro = null, $limit = null, $offset = null)
    {
        if ($filtro['programa']) {
            $programa = " WHERE p.id = '" . $filtro['programa'] . "'";
        }

        $conn = $this->_em->getConnection();

        $arrayResult = array();

        $sql = 'SELECT e.id,e.codigo, e.nombres, e.apellido
                FROM emprendedor e
                 INNER JOIN credito_emprendedor ce ON ce.emprendedor_id = e.id
                 INNER JOIN credito c ON ce.credito_id = c.id
                 INNER JOIN programa p ON c.programa_id = p.id
                WHERE p.individual = 1
                GROUP BY e.id,e.codigo, e.nombres, e.apellido
                ORDER BY e.codigo';

        if (!is_null($limit)) {
            if (is_null($offset))
                $offset = 1;

            $offset = ($offset - 1) * $limit;
            $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        $result = $conn->query($sql)->fetchAll();

        foreach ($result as $element) {

            $arrayEmprendedor = [
                'codigo' => $element['codigo'],
                'nombre' => $element['apellido'] . ' ' . $element['nombres']
            ];

            $query = $this->_em->createQueryBuilder();
            $query->select('c')
                ->from('App:credito', 'c')
                ->innerJoin('c.emprendedores', 'ce')
                ->innerJoin('c.programa', 'p')
                ->where('p.individual=1')
                ->andWhere('c.aprobado=1')
                ->andWhere('c.incobrable=0')
                ->andWhere('ce.emprendedor = ' . $element['id'])
                ->orderBy('c.fechaEntrega', 'DESC');

            $creditos = $query->getQuery()->getResult();

            $cantidadActual = 0;
            $cantidadCancelados = 0;
            $sumaCantidaDias = 0;
            $promedioCuotaCapital = 0;
            $diasDefase = 0;
            $diasDesfaseContador = 0;
            foreach ($creditos as $index => $creditos) {
                if ($index == 0) {
                    $arrayEmprendedor['fecha_entraga'] = $creditos->getFechaAprobacion();
                    $arrayEmprendedor['fecha_ultimo_pago'] = $creditos->getUltimoPago();
                    $arrayEmprendedor['cuota_actual'] = $creditos->getUltimaCuotaPaga();
                    $arrayEmprendedor['cuota_cantidad'] = $creditos->getCantCuotas();
                    $arrayEmprendedor['ko'] = $creditos->getMontoCredito();

                    $programa = $creditos->getPrograma();
                    if ($programa->getHabilitadoNuevoCredito() == false) {
                        $arrayEmprendedor['id'] = $creditos->getId() . ' - ' . $programa->getSigla();
                    } else {
                        $arrayEmprendedor['id'] = $creditos->getId();
                    }


                    $arrayEmprendedor['emprendor_nombre'] = '';
                    $arrayEmprendedor['emprendor_telefono'] = '';
                    $arrayEmprendedor['garante_nombre'] = '';
                    $arrayEmprendedor['garante_telefono'] = '';
                    foreach ($creditos->getEmprendedores() as $creditoEmprendores) {

                        if ($creditoEmprendores->getGarante() != '')
                            $arrayEmprendedor['garante_nombre'] = $creditoEmprendores->getGarante();

                        if ($creditoEmprendores->getGaranteTelefono() != '')
                            $arrayEmprendedor['garante_telefono'] = $creditoEmprendores->getGaranteTelefono();

                        $emprendedor = $creditoEmprendores->getEmprendedor();
                        $nombres = $emprendedor->getApellido() . ' ' . $emprendedor->getNombres();
                        $arrayEmprendedor['emprendor_nombre'] = $arrayEmprendedor['emprendor_nombre'] . $nombres;
                        if ($emprendedor->getTelefono()) {
                            $arrayEmprendedor['emprendor_telefono'] =
                                $arrayEmprendedor['emprendor_telefono'] . $emprendedor->getTelefono();
                        } else {
                            $arrayEmprendedor['emprendor_telefono'] =
                                $arrayEmprendedor['emprendor_telefono'];
                        }
                    }
                }
                $promedioCuotaCapital += 100 - (($creditos->getMontoCredito() / $creditos->getTotalCredito()) * 100);
                $status = $creditos->getStatus();
                $sumaCantidaDias += $creditos->getPeriodo();
                if ($status == "Moroso" or $status == "Al dia") {
                    $cantidadActual++;
                }
                if ($status == "Cancelado") {
                    $cantidadCancelados++;
                }
                $diasDefase = $diasDesfaseContador = 0;
                foreach ($creditos->getCuotas() as $cuota) {
                    if (!is_null($cuota->getFechaCancelacion())) {
                        $array = DefaultController::getPunitoriosDiasMora(
                            $this->_em,
                            $cuota,
                            $cuota->getFechaCancelacion(),
                            $creditos->getPrograma(),
                            true
                        );
                        $diasDefase += $array['dias'];
                        $diasDesfaseContador++;
                    }
                }
            }
            $arrayEmprendedor['creditos_activos'] = $cantidadActual;
            $arrayEmprendedor['creditos_cancelados'] = $cantidadCancelados;
            if ($cantidadActual + $cantidadCancelados == 0) {
                $total = 1;
            } else {
                $total = $cantidadActual + $cantidadCancelados;
            }
            $arrayEmprendedor['cuotas_frecuencias'] = intval($sumaCantidaDias / $total);
            $arrayEmprendedor['cuota_porcentaje'] = $promedioCuotaCapital / $total;
            if ($diasDesfaseContador == 0)
                $diasDesfaseContador = 1;
            if ($diasDesfaseContador == 0)
                $diasDesfaseContador = 1;
            $arrayEmprendedor['cuota_dias_desfase'] = $diasDefase / $diasDesfaseContador;


            $arrayResult[] = $arrayEmprendedor;

        }

        return $arrayResult;
    }

    public function getMontoColocacionMensual($filtro = null)
    {

        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\Credito', 'c')
            ->Where(' c.aprobado=1')
            ->andWhere(' c.fechaAprobacion IS NOT NULL');
        if (isset($filtro['desde']) and $filtro['desde'] != '') {
            $cadena = " c.fechaCredito >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
            $query->andWhere($cadena);
        }
        if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
            $cadena = " c.fechaCredito <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $query->andWhere($cadena);
        }
        $query->select('SUM(c.montoCredito) as monto', 'MONTH(c.fechaCredito) as mes', 'YEAR(c.fechaCredito) as year')
            ->groupBy('year', 'mes')
            ->orderBy('mes', 'ASC')
            ->orderBy('year', 'ASC');


        return $query->getQuery()->getResult();
    }

    public function getCantidadColocacionMensual($filtro = null)
    {
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\Credito', 'c')
            ->Where(' c.aprobado=1')
            ->andWhere(' c.fechaAprobacion IS NOT NULL');
        if (isset($filtro['desde']) and $filtro['desde'] != '') {
            $cadena = " c.fechaCredito >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
            $query->andWhere($cadena);
        }
        if (isset($filtro['hasta']) and $filtro['hasta'] != '') {
            $cadena = " c.fechaCredito <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $query->andWhere($cadena);
        }
        $query->select('COUNT(c.montoCredito) as cantidad', 'MONTH(c.fechaCredito) as mes', 'YEAR(c.fechaCredito) as year')
            ->groupBy('year', 'mes')
            ->orderBy('mes', 'ASC')
            ->orderBy('year', 'ASC');


        return $query->getQuery()->getResult();
    }


    public function getEstadoResultadosCredito($month, $year)
    {

        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\Credito', 'c');

        $query->select('SUM(c.montoCredito) as colocacion', 'MONTH(c.fechaCredito) as mes', 'YEAR(c.fechaCredito) as year')
            ->Where(' c.aprobado=1')
            ->andWhere(' c.fechaAprobacion IS NOT NULL')
            ->andWhere('MONTH(c.fechaCredito)=' . $month . '')
            ->andWhere('YEAR(c.fechaCredito) = ' . $year . '')
            ->groupBy('year', 'mes');


        return $query->getQuery()->getResult();
    }


    public function getMontoCreditos($fecha)
    {

        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\Credito', 'c');

        $query->select('SUM(c.montoCredito) as montoCredito')
            ->Where(' c.aprobado=1')
            ->andWhere(' c.fechaAprobacion IS NOT NULL');
        $cadena = "c.fechaCredito <='" . DefaultController::toAnsiDate($fecha) . " 23:59'";
        $query->where($cadena);


        return $query->getQuery()->getResult();
    }


    public function findPagoCreditos2($filtro = null)
    {

        $query = $this->_em->createQueryBuilder();

        $query->select('e')
            ->from('App\Entity\CreditoEmprendedor', 'e')
            ->innerJoin('e.emprendedor', 'em')
            ->innerJoin('e.credito', 'c')
            ->andWhere('c.aprobado=1')
            ->orderBy('e.id', 'ASC')
            ->groupBy('e.id');

        if ($filtro) {
            $cadena = " c.fechaEntrega >= '" . DefaultController::toAnsiDate($filtro) . " 00:00'";
            $cadena2 = " c.fechaEntrega <= '" . DefaultController::toAnsiDate($filtro) . " 23:59'";
            $query->andWhere($cadena);
            $query->andWhere($cadena2);
        }


        return $query->getQuery()->getResult();
    }


    public function findPagoCreditosWithoutAprobate()
    {

        $query = $this->_em->createQueryBuilder();
        $queryRenovacion = $this->_em->createQuery('select MAX(c2.nroRenovacion) AS renov from App\Entity\Credito c2
            where c2.nroCredito=c.nroCredito ');
        $ts = mktime(0, 0, 0, date("n") - 2, date("d"), date("Y"));
        $fecha = date("Y-m-d H:i:s", $ts);

        $query->select('e')
            ->from('App\Entity\CreditoEmprendedor', 'e')
            ->innerJoin('e.emprendedor', 'em')
            ->innerJoin('e.credito', 'c')
            ->andWhere('c.aprobado=0')
            ->andWhere($query->expr()->In(' c.nroRenovacion ', $queryRenovacion->getDQL()))
            ->andWhere('c.fechaEntrega >= :fecha')
            ->setParameter('fecha', new \DateTime($fecha), \Doctrine\DBAL\Types\Type::DATETIME)
            ->orderBy('e.id', 'DESC')
            ->groupBy('e.id');

        return $query->getQuery()->getResult();
    }

}