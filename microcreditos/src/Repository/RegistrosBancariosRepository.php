<?php

namespace App\Repository;

use App\Entity\RegistrosBancarios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use App\Controller\DefaultController;

/**
 * @extends ServiceEntityRepository<RegistrosBancarios>
 *
 * @method RegistrosBancarios|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrosBancarios|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrosBancarios[]    findAll()
 * @method RegistrosBancarios[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrosBancariosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrosBancarios::class);
    }

    public function getAll() {
   
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\RegistrosBancarios', 'g');
        $query->select('g');
         return $query->getQuery()->getResult();
      }


    public function findByMyCriteriaDQL($filtro=null) {
        $query = $this->_em->createQueryBuilder();
        
        $query->from('App\Entity\RegistrosBancarios', 'g');
        $query->select('g');     
  
   
      if(isset($filtro['desde']) AND $filtro['desde'] != '' ){
        $cadena = " g.fecha >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
        $query->andWhere($cadena);
      }
      if(isset($filtro['hasta']) AND $filtro['hasta'] != '' ){
        $cadena = " g.fecha <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
        $query->andWhere($cadena);
      }
   
        $query->orderBy('g.id','desc');
      return $query->getQuery();
    }  

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(RegistrosBancarios $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(RegistrosBancarios $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return RegistrosBancarios[] Returns an array of RegistrosBancarios objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegistrosBancarios
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getTotal($fecha){


        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\RegistrosBancarios', 'c');
                
        $query->select('SUM(c.monto) as monto');
      
        $cadena ="c.fecha <='".DefaultController::toAnsiDate($fecha)."'";
        $query->where($cadena);    
        
        
         return $query->getQuery()->getResult();
    }
}
