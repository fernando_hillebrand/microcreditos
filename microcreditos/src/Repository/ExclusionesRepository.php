<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

class ExclusionesRepository extends EntityRepository
{
    public function findFechas($fechaVendimiento){
        $query = $this->_em->createQueryBuilder();
        $query->select('e.id, e.fechaDesde, e.fechaHasta')
            ->from('App:Exclusiones', 'e')
            ->where('e.sinMora=1')
            ->andWhere("e.activo=1")
            ->andWhere( " (
                            '".$fechaVendimiento ."' < e.fechaDesde OR  
                            ('".$fechaVendimiento ."' > e.fechaDesde AND  '".$fechaVendimiento."' <  e.fechaHasta )
                           )");
        return $query->getQuery()->getResult();
    }

    public function findFechasAnteriores($fechaVendimiento){
        $query = $this->_em->createQueryBuilder();
        $query->select('e.id, e.fechaDesde, e.fechaHasta')
            ->from('App:Exclusiones', 'e')
            ->where('e.sinMora=1')
            ->andWhere("e.activo=1")
            ->andWhere( "'".$fechaVendimiento ."' > e.fechaHasta ");
        return $query->getQuery()->getResult();
    }
}
?>
