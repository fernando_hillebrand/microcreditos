<?php

namespace App\Repository;

use App\Controller\DefaultController;
use App\Entity\Caja;
use Doctrine\ORM\EntityRepository;

class CajaRepository extends EntityRepository
{

  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL($filtro=null, $tipoMovimiento = null) {
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
            ->from('App\Entity\Caja','g')
              ->leftJoin('g.concepto', 'co')
              ->leftJoin('g.programa', 'p')
            ->addOrderBy('g.id', 'DESC');

    if($tipoMovimiento){
        if($tipoMovimiento == 'ingreso'){
            $cadena = " g.ingreso > 0 ";
            $query->andWhere($cadena);
        }
    }
    if(isset($filtro['desde']) AND $filtro['desde'] != '' ){
      $cadena = " g.fecha >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['hasta']) AND $filtro['hasta'] != '' ){
      $cadena = " g.fecha <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['concepto']) AND $filtro['concepto'] > 0 ){
      $cadena = " co.id = '".$filtro['concepto']."'";
      $query->andWhere($cadena);
    }
    if(isset($filtro['programa']) AND $filtro['programa'] > 0 ){
      $cadena = " p.id = '".$filtro['programa']."'";
      $query->andWhere($cadena);
    }

      $query->orderBy('g.id','desc');
    return $query->getQuery();
  }

  public function getCajaByPago($pagoId){
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
          ->from('App\Entity\Caja','g')
          ->where('g.pago = '.$pagoId)
          ->addOrderBy('g.id', 'DESC');
      return $query->getQuery()->getResult();
  }

  public function obtenerTotalIngresosCobro($filtro){
      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('sum(g.ingreso) AS cantidad')
          ->from('App\Entity\Caja','g')
          ->innerJoin('g.pago', 'pago')
          ->add('where',
                       '('.
                          $query->expr()->between(
                              'g.fecha',
                              ':fechaInicio',
                              ':fechaFin'
                          ).
                      ') AND (
                          g.concepto = 210 
                      ) AND g.ingreso > 0'
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->addOrderBy('g.id', 'DESC');

      return $query->getQuery()->getResult();
  }

  public function obtenerTotalIngresos($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();

        $query->select('sum(g.ingreso) AS cantidad')
            ->from('App\Entity\Caja','g')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND 	g.concepto != 210 AND g.ingreso > 0'
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');
        return $query->getQuery()->getResult();
    }

  public function obtenerTotalEgresos($filtro){
      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('sum(g.egreso) AS cantidad')
          ->from('App\Entity\Caja','g')
          ->add('where',
              '('.
              $query->expr()->between(
                  'g.fecha',
                  ':fechaInicio',
                  ':fechaFin'
              ).
              ') AND g.concepto != 211 AND g.concepto != 209 AND g.egreso > 0'
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->addOrderBy('g.id', 'DESC');

      return $query->getQuery()->getResult();
  }

  public function obtenerTotalReinvertido($filtro){
      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('sum(g.egreso) AS cantidad')
          ->from('App\Entity\Caja','g')
          ->add('where',
              '('.
              $query->expr()->between(
                  'g.fecha',
                  ':fechaInicio',
                  ':fechaFin'
              ).
              ') AND (
                          g.concepto = 209
                      )'
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->addOrderBy('g.id', 'DESC');

      return $query->getQuery()->getResult();
  }

  public function obtenerTotalPagos($filtro){
    $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
    $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
    $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
    $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

    $query = $this->_em->createQueryBuilder();

    $query->select(
            'SUM(g.ingreso) AS total_pago_caja',
            'SUM(pago.total) AS total',
            'SUM(pago.capital+pago.intereses+pago.punitorios+pago.gastoAdministrativo+pago.refinPunitorios) AS total_sumado',
            'SUM(pago.capital) AS cantidad_capital',
            'SUM(pago.intereses) AS cantidad_intereses',
            'SUM(pago.refinPunitorios) AS cantidad_refinPunitorios',
            'SUM(pago.punitorios) AS cantidad_punitorios',
            'SUM(pago.gastoAdministrativo) AS cantidad_gastoAdministrativo'
    )
    ->from('App\Entity\Caja','g')
    ->innerJoin('g.pago', 'pago')
    ->add('where',
        '('.
        $query->expr()->between(
            'g.fecha',
            ':fechaInicio',
            ':fechaFin'
        ).
        ') AND (
                  g.concepto = 210
              ) AND g.ingreso > 0'
    )
    ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
    ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
    ->addOrderBy('g.id', 'DESC');
        return $query->getQuery()->getResult();
  }

  public function obtenerTotalPagosDesagregados($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();

        $query->select(
            'g.id as caja_id',
            'pago.id as pago_id',
            'g.ingreso AS total_pago_caja',
            'pago.total AS total',
            '(pago.capital+pago.intereses+pago.punitorios+pago.gastoAdministrativo) AS total_sumado',
            '(pago.capital) AS cantidad_capital',
            '(pago.intereses) AS cantidad_intereses',
            '(pago.refinPunitorios) AS cantidad_refinPunitorios',
            '(pago.punitorios) AS cantidad_punitorios',
            '(pago.gastoAdministrativo) AS cantidad_gastoAdministrativo'
        )
            ->from('App\Entity\Caja','g')
            ->innerJoin('g.pago', 'pago')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND (
                  g.concepto = 210
              ) AND g.ingreso > 0'
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');
        return $query->getQuery()->getResult();
    }

  public function guardarNuevoEgreso($egreso,$descripcion,$userId,$credito=null,$pago=null,$concepto=null,$gasto=null){
      $fecha = new \DateTime();
      $usuario = $this->_em->getRepository('App:User')->find(
          $userId
      );
      $cajaAnterior = $this->_em->getRepository('App:Caja')->findOneBy([], ['id' => 'desc']);
      $saldoAnterior = 0;

      if($cajaAnterior){
          $saldoAnterior = $cajaAnterior->getTotal();
      }else{
          $caja = new Caja();
          $caja->setTotal(0);
          $caja->setEgreso(0);
          $caja->setIngreso(0);
          $caja->setObservacion('Inicio de Caja');
          $caja->setFechaCreacion(new \DateTime());
          $caja->setFechaActualizacion(new \DateTime());
          $caja->setUsuario($usuario);
          $caja->setCreadoPor($usuario);
          $caja->setActualizadoPor($usuario);

          $this->_em->persist($caja);
          $this->_em->flush();
      }

      $caja = new Caja();
      $caja->setEgreso($egreso);
      $caja->setIngreso(0);
      $caja->setTotal($saldoAnterior - $egreso );
      $caja->setFechaCreacion($fecha);
      $caja->setFechaActualizacion($fecha);
      $caja->setUsuario($usuario);
      $caja->setCreadoPor($usuario);
      $caja->setActualizadoPor($usuario);
      $caja->setObservacion($descripcion);

      if(!is_null($credito)){
          $caja->setCredito($credito);
          $caja->setPrograma($credito->getPrograma());
      }

      if(!is_null($pago)){
          $caja->setPago($pago);
      }

      if(!is_null($concepto)){
          $caja->setConcepto($concepto);
      }
      if(!is_null($gasto)){
          $caja->setGastos($gasto);
      }

      $this->_em->persist($caja);
      $this->_em->flush();
  }

  public function guardarNuevoIngreso($ingreso,$descripcion,$userId,$credito=null,$pago=null,$concepto=null){
        $fecha = new \DateTime();
        $usuario = $this->_em->getRepository('App:User')->find(
            $userId
        );
        $cajaAnterior = $this->_em->getRepository('App:Caja')->findOneBy([], ['id' => 'desc']);
        $saldoAnterior = 0;

        if($cajaAnterior){
            $saldoAnterior = $cajaAnterior->getTotal();
        }else{
            $caja = new Caja();
            $caja->setTotal(0);
            $caja->setEgreso(0);
            $caja->setIngreso(0);
            $caja->setObservacion('Inicio de Caja');
            $caja->setFechaCreacion(new \DateTime());
            $caja->setFechaActualizacion(new \DateTime());
            $caja->setUsuario($usuario);
            $caja->setCreadoPor($usuario);
            $caja->setActualizadoPor($usuario);

            $this->_em->persist($caja);
            $this->_em->flush();
        }

        $caja = new Caja();
        $caja->setEgreso(0);
        $caja->setIngreso($ingreso);
        $caja->setTotal($saldoAnterior + $ingreso );
        $caja->setFechaCreacion($fecha);
        $caja->setFechaActualizacion($fecha);
        $caja->setUsuario($usuario);
        $caja->setCreadoPor($usuario);
        $caja->setActualizadoPor($usuario);
        $caja->setObservacion($descripcion);

        if(!is_null($credito)){
            $caja->setCredito($credito);
            $caja->setPrograma($credito->getPrograma());
        }

        if(!is_null($pago)){
            $caja->setPago($pago);
        }

        if(!is_null($concepto)){
            $caja->setConcepto($concepto);
        }

        $this->_em->persist($caja);
        $this->_em->flush();
  }

  public function obtenerCantidadCreditosCreadossMes($filtro){
        /*
         *  Tomando como mes 05/2020
              SELECT count(id) as finalizados
              FROM credito
              WHERE cancelado = 1
                    AND aprobado = 1

                    AND fecha_cancelacion BETWEEN
                          CAST('2020-05-01' AS DATE)
                          AND CAST('2020-05-30' AS DATE)   ;
         */

      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();

        $query->select('count(c.id) AS cantidad')
            ->from('App\Entity\Credito', 'c')
            ->andWhere(' c.aprobado=1')
            ->andWhere(' c.cancelado=0')
            ->add('where',
                $query->expr()->between(
                    'c.fechaAprobacion',
                    ':fechaInicio',
                    ':fechaFin'
                )
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)

            ->orderBy('c.id', 'DESC');
        return $query->getQuery()->getResult();
    }

  public function obtenerCantidadCreditosFinalizadosMes($filtro){
      /*
       *  Tomando como mes 05/2020
            SELECT count(id) as finalizados
            FROM credito
            WHERE cancelado = 1
                  AND aprobado = 1

                  AND fecha_cancelacion BETWEEN
                        CAST('2020-05-01' AS DATE)
                        AND CAST('2020-05-30' AS DATE)   ;
       */

      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('count(c.id) AS cantidad')
          ->from('App\Entity\Credito', 'c')
          ->andWhere(' c.aprobado=1')
          ->andWhere(' c.cancelado=1')
          ->add('where',
              $query->expr()->between(
                                          'c.fechaCancelacion',
                                          ':fechaInicio',
                                          ':fechaFin'
                                      )
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)

          ->orderBy('c.id', 'DESC');
      return $query->getQuery()->getResult();
  }

  public function obtenerListadoCreditosFinalizadosMes($filtro){
      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('c')
          ->from('App\Entity\Credito', 'c')
          ->andWhere(' c.aprobado=1')
          ->andWhere(' c.cancelado=1')
          ->add('where',
              $query->expr()->between(
                  'c.fechaCancelacion',
                  ':fechaInicio',
                  ':fechaFin'
              )
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)

          ->orderBy('c.id', 'DESC');
      return $query->getQuery()->getResult();
  }
  public function obtenerCantidadCreditosActivosMes($filtro){
      /*
       *  Tomando como mes 05/2020
            SELECT count(id) as finalizados
            FROM credito
            WHERE
                  aprobado = 1
                  AND (
                        (
                        cancelado = 1
                        AND fecha_cancelacion >
                           CAST('2020-06-01' AS DATE)
                        )
                        OR
                        ( cancelado = 0 )
                  )
                  AND incobrable = 0 ;
       */

      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQuery(
            'SELECT count(c.id) AS cantidad 
             FROM App\Entity\Credito c 
             WHERE c.aprobado=1 
                   AND ( 
                        (c.cancelado=1  AND  c.fechaCancelacion > :fechaFin ) 
                        OR c.cancelado=0 
                        )
                        AND c.incobrable = 0'
            )
             ->setParameter('fechaFin', $fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME);

      return $query->execute();
  }

  public function obtenerCantidadAldia($filtro){

      /*
       *
            SELECT count(cr.id) as cantidad
            FROM credito AS cr
                 INNER JOIN cuota AS cu ON cr.id = cu.credito_id
            WHERE
                  cr.aprobado = 1
                  AND (
                         cu.fecha_vencimiento >  CAST('2020-05-01' AS DATE)
                         AND cu.fecha_vencimiento <  CAST('2020-05-30' AS DATE)
                         AND cu.saldo = 0
                  )
                  AND
                   cr.cancelado = 0

            ;

       */

      $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
      $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
      $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
      $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

      $query = $this->_em->createQueryBuilder();

      $query->select('count(c.id) AS cantidad')
          ->from('App\Entity\Credito', 'c')
          ->innerJoin('c.cuotas', 'cu')
          ->andWhere(' c.aprobado=1')
          ->add('where', '( '.
              $query->expr()->between(
                  'cu.fechaVencimiento',
                  ':fechaInicio',
                  ':fechaFin'
              )
              .' AND cu.saldo = 0 )
              
              AND (
                    c.cancelado=0 
                    OR
                    (
                        c.cancelado=1 
                        AND c.fechaCancelacion >  :fechaFin
                    )
              )
              '
          )
          ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
          ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)

          ->orderBy('c.id', 'DESC');
      return $query->getQuery()->getResult();
  }

  public function obtenerCantidadMorosos15Dias($filtro){
       return $this->obtenerCantidadMorosos($filtro,15);
  }

  public function obtenerCantidadMorosos30Dias($filtro){
      return $this->obtenerCantidadMorosos($filtro,30);
  }

  public function obtenerCantidadMorosos($filtro,$dias){
        /*
         *
            SELECT count(cr.id) as cantidad, sum(saldo) as total
            FROM credito AS cr
                 INNER JOIN cuota AS cu ON cr.id = cu.credito_id
            WHERE
                  cr.aprobado = 1
                  AND (
                         cu.fecha_vencimiento <  CAST('2020-05-01' AS DATE)
                         AND cu.fecha_vencimiento >  CAST('2020-04-15' AS DATE)
                         AND cu.saldo > 0
                  )
                  AND (
                        (
                        cr.cancelado = 1
                        AND cr.fecha_cancelacion <
                           CAST('2020-05-01' AS DATE)
                        )
                        OR
                        ( cr.cancelado = 0 )
                  )
            ;
        */


        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);

        $fecha15Dia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');
        $fecha15Dia->modify('-'.$dias.' day');
        $fechaPrimerDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();
        //$query->select('count(c.id) AS cantidad')
        $query->select('c.id')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.cuotas', 'cu')
            ->andWhere('c.aprobado=1')
            ->add('where', ' ( 
                                ( '.
                                    $query->expr()->between(
                                        'cu.fechaVencimiento',
                                        ':fechaInicio',
                                        ':fechaFin').
                                    ' AND cu.saldo > 0 
                                )
                                AND (
                                    c.cancelado=0 
                                    OR
                                    (
                                        c.cancelado=1 
                                        AND c.fechaCancelacion >  :fechaFin
                                    )
                              )
                           )
                            AND c.incobrable = 0'
                )
            ->groupBy('c.id')

            ->setParameter('fechaInicio', $fecha15Dia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)

            ->orderBy('c.id', 'DESC');
        return count($query->getQuery()->getResult());
  }

  public function obtenerDetallePagos($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();
        $query->select('g')
            ->from('App\Entity\Caja','g')
            ->innerJoin('g.pago', 'pago')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND (
                  g.concepto = 210
              ) AND g.ingreso > 0'
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');
        return $query->getQuery()->getResult();
    }

  public function obtenerDetalleIngresos($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();
        $stringVacio='';
        $query->select('g')
            ->from('App\Entity\Caja','g')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND g.ingreso > 0 '
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');

        return $query->getQuery()->getResult();
    }

  public function obtenerDetalleEgresos($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();

        $query->select('g')
            ->from('App\Entity\Caja','g')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND g.concepto != 211 AND g.concepto != 209 AND g.egreso > 0'
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');

        return $query->getQuery()->getResult();
    }

  public function obtenerDetalleReinvertido($filtro){
        $fechaPrimerDia = DefaultController::toArray($filtro['desde']);
        $fechaPrimerDia = new \DateTime($fechaPrimerDia['Y'] .'-'. $fechaPrimerDia['m'] .'-'. $fechaPrimerDia['d']);
        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);
        $fechaUltimoDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();

        $query->select('g')
            ->from('App\Entity\Caja','g')
            ->add('where',
                '('.
                $query->expr()->between(
                    'g.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                ).
                ') AND (
                          g.concepto = 209
                      )'
            )
            ->setParameter('fechaInicio', $fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaUltimoDia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('g.id', 'DESC');

        return $query->getQuery()->getResult();
    }

  public function obtenerDetalleMorosos15Dias($filtro){
        return $this->obtenerDetalleMorosos($filtro,15);
    }

  public function obtenerDetalleMorosos30Dias($filtro){
        return $this->obtenerDetalleMorosos($filtro,30);
    }

  public function obtenerDetalleMorosos($filtro,$dias){
        /*
         *
            SELECT count(cr.id) as cantidad, sum(saldo) as total
            FROM credito AS cr
                 INNER JOIN cuota AS cu ON cr.id = cu.credito_id
            WHERE
                  cr.aprobado = 1
                  AND (
                         cu.fecha_vencimiento <  CAST('2020-05-01' AS DATE)
                         AND cu.fecha_vencimiento >  CAST('2020-04-15' AS DATE)
                         AND cu.saldo > 0
                  )
                  AND (
                        (
                        cr.cancelado = 1
                        AND cr.fecha_cancelacion <
                           CAST('2020-05-01' AS DATE)
                        )
                        OR
                        ( cr.cancelado = 0 )
                  )
            ;
        */


        $fechaUltimoDia = DefaultController::toArray($filtro['hasta']);

        $fecha15Dia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');
        $fecha15Dia->modify('-'.$dias.' day');
        $fechaPrimerDia = new \DateTime($fechaUltimoDia['Y'] .'-'. $fechaUltimoDia['m'] .'-'. $fechaUltimoDia['d'].' 23:59:59');

        $query = $this->_em->createQueryBuilder();
        //$query->select('count(c.id) AS cantidad')
        $query->select('c')
            ->from('App\Entity\Credito', 'c')
            ->innerJoin('c.cuotas', 'cu')
            ->andWhere('c.aprobado=1')
            ->add('where', ' ( 
                                ( '.
                $query->expr()->between(
                    'cu.fechaVencimiento',
                    ':fechaInicio',
                    ':fechaFin').
                ' AND cu.saldo > 0 
                                )
                                AND (
                                    c.cancelado=0 
                                    OR
                                    (
                                        c.cancelado=1 
                                        AND c.fechaCancelacion >  :fechaFin
                                    )
                              )
                           )
                            AND c.incobrable = 0'
            )
            ->groupBy('c.id')

            ->setParameter('fechaInicio', $fecha15Dia, \Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter('fechaFin',$fechaPrimerDia, \Doctrine\DBAL\Types\Type::DATETIME)

            ->orderBy('c.id', 'DESC');

        return ($query->getQuery()->getResult());
    }

  public function buscarCajaDesdeUltimoId($id){
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
          ->from('App\Entity\Caja','g')
          ->where('g.id >= '.$id)
          ->addOrderBy('g.id', 'ASC');
      return $query->getQuery()->getResult();
  }

  public function getTotalCaja($fecha){

    
    $query = $this->_em->createQueryBuilder();
    $query->select('g')
        ->from('App\Entity\Caja','g')
        ->select('g.total');
        $cadena ="g.fecha <='".DefaultController::toAnsiDate($fecha)." 23:59'";
        $query->where($cadena);
        $query->orderBy("g.id", "DESC")
        ->setMaxResults(1);
        
    return $query->getQuery()->getResult();
}
}
?>
