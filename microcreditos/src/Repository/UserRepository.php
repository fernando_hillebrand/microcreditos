<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL() {
    $query = $this->_em->createQuery('Select u from App\Entity\User u 
        where u.id>1 order by u.username' );
    return $query;
  }

}
?>
