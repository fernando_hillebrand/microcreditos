<?php

namespace App\Repository;
use App\Controller\DefaultController;
use Doctrine\ORM\EntityRepository;

class GastosRepository extends EntityRepository
{
  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByMyCriteriaDQL($filtro=null,$tipo='L') {
      $query = $this->_em->createQueryBuilder();
      $query->select('g')
            ->from('App\Entity\Gastos','g')
              ->innerJoin('g.concepto', 'co')
              ->innerJoin('g.programa', 'p')
            ->orderBy('g.fecha');  
      // Es reporte impreso, no se muestra la entrega a Emilio Marchi
     /* if($tipo=='R'){
          $query->where('co.boleano=0');
      }*/
    if($filtro['desde']){
          $cadena = " g.fecha >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
          $query->andWhere($cadena);
      }
      if($filtro['hasta']){
          $cadena = " g.fecha <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
          $query->andWhere($cadena);
      }
      if($filtro['concepto']){
          $cadena = " co.id = '".$filtro['concepto']."'";
          $query->andWhere($cadena);
      }
      if($filtro['programa']){
          $cadena = " p.id = '".$filtro['programa']."'";
          $query->andWhere($cadena);
      }
      $query->orderBy('g.id', 'DESC');
    return $query->getQuery();
  }

  public function all($filtro=null,$tipo='L') {
    $query = $this->_em->createQueryBuilder();
    $query->select('g')
          ->from('App\Entity\Gastos','g')
            ->innerJoin('g.concepto', 'co')
            ->innerJoin('g.programa', 'p')
          ->orderBy('g.fecha');  
 
          return $query->getQuery()->getResult();
}

  public function getEstadoResultadosGastos($month, $year){

 
     $query = $this->_em->createQueryBuilder();
     $query->from('App\Entity\Gastos', 'g');       
   

     $query->select('SUM(g.importe) as importe', 'MONTH(g.fecha) as mes','YEAR(g.fecha) as year')
     ->andWhere('MONTH(g.fecha)='.$month.'')
     ->andWhere('YEAR(g.fecha) = '.$year.'')  
     ->groupBy('year','mes');
    
     
  return $query->getQuery()->getResult();
}
  
}
?>
