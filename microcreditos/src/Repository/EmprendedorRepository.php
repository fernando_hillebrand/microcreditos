<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use App\Controller\DefaultController;

class EmprendedorRepository extends EntityRepository
{
    public function findByMyCriteria()
    {
        return $this->findByMyCriteriaDQL()->getResult();
    }

    public function findByMyCriteriaDQL()
    {
        $query = $this->_em->createQuery('Select e from App\Entity\Emprendedor e order by e.apellido,e.nombres');
        return $query;
    }

    public function findAllByTerm($term)
    {
        $query = $this->_em->createQuery("Select e.id,e.apellido,e.nombres, e.dni, e.cbu from App\Entity\Emprendedor e
          where (e.dni like '%" . $term . "%' OR e.nombres like '%" . $term . "%' OR e.apellido like '%" . $term . "%' OR CONCAT(CONCAT(e.apellido, ', '), e.nombres) like '%" . $term . "%') AND e.activo=1 order by e.apellido, e.nombres")
            ->setMaxResults(20);

        return $query->getScalarResult();
    }

    public function findByMyFiltroDQL($filtro = null)
    {
        $DQLFiltro = $this->armarFiltroDQL($filtro);
        $DQL = 'Select e from App\Entity\Emprendedor e LEFT JOIN e.grupo g LEFT JOIN e.programa p ';
        $DQLOrder = ' ORDER BY e.codigo ';
        $DQLFiltro = ($DQLFiltro == '') ? '' : ' WHERE ' . $DQLFiltro;
        $query = $this->_em->createQuery($DQL . ' ' . $DQLFiltro . ' ' . $DQLOrder);

        return $query;
    }

    protected function armarFiltroDQL($filtro)
    {
        $cadenaFiltro = '';
        if ($filtro['desde']) {
            $cadenaFiltro = " e.fechaAlta >= '" . DefaultController::toAnsiDate($filtro['desde']) . " 00:00'";
        }
        if ($filtro['hasta']) {
            $cadena = " e.fechaAlta <= '" . DefaultController::toAnsiDate($filtro['hasta']) . " 23:59'";
            $cadenaFiltro = ($cadenaFiltro == '') ? $cadena : $cadenaFiltro . ' AND ' . $cadena;
        }
        if ($filtro['programa']) {
            $cadena = " p.id = '" . $filtro['programa'] . "'";
            $cadenaFiltro = ($cadenaFiltro == '') ? $cadena : $cadenaFiltro . ' AND ' . $cadena;
        }
        if ($filtro['emprendedorId']) {
            $cadena = " e.id = '" . $filtro['emprendedorId'] . "'";
            $cadenaFiltro = ($cadenaFiltro == '') ? $cadena : $cadenaFiltro . ' AND ' . $cadena;
        }
        if ($filtro['grupoId']) {
            $cadena = ' g.id = ' . $filtro['grupoId'];
            $cadenaFiltro = ($cadenaFiltro == '') ? $cadena : $cadenaFiltro . ' AND ' . $cadena;
        }
        return $cadenaFiltro;
    }

    public function checkUniqueDNI($dni, $emprendedorId = null)
    {
        $DQL = 'Select e from App\Entity\Emprendedor e WHERE e.dni = ' . $dni;
        if ($emprendedorId) {
            $DQL .= ' AND e.id !=' . $emprendedorId;
        }
        $query = $this->_em->createQuery($DQL);

        return count($query->getResult());
    }

    public function getCreditosActivosByGaranteDni($dni, $creditoId = null)
    {
        $creditosReturn = array();
        //Buscar emprendedor por DNI
        $query = $this->_em->createQueryBuilder();
        $query->select('e')
            ->from('App\Entity\Emprendedor', 'e')
            ->where('e.dni = ' . $dni);
        $emprendedor = $query->getQuery()->getOneOrNullResult();
        //Verificar si esta en un credito activo
        if ($emprendedor) {
            $creditos = $emprendedor->getCreditos();
            foreach ($creditos as $credito) {
                $status = $credito->getStatus();
                if ($status == 'Moroso' or $status == 'Al dia' or $status == 'Solicitud Aprobada sin pago') {
                    $creditosReturn[$credito->getId()] = $credito->getTitle() . ' como Emprendedor con Estado: ' . $status;
                }
            }
        }

        //verificar si es garante de otro credito Activo
        $query = $this->_em->createQueryBuilder();
        $query->select('ce')
            ->from('App\Entity\CreditoEmprendedor', 'ce')
            ->where('ce.garanteDni = ' . $dni)
            ->orderBy('ce.credito', 'DESC');
        $creditosEmprendedor = $query->getQuery()->getResult();
        $creditoIdAnterior = 0;
        //Verificar si esta en un credito activo como garante
        foreach ($creditosEmprendedor as $creditoEmprendedor) {
            $credito = $creditoEmprendedor->getCredito();
            if ($credito->getId() != $creditoIdAnterior) {
                $creditoIdAnterior = $credito->getId();
                $status = $credito->getStatus();
                if ($status == 'Moroso' or $status == 'Al dia' or $status == 'Solicitud Aprobada sin pago') {
                    $creditosReturn[$credito->getId()] = $credito->getTitle() . ' como Garanate con Estado: ' . $status;
                }
            }
        }
        return $creditosReturn;
    }

    public function getEmprendedorbyCuit($cuit)
    {
        $query = $this->_em->createQueryBuilder();
        $query->select('e')
            ->from('App\Entity\Emprendedor', 'e')
            ->where('e.cuit = ' . $cuit)
            ->setMaxResults(1); // Limit the results to 1
        $emprendedor = $query->getQuery()->getResult();
        return $emprendedor;
    }

}
?>

