<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

class ParametroRepository extends EntityRepository
{
  public function findByMyCriteria() {
    return $this->findByMyCriteriaDQL()->getResult();
  }

  public function findByAgrupadorDQL($agrupador) {
    $query = $this->_em->createQuery('Select p from App\Entity\Parametro p
        where p.boleano=0 and p.activo=1 and p.agrupador_id='.$agrupador->getId() );
    return $query;
  } 

  public function findByAgrupador($agrupador) {
    $query = $this->_em->createQuery('Select p from App\Entity\Parametro p
        where p.boleano=0 and p.activo=1 and p.agrupador_id='.$agrupador );
    return $query;
  } 
  
  public function getAgrupadores() {
    $query = $this->_em->createQuery('Select g from App\Entity\Parametro g 
        where g.boleano=0 and g.activo=1 and g.agrupador_id is null order by g.nombre' );
    return $query->getResult();
  }
  
  public function getAgrupadoresLocalizacion() {
    $query = $this->_em->createQuery('Select g from App\Entity\Parametro g 
        where g.boleano=1 and g.activo=1 and g.agrupador_id is null' );
    return $query->getResult();
  }

  public function findByAgrupadoryPadre($agrupador,$padreId) {
    $padre_sql = '';
    if($padreId){
        $padre_sql = ' and p.padre_id='.$padreId;
    }
    $query = $this->_em->createQuery('Select p from App\Entity\Parametro p
        where p.boleano=1 and p.activo=1 and p.agrupador_id='.$agrupador->getId().$padre_sql );
    return $query;
  } 
  
  public function findPadres($padre){
      $query = $this->_em->createQuery('Select p from App\Entity\Parametro p
          where p.boleano=1 and p.activo=1 and p.padre_id='.$padre);
      return $query;
  }
  
  public function findByAgrupadorLocalizacion($agrupador) {
    $query = $this->_em->createQuery('Select p from App\Entity\Parametro p
        where p.boleano=1 and p.activo=1 and p.agrupador_id='.$agrupador->getId() );
    return $query;
  } 
 
  public function getUltimosNumeros(){
      $agrupador = $this->findOneByDescripcion('ultimo-nro');
      $query = $this->_em->createQuery("Select p from App\Entity\Parametro p
          where p.agrupador_id=".$agrupador->getId());
      return $query->getResult();
  }
  public function getRepresentante(){
      $agrupador = $this->findOneByDescripcion('representante');
      $query = $this->_em->createQuery("Select p from App\Entity\Parametro p
          where p.agrupador_id=".$agrupador->getId());
      return $query->getOneOrNullResult();
  }

  public function getConceptosMovimientosCaja(){
      $agrupador = $this->findOneByDescripcion('conceptos-caja');
      $agrupador2 = $this->findOneByDescripcion('automaticos-sistema');
      $query = $this->_em->createQuery("Select p from App\Entity\Parametro p
          where p.agrupador_id=".$agrupador->getId()." OR p.agrupador_id=".$agrupador2->getId()." order by p.numerico DESC,p.nombre ");
      return $query->getResult();
  }
  public function getConceptosCaja(){
      $agrupador = $this->findOneByDescripcion('conceptos-caja');
      $query = $this->_em->createQuery("Select p from App\Entity\Parametro p
          where p.agrupador_id=".$agrupador->getId()." order by p.numerico ASC,p.nombre ");
      return $query->getResult();
  }

  public function getUltimoNroDQL($entidad) {
    $query = $this->_em->createQuery("Select p from App\Entity\Parametro p
         where p.nombre='".$entidad."'" );
    return $query->getSingleResult();
  }

  public function conflictoRangoGastosAdm($p, $d, $h){
    $query = $this->_em->createQuery("Select tg from App\Entity\TablaGastos tg
         where tg.desde>".$d." and tg.hasta<".$h." and tg.periodo= ".$p );
    return $query->getResult();
  }
 
  public function findFechas(){
      $query = $this->_em->createQuery("Select f.fecha from App\Entity\TablaFeriados f
         order by f.fecha" );
    return $query->getArrayResult();
  }

  public function findByFechas($fechaVencimiento, $fechaHoy){
      $query = $this->_em->createQuery("SELECT f.fecha 
                                        FROM App\Entity\TablaFeriados f 
                                        WHERE f.fecha BETWEEN '".$fechaVencimiento ."' AND '".$fechaHoy."'
                                        ORDER BY f.fecha" );
      return $query->getArrayResult();
  }
}
