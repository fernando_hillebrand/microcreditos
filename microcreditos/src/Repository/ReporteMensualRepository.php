<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class ReporteMensualRepository extends EntityRepository {

    /* Datos Generales */
    public function findByPeriodo($filtro) {
        $query = $this->_em->createQueryBuilder($filtro);
        $query->select('r')
                ->from('App\Entity\ReporteMensual', 'r')
                ->where(" r.mes = " . $filtro['mes'])
                ->andWhere(" r.anio = " . $filtro['anio']);
        return $query->getQuery()->getResult();
    }
    
    /* Datos por periodo */
    public function getCarteraByPrograma($filtro){
        $data = array();
        $data['activa'] = $this->getDataByProgramaActiva($filtro);
        $data['renovacion'] = $this->getDataByProgramaOtros($filtro, '>0');
        $data['nueva'] = $this->getDataByProgramaOtros($filtro, '=0');
        $data['cobranzas'] = $this->getCobranzaByPrograma($filtro);
        $data['incobrables'] = $this->getIncobrablesByPrograma($filtro);
        $data['intereses'] = $this->getInteresesByPrograma($filtro);
        $data['gastosAdm'] = $this->getGastosAdmByPrograma($filtro);
        $data['punitorios'] = $this->getPunitoriosByPrograma($filtro);
        $data['gastos'] = $this->getGastosByPrograma($filtro);
        $data['honorarios'] = $this->getHonorariosByPrograma($filtro); 
        //die;
        return $data;
    }

    public function findByProgramayPeriodo($filtro) {
        $query = $this->_em->createQueryBuilder();
        $query->select('r')
                ->from('App\Entity\ReporteMensual', 'r')
                ->innerJoin('r.programa', 'p')
                ->where(" r.mes = " . $filtro['mes'])
                ->andWhere(" r.anio = " . $filtro['anio'])
                ->andWhere(" p.id = ".$filtro['programaId'] );
        
        return $query->getQuery()->getOneOrNullResult();
    }

    /* Detalle */
    public function getDetalleByProgramayPeriodo($filtro){
        $periodo = $this->getPeriodo($filtro);
        /* Activa */
        $query = $this->_em->createQueryBuilder();
        $cantCtas= $this->_em->createQueryBuilder();
        if($filtro['programaId']==5){
          $query->select(' DISTINCT c.id, c.nroCredito, c.nroRenovacion, c.montoCredito, c.cantCuotas, CONCAT(CONCAT(e.apellido, \', \'),e.nombres) as grupo')
                ->from('App\Entity\Credito', 'c')
                ->innerJoin('c.programa', 'p')
                ->innerJoin('c.emprendedores', 'es')
                ->innerJoin('es.emprendedor', 'e')
                ->innerJoin('c.cuotas', 'cu')
                ->where(" cu.fechaVencimiento >= '" . $periodo['ini'] . "' AND "." cu.fechaVencimiento <= '" . $periodo['fin'] . "'")
                ->orWhere(" c.fechaEntrega >= '" . $periodo['ini'] . "' AND ". " c.fechaEntrega <= '" . $periodo['fin'] . "'")
                ->andWhere(' p.id=' . $filtro['programaId'])
                ->andWhere(' c.aprobado = 1 ')
                ->andWhere(' c.incobrable = 0 ')
                ->orderBy(" e.apellido,e.nombres ");    
        }else{
          $query->select(' DISTINCT c.id, c.nroCredito, c.nroRenovacion, c.montoCredito, c.cantCuotas, g.nombre as grupo')
                ->from('App\Entity\Credito', 'c')
                ->innerJoin('c.programa', 'p')
                ->leftJoin('c.grupo', 'g')
                ->innerJoin('c.cuotas', 'cu')
                ->where(" cu.fechaVencimiento >= '" . $periodo['ini'] . "' AND "." cu.fechaVencimiento <= '" . $periodo['fin'] . "'")
                ->orWhere(" c.fechaEntrega >= '" . $periodo['ini'] . "' AND ". " c.fechaEntrega <= '" . $periodo['fin'] . "'")
                ->andWhere(' p.id=' . $filtro['programaId'])
                ->andWhere(' c.aprobado = 1 ')
                  ->andWhere(' c.incobrable = 0 ')
                ->orderBy(" g.nombre ");
        }
        $result = $query->getQuery()->getArrayResult();
        $detalle = array();
        $detalles = array();
        foreach ($result as $res){
            $detalle['grupo'] = $res['grupo'];
            $detalle['credito'] = $res['nroCredito'].'-'.$res['nroRenovacion'];
            /*Inicial*/
            $cantCtas = $this->_em->createQuery("select COUNT(cu.id) 
            from App\Entity\Cuota cu
            where cu.credito= " .$res['id']. " and (cu.cancelado = 0 OR cu.fechaCancelacion > '". $periodo['ini'] . "')");
            $detalle['cai']['ctas']= $cantCtas->getSingleScalarResult();
            $activa = $this->getDataByProgramaActiva($filtro, $res['id']);
            $nueva = $this->getDataByProgramaOtros($filtro, '=0', $res['id']);
            $renov = $this->getDataByProgramaOtros($filtro, '>0', $res['id']);
            //$cai = ($activa==0) ? (($nueva==0) ? $renov : $nueva) : $activa;
            $detalle['cai']['montoActiva']=($activa==0) ? $nueva : $activa;
            $detalle['cai']['montoRenov']=$renov;
            /* pagos */
            $pagos = $this->getDetalleInteresesByPrograma($filtro, $res['id']);
            $detalle['caf']['ctas'] = $detalle['cai']['ctas'];
            $detalle['caf']['monto'] = $detalle['cai']['montoActiva']+$detalle['cai']['montoRenov'];
            for($i = 1; $i <= 5; ++$i) {
                $detalle['semanas'][$i]['capital']=0;
                $detalle['semanas'][$i]['intereses']=0;
                $detalle['semanas'][$i]['total']=0;
                if($pagos[$i]){
                    $pagoCap=0;
                    $pagoInt=0;
                    $pagoTot=0;
                    foreach($pagos[$i] as $pag){
                        $pagoCap = $pagoCap+$pag['capital'];
                        $pagoInt = $pagoInt+$pag['intereses'];
                        $pagoTot = $pagoTot+$pag['total'];
                    }
                    $detalle['semanas'][$i]['capital']=$pagoCap;
                    $detalle['semanas'][$i]['intereses']=$pagoInt;
                    $detalle['semanas'][$i]['total']=$pagoTot;
                    $detalle['caf']['ctas'] = $detalle['caf']['ctas'] - 1;
                    $detalle['caf']['monto'] = $detalle['caf']['monto'] - $pagoCap;
                }
            }
            array_push($detalles, $detalle);
        }
        return $detalles;
        
    }

    public function getDataByProgramaActiva($filtro, $cred = null) {
        $periodo = $this->getPeriodo($filtro);
        $result = 0;

        // buscar cartera inicial guardada
        $q1 = $this->_em->createQuery("select MAX(rm.id) from App\Entity\ReporteMensual rm
          where rm.programa=" . $filtro['programaId']." AND rm.fecha < '". $periodo['fin'] ."'");
        $queryCartera = $this->_em->createQueryBuilder();
        $queryCartera->select('r')
                ->from('App\Entity\ReporteMensual', 'r')
                ->innerJoin('r.programa', 'p')
                ->where(" p.id = " . $filtro['programaId'])
                ->andWhere(" r.id = ( " . $q1->getDQL() . ")");

        $reporte = $queryCartera->getQuery()->getOneOrNullResult();
        if ($reporte and is_null($cred)) {
            //busca el reporte anterior
            $result = ($reporte->getCarteraActiva() + $reporte->getCarteraNueva() + $reporte->getRenovaciones()) - ($reporte->getCobranzas() + $reporte->getIncobrables());
        } else {
            /// no hay resumen anterior
            $query = $this->_em->createQueryBuilder();
            $query->select(' c.id, c.montoCredito')
                    ->from('App\Entity\Credito', 'c')
                    ->innerJoin('c.programa', 'p')
                    ->where(" c.fechaEntrega < '" . $periodo['ini'] . "'")

                    ->andWhere(" c.aprobado = 1 ")
                    ->andWhere(" c.incobrable = 0 ");
            // VER SI HACE FALTA TOMAR LA FECHA TAMBIEN      
            if ($filtro['programaId'] > '0') {
                $query->andWhere(' p.id=' . $filtro['programaId']);
            }
            if (!is_null($cred)) {
                $query->andWhere(" c.id = " . $cred);
            }
            $array = $query->getQuery()->getArrayResult();
            foreach ($array as $reg) {
                $queryPagos = $this->_em->createQueryBuilder();
                $queryPagos->select(' SUM(pa.capital) as pago ')
                        ->from('App\Entity\Pago', 'pa')
                        ->innerJoin('pa.cuota', 'cu')
                        //->where(" cu.fechaVencimiento <= '" . $periodo['fin'] . "'")
                        ->where(" pa.fecha < '" . $periodo['ini'] . "'")
                        ->andWhere(" cu.credito = " . $reg['id']);
                $pagos = $queryPagos->getQuery()->getScalarResult();
                $result = $result + ($reg['montoCredito'] - ($pagos ? $pagos[0]['pago'] : 0));
            }
        }

        return $result;
    }

    public function getDataByProgramaOtros($filtro,$tipo, $cred=null){
        $periodo = $this->getPeriodo($filtro);
        $query = $this->_em->createQueryBuilder();
      
        $query->select(' SUM(c.montoCredito) as dato')
              ->from('App\Entity\Credito', 'c')
              ->innerJoin('c.programa', 'p')
              ->where(" c.fechaEntrega >= '" . $periodo['ini'] . "'")
              ->andWhere(" c.fechaEntrega <= '" . $periodo['fin'] . "'")
              ->andWhere(" c.nroRenovacion ". $tipo )
              ->andWhere(" c.aprobado = 1 ")
             ->andWhere(" c.incobrable = 0 ");   
        if ($filtro['programaId'] > '0') {
            $query->andWhere(' p.id=' . $filtro['programaId']);
        }   
        if( ! is_null($cred) ){
            $query->andWhere(" c.id = ".$cred);
        }
       // var_dump($query->getQuery()->getSQL());die;
        $value = $query->getQuery()->getScalarResult();
       
        return $value ? $value[0]['dato'] : 0; 
    }
    
    public function getCobranzaByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);
        
        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(p.capital) AS dato')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(" p.fecha bete :fechaInicio")
//                ->andWhere(" p.fecha <= :fechaFin")
                ->andWhere(" p.reporte is null ")
                ->add('where', $query->expr()->between(
                    'p.fecha',
                    ':fechaInicio',
                    ':fechaFin'
                    )
                )
                ->setParameter('fechaInicio', new \DateTime($periodo['ini']), \Doctrine\DBAL\Types\Type::DATETIME)
                ->setParameter('fechaFin',new \DateTime($periodo['fin']), \Doctrine\DBAL\Types\Type::DATETIME);
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' c.programa=' . $filtro['programaId']);
        }        
//$query->andWhere('c.id=31');     
        return $query->getQuery()->getSingleScalarResult();
    }

    public function getIncobrablesByPrograma($filtro) {
        $periodo = $this->getPeriodo($filtro);
        $result = 0;
        $query = $this->_em->createQueryBuilder();
            $query->select(' c.id, c.montoCredito')
                    ->from('App\Entity\Credito', 'c')
                    ->innerJoin('c.programa', 'p')
                    ->where(" c.fechaEntrega < '" . $periodo['ini'] . "'")
                    ->andWhere(" c.aprobado = 1 ")
                    ->andWhere(" c.incobrable = 1 ")
                    ->andWhere(" c.fechaIncobrable >= '" . $periodo['ini'] . "'")
                    ->andWhere(" c.fechaIncobrable <= '" . $periodo['fin'] . "'");
            // VER SI HACE FALTA TOMAR LA FECHA TAMBIEN      
            if ($filtro['programaId'] > '0') {
                $query->andWhere(' p.id=' . $filtro['programaId']);
            }
            
            $array = $query->getQuery()->getArrayResult();
            foreach ($array as $reg) {
                $queryPagos = $this->_em->createQueryBuilder();
                $queryPagos->select(' SUM(pa.capital) as pago ')
                        ->from('App\Entity\Pago', 'pa')
                        ->innerJoin('pa.cuota', 'cu')
                        //->where(" cu.fechaVencimiento <= '" . $periodo['fin'] . "'")
                        ->where(" pa.fecha < '" . $periodo['ini'] . "'")
                        ->andWhere(" cu.credito = " . $reg['id']);
                $pagos = $queryPagos->getQuery()->getScalarResult();
                $result = $result + ($reg['montoCredito'] - ($pagos ? $pagos[0]['pago'] : 0));
            }

        return $result;
    }

    public function getPagosReporte($filtro){
        $periodo = $this->getPeriodo($filtro);
        $query = $this->_em->createQueryBuilder();
        $query->select('p')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(" p.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" p.fecha <= '" . $periodo['fin'] . "'");
//                ->andWhere(" p.reporte is null ");
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' c.programa=' . $filtro['programaId']);
        }
        return $query->getQuery()->getResult();        
    }

    public function setReporteMensualMasive($filtro,$reporteId){
        $pagos = $this->getPagosReporte($filtro);

        $id = array();
        foreach($pagos as $pago){
            $id[] = $pago->getId();
        }

        $q = $this->_em->createQuery('update App\Entity\Pago m set m.reporte='.$reporteId.' WHERE m.id IN (:id) ');
        $q->setParameter('id', $id);
        $numUpdated = $q->execute();
        return $numUpdated;
    }

    public function getGastosAdmByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);

        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(p.gastoAdministrativo) AS dato')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(" p.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" p.fecha <= '" . $periodo['fin'] . "'") ;
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' c.programa=' . $filtro['programaId']);
        }
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getPunitoriosByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);

        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(p.punitorios) AS dato')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(" p.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" p.fecha <= '" . $periodo['fin'] . "'") ;
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' c.programa=' . $filtro['programaId']);
        }
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getInteresesByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);

        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(p.intereses) AS dato')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c');
               /* ->where(" p.fecha > '" . $periodo['ini'] . "'")
                ->andWhere(" p.fecha <= '" . $periodo['fin'] . "'") ;*/
        if ($filtro['programaId'] != '0') {
            $query->where(' c.programa=' . $filtro['programaId']);
        }
//$query->andWhere('c.id=31');       

        $intereses = array();
        $ini = $periodo['ini'];
        for($i = 1; $i <= 5; ++$i) {
            if($ini<$periodo['int'.$i]){   
            $query2 = clone $query;
            $query2->andWhere(" p.fecha >= '" . $ini . "'")
                ->andWhere(" p.fecha <= '" . $periodo['int'.$i] . "'") ; 
            $intereses[$i] = $query2->getQuery()->getSingleScalarResult();
            $ini = $periodo['int'.$i];
            }
        }
        return $intereses;
    }
    
    public function getGastosByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);
        
        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(g.importe) AS dato')
                ->from('App\Entity\Gastos', 'g')
                ->innerJoin('g.concepto', 'c')
                ->where("c.descripcion='E'")
               // ->andWhere('c.boleano=0')
                ->andWhere('c.numerico=0')
                ->andWhere(" g.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" g.fecha <= '" . $periodo['fin'] . "'") ;
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' g.programa=' . $filtro['programaId']);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function getHonorariosByPrograma($filtro){
        $periodo = $this->getPeriodo($filtro);
        
        $query = $this->_em->createQueryBuilder();
        
        $query->select('SUM(g.importe) AS dato')
                ->from('App\Entity\Gastos', 'g')
                ->innerJoin('g.concepto', 'c')
                ->where("c.descripcion='E'")
                //->andWhere('c.boleano=0')
                ->andWhere('c.numerico=1')
                ->andWhere(" g.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" g.fecha <= '" . $periodo['fin'] . "'") ;
        if ($filtro['programaId'] != '0') {
            $query->andWhere(' g.programa=' . $filtro['programaId']);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    public function getSaldoCaja($filtro){
        $periodo = $this->getPeriodo($filtro);
        $creditos = $this->_em->createQueryBuilder();
        $creditos->select('SUM(c.montoCredito) AS creditos')
                ->from('App\Entity\Credito', 'c')
                ->innerJoin('c.programa', 'p')
                ->where(" c.fechaEntrega >= '" . $periodo['ini'] . "'")
                ->andWhere(" c.fechaEntrega <= '" . $periodo['fin'] . "'")
                ->andWhere(" c.aprobado = 1 ")
                ->andWhere(' p=' . $filtro['programaId']);
        
        $pagos = $this->_em->createQueryBuilder();
        $pagos->select('SUM(p.total) AS pagos ')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(" p.fecha >= '" . $periodo['ini'] . "'")
                ->andWhere(" p.fecha <= '" . $periodo['fin'] . "'") 
                ->andWhere(' c.programa=' . $filtro['programaId']);
        $saldoCaja = $pagos->getQuery()->getSingleScalarResult()  
                     - ($this->getGastosByPrograma($filtro) + $this->getHonorariosByPrograma($filtro) ) 
                     - $creditos->getQuery()->getSingleScalarResult() ;
        return $saldoCaja;
    }

    private function getDetalleInteresesByPrograma($filtro,$cred){
        $periodo = $this->getPeriodo($filtro);
        $query = $this->_em->createQueryBuilder();
        $query->select('p.capital, p.intereses, p.total')
                ->from('App\Entity\Pago', 'p')
                ->innerJoin('p.cuota', 'cu')
                ->innerJoin('cu.credito', 'c')
                ->where(' c.programa=' . $filtro['programaId'])
                ->andWhere(' c.id= '.$cred);
        $intereses = array();
        $ini = $periodo['ini'];
        for($i = 1; $i <= 5; ++$i) {
            if($ini<$periodo['int'.$i]){    
            $query2 = clone $query;
            $query2->andWhere(" p.fecha >= '" . $ini . "'")
                ->andWhere(" p.fecha <= '" . $periodo['int'.$i] . "'") ; 
            $intereses[$i] = $query2->getQuery()->getArrayResult();
            $ini = $periodo['int'.$i];
            }
        }
        return $intereses;
    }

    /* Funciones Generales */
    private function getPeriodo($filtro){
        $part = $filtro['anio'] . '-' . str_pad($filtro['mes'], 2, '0', STR_PAD_LEFT);
        $ult = date("d",(mktime(0,0,0,$filtro['mes']+1,1,$filtro['anio'])-1));
        
        $intereses = array();
        $dia = 1;
        for($i = 1; $i <= 5; ++$i) {
            $fecha = date("Y-m-d", strtotime ("next Saturday",mktime(0,0,0,$filtro['mes'],$dia,$filtro['anio'])));
            if($fecha > $part.'-'.$ult ){
                $fecha = $part.'-'.$ult;
            }
            array_push($intereses, $fecha );
            $dia = $dia + 7 ;
        } 
        $periodo = array('ini'=> $part.'-01 00:00',
                         'fin'=> $part.'-'.$ult.' 23:59',
                         'int1'=> $intereses[0],
                         'int2'=> $intereses[1],
                         'int3'=> $intereses[2],
                         'int4'=> $intereses[3],
                         'int5'=> $intereses[4]);
        return $periodo;
    }
    
    public function getFirstCAI($programa){
        $query = $this->_em->createQueryBuilder();
        $query->select('pa.numerico as valor')
                ->from('App\Entity\Parametro', 'pa')
                ->where(" pa.nombre ='" . trim($programa) . "'")
                ->andWhere(" pa.descripcion = 'CAI'") ;
        $value = $query->getQuery()->getScalarResult();
        return $value ? $value[0]['valor'] : 0;        
    }
    
    public function reporteCerrado($credito,$fechaPago=null){
        if($fechaPago)
            $fecha = $fechaPago;
        else
            $fecha = $credito->getFechaEntrega();
        $query = $this->_em->createQueryBuilder();
        $query->select('count(r)')
              ->from('App\Entity\ReporteMensual', 'r')
              ->innerJoin('r.programa', 'p')
              ->where('p.id='.$credito->getPrograma()->getId())
              ->andWhere('r.mes='.$fecha->format('m'))
              ->andWhere('r.anio='.$fecha->format('Y'))  ;
        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function getReportePrevio($id,$prog){
        $query = $this->_em->createQueryBuilder();
        $query->select('max(r.id)')
              ->from('App\Entity\ReporteMensual', 'r')
              ->innerJoin('r.programa', 'p')
              ->where('p.id='.$prog)
              ->andWhere('r.id <> '.$id)  ;
        $ant =  $query->getQuery()->getSingleScalarResult();
        return $this->find($ant);
    }
}