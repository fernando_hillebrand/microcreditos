<?php
namespace App\Repository;
use Doctrine\ORM\EntityRepository;

class GruposEmprendedoresRepository extends EntityRepository
{
    public function findByMyCriteria() {
        return $this->findByMyCriteriaDQL()->getResult();
    }

    public function findByMyCriteriaDQL() {
        $query = $this->_em->createQuery('Select g from GruposEmpredendores g' );
        return $query;
    }
}