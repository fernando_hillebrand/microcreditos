<?php

namespace App\Repository;

use App\Entity\GastosTotales;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use App\Controller\DefaultController;

/**
 * @extends ServiceEntityRepository<GastosTotales>
 *
 * @method GastosTotales|null find($id, $lockMode = null, $lockVersion = null)
 * @method GastosTotales|null findOneBy(array $criteria, array $orderBy = null)
 * @method GastosTotales[]    findAll()
 * @method GastosTotales[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GastosTotalesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GastosTotales::class);
    }

    
    public function getAll() {
   
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\GastosTotales', 'g')
        ->innerJoin('g.concepto', 'c');
        $query->select('g');
        
         return $query->getQuery()->getResult();
      }


    public function findByMyCriteriaDQL($filtro=null) {
        $query = $this->_em->createQueryBuilder();
        
        $query->from('App\Entity\GastosTotales', 'g')
              ->innerJoin('g.concepto', 'c');
              $query->select('g');       
  
   
      if(isset($filtro['desde']) AND $filtro['desde'] != '' ){
        $cadena = " g.fecha >= '".DefaultController::toAnsiDate($filtro['desde'])." 00:00'";
        $query->andWhere($cadena);
      }
      if(isset($filtro['hasta']) AND $filtro['hasta'] != '' ){
        $cadena = " g.fecha <= '".DefaultController::toAnsiDate($filtro['hasta'])." 23:59'";
        $query->andWhere($cadena);
      }
      if(isset($filtro['concepto']) AND $filtro['concepto'] > 0 ){
        $cadena = " c.id = '".$filtro['concepto']."'";
        $query->andWhere($cadena);
      }
    
  
        $query->orderBy('g.id','desc');
      return $query->getQuery();
    }


      public function getConceptoId($id) {
   
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\GastosTotales', 'g')
        ->where('g.concepto='.$id.'');
        $query->select('g');
        
         return $query->getQuery()->getResult();
      }


      public function getEstadoResultadoGastosTotales($month, $year) {
        
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\GastosTotales', 'g')
        ->select('g')
        ->innerJoin('g.concepto', 'c')
        ->Where('MONTH(g.fecha)='.$month.'')
        ->andWhere('YEAR(g.fecha) = '.$year.'')
        ->addSelect( 'SUM(g.monto) as total', 'c')
        //$query->select('g');
        ->groupBy('g.concepto');
        
         return $query->getQuery()->getResult();     
      }

      public function getEstadoResultadoGastosTotalesSum($month, $year) {
        
        $query = $this->_em->createQueryBuilder();
        $query->from('App\Entity\GastosTotales', 'g')
        ->select('g')
        ->Where('MONTH(g.fecha)='.$month.'')
        ->andWhere('YEAR(g.fecha) = '.$year.'')
        ->addSelect( 'SUM(g.monto) as total');
        
       
         return $query->getQuery()->getResult();     
      }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(GastosTotales $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(GastosTotales $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return GastosTotales[] Returns an array of GastosTotales objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GastosTotales
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
