<?php

namespace App\Controller;

use App\Entity\ConceptoGastos;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ConceptoGastosType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class ConceptogastoController extends Controller
{
    /**
     * @Route("/conceptogasto", name="app_conceptogasto")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

      

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:ConceptoGastos')->getAll();

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('ConceptoGasto/index.html.twig', array(
                        'entityName'   => 'conceptoGastos',
                        'entities'     => $entities,
                        'paginator'    => $paginator,
        ));
    }

    public function newAction()
    {
        $entity = new ConceptoGastos();
        $form   = $this->createForm(ConceptoGastosType::class, $entity);

        return $this->render('ConceptoGasto/conceptoNew.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        )); 
    }


    public function createAction()
    {
        $entity  = new ConceptoGastos();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(ConceptoGastosType::class, $entity);

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El nuevo concepto se ha creado con éxito');
                return $this->redirect($this->generateUrl('concepto_index'));
            }
        }
        

        return $this->render('ConceptoGasto/conceptoNew.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }


    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:ConceptoGastos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el registro de concepto de gastos que está buscando.');
        }

        //dd($entity);

        $editForm = $this->createForm(ConceptoGastosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ConceptoGasto/conceptoNew.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:ConceptoGastos')->find($id);

                $comprobar = $em->getRepository('App:GastosTotales')->getConceptoId($id);

                if($comprobar){
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', 'El concepto no se puede eliminar. Esta asociado a un registro');
                    return $this->redirect($this->generateUrl('concepto_index'));
                }



                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Concepto  que está buscando.');
                }
                try {
                    $em->remove($entity);
                    $em->flush();
                } catch (\PDOException $e) {
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', 'El concepto no se puede eliminar');
                    //return $this->redirect($this->generateUrl('gastos_totales_edit', array('id' => $id)));
                    return $this->redirect($this->generateUrl('concepto_index'));
                }

                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El concepto  se ha eliminado con éxito');
            }
        }
        return $this->redirect($this->generateUrl('concepto_index'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }


    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:ConceptoGastos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Concepto que está buscando.');
        }

        $editForm   = $this->createForm( ConceptoGastosType::class, $entity);
      

        $request = $this->container->get('request_stack')->getCurrentRequest();

     
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('concepto_index'));
            }
        }

        return $this->render('ConceptoGasto/conceptoNew.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }

   
}
