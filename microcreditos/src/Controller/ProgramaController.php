<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Entity\Programa;
use App\Form\ProgramaType;

/**
 * Programa controller.
 *
 */
class ProgramaController extends Controller
{
    /**
     * Lists all Programa entities.
     *
     */
    public function indexAction()
    {
        $filtro = array(
            'itemsperpage' => 20
        );
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $varItems = ($filtro['itemsperpage']==0) ? 1000 : $filtro['itemsperpage'];
        $query =  $em->getRepository('App:Programa')->findByMyCriteriaDQL();

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('itemsperpage', 1), /*page number*/
            10 /*limit per page*/
        );

        $vars = array(  
            'entityName'   => 'programa',
            'entities'     => $entities,
            'paginator'    => $paginator);
       
        return $this->render('Programa/programaIndex.html.twig', $vars );
    }

    /**
     * Displays a form to create a new Programa entity.
     *
     */
    public function newAction()
    {
        $entity = new Programa();
        $form   = $this->createForm(ProgramaType::class, $entity);

        return $this->render('Programa/programaEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Programa entity.
     *
     */
    public function createAction()
    {
        $entity  = new Programa();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(ProgramaType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('programa_edit', array('id' => $entity->getId())));
            }
        }
        return $this->render('Programa/programaEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Programa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Programa de Fondos que está buscando.');
        }

        $editForm = $this->createForm(ProgramaType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Programa/programaEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Programa entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Programa de Fondos que está buscando.');
        }

        $editForm   = $this->createForm(ProgramaType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('programa_edit', array('id' => $id)));
            }
        }

        return $this->render('Programa/programaEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a Programa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Programa de Fondos que está buscando.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Programa/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }    
    
    /**
     * Deletes a Programa entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Programa')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Programa de Fondos que está buscando.');
                }
                try {
                    $em->remove($entity);
                    $em->flush();
                } catch (\PDOException $e) {
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', 'El Programa no se puede eliminar');
                    return $this->redirect($this->generateUrl('programa_edit', array('id' => $id)));
                }

                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El Programa se ha eliminado con éxito');
            }
        }
        return $this->redirect($this->generateUrl('programa'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    public function getDatosProgramaAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Programa')->findArray($id) ;
        return new Response(json_encode($entity));
    }    
    public function esProgramaIndividualAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Programa')->find($id);
        $result = ($entity) ? $entity->getIndividual() : false;
        return new Response(json_encode($result));
    }         
    public function ajusteTasaAction(){
        return $this->render('Programa/ajusteTasa.html.twig');
    }
    public function ajusteTasaSaveAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $interes = $request->get('porc_interes');
        $porcPun = $request->get('porc_punitorio');
        $montoPun = $request->get('monto_punitorio');
        $em = $this->getDoctrine()->getManager();

        $em->getRepository('App:Programa')->setTasas($interes,$porcPun,$montoPun);

        return $this->redirect($this->generateUrl('programa'));
    }
}
