<?php

namespace App\Controller;

use App\Entity\GruposEmprendedores;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class DefaultController extends Controller
{

    public function index(){
        return $this->render('Default/index.html.twig');
    }

    public function indexAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();
        $solicitudes = $em->getRepository('App:Credito')->getSolicitudes();
        $entregados = $em->getRepository('App:Credito')->getEntregados();
        $entregados_efectivos = $em->getRepository('App:Credito')->getEntregadosEfectivo();
        $renovaciones = $em->getRepository('App:Credito')->getRenovaciones();
        $venceEstaSemana = $em->getRepository('App:Cuota')->getVenceEstaSemana();
        $vencimientos = array();

        foreach ($venceEstaSemana as $cuota) {
            $credito = $em->getRepository('App:Credito')->find($cuota['credId']);
            $grupoEmp = ($credito->getGrupo())? $credito->getGrupo()->__toString()
                : $credito->getEmprendedor()->getEmprendedor()->__toString();
            //$nroCredito = $credito->getNroCredito().'-'.$credito->getNroRenovacion();
            $nroCuota = $cuota['nroCuota'].'/'.$credito->getCantCuotas();

            $array = array('credito'=>$credito->getId() ,'grupoEmp'=>$grupoEmp,'grp'=>$credito->getGrupo(),
                'nroCuota'=> $nroCuota, 'montoTotal'=>$cuota['montoTotal'],
                'fechaVencimiento'=>$cuota['fechaVencimiento']);
            array_push($vencimientos, $array);
        }
        $asesores = $em->getRepository('App:User')->findByMyCriteria();
        return $this->render('Default/index.html.twig',
            array('vencimientos'=>$this->orderArray($vencimientos,'grupoEmp'),
                'solicitudes'=>$solicitudes,'entregados'=>$entregados,'entregados_efectivos'=>$entregados_efectivos,'asesores'=>$asesores,
                'renovaciones'=>$renovaciones, 'semanas'=>$this->getSemanas(4) ));

    }

    public function cambiarActivoAction() {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $entidad = $request->get('entidad');
        if($entidad == 'usuario'){
            $entidad = 'User';
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:' . $entidad)->find($id);
        $entity->setActivo(!$entity->getActivo());
        $em->flush();
        $array = array('message' => 'OK', 'value' => intval($entity->getActivo()));
        return new Response(json_encode($array));
    }

    public static function dateDiff($start, $end) {
        $start_ts = strtotime($start);
        $end_ts = strtotime($end);
        $diff = $end_ts - $start_ts;
        return round($diff / 86400);
    }


    public static function dateTimeDiff($fechaVencimiento, $fechaCancelacion) {
        $diff = $fechaVencimiento->diff($fechaCancelacion);
        $multiplicador = 1;
        if($fechaVencimiento > $fechaCancelacion)
            $multiplicador = -1;
        return ($diff->days * $multiplicador);
    }

    public static function getDateDiffWithExclusion($diasExcluidos,$cuota,$feriados=null,$fechaUltimoPago=null,$devolverDiasNegativo=false, $fechaPago = null){

        if(is_null($fechaPago)) {
            $fechaHoy = new \Datetime;
        }else{
            $fechaHoy = $fechaPago;
        }

        //Controlo si tengo que tomar la fecha de la cuota (si es que no tuvo ningun pago) o del ultimo pago
        if(!is_null($fechaUltimoPago)){
            if($fechaUltimoPago > $cuota->getFechaVencimiento()){
                $fechaCalculo = $fechaUltimoPago;
            }else{
                $fechaCalculo = $cuota->getFechaVencimiento();
            }
        }else{
            $fechaCalculo = $cuota->getFechaVencimiento();
        }

        if($devolverDiasNegativo == true AND $cuota->getFechaCancelacion()){
            $fechaHoy = $cuota->getFechaCancelacion();
        }

        //Obtengo el total de dias entre las fechas de vencimiento y de pago
        $dias = DefaultController::dateDiff($fechaCalculo->format('d-m-Y'), $fechaHoy->format('d-m-Y'));

        //Ahora resto los dias que estan dentro de la exclusión
        $cantidadDiasExcluidos = 0;
        if (count($diasExcluidos) > 0){
            foreach ($diasExcluidos as $diaExcluido) {
                $diasExcluidos = 0;
                if($fechaCalculo < $diaExcluido['fechaDesde']){
                    if($fechaHoy > $diaExcluido['fechaHasta'] ){
                        $diasExcluidos = $diasExcluidos + DefaultController::dateDiff($diaExcluido['fechaDesde']->format('d-m-Y'),$diaExcluido['fechaHasta']->format('d-m-Y') );
                    }else{
                        $diasExcluidos = $diasExcluidos + DefaultController::dateDiff($diaExcluido['fechaDesde']->format('d-m-Y'),$fechaHoy->format('d-m-Y') );
                    }
                    //Fix, Se suma el ultimo dia
                    $diasExcluidos++;
                }
                if( $fechaCalculo > $diaExcluido['fechaDesde'] AND $fechaCalculo < $diaExcluido['fechaHasta']){
                    $diasExcluidos = $diasExcluidos + DefaultController::dateDiff( $fechaCalculo->format('d-m-Y'), $diaExcluido['fechaHasta']->format('d-m-Y'));
                }
                $cantidadDiasExcluidos = $cantidadDiasExcluidos + $diasExcluidos;
            }
        }

        if($cantidadDiasExcluidos>0){
            if($cantidadDiasExcluidos>$dias){
                $dias =0;
            }else{
                $dias = $dias - $cantidadDiasExcluidos;
            }
        }
        if(!is_null($feriados)) {
            foreach ($feriados as $feriado) {
                if($dias > 0) {
                    $dias = $dias - 1;
                }
            }
        }

        //Si fueron mas dias de exclusión que dias que pasaron, lo pongo en 0. Pero no deberia pasar.
        if($dias< 0 AND $devolverDiasNegativo == false){
            $dias = 0;
        }
        return $dias;
    }

    public static function getDiasRetrasoCuotaCanccelada($em,$cuota,$fecha,$programa,$devolverDiasNegativo=false){
        $diasExcluidos = $em->getRepository('App:Exclusiones')->findFechas($cuota->getFechaVencimiento()->format('Y-m-d'));
        $diasFeriados = $em->getRepository('App:TablaFeriados')->findByFechas(
            $cuota->getFechaVencimiento()->format('Y-m-d'),
            $fecha->format('Y-m-d')
        );

        //Cuenta los dias que desde que vencio la cuota.
        $dias = DefaultController::getDateDiffWithExclusion($diasExcluidos,$cuota,$diasFeriados,null,$devolverDiasNegativo);
        return $dias;
    }
     public static function getPunitoriosDiasMora($em,$cuota,$fecha,$programa,$devolverDiasNegativo=false){
        $diasExcluidos = $em->getRepository('App:Exclusiones')->findFechas($cuota->getFechaVencimiento()->format('Y-m-d'));
        $diasFeriados = $em->getRepository('App:TablaFeriados')->findByFechas(
            $cuota->getFechaVencimiento()->format('Y-m-d'),
            $fecha->format('Y-m-d')
        );

        $tasa = $programa->getPorcPunitorio()/30;
        $montoPun = $programa->getMontoPunitorio();

        $capitalCuota = $cuota->getMontoCapital();

         //Suma lo que se fue pagando en pagos parciales
         $porcCapital = $porcInteres = $porcGasto = $porPunitorios = 0;
         $fechaUltimoPago = null;
         foreach($cuota->getPagos() as $pagoRealizdo){
             $porcCapital += $pagoRealizdo->getCapital();
             $porcInteres += $pagoRealizdo->getIntereses();
             $porcGasto   += $pagoRealizdo->getGastoAdministrativo();
             $porPunitorios += $pagoRealizdo->getPunitorios();
             //Busco si tiene un pago realizado.. para realizar el control desde ese fecha
//             if(is_null($fechaUltimoPago)){
//                 $fechaUltimoPago = $pagoRealizdo->getFecha();
//             }else{
//                if($fechaUltimoPago < $pagoRealizdo->getFecha()){
//                    $fechaUltimoPago = $pagoRealizdo->getFecha();
//                }
//             }
         }

         //Cuenta los dias que desde que vencio la cuota.
         $dias = DefaultController::getDateDiffWithExclusion($diasExcluidos,$cuota,$diasFeriados,$fechaUltimoPago,$devolverDiasNegativo,$fecha);

        $montoPunitorios = $montoPunitoriosSinRound = 0;
        $monto = $cuota->getMontoCapital()+ $cuota->getMontoIntereses() + $cuota->getGastoAdministrativo();
        $montoPagado = $porcCapital+$porcInteres+$porcGasto;
//        if( !$cuota->getgetCantidadCuotaCredito() && $dias>0 && $cuota->getSaldo()>0){
        if( !$cuota->getCancelado() && $dias>0 && $cuota->getSaldo()>0){
            if($montoPun>0) {
                $montoPunitorios = $montoPun * $dias;
                $montoPunitoriosSinRound = $montoPun * $dias;
            }
            else {
                $montoPunitorios = round(((($monto - $montoPagado) * $tasa / 100) * $dias));
                $montoPunitoriosSinRound = ((($monto - $montoPagado) * $tasa / 100) * $dias);
            }
        }
        
        return [
            'dias' => $dias,
            'porcCapital'=> ($monto - $montoPagado) ,
            'montoPunitoriosSinRound' => $montoPunitoriosSinRound,
            'montoPunitorios' => $montoPunitorios
        ];
     }

    public static function toAnsiDate($value){
        if(is_array($value))
            $value = isset($value['text']) ? $value['text'] : null;

        if(strpos($value,'-') === false)
            return $value;

        $date = DefaultController::toArray($value);

        $ansi = $date['Y'].'-'.$date['m'].'-'.$date['d'];
        //$ansi .= isset($date['H']) ? ' '.$date['H'].':'.$date['i'].':'.$date['s'] : '';

        return $ansi;
    }

    public static function toArray($value){
        if(strpos($value,'-') === false)
            return array('Y'=>'1969','m'=>'01','d'=>'01');

        $parts = explode('-',$value);
        $years = explode(' ',$parts[2]);
        //    $hours = isset($years[1]) ? explode(':',$years[1]) : null;

        $date = array('d'=>$parts[0],'m'=>$parts[1],'Y'=>$years[0]);
        //       if($hours)
        //       $date = array_merge($date,array('H'=>$hours[0],'i'=>$hours[1],'s'=>$hours[2]));

        return $date;
    }

    public static function controlaCredititoActivo($status){
        $statusReturn = true;
        if( $status == 'Al dia' OR $status == 'Moroso' ){
            $statusReturn = false;
        }
        return $statusReturn;
    }

    public static function longDateSpanish($fecha,$dayname=FALSE){
        $date = strtotime($fecha->format('Y-m-d'));
        $dia=date("l", $date );

        if ($dia=="Monday") $dia="Lunes";
        if ($dia=="Tuesday") $dia="Martes";
        if ($dia=="Wednesday") $dia="Miércoles";
        if ($dia=="Thursday") $dia="Jueves";
        if ($dia=="Friday") $dia="Viernes";
        if ($dia=="Saturday") $dia="Sabado";
        if ($dia=="Sunday") $dia="Domingo";

        $dia2=date("d", $date);

        $mes=date("F", $date);

        if ($mes=="January") $mes="Enero";
        if ($mes=="February") $mes="Febrero";
        if ($mes=="March") $mes="Marzo";
        if ($mes=="April") $mes="Abril";
        if ($mes=="May") $mes="Mayo";
        if ($mes=="June") $mes="Junio";
        if ($mes=="July") $mes="Julio";
        if ($mes=="August") $mes="Agosto";
        if ($mes=="September") $mes="Setiembre";
        if ($mes=="October") $mes="Octubre";
        if ($mes=="November") $mes="Noviembre";
        if ($mes=="December") $mes="Diciembre";

        $ano=date("Y", $date);
        if($dayname) $fecha = "$dia, $dia2 de $mes de $ano";
        else         $fecha = "$dia2 de $mes de $ano";

        return $fecha;

    }

    public function getPdfAction() {
        $html = $this->getRequest()->get('html');
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="file.pdf"'
            )
        );
    }

    private function getSemanas($cant){
        $hoy = date('w', strtotime ("today"));
        if($hoy==1)
            $lunes = date('Y-m-d');
        elseif ($hoy == 0) $lunes = date("Y-m-d", strtotime ("next Monday"));
        else $lunes = date("Y-m-d", strtotime ("last Monday"));
        if($hoy==5)
            $viernes = date('Y-m-d');
        elseif ($hoy < 5) $viernes = date("Y-m-d", strtotime ("next Friday"));
        else $viernes = date("Y-m-d", strtotime ("last Friday"));
        $semanas = $semana = array();
        for($i = 1; $i <= $cant; ++$i) {
            $semana = array('ini' => $lunes , 'fin' => $viernes);
            $lunes = date("Y-m-d", strtotime("$lunes + 7 days"));
            $viernes = date("Y-m-d", strtotime("$viernes + 7 days"));
            //  $viernes = $viernes + $dias;
            array_push($semanas, $semana );
        }
        return $semanas;
    }

    private function orderArray ($toOrderArray, $field, $inverse = false) {
        $position = array();
        $newRow = array();
        foreach ($toOrderArray as $key => $row) {
            $position[$key]  = $row[$field];
            $newRow[$key] = $row;
        }
        if ($inverse) {
            arsort($position);
        }
        else {
            asort($position);
        }
        $returnArray = array();
        foreach ($position as $key => $pos) {
            $returnArray[] = $newRow[$key];
        }
        return $returnArray;
    }

    public function accessDeniedAction(){
        throw new AccessDeniedException('Acceso Prohibido');
    }

    public function updateGruposEmprendedoresAction()
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        try {
            $emprendedores = $em->getRepository('App:Emprendedor')->findAll();
            foreach ($emprendedores as $emprendedor) {
                if(!is_null($emprendedor->getGrupoLegacy())) {
                    $grupo = $emprendedor->getGrupoLegacy();
                    $grupoEmprendedor = new GruposEmprendedores();
                    $grupoEmprendedor->setEmprendedor($emprendedor);
                    $grupoEmprendedor->setGrupo($grupo);
                    $fecha = new \DateTime();
                    $grupoEmprendedor->setFechaCreacion($fecha);
                    $grupoEmprendedor->setFechaActualizacion($fecha);
                    $em->persist($grupoEmprendedor);
                }
            }
            $em->flush();
            $em->getConnection()->commit();
            var_dump( 'OK');exit;
        } catch (\PDOException $e) {
            $em->getConnection()->rollback();
            var_dump( 'NO OK'.'<br>'.$e->getMessage().'<br>'.$e->getFile().'<br>'.$e->getLine().'<br>'.$e->getCode());exit;
        }

    }

    public function revisionPagoAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder();
        $query->select('p')
                ->from('App\Entity\Pago', 'p')
                ->add(
                    'where',
                    ' ( '.
                        ' ( '.
                            ' ( (p.capital+p.intereses+p.punitorios+p.gastoAdministrativo+p.refinPunitorios) / p.total > 1.1 ) OR  '.
                            ' ( (p.capital+p.intereses+p.punitorios+p.gastoAdministrativo+p.refinPunitorios) / p.total < 0.9 ) '.
                        ' ) AND '.
                    '    (p.capital+p.intereses+p.punitorios+p.gastoAdministrativo+p.refinPunitorios) <> p.total  '.
                    ' ) AND '.
                      ' p.activo = 1 '.
                      ' '
                )
                ->addOrderBy('p.id', 'DESC')
        ;

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

//        $entities = $query->getQuery()->getResult();
//        dump('cantidad: '.count($entities));
//        $cantidad = $totalDef = 0;
//        foreach($entities as $pago){
//            if (
//                (
//                    round($pago->getTotal(), 2) -
//                    round(
//                        $pago->getCapital() +
//                        $pago->getIntereses() +
//                        $pago->getGastoAdministrativo() +
//                        $pago->getPunitorios() +
//                        $pago->getRefinPunitorios()
//                        , 2)
//                ) != 0
//            ) {
//
//                $cuota = $pago->getCuota();
//
//                //Suma lo que se fue pagando en pagos anteriores
//                $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
//                foreach ($cuota->getPagos() as $pagoRealizdo) {
//                    if ($pago->getId() > $pagoRealizdo->getId()) {
//                        $porcCapital += $pagoRealizdo->getCapital();
//                        $porcInteres += $pagoRealizdo->getIntereses();
//                        $porcGasto += $pagoRealizdo->getGastoAdministrativo();
//                        $porPunitorios += $pagoRealizdo->getPunitorios();
//                        $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
//                    }
//                }
//
//                $totalReimputar = round($pago->getTotal(), 2);
//
//                //RefPunitorios
//                if ($totalReimputar > 0) {
//                    if (($cuota->getSaldoPunitorios() - $porRefPunitorios) > $totalReimputar) {
//
//                        $pago->setRefinPunitorios($totalReimputar);
//                        $totalReimputar = 0;
//
//                    } else {
//                        $pago->setRefinPunitorios($cuota->getSaldoPunitorios() - $porRefPunitorios);
//                        $totalReimputar = $totalReimputar - ($cuota->getSaldoPunitorios() - $porRefPunitorios);
//                    }
//                } else {
//                    $pago->setRefinPunitorios(0);
//                }
//
//                //Punitorios
//                $credito = $cuota->getCredito();
//                if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
//                    if ($totalReimputar > 0) {
//                        if (($cuota->getMontoPunitorios() - $porPunitorios) > $totalReimputar) {
//                            $pago->setPunitorios($totalReimputar);
//                            $totalReimputar = 0;
//                        } else {
//                            $pago->setPunitorios($cuota->getMontoPunitorios() - $porPunitorios);
//                            $totalReimputar = $totalReimputar - ($cuota->getMontoPunitorios() - $porPunitorios);
//                        }
//                    } else {
//                        $pago->setPunitorios(0);
//                    }
//                }
//
//                //intereses
//                if ($totalReimputar > 0) {
//                    if (($cuota->getMontoIntereses() - $porcInteres) > $totalReimputar) {
//                        $pago->setIntereses($totalReimputar);
//                        $totalReimputar = 0;
//                    } else {
//                        $pago->setIntereses($cuota->getMontoIntereses() - $porcInteres);
//                        $totalReimputar = $totalReimputar - ($cuota->getMontoIntereses() - $porcInteres);
//                    }
//                } else {
//                    $pago->setIntereses(0);
//                }
//
//
//                //Gastos
//                if ($totalReimputar > 0) {
//                    if (($cuota->getGastoAdministrativo() - $porcGasto) > $totalReimputar) {
//                        $pago->setGastoAdministrativo($totalReimputar);
//                        $totalReimputar = 0;
//                    } else {
//                        $pago->setGastoAdministrativo($cuota->getGastoAdministrativo() - $porcGasto);
//                        $totalReimputar = $totalReimputar - ($cuota->getGastoAdministrativo() - $porcGasto);
//                    }
//                } else {
//                    $pago->setGastoAdministrativo(0);
//                }
//
//                //Capital
//                if ($totalReimputar > 0) {
//                    if (($cuota->getMontoCapital() - $porcCapital) > $totalReimputar) {
//                        $pago->setCapital($totalReimputar);
//                        $totalReimputar = 0;
//                    } else {
//                        $pago->setCapital($cuota->getMontoCapital() - $porcCapital);
//                        $totalReimputar = $totalReimputar - ($cuota->getMontoCapital() - $porcCapital);
//                    }
//                } else {
//                    $pago->setCapital(0);
//                }
//
//                $diferenciaPago = round($pago->getTotal(), 2) -
//                    round(
//                        $pago->getCapital() +
//                        $pago->getIntereses() +
//                        $pago->getGastoAdministrativo() +
//                        $pago->getPunitorios() +
//                        $pago->getRefinPunitorios()
//                        , 2);
//                if ($diferenciaPago > 0) {
//                    $pago->setCapital($cuota->getMontoCapital() + $diferenciaPago);
//                    $totalDef += $diferenciaPago;
//                    dump('Controla pago ID: ' . $pago->getId().' con fecha '.$pago->getFecha()->format('d/m/Y') );
//                    dump('> ' . $diferenciaPago);
//                } elseif ($diferenciaPago == 0) {
////                        dump('=');
//                } else {
//                    dump('Controla pago ID: ' . $pago->getId().' con fecha '.$pago->getFecha()->format('d/m/Y') );
//                    dump('< ' . $diferenciaPago);
//                }
//                $em->persist($pago);
//                $em->flush();
//                                dump($pago);
//
//            }
//            $cantidad ++;
//        }
//        dd('Termino de ejecutar, se verifico '.$cantidad.' registros. El defasaje fue es de '.$totalDef);

        $vars = array(
            'entities'     => $entities,
            'entityName'   => 'Pago',
            'paginator'=>null
        );

        return $this->render('Caja/listadoPago.html.twig', $vars );
    }

    public function connectFTPAction()
    {
        ini_set('max_execution_time', 0);
        $ftpHost   = $this->getParameter('ftp_host.paramname');
        $ftpUsername = $this->getParameter('ftp_user.paramname');
        $ftpPassword = $this->getParameter('ftp_password.paramname');
        $path = $this->getParameter('ftp_path_origen.paramname');

        // open an FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");

        // login to FTP server
        $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);


        $dir = opendir($path);
        while ($elemento = readdir($dir)){
            if( $elemento != "." && $elemento != ".."){
                if( !is_dir($path.'/'.$elemento) ){
                    //enviar
                    $tamanioArchivo = ftp_size($connId,$elemento);

                    if ($tamanioArchivo == -1) {
                        // try to upload file
                        var_dump($elemento,$path.$elemento);
                        if(ftp_put($connId, $elemento, $path.'/'.$elemento, FTP_ASCII)){
                            echo "File transfer successful - $elemento";
                        }else{
                            echo "There was an error while uploading $elemento";
                        }
                    }

                }
            }
        }

        // close the connection
        ftp_close($connId);

        return $this->json('ok');

    }

}