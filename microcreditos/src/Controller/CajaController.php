<?php

namespace App\Controller;

use App\Entity\Caja;
use App\Entity\Credito;
use App\Form\IngresoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Ingresos controller.
 *
 */
class CajaController extends Controller
{
    /**
     * Lists all Ingresos entities.
     *
     */
    public function indexAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','concepto'=>'','desde'=>'','hasta'=>'','page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'  =>$request->get('_programa'),
                    'concepto'  =>$request->get('_concepto'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'page'      =>$request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'  =>$sessionFiltro['programa'],
                    'concepto'  =>$sessionFiltro['concepto'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'page'      => ( isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1 )
                    );
                break;
        }
                   
        $session->set('filtro_ingreso',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:Caja')
                        ->findByMyCriteriaDQL($session->get('filtro_ingreso'),'ingreso');

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $conceptos = $em->getRepository('App:Parametro')->getConceptosCaja();
        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        
        $vars = array(  
            'entityName'   => 'caja',
            'entities'     => $entities,
            'filtro'       => $session->get('filtro_ingreso'),
            'conceptos'    => $conceptos,
            'programas'    => $programas, 
            'paginator'    => $paginator);
       
        return $this->render('Caja/ingresoIndex.html.twig', $vars );
    }

     /**
     * Displays a form to create a new Ingresos entity.
     *
     */
    public function newAction()
    {
        $entity = new Caja();
        $form   = $this->createForm(IngresoType::class, $entity);

        return $this->render('Caja/IngresosEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
    /**
     * Creates a new Ingresos entity.
     *
     */
    public function createAction()
    {
        $entity  = new Caja();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(IngresoType::class, $entity);
        $session = $request->getSession();
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $cajaAnterior = $em->getRepository('App:Caja')->findOneBy([], ['id' => 'desc']);
                $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());

                $saldoAnterior = 0;
                if($cajaAnterior){
                    $saldoAnterior = $cajaAnterior->getTotal();
                }else{
                    $caja = new Caja();
                    $caja->setTotal(0);
                    $caja->setEgreso(0);
                    $caja->setIngreso(0);
                    $caja->setObservacion('Inicio de Caja');
                    $caja->setFechaCreacion(new \DateTime());
                    $caja->setFechaActualizacion(new \DateTime());
                    $caja->setUsuario($usuario);
                    $caja->setCreadoPor($usuario);
                    $caja->setActualizadoPor($usuario);
                    $em->persist($caja);
                    $em->flush();
                }
                $entity->setEgreso(0);
                $entity->setTotal($saldoAnterior + $entity->getIngreso() );
                $entity->setFechaCreacion(new \DateTime());
                $entity->setFechaActualizacion(new \DateTime());
                $entity->setUsuario($usuario);
                $entity->setCreadoPor($usuario);
                $entity->setActualizadoPor($usuario);

                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('ingreso_edit', array('id' => $entity->getId())));

            }
        }
        return $this->render('Caja/IngresosEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));               
    }

    /**
     * Displays a form to edit an existing Ingresos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Caja')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el registro de Caja que está buscando.');
        }

        $editForm = $this->createForm(IngresoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Caja/IngresosEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Ingresos entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Caja')->find($id);
        $entity2 = clone $entity;
        if (!$entity) {
            throw $this->createNotFoundException('No existe el registro de Caja que está buscando.');
        }

        $cajaAnterior = $em->getRepository('App:Caja')->findOneBy([], ['id' => 'desc']);

        if (!$cajaAnterior->getId() > $id) {
            throw $this->createNotFoundException('No se puede editar el registro de la caja por que ya existe un nuevo movimiento.');
        }

        $editForm   = $this->createForm(IngresoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {

                if( $entity->getIngreso() > doubleval($entity2->getIngreso()) ){
                    $entity->setTotal(
                        $entity->getTotal() +
                        ( $entity->getIngreso() - doubleval($entity2->getIngreso()) )
                    );
                }else{
                    $entity->setTotal(
                        $entity->getTotal() -
                        ( doubleval($entity2->getIngreso()) - $entity->getIngreso() )
                    );
                }

                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('ingreso_edit', array('id' => $id)));
            }
        }

        return $this->render('Caja/IngresosEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Ingresos entity.
     *
     */
    public function deleteAction($id)
    {
        return $this->redirect($this->generateUrl('ingreso'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    public function getopListAction(){
        $partial = $this->renderView('Caja/opList.html.twig');
        return new Response($partial);  
    }
    
    public function getDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $option = $request->get('option');
        $format = $request->get('format');
        $sessionFiltro = $session->get('filtro_ingreso');
        if($option=='D')
            $gastos = $em->getRepository('App:Caja')->findByMyCriteriaDQL($sessionFiltro,'R')->getResult();
        else
            $gastos = $em->getRepository('App:Caja')->findGroupByCriteriaDQL($sessionFiltro)->getResult();

        if($format=='PDF'){
            $html = $this->renderView('Caja/listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'ingresos'=>$gastos));
            $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="file.pdf"' )
                         ); 
        }else{
            $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
            $html= $this->renderView('Caja/listXls.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'ingresos'=>$gastos));
            $response->setContent($html);
        }
        return $response; 
    }

    /**
     * Lists all Ingresos entities.
     *
     */
    public function indexCajaAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','concepto'=>'','desde'=>'','hasta'=>'','page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'  =>$request->get('_programa'),
                    'concepto'  =>$request->get('_concepto'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'page'      =>$request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'  =>$sessionFiltro['programa'],
                    'concepto'  =>$sessionFiltro['concepto'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'page'      => ( isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1 )
                );
                break;
        }

        $session->set('filtro_ingreso',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:Caja')
            ->findByMyCriteriaDQL($session->get('filtro_ingreso'));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $conceptos = $em->getRepository('App:Parametro')->getConceptosMovimientosCaja();
        $programas = $em->getRepository('App:Programa')->findByActivosDQL();

        $ultimaCaja = $em->getRepository('App:Caja')->findOneBy([], ['id' => 'desc']);

        $vars = array(
            'entityName'   => 'caja',
            'entities'     => $entities,
            'totalCaja'    => $ultimaCaja->getTotal(),
            'filtro'       => $session->get('filtro_ingreso'),
            'conceptos'    => $conceptos,
            'programas'    => $programas,
            'paginator'    => $paginator);

        return $this->render('Caja/cajaIndex.html.twig', $vars );
    }

    public function getCajaOpListAction(){
        $partial = $this->renderView('Caja/opList.html.twig');
        return new Response($partial);
    }

    public function getCajaDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        $option = $request->get('option');
        $format = $request->get('format');
        $sessionFiltro = $session->get('filtro_ingreso');

        $gastos =  $em->getRepository('App:Caja')->findByMyCriteriaDQL($sessionFiltro)->getResult();

        if($format=='PDF'){
            $html = $this->renderView('Caja/listPdf.html.twig',
                array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'',
                    'ingresos'=>$gastos));
            $response = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
                200,
                array( 'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="file.pdf"' )
            );
        }else{
            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
            $html= $this->renderView('Caja/listXls.html.twig',
                array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'',
                    'ingresos'=>$gastos));
            $response->setContent($html);
        }
        return $response;
    }

    public function reporteCajaAction(){
        ini_set('max_execution_time', 30000);
        $em = $this->getDoctrine()->getManager();
        $fecha = new \DateTime();
        $fecha->modify('last day of -1 month');

        $filtro = array(
            'desde'     => $fecha->format('Y/m').'/01',
            'hasta'     => $fecha->format('Y/m/d'),
        );

        $request = $this->container->get('request_stack')->getCurrentRequest();
        if (!$request->isMethod('POST')) {
            $fechaDesde = $fecha->format('Y/m').'/01';
            $fechaHasta = $fecha->format('Y/m/d');

            $vars = array(
                'fechaDesde'       => $fechaDesde,
                'fechaHasta'     => $fechaHasta);


            return $this->render('Caja/listCaja.html.twig', $vars );

        }
        $filtro = array(
            'desde' => $request->get('fechaDesde'),
            'hasta' => $request->get('fechaHasta')
        );

        $conDetalle = $request->get('detalle');

        if(!is_null($conDetalle)){
            $conDetalle = true;
        }else{
            $conDetalle = false;
        }

        $movimientoCaja =  $em->getRepository('App:Caja')->findByMyCriteriaDQL($filtro)->getResult();
        //dd($movimientoCaja,$filtro);
        if(count($movimientoCaja)> 0){
            $ultimaCaja = $movimientoCaja[0];
        }else{
            $ultimaCaja = null;
        }

        if(count($movimientoCaja)> 0){
            $primerMovimientoObj = $movimientoCaja[count($movimientoCaja)-1] ;
            $primerMovimiento = $primerMovimientoObj->getTotal()+$primerMovimientoObj->getEgreso()-$primerMovimientoObj->getIngreso();
        }else{
            $primerMovimiento = null;
        }

        $cantidadPagos =  $em->getRepository('App:Caja')->obtenerTotalPagos( $filtro);
//        foreach ($totalIngresos as $index => $data){
//
//            if(round($data['total'],2) != round($data['total_sumado'],2)
//                OR round($data['total_sumado'],2) !=
//                   round($data['cantidad_capital']+
//                        $data['cantidad_intereses']+
//                        $data['cantidad_refinPunitorios']+
//                        $data['cantidad_punitorios']+
//                        $data['cantidad_gastoAdministrativo'],2
//                    ) ){
//                $totalIngresos
//                dump(
//                    $data,
//                    round($data['total'] - $data['total_sumado'],3),
//                    round($data['total_sumado'] -
//                    ($data['cantidad_capital']+
//                        $data['cantidad_intereses']+
//                        $data['cantidad_refinPunitorios']+
//                        $data['cantidad_punitorios']+
//                        $data['cantidad_gastoAdministrativo']
//                    ),3)
//                );
//            }
//
//        }
//        dd('finalizo');
//        exit;
        if(count($cantidadPagos)> 0){
            $totalIngresos = $cantidadPagos[0]['total'];

        }else{
            $totalIngresos = 0;
        }


        $totalIngresosNoCobros =  $em->getRepository('App:Caja')->obtenerTotalIngresos( $filtro);
        if(count($totalIngresosNoCobros)> 0){
            $totalIngresosNoCobros = $totalIngresosNoCobros[0]['cantidad'];
        }else{
            $totalIngresosNoCobros = 0;
        }


        $totalEgresos=  $em->getRepository('App:Caja')->obtenerTotalEgresos( $filtro);
        if(count($totalEgresos)> 0){
            $totalEgresos = $totalEgresos[0]['cantidad'];
        }else{
            $totalEgresos = 0;
        }

        $totalReinvertido=  $em->getRepository('App:Caja')->obtenerTotalReinvertido( $filtro);
        if(count($totalReinvertido)> 0){
            $totalReinvertido = $totalReinvertido[0]['cantidad'];
        }else{
            $totalReinvertido = 0;
         }

        if(count($cantidadPagos)> 0){

            $cantidadPagosCapital = $cantidadPagos[0]['cantidad_capital'];
            $cantidadPagosIntereses = $cantidadPagos[0]['cantidad_intereses'];
            $cantidadPagosRefPuni = $cantidadPagos[0]['cantidad_refinPunitorios'];
            $cantidadPagoPunitorios = $cantidadPagos[0]['cantidad_punitorios'];
            $cantidadPagosGastos = $cantidadPagos[0]['cantidad_gastoAdministrativo'];
            $cantidadRefinPunitorios = $cantidadPagos[0]['cantidad_refinPunitorios'];
        }else{
            $cantidadPagosCapital = 0;
            $cantidadPagosIntereses = 0;
            $cantidadPagosRefPuni = 0;
            $cantidadPagoPunitorios = 0;
            $cantidadPagosGastos =  0;
            $cantidadRefinPunitorios = 0;
        }

        $movimientBanco =  $em->getRepository('App:PagoBancario')->findByMyCriteriaDQL($filtro)->getResult();
        $totalPagoBanco =0;

        $cantidadPagosBancoCapital = 0;
        $cantidadPagosBancoIntereses = 0;
        $cantidadPagosBancoRefPuni = 0;
        $cantidadPagoBancoPunitorios = 0;
        $cantidadPagosBancoGastos =  0;
        $cantidadPagosBancoRefinPunitorios = 0;

        foreach($movimientBanco as $mov){
            $totalPagoBanco += ($mov->getPago()->getTotal());
            $cantidadPagosBancoCapital += $mov->getPago()->getCapital();
            $cantidadPagosBancoIntereses += $mov->getPago()->getIntereses();
            $cantidadPagoBancoPunitorios += $mov->getPago()->getPunitorios();
            $cantidadPagosBancoGastos += $mov->getPago()->getGastoAdministrativo();
            $cantidadPagosBancoRefinPunitorios += $mov->getPago()->getRefinPunitorios();

//            if($totalPagoBanco != ($cantidadPagosBancoCapital+$cantidadPagosBancoIntereses+$cantidadPagoBancoPunitorios+$cantidadPagosBancoGastos+$cantidadPagosBancoRefinPunitorios)){
//                dd($mov->getPago(),$totalPagoBanco, ($cantidadPagosBancoCapital+$cantidadPagosBancoIntereses+$cantidadPagoBancoPunitorios+$cantidadPagosBancoGastos+$cantidadPagosBancoRefinPunitorios));
//            }

        }

//        dd($totalPagoBanco, ($cantidadPagosBancoCapital+$cantidadPagosBancoIntereses+$cantidadPagoBancoPunitorios+$cantidadPagosBancoGastos+$cantidadPagosBancoRefinPunitorios));
       //$totalInresosBanco =  $em->getRepository('App:PagoBancario')->obtenerTotalPagos( $filtro);

        if($conDetalle){
            $movimientBanco =  $em->getRepository('App:PagoBancario')->findByMyCriteriaDQL($filtro)->getResult();
            $cobrosDetalle = $em->getRepository('App:Caja')->obtenerDetallePagos( $filtro);
            $ingresosDetalle = $em->getRepository('App:Caja')->obtenerDetalleIngresos( $filtro);
            $gastosDetalle = $em->getRepository('App:Caja')->obtenerDetalleEgresos( $filtro);
            $reinvertidoDetalle = $em->getRepository('App:Caja')->obtenerDetalleReinvertido( $filtro);
            $creditosMorosos15Detalle =  $em->getRepository('App:Caja')->obtenerDetalleMorosos15Dias( $filtro);
            $creditosMorosos30Detalle =  $em->getRepository('App:Caja')->obtenerDetalleMorosos30Dias( $filtro);
        }else{
            $movimientBanco = null;
            $cobrosDetalle = null;
            $ingresosDetalle = null;
            $gastosDetalle = null;
            $reinvertidoDetalle = null;
            $creditosMorosos15Detalle =null;
            $creditosMorosos30Detalle = null;
        }

        $total = 0;
        if(!is_null($cobrosDetalle)) {
            foreach ($cobrosDetalle as $cobro) {
                $total = $total + $cobro->getIngreso();
            }
        }

        /** Certera */
        $cantidaCreditosCreadoMes =  $em->getRepository('App:Caja')->obtenerCantidadCreditosCreadossMes( $filtro);
        if(count($cantidaCreditosCreadoMes)> 0){
            $cantidaCreditosCreadoMes = $cantidaCreditosCreadoMes[0]['cantidad'];
        }else{
            $cantidaCreditosCreadoMes = 0;
        }

        $cantidaCreditosFinalizadosMes =  $em->getRepository('App:Caja')->obtenerCantidadCreditosFinalizadosMes( $filtro);
        if(count($cantidaCreditosFinalizadosMes)> 0){
            $cantidaCreditosFinalizadosMes = $cantidaCreditosFinalizadosMes[0]['cantidad'];
        }else{
            $cantidaCreditosFinalizadosMes = 0;
        }

        $cantidadCreditosActivos =  $em->getRepository('App:Caja')->obtenerCantidadCreditosActivosMes( $filtro);
        if(count($cantidadCreditosActivos)> 0){
            $cantidadCreditosActivos = $cantidadCreditosActivos[0]['cantidad']; }else{ $cantidadCreditosActivos = 0;
        }

        $cantidadCreditosAlDia =  $em->getRepository('App:Caja')->obtenerCantidadAldia( $filtro);
        if(count($cantidadCreditosAlDia)> 0){
            $cantidadCreditosAlDia = $cantidadCreditosAlDia[0]['cantidad']; }else{ $cantidadCreditosAlDia = 0;
        }
        $cantidadCreditosMorosos15 =  $em->getRepository('App:Caja')->obtenerCantidadMorosos15Dias( $filtro);
        $cantidadCreditosMorosos30 =  $em->getRepository('App:Caja')->obtenerCantidadMorosos30Dias( $filtro);

        $listadoCreditosFinalizadosMes = $em->getRepository('App:Caja')->obtenerListadoCreditosFinalizadosMes( $filtro);

        $data = array(
            'filtro' => $filtro,
            'conDetalle' =>$conDetalle,
            'movimientoCaja'=> $movimientoCaja,
            'totalIngresos' => $totalIngresos,
            'totalIngresosNoCobros' => $totalIngresosNoCobros,
            'totalEgresos' => $totalEgresos,
            'totalReinvertido' => $totalReinvertido,
            'ultimoMovimiento' => $ultimaCaja,
            'primerMovimiento' => $primerMovimiento,

            'cantidadPagosCapital' => $cantidadPagosCapital,
            'cantidadPagosIntereses' => $cantidadPagosIntereses,
            'cantidadPagosRefPuni' => $cantidadPagosRefPuni,
            'cantidadPagoPunitorios' => $cantidadPagoPunitorios,
            'cantidadPagosGastos' => $cantidadPagosGastos,
            'cantidadRefinPunitorios'=> $cantidadRefinPunitorios,

            'cantidaCreditosCreadoMes' => $cantidaCreditosCreadoMes,
            'cantidaCreditosFinalizadosMes' => $cantidaCreditosFinalizadosMes,
            'cantidadCreditosActivos' => $cantidadCreditosActivos,
            'cantidadCreditosAlDia' => $cantidadCreditosAlDia,
            'cantidadCreditosMorosos15' => $cantidadCreditosMorosos15,
            'cantidadCreditosMorosos30' => ($cantidadCreditosMorosos30 - $cantidadCreditosMorosos15),

            'movimientBanco' => $movimientBanco,
            'cantidadPagosBancoCapital' => $cantidadPagosBancoCapital,
            'cantidadPagosBancoIntereses' => $cantidadPagosBancoIntereses,
            'cantidadPagoBancoPunitorios' => $cantidadPagoBancoPunitorios,
            'cantidadPagosBancoGastos' => $cantidadPagosBancoGastos,
            'cantidadPagosBancoRefinPunitorios' =>$cantidadPagosBancoRefinPunitorios,

            'cobrosDetalle' => $cobrosDetalle,
            'ingresosDetalle' => $ingresosDetalle,
            'gastosDetalle' => $gastosDetalle,
            'reinvertidoDetalle' => $reinvertidoDetalle,
            'creditosMorosos15Detalle' => $creditosMorosos15Detalle,
            'creditosMorosos30Detalle' => $creditosMorosos30Detalle,

            'listadoCreditosFinalizadosMes' => $listadoCreditosFinalizadosMes,
        );


        //return $this->render('Caja/listCajaPdf.html.twig', $data );

        $html = $this->renderView('Caja/listCajaPdf.html.twig',$data);

        $response = new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="file.pdf"'
            )
        );

        return $response;
    }

    public function reprocesarPagosAction($id){

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('App:Caja')->buscarCajaDesdeUltimoId( ($id-1) );
        $total = 0;
        $em->getConnection()->beginTransaction();
        try {
            foreach ($result as $index => $caja) {
                if ($index == 0) {
                    $total = $caja->getTotal();
                } else {
                    $total = $total + ($caja->getIngreso() - $caja->getEgreso());
                    $caja->setTotal($total);
                    $em->persist($caja);
                }
            }
            $em->flush();
            $em->getConnection()->commit();
            $session->getFlashBag()->set('success', 'Se proceso correctamente los calculos sumados');

        }catch (\PDOException $e) {
            $em->getConnection()->rollback();
            $session = $request->getSession();
            $session->getFlashBag()->set('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('caja_index'));

    }

}
