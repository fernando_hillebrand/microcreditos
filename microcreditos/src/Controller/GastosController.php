<?php

namespace App\Controller;

use App\Entity\Credito;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\Gastos;
use App\Form\GastosType;

/**
 * Gastos controller.
 *
 */
class GastosController extends Controller
{
    /**
     * Lists all Gastos entities.
     *
     */
    public function indexAction()
    {
        /*
        $em = $this->getDoctrine()->getManager();
        $query = null;
        $query =  $em->getRepository('App:Gastos')
                        ->getEstadoResultadosGastos('02','2022');
        dd($query);
        */
        
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_gastos');



        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','concepto'=>'','desde'=>'','hasta'=>'','page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'  =>$request->get('_programa'),
                    'concepto'  =>$request->get('_concepto'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'page'      =>$request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'  =>$sessionFiltro['programa'],
                    'concepto'  =>$sessionFiltro['concepto'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'page'      => ( isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1 )
                    );
                break;
        }
                   
        $session->set('filtro_gastos',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:Gastos')
                        ->findByMyCriteriaDQL($session->get('filtro_gastos'),'L');

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $conceptos = $em->getRepository('App:Parametro')->getConceptosCaja();
        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        
        $vars = array(  
            'entityName'   => 'gasto',
            'entities'     => $entities,
            'filtro'       => $session->get('filtro_gastos'),
            'conceptos'     => $conceptos,
            'programas'    => $programas, 
            'paginator'    => $paginator);
       
        return $this->render('Gastos/gastoIndex.html.twig', $vars );
    }

     /**
     * Displays a form to create a new Gastos entity.
     *
     */
    public function newAction()
    {
        $entity = new Gastos();
        $form   = $this->createForm(GastosType::class, $entity);

        return $this->render('Gastos/gastosEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
    /**
     * Creates a new Gastos entity.
     *
     */
    public function createAction()
    {
        $entity  = new Gastos();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(GastosType::class, $entity);
        $session = $request->getSession();
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $credito = new Credito();
                $credito->setPrograma($entity->getPrograma());
                $credito->setFechaEntrega($entity->getFecha());
                $reporteCerrado = $em->getRepository('App:ReporteMensual')->reporteCerrado($credito);

                if ($reporteCerrado) {
                    $session->getFlashBag()->set('error', 'El Reporte Mensual ya fue cerrado. Ingrese una fecha posterior para el pago');
                    //return $this->redirect($this->generateUrl('gasto_new'));
                } else {
                    $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
                    $entity->setUsuario($usuario);
                    $em->persist($entity);
                    $em->flush();

                    $em->getRepository('App:Caja')->guardarNuevoEgreso(
                        $entity->getImporte(),
                        'Gasto: '.$entity->getObservacion(),
                        $usuario,
                        null,
                        null,
                        $entity->getConcepto(),
                        $entity
                    );

                    $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                    return $this->redirect($this->generateUrl('gasto_edit', array('id' => $entity->getId())));
                }
            }
        }
        return $this->render('Gastos/gastosEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));               
    }

    /**
     * Displays a form to edit an existing Gastos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Gastos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Gasto que está buscando.');
        }

        $editForm = $this->createForm(GastosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Gastos/gastosEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Gastos entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Gastos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Gasto que está buscando.');
        }

        $editForm   = $this->createForm(GastosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('gasto_edit', array('id' => $id)));
            }
        }

        return $this->render('Programa/gastosEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Gastos entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Gastos')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Gastos entity.');
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('gasto'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    public function getopListAction(){
        $partial = $this->renderView('Gastos/opList.html.twig');
        return new Response($partial);  
    }
    
    public function getDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $option = $request->get('option');
        $format = $request->get('format');
        $sessionFiltro = $session->get('filtro_gastos');
        if($option=='D')
            $gastos = $em->getRepository('App:Gastos')->findByMyCriteriaDQL($sessionFiltro,'R')->getResult();
        else
            $gastos = $em->getRepository('App:Gastos')->findGroupByCriteriaDQL($sessionFiltro)->getResult();

        if($format=='PDF'){
            $html = $this->renderView('Gastos/listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'gastos'=>$gastos));
            $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="file.pdf"' )
                         ); 
        }else{
            $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Gastos.xls');
            $html= $this->renderView('Gastos/listXls.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'gastos'=>$gastos));
            $response->setContent($html);
        }
        return $response; 
    }    
}
