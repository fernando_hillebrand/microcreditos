<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\CreditosAdmin;
use App\Form\CreditosAdminType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CreditoAdminController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('concepto'=>'','desde'=>'','hasta'=>'','page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                   
                    'concepto'  =>$request->get('_concepto'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'page'      =>$request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'concepto'  =>$sessionFiltro['concepto'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'page'      => ( isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1 )
                    );
                break;
        }

        $session->set('filtro_ingreso',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:CreditosAdmin')->findByMyCriteriaDQL($session->get('filtro_ingreso'),'ingreso');
        //var_dump($query);
        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('CreditoAdmin/index.html.twig', array(
                        'entityName'   => 'RegistrosBancarios',
                        'entities'     => $entities,
                        'paginator'    => $paginator,
                        'filtro'       => $session->get('filtro_ingreso')
        ));
    }

    public function newAction()
    {
        $entity = new CreditosAdmin();
        $form   = $this->createForm(CreditosAdminType::class, $entity);

        return $this->render('CreditoAdmin/CreditoAdminEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        )); 
    }

    public function createAction()
    {
        $entity  = new CreditosAdmin();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(CreditosAdminType::class, $entity);
        
        if ($request->isMethod('POST')) {
            //var_dump($request->get('monto'));
            $form->submit($request->get($form->getName()));
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El nuevo rergistro de credito a administración se ha creado con éxito');
                return $this->redirect($this->generateUrl('creditos_admin_index'));
            }
        }
        

        return $this->render('CreditoAdmin/CreditoAdminEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:CreditosAdmin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el registro  que está buscando.');
        }

        $editForm = $this->createForm(CreditosAdminType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CreditoAdmin/CreditoAdminEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:CreditosAdmin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Registro que está buscando.');
        }

        $editForm   = $this->createForm(CreditosAdminType::class, $entity);
      

        $request = $this->container->get('request_stack')->getCurrentRequest();

     
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('creditos_admin_index'));
            }
        }

        return $this->render('CreditoAdmin/CreditoAdminEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }


    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:CreditosAdmin')->find($id);

            
                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Registro  que está buscando.');
                }
                try {
                    $em->remove($entity);
                    $em->flush();
                } catch (\PDOException $e) {
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', 'El Registro no se puede eliminar');
                    //return $this->redirect($this->generateUrl('gastos_totales_edit', array('id' => $id)));
                    return $this->redirect($this->generateUrl('creditos_admin_index'));
                }

                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El Registro  se ha eliminado con éxito');
            }
        }
        return $this->redirect($this->generateUrl('creditos_admin_index'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
