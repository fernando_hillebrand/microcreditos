<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UsuarioPassType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use App\Entity\Usuario;
use App\Form\UsuarioType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/**
 * Usuario controller.
 *
 */
class UsuarioController extends Controller
{


    /**
     * Lists all Usuario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:User')->findByMyCriteriaDQL();

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        //$structure =$em->getClassMetadata('Microcreditos\MicrocreditosBundle\Entity\Usuario')->getFieldNames();
        $vars = array(  
            'entityName'   => 'usuario',
            'entities'     => $entities,
            'paginator'    => $paginator);
       
        return $this->render('Usuario/usuarioIndex.html.twig', $vars );
    }

    /**
     * Finds and displays a Usuario entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Usuario')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Usuario/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     */
    public function newAction()
    {
        $entity = $this->get('fos_user.user_manager')->createUser();
        $form   = $this->createForm(UsuarioType::class, $entity);

//        $formFactory = $this->get('fos_user.registration.form.factory');
//        /** @var $userManager UserManagerInterface */
//        $userManager = $this->get('fos_user.user_manager');
//
//        $entity = $userManager->createUser();
//        $entity->setEnabled(true);
//
//        $form = $this->formFactory->createForm();
//        $form->setData($entity);

        return $this->render('Usuario/usuarioEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Usuario entity.
     *
     */
    public function createAction()
    {
        $entity  = $this->get('fos_user.user_manager')->createUser();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(UsuarioType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setEnabled(true);
                $entity->setPlainPassword('abc123');
                $this->get('fos_user.user_manager')->updateUser($entity, false);
                $em->flush();
                $request = $this->container->get('request_stack')->getCurrentRequest();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El Usuario se ha creado con éxito');
                return $this->redirect($this->generateUrl('usuario_pass', array('id' => $entity->getId())));
            }
        }

        return $this->render('Usuario/usuarioEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Usuario/usuarioEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        if ($request->getMethod() == 'POST') {
//            $passwordOriginal = $editForm->getData()->getPassword();

            $editForm->submit($request->get($editForm->getName()));

            if ($editForm->isValid()) {

                $this->get('fos_user.user_manager')->updateUser($entity, false);
                $em->flush();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('usuario_edit', array('id' => $entity->getId())));
            }
        }
        return $this->render('Usuario/usuarioEdit.html.twig', array(
                    'entity' => $entity,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));
    }

    /**
     * Deletes a Usuario entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $entity = $em->getRepository('App:User')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Usuario que está buscando.');
                }
                $entity->setActivo(false);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('usuario'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }

    public function changePassAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioPassType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Usuario/usuarioChangePass.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateChangePassAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioPassType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        if ($request->getMethod() == 'POST') {
            $passwordOriginal = $editForm->getData()->getPassword();

            $editForm->submit($request->get($editForm->getName()));

            if ($editForm->isValid()) {
                $entity->setPlainPassword($request->get($editForm->getName())['plainPassword']['first']);
                $this->get('fos_user.user_manager')->updateUser($entity, false);
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('usuario_edit', array('id' => $id)));
            }
        }
        return $this->render('Usuario/usuarioChangePass.html.twig', array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    public function changePassActualUserAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioPassType::class, $entity);
        $deleteForm = $this->createDeleteForm($this->get('security.token_storage')->getToken()->getUser()->getId());

        return $this->render('Usuario/CambiarPassword/cambiarPassword.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateChangePassActualUserAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Usuario que está buscando.');
        }

        $editForm = $this->createForm( UsuarioPassType::class, $entity);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        if ($request->getMethod() == 'POST') {
            $passwordOriginal = $editForm->getData()->getPassword();

            $editForm->submit($request->get($editForm->getName()));

            if ($editForm->isValid()) {
                $entity->setPlainPassword($request->get($editForm->getName())['plainPassword']['first']);
                $this->get('fos_user.user_manager')->updateUser($entity, false);
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('index'));
            }
        }
        return $this->render('Usuario/CambiarPassword/cambiarPassword.html.twig', array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        ));
    }
}
