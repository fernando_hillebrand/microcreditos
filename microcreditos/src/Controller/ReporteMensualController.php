<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\ReporteMensual;
use App\Entity\Programa;
use App\Form\ReporteMensualType;
/**
 * ReporteMensual controller.
 *
 */
class ReporteMensualController extends Controller
{
    /**
     * Displays a form to create a new entity.
     *
     */
    var $meses = array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
   
    public function showAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession() ;
        $session->remove('information');
        $sessionFiltro = $session->get('filtro_reporte');
        $em = $this->getDoctrine()->getManager();
        switch ($request->get('_opFiltro')) {
            case NULL:
            case 'limpiar':
                $ultPeriodo = $em->getRepository('App:Programa')->findUltPeriodo();
               // $nextPeriodo = date("Y-m-d", strtotime ("next Month",mktime(0,0,0,$ultPeriodo['mes'],1,$ultPeriodo['anio'])));
                $filtro = array('programa'=>'',
                                'programaId'=>'',
                              //  'mes'     =>date('m',  strtotime($nextPeriodo)),
                                'mes'     =>$ultPeriodo['mes'],
                                'anio'    =>$ultPeriodo['anio']);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'    =>$request->get('_programa'),
                    'programaId'  =>$request->get('_programaId'),
                    'mes'         =>$request->get('_mes'),
                    'anio'        =>$request->get('_anio'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'    =>$sessionFiltro['programa'],
                    'programaId'  =>$sessionFiltro['programaId'],
                    'mes'         =>$sessionFiltro['mes'],
                    'anio'        =>$sessionFiltro['anio']);
                break;
        }
                   
        $session->set('filtro_reporte',$filtro);
        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
        $periodo = $filtro['anio'] . '-' . str_pad($filtro['mes'], 2, '0', STR_PAD_LEFT) . '-01';   
        if( $filtro['programaId']==0 OR $filtro['programaId']=='' ){
            /* Informe general */
            $reportes = array();
            foreach($programas as $prog){
                 $filtro['programaId']=$prog->getId();
                 $reporteGral =  $em->getRepository('App:ReporteMensual')->findByProgramayPeriodo($filtro);

                 if(!is_null($reporteGral) AND is_array($reporteGral)){
                     if( count($reporteGral) > 0) {
                         array_push($reportes, $reporteGral);
                     }
                 }else{
                     /* Generar datos */
                     $auxi = $this->getReporteMensualByPrograma($filtro,$prog);
                     array_push($reportes,$auxi);
                 }
             }

            $vars = array( 
            'filtro'       => $session->get('filtro_reporte'),
            'entities'     => $reportes,
            'programas'    => $programas, 
            'meses'      => $this->meses);
            return $this->render('ReporteMensual/ReporteMensualGeneral.html.twig', $vars );
            
            
         /*   $reporteGral = $em->getRepository('App:ReporteMensual')->findByPeriodo($filtro);
            if( count($reporteGral)==0 ){
                $session->getFlashBag()->set('information', 'No existe ningún informe del periodo seleccionado');
            }
            
            $vars = array( 
            'filtro'       => $session->get('filtro_reporte'),
            'entities'     => $reporteGral,
            'programas'    => $programas, 
            'meses'      => $this->meses);
            return $this->render('MicrocreditosBundle:ReporteMensual:ReporteMensualGeneral.html.twig', $vars ); */
        }else{
            /* Reporte x programa */
            $entity = $em->getRepository('App:ReporteMensual')->findByProgramayPeriodo($filtro);
            $programa = $em->getRepository('App:Programa')->find($filtro['programaId']);
            if( !$entity ){
                /* Verificar que sea periodo correspondiente y correlativo */
                $siguientePeriodo = date("Y-m-d", strtotime ("next Month",mktime(0,0,0,$programa->getMes(),1,$programa->getAnio())));
                if($periodo == $siguientePeriodo){
                    
                    $entity = new ReporteMensual();
                    $entity->setUsuario($usuario);
                    $entity->setMes($filtro['mes']);
                    $entity->setAnio($filtro['anio']);
                    // Calculo Cartera 
                    $cartera = $em->getRepository('App:ReporteMensual')->getCarteraByPrograma($filtro);

                   // $cartera['activa']=$em->getRepository('App:ReporteMensual')->getFirstCAI($filtro['programa'])+$cartera['activa'];
                    $entity->setCarteraActiva( $cartera['activa'] ? $cartera['activa'] : 0 );

                    $entity->setCarteraNueva( $cartera['nueva'] ? $cartera['nueva'] : 0 );
                    $entity->setRenovaciones( $cartera['renovacion'] ? $cartera['renovacion'] : 0 );
                    $entity->setCobranzas( $cartera['cobranzas'] ? $cartera['cobranzas'] : 0 );
                    $entity->setIncobrables( $cartera['incobrables'] ? $cartera['incobrables'] : 0 );
                    $entity->setInteresSemana1( isset($cartera['intereses'][1]) ? $cartera['intereses'][1] : 0 );
                    $entity->setInteresSemana2( isset($cartera['intereses'][2]) ? $cartera['intereses'][2] : 0 );
                    $entity->setInteresSemana3( isset($cartera['intereses'][3]) ? $cartera['intereses'][3] : 0 );
                    $entity->setInteresSemana4( isset($cartera['intereses'][4]) ? $cartera['intereses'][4] : 0 );
                    $entity->setInteresSemana5( isset($cartera['intereses'][5]) ? $cartera['intereses'][5] : 0 );

                    $entity->setGastosAdministrativos( $cartera['gastosAdm'] ? $cartera['gastosAdm'] : 0 );
                    $entity->setPunitorios( $cartera['punitorios'] ? $cartera['punitorios'] : 0 );
                    $entity->setGastos( $cartera['gastos'] ? $cartera['gastos'] : 0 );
                    $entity->setHonorarios( $cartera['honorarios'] ? $cartera['honorarios'] : 0 );
                    $entity->setSaldoBanco($programa->getBanco());
                    /* Calculo Caja */
                    $caja = $em->getRepository('App:ReporteMensual')->getSaldoCaja($filtro);
                    $entity->setSaldoCaja($programa->getCaja()+$caja);
                    $entity->setAjuste(0);                    
                }  else {
                    $session->getFlashBag()->set('information', 'No existe informe del periodo seleccionado');
                }
                
            }
            $form = $this->createForm(ReporteMensualType::class, $entity);
            
            $vars = array(  
            'programa'   => $programa,
            'filtro'       => $session->get('filtro_reporte'),
            'programas'    => $programas, 
            'meses'      => $this->meses,
            'entity'     => $entity,
            'form'    => $form->createView());
            
            return $this->render('ReporteMensual/ReporteMensualPrograma.html.twig', $vars );
        }

    } 
    
    public function createAction(){

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $reporteMensual = new ReporteMensual();
        $form = $this->createForm(ReporteMensualType::class, $reporteMensual);

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));

            if ($form->isValid()) {
                $filtro = $session->get('filtro_reporte');
                $programa = $em->getRepository('App:Programa')->find($filtro['programaId']);
                $em->getConnection()->beginTransaction();
                try {

                    $reporteMensual->setUsuario($usuario);
                    $reporteMensual->setFecha(new \DateTime());
                    $em->persist($reporteMensual);
                    $em->flush();
                    $programa->setCaja($reporteMensual->getSaldoCaja() + $reporteMensual->getAjuste());
                    $programa->setBanco($reporteMensual->getSaldoBanco());
                    $programa->setMes($reporteMensual->getMes());
                    $programa->setAnio($reporteMensual->getAnio());
                    $em->persist($programa);
                    $em->flush();
                    /* Marcar pagos procesados  */
//                    $pagos = $em->getRepository('App:ReporteMensual')->getPagosReporte($filtro, $reporteMensual);
//                    foreach ($pagos as $pago) {
//                        $pago->setReporte($reporteMensual);
//                        $em->persist($pago);
//                        $em->flush();
//                    }
                    $pagos = $em->getRepository('App:ReporteMensual')->setReporteMensualMasive($filtro, $reporteMensual->getId());
                    $em->getConnection()->commit();

                } catch (\PDOException $e) {
                    $em->getConnection()->rollback();
                    // $session = $request->getSession();
                    $session->getFlashBag()->set('error', $e->getMessage());
                }
                $session->getFlashBag()->set('info', 'Se genero el Informe de Cierre');
            }
        } 
        return $this->redirect($this->generateUrl('reporte_mensual', array('_opFiltro' => 'show')));
    }
    
    public function deleteLastReporteAction($id) {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $reporte = $em->getRepository('App:ReporteMensual')->find($id);
        $programa = $em->getRepository('App:Programa')->find( $reporte->getPrograma()->getId() );
        $previo = $em->getRepository('App:ReporteMensual')->getReportePrevio($id,$programa->getId());
        $em->getConnection()->beginTransaction();
        try {
            /* Liberar pagos  */
            $pagos = $em->getRepository('App:Pago')->findByReporte($id);
            foreach ($pagos as $pago) {
                $pago->unsetReporte();
                $em->persist($pago);
            }
            /* Cambiar valores del programa */
            $programa->setCaja($previo->getSaldoCaja());
            $programa->setBanco($previo->getSaldoBanco());
            $programa->setMes($previo->getMes());
            $programa->setAnio($previo->getAnio());
            $em->persist($programa);
            
            $em->remove($reporte);            
            
            $em->flush();
            $em->getConnection()->commit();
           // $session->getFlashBag()->set('information', 'Se ha eliminado con éxito el Reporte' );
        } catch (\PDOException $e) {
            $em->getConnection()->rollback();
            // $session = $request->getSession();
            $session->getFlashBag()->set('error', $e->getMessage());
        }
        return $this->redirect($this->generateUrl('reporte_mensual', array('_opFiltro' => 'show')));
    }

    public function programaPdfAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $request = $this->getRequest() ;
        $session = $request->getSession();
        $filtro = $session->get('filtro_reporte');
        $tipo = '';
        $formData = $request->get('reportemensual');
        if($formData){
            $reporte = new ReporteMensual();
            $form = $this->createForm(ReporteMensualType::class, $reporte);
            $form->bindRequest($request);     
            $tipo = 'REPORTE PARCIAL EMITIDO EL';
        }else{
            $em = $this->getDoctrine()->getManager();
            $reporte = $em->getRepository('App:ReporteMensual')->findByProgramayPeriodo($filtro);
        }
        $programa = $em->getRepository('App:Programa')->find($filtro['programaId']);
        $partial = $this->renderView('ReporteMensual/ReporteMensualProgramaPdf.html.twig',
                array('entity'=>$reporte,'programa'=>$programa,'meses'=>$this->meses,'tipo'=>$tipo) );
        $periodo = str_pad($filtro['mes'], 2, '0', STR_PAD_LEFT) . $filtro['anio'] ;
        $fileName = 'ReporteMensual_'.$programa->getSigla().'_'.$periodo;
      //  return new Response($partial);
        $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($partial),array()),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="'.$fileName.'.pdf"' )
                         );
        return $response;
    }
    
    public function generalPdfAction(){
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $filtro = $session->get('filtro_reporte');
        //$reporteGral = $em->getRepository('App:ReporteMensual')->findByPeriodo($filtro);
        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $reportes = array();
        $tipo='';
            foreach($programas as $prog){
                 $filtro['programaId']=$prog->getId();
                 $reporteGral =  $em->getRepository('App:ReporteMensual')->findByProgramayPeriodo($filtro);
                 if(!is_null($reporteGral) AND $reporteGral){
                    array_push($reportes,$reporteGral);
                 }else{
                     /* Generar datos */
                     $tipo = 'REPORTE PARCIAL EMITIDO EL: ';
                     $auxi = $this->getReporteMensualByPrograma($filtro,$prog);
                     array_push($reportes,$auxi);
                 }
             }
        
        $partial = $this->renderView('ReporteMensual/reporteMensualGeneralPdf.html.twig',
                array('entities'=>$reportes,'filtro'=>$filtro,'programas'=>$programas,'meses'=>$this->meses,'tipo'=>$tipo) );
        $periodo = str_pad($filtro['mes'], 2, '0', STR_PAD_LEFT) . $filtro['anio'] ;
        $fileName = 'ReporteMensualGeneral_'.$periodo;
       // return new Response($partial);
        $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($partial),array('orientation'=>'Landscape')),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="'.$fileName.'.pdf"' )
                         );
        return $response;
    }
    
    public function verDetalleAction(){
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $filtro = $session->get('filtro_reporte');
        $detalle = $em->getRepository('App:ReporteMensual')->getDetalleByProgramayPeriodo($filtro);
        $partial = $this->renderView('ReporteMensual/ReporteMensualDetalle.html.twig',
               array('detalle'=>$detalle, 'filtro'=>$filtro,'meses'=>$this->meses)  );
        $periodo = str_pad($filtro['mes'], 2, '0', STR_PAD_LEFT) . $filtro['anio'] ;
        $fileName = 'ReporteMensual_Detalle_'.trim($filtro['programa']).'_'.$periodo;
        $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename="'.$fileName.'.xls"');
            $response->setContent($partial);
        return $response;
    }
    
    private function getReporteMensualByPrograma($filtro,$programa)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new ReporteMensual();
        //$entity->setUsuario($usuario);
        $entity->setPrograma($programa);
        $entity->setMes($filtro['mes']);
        $entity->setAnio($filtro['anio']);
        // Calculo Cartera
        $cartera = $em->getRepository('App:ReporteMensual')->getCarteraByPrograma($filtro);

       // $cartera['activa']=$em->getRepository('App:ReporteMensual')->getFirstCAI($filtro['programa'])+$cartera['activa'];
        $entity->setCarteraActiva( $cartera['activa'] ? $cartera['activa'] : 0 );

        $entity->setCarteraNueva( $cartera['nueva'] ? $cartera['nueva'] : 0 );
        $entity->setRenovaciones( $cartera['renovacion'] ? $cartera['renovacion'] : 0 );
        $entity->setCobranzas( $cartera['cobranzas'] ? $cartera['cobranzas'] : 0 );
        $entity->setIncobrables( $cartera['incobrables'] ? $cartera['incobrables'] : 0 );
        $entity->setInteresSemana1( isset($cartera['intereses'][1]) ? $cartera['intereses'][1] : 0 );
        $entity->setInteresSemana2( isset($cartera['intereses'][2]) ? $cartera['intereses'][2] : 0 );
        $entity->setInteresSemana3( isset($cartera['intereses'][3]) ? $cartera['intereses'][3] : 0 );
        $entity->setInteresSemana4( isset($cartera['intereses'][4]) ? $cartera['intereses'][4] : 0 );
        $entity->setInteresSemana5( isset($cartera['intereses'][5]) ? $cartera['intereses'][5] : 0 );
        $entity->setGastosAdministrativos( $cartera['gastosAdm'] ? $cartera['gastosAdm'] : 0 );
        $entity->setPunitorios( $cartera['punitorios'] ? $cartera['punitorios'] : 0 );
        $entity->setGastos( $cartera['gastos'] ? $cartera['gastos'] : 0 );
        $entity->setHonorarios( $cartera['honorarios'] ? $cartera['honorarios'] : 0 );
        $entity->setSaldoBanco($programa->getBanco());
        $entity->setObservacion('*RGA*');
        /* Calculo Caja */
        $caja = $em->getRepository('App:ReporteMensual')->getSaldoCaja($filtro);
        $entity->setSaldoCaja($programa->getCaja()+$caja);
        $entity->setAjuste(0);

        return $entity;
        
    }
}