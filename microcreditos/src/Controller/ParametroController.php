<?php

namespace App\Controller;

use App\Entity\TablaGastos;
use App\Form\TablaGastosType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\Parametro;
use App\Form\ParametroType;
use App\Form\RepresentanteType;
use App\Form\UltimosNrosType;
use App\Form\ConceptosCajaType;

/**
 * Parametro controller.
 *
 */
class ParametroController extends Controller
{
    /**
     * Lists all Parametro entities.
     *
     */
    public function indexAction($agrupador = null)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $agrupadores = $em->getRepository('App:Parametro')->getAgrupadores();
        if($agrupador == null ){
            $agrupador = $agrupadores[0];
        }else{
            $agrupador = $em->getRepository('App:Parametro')->find($agrupador);
        }

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:Parametro')->findByAgrupadorDQL($agrupador);

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('Parametro/parametroIndex.html.twig', array(
                        'entityName'   => 'parametro',
                        'agrupadorId'  => $agrupador->getId(), 
                        'entities'     => $entities,
                        'agrupadores'  => $agrupadores,
                        'paginator'    => $paginator
        ));
    }

    /**
     * Finds and displays a Parametro entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Parametro/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Parametro entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->find($this->getRequest()->get('agrupador'));

        $entity = new Parametro();
        $entity->setAgrupador($agrupador);
        
        $form   = $this->createForm(ParametroType::class, $entity);

        return $this->render('Parametro/parametroEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Parametro entity.
     *
     */
    public function createAction()
    {
        $entity  = new Parametro();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(ParametroType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El nuevo parámetro se ha creado con éxito');
                return $this->redirect($this->generateUrl('parametro_edit', array('id' => $entity->getId())));
            }
        }
        return $this->render('Parametro/parametroEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Parametro entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm = $this->createForm(ParametroType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Parametro/parametroEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Edits an existing Parametro entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm   = $this->createForm(ParametroType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('parametro_edit', array('id' => $id)));
            }
        }

        return $this->render('Parametro/parametroEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Parametro entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Parametro')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Parametro entity.');
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('parametro_back', array('agrupador' => $entity->getAgrupador()->getId())));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }    
    
    /* PARAMETRO ULTIMOS NUMEROS */
    public function ultimosNrosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities  = $em->getRepository('App:Parametro')->getUltimosNumeros();

        return $this->render('Parametro/ultimosNrosIndex.html.twig', array(
                        'entityName'   => 'parametro',
                        'entities'     => $entities
        ));
    }
    public function editUltimoNroAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm = $this->createForm(new UltimosNrosType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return $this->render('Parametro/ultimosNrosEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }    
    public function updateUltimoNroAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }
        $editForm   = $this->createForm(UltimosNrosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('parametro_ultimo_nro_edit', array('id' => $id)));
            }
        }

        return $this->render('Parametro/ultimosNrosEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /* PARAMETROS CONCEPTOS DE CAJA */
    public function conceptosCajaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities  = $em->getRepository('App:Parametro')->getConceptosCaja();

        return $this->render('Parametro/conceptosCajaIndex.html.twig', array(
                        'entityName'   => 'parametro',
                        'entities'     => $entities
        ));
    }
    public function newConceptosCajaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->findOneByDescripcion('conceptos-caja');
        $entity = new Parametro();
        $entity->setAgrupador($agrupador);
        $entity->setDescripcion('E');
        $entity->setNumerico(0);
        $form   = $this->createForm(ConceptosCajaType::class, $entity);

        return $this->render('Parametro/conceptosCajaEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
    public function createConceptosCajaAction()
    {
        $entity  = new Parametro();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(ConceptosCajaType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setDescripcion('E');
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El nuevo concepto se ha creado con éxito');
                return $this->redirect($this->generateUrl('parametro_conceptos_caja_edit', array('id' => $entity->getId())));
            }
        }
        return $this->render('Parametro/conceptosCajaEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
    public function editConceptosCajaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Parametro')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }
        
        $editForm = $this->createForm(ConceptosCajaType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);
        
        return $this->render('Parametro/conceptosCajaEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }    
    public function updateConceptosCajaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }
       
        $editForm   = $this->createForm(ConceptosCajaType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);
 
        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('parametro_conceptos_caja_edit', array('id' => $id)));
            }
        }
        return $this->render('Parametro/conceptosCajaEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }    
    public function deleteConceptosCajaAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Parametro')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Parametro entity.');
                }
                $em->remove($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Se ha eliminado el concepto.');
            }
        }
        return $this->redirect($this->generateUrl('parametro_conceptos_caja'));
    }    
    
    
    
    public function parametrosByAgrupadorAction(){
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->find($this->getRequest()->get('agrupador'));
        
        $paginator = $this->get('Ideup.simple_paginator');
        $entities  = $paginator->paginate($em->getRepository('App:Parametro')->findByAgrupador($agrupador->getId()))->getResult();

        $partialParametros = $this->renderView('Parametro/parametroList.html.twig', array(
            'entities'  =>  $entities,
            'entityName'   => 'parametro',
            'paginator'    => $paginator
            ));
        return new Response(json_encode($partialParametros));  
    }

    public function gastosAdmIndexAction(){
        $em = $this->getDoctrine()->getManager();
        $gastosadm = $em->getRepository('App:TablaGastos')->findBy(array(),array('periodo' => 'ASC','desde' => 'ASC'));
        return $this->render('Parametro/gastosAdmIndex.html.twig', array(
            'entities'      => $gastosadm
        ));                
    }
    
    public function gastosAdmNewAction(){
        $entity = new TablaGastos();
        $form   = $this->createForm(TablaGastosType::class, $entity);

        return $this->render('Parametro/gastosAdmEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
        
        
    }
    public function gastosAdmCreateAction(){
        $entity  = new TablaGastos();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(TablaGastosType::class, $entity);
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $session = $request->getSession();
                $existe = $em->getRepository('App:Parametro')->conflictoRangoGastosAdm($entity->getPeriodo(), $entity->getDesde(), $entity->getHasta());

                if ($existe) {
                    $session->getFlashBag()->set('error', 'El rango se superpone con otro existente');
                } else {
                    $em->persist($entity);
                    $em->flush();
                    $session->getFlashBag()->set('success', 'El nuevo registro se ha creado con éxito');
                    return $this->redirect($this->generateUrl('parametro_gastosAdmIdx'));
                }
            }
        }
        return $this->render('Parametro/gastosAdmEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));        
        
    }
    public function gastosAdmEditAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:TablaGastos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tabla Gastos entity.');
        }

        $editForm = $this->createForm(TablaGastosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Parametro/gastosAdmEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
    }
    
    public function gastosAdmUpdateAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:TablaGastos')->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }
        $editForm   = $this->createForm(TablaGastosType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('parametro_gastosAdmIdx'));
            }
        }


        return $this->render('Parametro:gastosAdmEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    public function gastosAdmDeleteAction($id){
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:TablaGastos')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Parametro entity.');
                }
                $em->remove($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Se ha eliminado el concepto.');
            }
        }
        return $this->redirect($this->generateUrl('parametro_gastosAdmIdx'));        
    }

    /**
     * Displays a form to edit an existing Parametro entity.
     *
     */
    public function datosRepresentanteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Parametro')->getRepresentante();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm = $this->createForm(RepresentanteType::class, $entity);

        return $this->render('Parametro/representanteEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView()
        ));
    }    
    public function updateDatosRepresentanteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }
        $editForm   = $this->createForm(RepresentanteType::class, $entity);

        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('parametro_representante', array('id' => $id)));
            }
        }

        return $this->render('Parametro/representanteEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView()
        ));
    } 
    
    
    
}