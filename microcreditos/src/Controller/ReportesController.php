<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/**
 * Ingresos controller.
 *
 */
class ReportesController extends Controller
{


    /**
     * Listado de Clientes por Grupos
     *
     */
    public function indexListadoClientesAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa' => '',  'page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                    'programa' => $request->get('_programa'),
                    'page' => $request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa' => $sessionFiltro['programa'],
                    'page' => (isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1)
                );
                break;
        }

        $session->set('filtro_ingreso', $filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;

        $entitiesArray = $em->getRepository('App:Credito')
            ->getListadoClintes($session->get('filtro_ingreso'),10, $request->query->getInt('page', 1) );

        $query = $em->getRepository('App:Credito')->getListadoClientePaginador($session->get('filtro_ingreso'));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );


        $programas = $em->getRepository('App:Programa')->findByActivosDQL();

        $vars = array(
            'entityName' => 'caja',
            'entities' => $entities,
            'entitiesArray' =>$entitiesArray,
            'filtro' => $session->get('filtro_ingreso'),
            'programas' => $programas,
            'paginator' => $paginator);

        return $this->render('Reportes/listadoClientes/listado.html.twig', $vars);
    }

    public function getListadoClientesOpListAction()
    {
        $partial = $this->renderView('Reportes/listadoClientes/opList.html.twig');
        return new Response($partial);
    }

    public function getListadoClientesDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        set_time_limit(0);

        $sessionFiltro = $session->get('filtro_ingreso');

        $entitiesArray = $em->getRepository('App:Credito')->getListadoClintes();

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
        $html= $this->renderView('Reportes/listadoClientes/listXls.html.twig',
            array('filtro'=>($sessionFiltro)?$sessionFiltro:'',
                  'entities'=>$entitiesArray));
        $response->setContent($html);

        return $response;
    }

    /**
     * Listado de Clientes por Emprendedores con Creditos individuales
     *
     */
    public function indexListadoClientesEmprendedorAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa' => '',  'page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                    'programa' => $request->get('_programa'),
                    'page' => $request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa' => $sessionFiltro['programa'],
                    'page' => (isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1)
                );
                break;
        }

        $session->set('filtro_ingreso', $filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;

        $entitiesArray = $em->getRepository('App:Credito')
            ->getListadoClintesEmprendedores($session->get('filtro_ingreso'),10, $request->query->getInt('page', 1) );

        $query = $em->getRepository('App:Credito')->getListadoClientePaginadorEmprendedores($session->get('filtro_ingreso'));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );


        $programas = $em->getRepository('App:Programa')->findByActivosDQL();

        $vars = array(
            'entityName' => 'caja',
            'entities' => $entities,
            'entitiesArray' =>$entitiesArray,
            'filtro' => $session->get('filtro_ingreso'),
            'programas' => $programas,
            'paginator' => $paginator);

        return $this->render('Reportes/listadoClientesEmprendedores/listado.html.twig', $vars);
    }

    public function getListadoClientesEmprendedorOpListAction()
    {
        $partial = $this->renderView('Reportes/listadoClientesEmprendedores/opList.html.twig');
        return new Response($partial);
    }

    public function getListadoClientesEmprendedorDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        set_time_limit(0);

        $sessionFiltro = $session->get('filtro_ingreso');

        $entitiesArray = $em->getRepository('App:Credito')->getListadoClintesEmprendedores();

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
        $html= $this->renderView('Reportes/listadoClientesEmprendedores/listXls.html.twig',
            array('filtro'=>($sessionFiltro)?$sessionFiltro:'',
                'entities'=>$entitiesArray));
        $response->setContent($html);

        return $response;
    }

    /**
     * Listado de Informe de Cuotas
     *
     */
    public function indexReporteInformeCuotasAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_reporte_informe_cuotas');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','page' => 1,'desde','hasta');
                break;
            case 'buscar':
                $filtro = array(
                    'programa' => $request->get('_programa'),
                    'desde' => $request->get('_desde'),
                    'hasta' => $request->get('_hasta'),
                    'page' => $request->get('page')
                );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa' => $sessionFiltro['programa'],
                    'desde' => $sessionFiltro['desde'],
                    'hasta' => $sessionFiltro['hasta'],
                    'page' => (isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1),
                );
                break;
        }

        $filtro['itemsperpage'] = '';
        $filtro['grupo'] = '';
        $filtro['grupoId'] = '';
        $filtro['asesor'] = '';
        $filtro['emprendedor']  = '';
        $filtro['emprendedorId'] = '';
        $filtro[ 'desdeCarga'] = '';
        $filtro['hastaCarga']  = '';
        $filtro[ 'condonacion'] = '';

        $session->set('filtro_reporte_informe_cuotas', $filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;

        $query =  $em->getRepository('App:Pago')
            ->findPagosByFiltroDQL($session->get("filtro_reporte_informe_cuotas"));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );


        $programas = $em->getRepository('App:Programa')->findByActivosDQL();

        $vars = array(
            'entityName' => 'caja',
            'entities' => $entities,
            'filtro' => $session->get('filtro_reporte_informe_cuotas'),
            'programas' => $programas,
            'paginator' => $paginator);

        return $this->render('Reportes/informeCuotas/listado.html.twig', $vars);
    }

    public function getReporteInformeCuotasOpListAction()
    {
        $partial = $this->renderView('Reportes/informeCuotas/opList.html.twig');
        return new Response($partial);
    }

    public function getReporteInformeCuotasDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        set_time_limit(0);

        $sessionFiltro = $session->get('filtro_reporte_informe_cuotas');

        $query =  $em->getRepository('App:Pago')
            ->findPagosByFiltroDQL($session->get("filtro_reporte_informe_cuotas"))->getResult();

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
        $html= $this->renderView('Reportes/informeCuotas/listXls.html.twig',
            array('filtro'=>($sessionFiltro)?$sessionFiltro:'',
                'entities'=>$query));
        $response->setContent($html);

        return $response;
    }


    /**
     * Listado de Informe de RECA
     *
     */
    public function indexReporteRecaAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_reporte_reca');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','page' => 1,'desde','hasta');
                break;
            case 'buscar':
                $filtro = array(
                    'programa' => $request->get('_programa'),
                    'desde' => $request->get('_desde'),
                    'hasta' => $request->get('_hasta'),
                    'page' => $request->get('page')
                );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa' => $sessionFiltro['programa'],
                    'desde' => isset($sessionFiltro['desde']) ? $sessionFiltro['desde'] : date('d-m-Y'),
                    'hasta' => isset($sessionFiltro['hasta']) ? $sessionFiltro['hasta'] : date('d-m-Y'),
                    'page' => (isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1),
                );
                break;
        }

        $filtro['itemsperpage'] = '';
        $filtro['grupo'] = '';
        $filtro['grupoId'] = '';
        $filtro['asesor'] = '';
        $filtro['emprendedor']  = '';
        $filtro['emprendedorId'] = '';
        $filtro['desdeCarga'] = '';
        $filtro['hastaCarga']  = '';
        $filtro['condonacion'] = '';

        $session->set('filtro_reporte_reca', $filtro);
        $em = $this->getDoctrine()->getManager();



        $arrayValues =  $em->getRepository('App:Pago')
            ->getRecaValues($session->get("filtro_reporte_reca"));


        $programas = $em->getRepository('App:Programa')->findByActivosDQL();

        $vars = array(
            'entityName' => 'caja',
            'entities' => $arrayValues,
            'filtro' => $session->get('filtro_reporte_reca'),
            'programas' => $programas,
            'paginator'=>null);

        return $this->render('Reportes/reca/listado.html.twig', $vars);
    }

    public function getReporteRecaOpListAction()
    {
        $partial = $this->renderView('Reportes/reca/opList.html.twig');
        return new Response($partial);
    }

    public function getReporteRecaDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        set_time_limit(0);

        $sessionFiltro = $session->get('filtro_reporte_informe_cuotas');

        $arrayValues =  $em->getRepository('App:Pago')
            ->getRecaValues($session->get("filtro_reporte_reca"));

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
        $html= $this->renderView('Reportes/reca/listXls.html.twig',
            array('filtro'=>($sessionFiltro)?$sessionFiltro:'',
                'entities'=>$arrayValues));
        $response->setContent($html);

        return $response;
    }

    /**
     * Listado de Informe de Cartera
     *
     */
    public function indexReporteInformeCarteraAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_reporte_informe_cartera');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('asesor'=>'','page' => 1,'desde','hasta','desde_mora','hasta_mora');
                break;
            case 'buscar':
                $filtro = array(
                    'desde' => $request->get('_desde'),
                    'hasta' => $request->get('_hasta'),
                    'desde_mora' => $request->get('_desde_mora'),
                    'hasta_mora' => $request->get('_hasta_mora'),
                    'asesor' => $request->get('_asesor'),
                    'page' => $request->get('page'),
                );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'asesor' => $sessionFiltro['asesor'],
                    'desde' => isset($sessionFiltro['desde']) ? $sessionFiltro['desde'] : date('d-m-Y'),
                    'hasta' => isset($sessionFiltro['hasta']) ? $sessionFiltro['hasta'] : date('d-m-Y'),
                    'desde_mora' => isset($sessionFiltro['desde_mora']) ? $sessionFiltro['desde_mora'] : '',
                    'hasta_mora' => isset($sessionFiltro['hasta_mora']) ? $sessionFiltro['hasta_mora'] : '',
                    'page' => (isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1),
                );
                break;
        }

        $session->set('filtro_reporte_informe_cartera', $filtro);
        $em = $this->getDoctrine()->getManager();

        $arrayValues =  $em->getRepository('App:Pago')
            ->getInformeCarteraValues($session->get("filtro_reporte_informe_cartera"));

        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $usuarios = $em->getRepository('App:User')->findByMyCriteria($sessionFiltro['asesor']);

        $vars = array(
            'entityName' => 'caja',
            'entities' => $arrayValues,
            'filtro' => $session->get('filtro_reporte_informe_cartera'),
            'asesores' => $usuarios,
            'paginator'=>null);

        return $this->render('Reportes/informe_cartera/listado.html.twig', $vars);
    }

    public function getReporteInformeCarteraOpListAction()
    {
        $partial = $this->renderView('Reportes/informe_cartera/opList.html.twig');
        return new Response($partial);
    }

    public function getReporteInformeCarteraDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        set_time_limit(0);

        $sessionFiltro = $session->get('filtro_reporte_informe_cartera');

        $arrayValues =  $em->getRepository('App:Pago')
            ->getInformeCarteraValues($session->get("filtro_reporte_informe_cartera"));

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Ingresos.xls');
        $html= $this->renderView('Reportes/informe_cartera/listXls.html.twig',
            array('filtro'=>($sessionFiltro)?$sessionFiltro:'',
                'entities'=>$arrayValues));
        $response->setContent($html);

        return $response;
    }

    public function estado_resultados(): Response
    {
    
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();
        $year = 0;
        if(!empty($request->get('year'))){
            $year = $request->get('year');
        }else{
            $year = "2022";
        }

        if(!empty($request->get('mes'))){
            $month = $request->get('mes');
        }else{
            $month = "1";
        }
        
        $em = $this->getDoctrine()->getManager();
        $creditos = $em->getRepository('App:Credito')->getEstadoResultadosCredito($month, $year);
        $cuota = $em->getRepository('App:Cuota')->getEstadoResultadosCuota($month, $year);
       
        //dd($gastos_totales_total);
        
        foreach($creditos as $credito){
               
            $colocacion[] = [ "fecha" => $credito['mes'].'-'.$credito['year'] , "colocacion" =>  floatval($credito['colocacion']) ] ;
        }

        foreach($cuota as $cuotas){
               
            $cuota_mes[] = [  "interes" =>  floatval($cuotas['intereses']), "punitorios" =>  floatval($cuotas['punitorios']), "gastoAdmin" =>  floatval($cuotas['gastoAdmin']) ] ;
        }

       

        $adelanta = 0;
            $anios = array();
            for($i = date("Y"); $i >= date("Y") - 5; $i--){
                $anios[] = array("a" => $i, $i + $adelanta);
            }
        

       if($creditos == null){
        $colocacion = 0;
       }
       if($cuota == null){
        $cuota_mes = 0;
       }

       

        return $this->render('Reportes/estado_resultados/index.html.twig', 
          [ 
            'creditos'       => $colocacion, 
            'cuota'          => $cuota_mes,  
            'year'           =>$anios]
         );
      
    }

    public function patrimonio(): Response
    {
    
        $em = $this->getDoctrine()->getManager();

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if(!empty($request->get('fecha'))){
            $fecha = $request->get('fecha');
        }else{
            $fecha = date("d-m-Y");
        }
        

  

        $caja = $em->getRepository('App:Caja')->getTotalCaja($fecha);

        $creditos = $em->getRepository('App:Credito')->getMontoCreditos($fecha);

        $cuota = $em->getRepository('App:Cuota')->getCapitalCuotas($fecha);

        $cuota = $em->getRepository('App:Cuota')->getCapitalCuotas($fecha);

        $intereses = $em->getRepository('App:Cuota')->getInteresesCobrar($fecha);
        

        
        if($request->get('saldo_bancos')==null){
            $bancos = 0;
        }else{
            $bancos = $request->get('saldo_bancos');

        }

        $creditosAdmin = $em->getRepository('App:CreditosAdmin')->getTotal($fecha);
   
        $fecha = date("d-m-Y");

        return $this->render('Reportes/patrimonio/index.html.twig', 
          ['caja' => $caja, 'fecha' => $fecha, 'monto_credito' => $creditos, 'cuota' => $cuota,
           'intereses' => $intereses,
          'bancos' => $bancos, 'creditosAdmin' => $creditosAdmin]
         );
      

    }


    public function colocacionMensual(): Response
    {
        //return $this->render('graficos/index.html.twig', [
        //    'controller_name' => 'GraficosController',
        //]);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_gastos');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('desde'=>'','hasta'=>'');
                break;
            case 'buscar':
                $filtro = array(
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta')
                );
                break;
            default:
               
                $filtro = array(
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta']
                    );
                break;
        }
             
        $session->set('filtro_gastos',$filtro);
       
        $em = $this->getDoctrine()->getManager();

        $creditos = $em->getRepository('App:Credito')->getMontoColocacionMensual($session->get('filtro_gastos'));

        foreach($creditos as $credito){
               
            $colocacionMensual[] = [  "name" => $credito['mes'].'-'.$credito['year'] , "y" =>  floatval($credito['monto']) ] ;
        }

        $cantidad = $em->getRepository('App:Credito')->getCantidadColocacionMensual($session->get('filtro_gastos'));
        //$emprendedores = $em->getRepository('App:Credito')->getEmprendedores($session->get('filtro_gastos'));

        foreach($cantidad as $cant){
               
            $cantidadColocacionMensual[] = [  "name" => $cant['mes'].'-'.$cant['year'] , "y" =>  floatval($cant['cantidad']) ] ;
        }


        $RAW_QUERY = 'SELECT  MONTH(fecha_credito) as mes, YEAR(fecha_credito) AS year, count(grupos_emprendedores.emprendedor_id) as cantidad  FROM `credito` 
        INNER JOIN grupos_emprendedores ON grupos_emprendedores.grupo_id = credito.grupo_id
        GROUP BY mes, year;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $emprendedores = $statement->fetchAll();

        
        foreach($emprendedores as $cant){
               
            $emprendedoresCantMensual[] = [  "name" => $cant['mes'].'-'.$cant['year'] , "y" =>  floatval($cant['cantidad']) ] ;
        }

        if($creditos!=null){
            return $this->render('Reportes/graficos/index.html.twig', 
            ['emprendedoresCantMensua' => json_encode($emprendedoresCantMensual),'colocacionMensual' => json_encode($colocacionMensual), 'cantidadColocacionMensual' => json_encode($cantidadColocacionMensual)]
           );
  
        }else{
            return $this->render('Reportes/graficos/index.html.twig', 
            ['emprendedoresCantMensua' => null,'colocacionMensual' => null, 'cantidadColocacionMensual' => null]
           );

        } 
        
    }

    public function cobranza(): Response
    {
    
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_gastos');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('desde'=>'','hasta'=>'');
                break;
            case 'buscar':
                $filtro = array(
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta')
                );
                break;
            default:
               
                $filtro = array(
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta']
                    );
                break;
        }
             
        $session->set('filtro_gastos',$filtro);
       
        $em = $this->getDoctrine()->getManager();

        $capital_cuota = $em->getRepository('App:Pago')->getCuotaCapitalMensual($session->get('filtro_gastos'));
        
        foreach($capital_cuota as $cuota){
               
            $capital[] = [  "name" => $cuota['mes'].'-'.$cuota['year'] , "y" =>  floatval($cuota['capital']) ] ;
        }


        $interes_cuota = $em->getRepository('App:Pago')->getCuotaInteresMensual($session->get('filtro_gastos'));

        foreach($interes_cuota as $int_cuo){
               
            $interes[] = [  "name" => $int_cuo['mes'].'-'.$int_cuo['year'] , "y" =>  floatval($int_cuo['interes']) ];
        }

        $total_cuota = $em->getRepository('App:Pago')->getCuotaTotalMensual($session->get('filtro_gastos'));

        foreach($total_cuota as $tot_cu){
               
            $total[] = [  "name" => $tot_cu['mes'].'-'.$tot_cu['year'] , "y" =>  floatval($tot_cu['total']) ] ;
        }

        $cantidad_cuota = $em->getRepository('App:Pago')->getCuotaCantidadMensual($session->get('filtro_gastos'));

        foreach($cantidad_cuota as $cant_cu){
               
            $cantidad[] = [  "name" => $cant_cu['mes'].'-'.$cant_cu['year'] , "y" =>  floatval($cant_cu['cantidad']) ] ;
        }

        if( $capital_cuota !=null){
            return $this->render('Reportes/graficos/cobranza.html.twig', 
            ['capital' => json_encode($capital), 'interes' => json_encode($interes), 'total' => json_encode($total), 'cantidad' => json_encode($cantidad)]
           );

        }else{

            return $this->render('Reportes/graficos/cobranza.html.twig', 
            ['capital' => null, 'interes' => null, 'total' => null, 'cantidad' => null]
            );
        }
    }



}
