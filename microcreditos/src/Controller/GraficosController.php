<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Credito;
use App\Entity\Usuarios;
use App\Entity\Cuota;

class GraficosController extends Controller
{
  
    public function colocacionMensual(): Response
    {
        //return $this->render('graficos/index.html.twig', [
        //    'controller_name' => 'GraficosController',
        //]);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_gastos');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('desde'=>'','hasta'=>'');
                break;
            case 'buscar':
                $filtro = array(
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta')
                );
                break;
            default:
               
                $filtro = array(
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta']
                    );
                break;
        }
             
        $session->set('filtro_gastos',$filtro);
       
        $em = $this->getDoctrine()->getManager();

        $creditos = $em->getRepository('App:Credito')->getMontoColocacionMensual($session->get('filtro_gastos'));

        foreach($creditos as $credito){
               
            $colocacionMensual[] = [  "name" => $credito['mes'].'-'.$credito['year'] , "y" =>  floatval($credito['monto']) ] ;
        }

        $cantidad = $em->getRepository('App:Credito')->getCantidadColocacionMensual($session->get('filtro_gastos'));

        foreach($cantidad as $cant){
               
            $cantidadColocacionMensual[] = [  "name" => $cant['mes'].'-'.$cant['year'] , "y" =>  floatval($cant['cantidad']) ] ;
        }
      
        return $this->render('graficos/index.html.twig', 
          ['colocacionMensual' => json_encode($colocacionMensual), 'cantidadColocacionMensual' => json_encode($cantidadColocacionMensual)]
         );

    }

    public function cobranza(): Response
    {
    
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();

        $sessionFiltro = $session->get('filtro_gastos');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('desde'=>'','hasta'=>'');
                break;
            case 'buscar':
                $filtro = array(
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta')
                );
                break;
            default:
               
                $filtro = array(
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta']
                    );
                break;
        }
             
        $session->set('filtro_gastos',$filtro);
       
        $em = $this->getDoctrine()->getManager();

        $capital_cuota = $em->getRepository('App:Cuota')->getCuotaCapitalMensual($session->get('filtro_gastos'));

        foreach($capital_cuota as $cuota){
               
            $capital[] = [  "name" => $cuota['mes'].'-'.$cuota['year'] , "y" =>  floatval($cuota['capital']) ] ;
        }


        $interes_cuota = $em->getRepository('App:Cuota')->getCuotaInteresMensual($session->get('filtro_gastos'));

        foreach($interes_cuota as $int_cuo){
               
            $interes[] = [  "name" => $int_cuo['mes'].'-'.$int_cuo['year'] , "y" =>  floatval($int_cuo['interes']) ] ;
        }

        $total_cuota = $em->getRepository('App:Cuota')->getCuotaTotalMensual($session->get('filtro_gastos'));

        foreach($total_cuota as $tot_cu){
               
            $total[] = [  "name" => $tot_cu['mes'].'-'.$tot_cu['year'] , "y" =>  floatval($tot_cu['total']) ] ;
        }

        $cantidad_cuota = $em->getRepository('App:Cuota')->getCuotaCantidadMensual($session->get('filtro_gastos'));

        foreach($cantidad_cuota as $cant_cu){
               
            $cantidad[] = [  "name" => $cant_cu['mes'].'-'.$cant_cu['year'] , "y" =>  floatval($cant_cu['cantidad']) ] ;
        }

        return $this->render('graficos/cobranza.html.twig', 
          ['capital' => json_encode($capital), 'interes' => json_encode($interes), 'total' => json_encode($total), 'cantidad' => json_encode($cantidad)]
         );
      

    }
}
