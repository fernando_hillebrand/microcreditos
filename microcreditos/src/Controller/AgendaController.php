<?php
namespace App\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Agenda;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class AgendaController extends Controller
{

    public function getTurnosAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $ini = $request->get('ini');
        if(!$ini)
            $ini = date("d-m-Y", strtotime(date("Y-m-d")." -1 week"));
        $fin = $request->get('fin');
        if(!$fin)
            $fin = date("d-m-Y", strtotime(date("Y-m-d")." +1 month"));

        $turnos = $em->getRepository('App:Agenda')
                     ->findTurnosByFiltro($request->get('asesor'),$ini,$fin);
        //turnos
        $salidaTurnos = array(); 
        foreach($turnos as $turno){
                //if($turno->getFechaCancelado()) $color='grey'; else $color='#6EAC2C';
                if($turno->getAsistio()) $color='grey'; else $color='darkslategrey';
                //$this->generateUrl('turno_reservarTurno', array('id' => $turno->getId()));
                $valTurnos = array(
			'id' => $turno->getId(),
			'title' => $turno->getNombre(),
			'start' => $turno->getFechaInicio()->format('Y-m-d H:i'),
                        'end'   => $turno->getFechaFin()->format('Y-m-d H:i'),
                        'color' => $color,
                        'asesor' => $turno->getAsesor()->getUsername(),
                        'grupo' => ($turno->getGrupo())?$turno->getGrupo()->getNombre():'',
                        'grupoId' => ($turno->getGrupo())?$turno->getGrupo()->getId():'',
                        'emp' => ($turno->getEmprendedor())?$turno->getEmprendedor()->__toString():'',
                        'empId' => ($turno->getEmprendedor())?$turno->getEmprendedor()->getId():'',
                        'newemp' => $turno->getNuevoEmprendedor(),
                        'obs' => $turno->getObservaciones(),
                        'est' => $turno->getAsistio()
		);
                array_push($salidaTurnos, $valTurnos); 
            }
        return new Response(json_encode(array('turnos'=>$salidaTurnos,'ini'=>$ini,'fin'=>$fin)));
    }
    
    public function turnoAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $tipo = 'pempnew';
        if($request->get('id')){
            $turno =  $em->getRepository('App:Agenda')->find($request->get('id'));
            if($turno->getGrupo())              $tipo='pgrupo';
            elseif($turno->getEmprendedor())    $tipo='pemp';
            else                                $tipo='pempnew';
        }
        else{
            $fechaInicio = new \DateTime( $request->get('start'));
            $minutos = ($fechaInicio->format('i') == "00" || $fechaInicio->format('i')== "30")?'+30 minute':'+15 minute';
            $fin = date ( 'Y-m-d H:i' , strtotime ( $minutos , strtotime ( $request->get('start') ) ));
            $turno = new Agenda();
            $turno->setFechaInicio($fechaInicio);
            $turno->setFechaFin(new \DateTime( $fin ));
        }
        $asesores = $em->getRepository('App:Usuario')->findByMyCriteria();
        $dia = DefaultController::longDateSpanish($turno->getFechaInicio(),true);
        $partial = $this->renderView('Agenda/_turno.html.twig',
                array('tipoemp'=>$tipo,'dia'=>$dia,'turno'=>$turno,'asesores'=>$asesores)); 
        return new Response(json_encode($partial));
    }


    public function reservarTurnoAction(){
        /* Reserva o Guarda los cambios del Turno */
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $fechaInicio = new \DateTime( $request->get('startTurno'));
        $asesor = $em->getRepository('App:Usuario')->find($request->get('asesor_turno'));
        $grupo = $em->getRepository('App:Grupo')->find($request->get('_grupoId'));
        $emprendedor = $em->getRepository('App:Emprendedor')->find($request->get('_emprendedorId'));
        $nuevo = $request->get('_emprendedor_nuevo');
        $observaciones = $request->get('observ');
        try {
        if($request->get('_turnoId')){
            $agenda =  $em->getRepository('App:Agenda')->find($request->get('_turnoId'));
            $fin = $request->get('endTurno');
            $agenda->unsetEmprendedor();
            $agenda->unsetGrupo();
            $agenda->setNuevoEmprendedor(NULL);
            if( is_null($request->get('atendido')) || $request->get('atendido')=='' ) $agenda->setAsistio(0); 
                else $agenda->setAsistio(1);
        }else{
            $agenda = new Agenda();
            $minutos = ($fechaInicio->format('i') == "00" || $fechaInicio->format('i')== "30")?'+30 minute':'+15 minute';
            $fin = date ( 'Y-m-d H:i' , strtotime ( $minutos , strtotime ( $request->get('startTurno') ) ));
        }
        
        $fechaFin = new \DateTime( $fin );
        $agenda->setAsesor($asesor);
        if($grupo)              $agenda->setGrupo($grupo);
        elseif($emprendedor)    $agenda->setEmprendedor($emprendedor);
        else                    $agenda->setNuevoEmprendedor($nuevo);
        $agenda->setFechaInicio($fechaInicio);
        $agenda->setFechaFin($fechaFin);
        $agenda->setObservaciones($observaciones);
        $agenda->setFechaAsignado(new \Datetime);
        $em->persist($agenda);
        $em->flush();
        $msg='OK';
        } catch (Exception $exc) {
            $msg= $exc->getTraceAsString();
        }
        return new Response(json_encode($msg));
    }
    
    public function cancelarTurnoAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        try {
            $em->getRepository('App:Agenda')->deleteTurno($request->get('id'));
            $msg='OK';
        } catch (Exception $exc) {
            $msg= $exc->getTraceAsString();
        }
        return new Response(json_encode($msg));
    }
    public function dropTurnoAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $dias = $request->get('dias');
        $minutos = $request->get('minutos');
        $em = $this->getDoctrine()->getManager();
        try {
            $turno =  $em->getRepository('App:Agenda')->find($id);
     
            $periodo = strtotime($turno->getFechaFin()->format("Y-m-d H:i")) - strtotime($turno->getFechaInicio()->format("Y-m-d H:i"));
            $cantdias = ($dias<0)? strval($dias).' days' : '+'.strval($dias).' days';
            $cantmin = ($minutos<0)? strval($minutos).' minutes' : '+'.strval($minutos).' minutes';
            $cambio = $cantdias.' '.$cantmin;
            $fechaInicio = date ( 'Y-m-d H:i' , strtotime ( $cambio , strtotime ( $turno->getFechaInicio()->format('Y-m-d H:i') ) ));
            $turno->setFechaInicio(new \DateTime( $fechaInicio ));
            $dif = '+'.$periodo.' seconds';
            $fechaFin = date ( 'Y-m-d H:i' , strtotime ( $dif , strtotime ( $turno->getFechaInicio()->format('Y-m-d H:i') ) ));
            $turno->setFechaFin(new \DateTime( $fechaFin ));
            $em->persist($turno);
            $em->flush();
            $msg='OK';
        } catch (Exception $exc) {
            $msg= $exc->getTraceAsString();
        }
        return new Response(json_encode($msg));
    }
    public function resizeTurnoAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $minutos = $request->get('minutos');
        $em = $this->getDoctrine()->getManager();
        try {
            $turno =  $em->getRepository('App:Agenda')->find($id);

            $cantmin = ($minutos<0)? strval($minutos).' minutes' : '+'.strval($minutos).' minutes';
           // echo $cantmin;
            $fechafin = date ( 'Y-m-d H:i' , strtotime ( $cantmin , strtotime ( $turno->getFechaFin()->format('Y-m-d H:i') ) ));
            $turno->setFechaFin(new \DateTime( $fechafin ));
           // var_dump($fechafin);die;
            $em->persist($turno);
            $em->flush();
            $msg='OK';
        } catch (Exception $exc) {
            $msg= $exc->getTraceAsString();
        }
        return new Response(json_encode($msg));
    }
    
    public function listTurnosAction($id){
        $em = $this->getDoctrine()->getManager();
        $credito = $em->getRepository('App:Credito')->find($id);
        if($credito->getGrupo()){
             $val = $credito->getGrupo()->getId();
             $turnos = $em->getRepository('App:Agenda')->findByCriteria($val,'grupo');
        }else{
            $emp = $credito->getEmprendedores();
            $val = $emp[0]->getEmprendedor()->getId();
            $turnos = $em->getRepository('App:Agenda')->findByCriteria($val,'emprendedor');
        }
        $dias =array('Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo');
         $partial = $this->renderView('Agenda/listTurnos.html.twig',array('turnos'=>$turnos,'dias'=>$dias));
        return new Response($partial);
    }
    /*Funciones para fechas*/

}
