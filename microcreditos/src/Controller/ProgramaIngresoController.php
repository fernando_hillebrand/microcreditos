<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Entity\ProgramaIngreso;
use App\Form\ProgramaIngresoType;

class ProgramaIngresoController extends Controller
{
    function newAction(){
        $entity = new ProgramaIngreso();
        $form   = $this->createForm(new ProgramaIngresoType(), $entity);

        $partialIngresos = $this->renderView('MicrocreditosBundle:Programa:_Ingreso.html.twig', array(
            'ingresoRow' => $entity,
            'formIngreso'   => $form->createView()
            ));
        return new Response(json_encode($partialIngresos));
    }
    function editAction(){
        $em = $this->getDoctrine()->getManager();
        $id = $this->getRequest()->get('id');
        $entity = new ProgramaIngreso();
        if($id>0)
            $entity = $em->getRepository('App:ProgramaIngreso')->find($id);

        $form   = $this->createForm(new ProgramaIngresoType(), $entity);

        $partialIngresos = $this->renderView('MicrocreditosBundle:Programa:_Ingreso.html.twig', array(
            'ingresoRow' => $entity,
            'formIngreso'   => $form->createView()
            ));
        
        return new Response(json_encode(array('partial'=>$partialIngresos,'id'=>$entity->getId())));
    }
    function updateAction(){
        $em = $this->getDoctrine()->getManager();
        $id = $this->getRequest()->get('id');
        if($id>0){
            //edicion
            $entity = $em->getRepository('App:ProgramaIngreso')->find($this->getRequest()->get('id'));
        }  else {
            //nuevo
            $programa = $em->getRepository('App:Programa')->find($this->getRequest()->get('programa'));
            $entity = new ProgramaIngreso();
            $entity->setPrograma($programa);
        }
        $entity->setMonto($this->getRequest()->get('monto'));
        $date = new \DateTime($this->getRequest()->get('fecha'));
        $entity->setFechaIngreso($date);
        $entity->setUsuario($this->get('security.token_storage')->getToken()->getUser());
        $em->persist($entity);
        $em->flush();
        $partialIngresos = $this->renderView('MicrocreditosBundle:Programa:_Ingreso.html.twig', array(
            'ingresoRow' => $entity
            ));
        $total = $em->getRepository('App:ProgramaIngreso')->findAll();
        return new Response(json_encode(array('partial'=>$partialIngresos,'id'=>$entity->getId(),'total'=>count($total))));
    }
    function showAction(){
        $em = $this->getDoctrine()->getManager();
        $id = $this->getRequest()->get('id');
        $entity = $em->getRepository('App:ProgramaIngreso')->find($this->getRequest()->get('id'));
        $partialIngresos = $this->renderView('MicrocreditosBundle:Programa:_Ingreso.html.twig', array(
            'ingresoRow' => $entity
            ));
        return new Response(json_encode(array('partial'=>$partialIngresos,'id'=>$entity->getId())));
    }
 
    function deleteAction(){
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:ProgramaIngreso')->find($id);
        $em->remove($entity);
        $em->flush();
        $total = $em->getRepository('App:ProgramaIngreso')->findAll();
        return new Response(json_encode(array('msg'=>"OK",'id'=>$id,'total'=>count($total))));
    }
}
