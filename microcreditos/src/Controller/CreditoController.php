<?php

namespace App\Controller;

use App\Entity\Caja;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


use App\Entity\Num2Str;
use App\Entity\Credito;
use App\Entity\CreditoFile;
use App\Form\CreditoType;
use App\Form\CreditoFileAnalyzerType;
use App\Entity\Emprendedor;
use App\Entity\CreditoEmprendedor;
use App\Entity\CreditoRefinanciacion;
use App\Entity\Cuota;
use App\Entity\Pago;

// require 'vendor/autoload.php';


/**
 * Credito controller.
 *
 */
class CreditoController extends Controller
{
    protected $estado = 0;

    /**
     * Lists all Credito entities.
     *
     */
    public function solicitudAction()
    {
        $this->estado = 0;
        return $this->indexAction();
    }
    public function creditoAction()
    {
        $this->estado = 1;
        return $this->indexAction();
    }
    public function morososAction()
    {
        $this->estado = 2;
        return $this->indexAction();
    }
    public function incobrablesAction()
    {
        $this->estado = 3;
        return $this->indexAction();
    }

    public function indexAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();
        $entityName = 'credito';
        //$varFiltro = ($this->estado) ? 'filtro_credito' : 'filtro_solicitud';
        switch ($this->estado) {
            case 0:
                $varFiltro = 'filtro_solicitud';
                $entityName = 'credito';
                break;
            case 1:
                $varFiltro = 'filtro_credito';
                $entityName = 'solicitud';
                break;
            case 2:
                $varFiltro = 'filtro_morosos';
                $entityName = 'morosos';
                break;
            case 3:
                $varFiltro = 'filtro_incobrables';
                $entityName = 'incobrables';
                break;
        }
        $sessionFiltro = $session->get($varFiltro);
        echo ($request->get('_opFiltro'));
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array(
                    'programa' => '',
                    'grupo' => '',
                    'grupoId' => '',
                    'asesor' => '',
                    'emprendedor' => '',
                    'emprendedorId' => '',
                    'desde' => '',
                    'hasta' => ''
                );
                break;
            case 'buscar':
                $filtro = array(
                    'programa' => $request->get('_programa'),
                    'grupo' => $request->get('_grupo'),
                    'grupoId' => $request->get('_grupoId'),
                    'asesor' => $request->get('_asesor'),
                    'emprendedor' => $request->get('_emprendedor'),
                    'emprendedorId' => $request->get('_emprendedorId'),
                    'desde' => $request->get('_desde'),
                    'hasta' => $request->get('_hasta'),
                    'page' => $request->get('page')
                );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa' => $sessionFiltro['programa'],
                    'grupo' => $sessionFiltro['grupo'],
                    'grupoId' => $sessionFiltro['grupoId'],
                    'asesor' => $sessionFiltro['asesor'],
                    'emprendedor' => $sessionFiltro['emprendedor'],
                    'emprendedorId' => $sessionFiltro['emprendedorId'],
                    'desde' => $sessionFiltro['desde'],
                    'hasta' => $sessionFiltro['hasta'],
                    'page' => $request->get('page')
                );
                break;
        }

        $session->set($varFiltro, $filtro);
        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');
        $query = null;
        if ($this->estado == 2) {
            $query = $em->getRepository('App:Credito')
                ->findMorososDQL($session->get($varFiltro));
        } elseif ($this->estado == 3) {
            $query = $em->getRepository('App:Credito')
                ->findIncobrablesDQL($session->get($varFiltro));
        } elseif ($this->estado == 0) {
            $query = $em->getRepository('App:Credito')
                ->findByMyFiltroDQL($session->get($varFiltro), 0);
        } elseif ($this->estado == 1) {
            $query = $em->getRepository('App:Credito')
                ->findByMyFiltroDQL($session->get($varFiltro), 1);
        } else {
            $query = $em->getRepository('App:Credito')
                ->findByMyFiltroDQL($session->get($varFiltro), $this->estado);
        }

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            /*page number*/
            10 /*limit per page*/
        );

        $entities = self::setUltimaCuota($entities, $em);

        //dd($query);

        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $usuarios = $em->getRepository('App:User')->findByMyCriteria();
        $vars = array(
            'entityName' => $entityName,
            'filtro' => $session->get($varFiltro),
            'programas' => $programas,
            'usuarios' => $usuarios,
            'entities' => $entities,
            'paginator' => $paginator
        );

        return $this->render('Credito/creditoIndex.html.twig', $vars);
    }

    /**
     * Displays a form to create a new Credito entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Credito');
        $nro = $ultimo->getNumerico() + 1;
        $codigo = str_pad($nro, 6, "0", STR_PAD_LEFT);
        /* $ultimo->setNumerico($nro);
        $em->persist($ultimo);
        $em->flush();*/

        $entity = new Credito();
        $entity->setNroCredito($codigo);
        $entity->setPrograma($em->getRepository('App:Programa')->find(5));
        $entity->setFechaCredito(new \Datetime);
        $asesor = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
        $entity->setAsesor($asesor);
        $form = $this->createForm(CreditoType::class, $entity);

        return $this->render(
            'Credito/creditoNew.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView()
            )
        );
    }


    public function exportAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();


        $fechaPago = $request->query->get('fecha_pago');
        $entityName = 'credito';

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');
        $query = null;

        $query = $em->getRepository('App:Credito')
            ->findPagoCreditos2($fechaPago);

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            /*page number*/
            10 /*limit per page*/
        );

        $vars = array(
            'fechaPago' => $fechaPago,
            'entityName' => $entityName,
            'entities' => $entities,
            'paginator' => $paginator
        );

        return $this->render('Credito/exportacionPagos.html.twig', $vars);
    }

    public function expPagosAction()
    {

        $request = $this->container->get('request_stack')->getCurrentRequest();


        $fechaPago = $request->query->get('fecha_pago2');


        $em = $this->getDoctrine()->getManager();

        $entities = null;

        $entities = $em->getRepository('App:Credito')
            ->findPagoCreditos2($fechaPago);




        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Entregados.xls');
        $html = $this->renderView('Credito/pagosCreditosXls.html.twig', array('entities' => $entities, 'fecha' => $fechaPago));
        $response->setContent($html);
        return $response;
    }

    public function expNoAprobatesAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $entities = null;

        $fecha = new \Datetime('now');
        $fechaFormateada = $fecha->format('d/m/Y');

        $entities = $em->getRepository('App:Credito')
            ->findPagoCreditosWithoutAprobate();

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Entregados.xls');
        $html = $this->renderView('Credito/pagosCreditosXls.html.twig', array('entities' => $entities, 'fecha' => $fechaFormateada));
        $response->setContent($html);
        return $response;
    }

    /**
     * Displays a form to create a new Credito entity.
     *
     */
    public function renovarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $old = $em->getRepository('App:Credito')->find($id);

        $entity = new Credito();
        // $eee=new Emprendedor();
        //$entity->setAsesor($old->getAsesor());
        $entity->setAsesor($this->get('security.token_storage')->getToken()->getUser());
        $entity->setCantCuotas($old->getCantCuotas());
        $entity->setFechaCredito(new \Datetime);
        $entity->setPorcInteres($old->getPrograma()->getPorcInteres());
        $entity->setPorcPunitorio($old->getPrograma()->getPorcPunitorio());
        $entity->setMontoCredito($old->getMontoCredito());

        $entity->setSinIntereses($old->getSinIntereses());
        $entity->setSinGastos($old->getSinGastos());
        $entity->setSinPunitorios($old->getSinPunitorios());

        if ($old->getPrograma()->getActivo() == FALSE or $old->getPrograma()->getHabilitadoNuevoCredito() == FALSE) {
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $session = $request->getSession();
            $session->getFlashBag()->set('error', 'No se puede Ronovar el Credito #' . $old->getNroCredito() . ' (' . $old->getNroRenovacion() . ') selecionado por que el Programa al cual pertenecia el credito no se encuentra habilitada para nuevos créditos');
            return $this->redirect($this->generateUrl('credito_new'));
        }

        if ($old->getGrupo()) {
            /* grupo */
            $modif = false;
            $entity->setGrupo($old->getGrupo());
            $grupo = $em->getRepository('App:Grupo')->find($old->getGrupo()->getId());
            foreach ($grupo->getEmprendedores() as $empGrupo) {
                $esta = false;
                foreach ($old->getEmprendedores() as $credEmp) {
                    if ($empGrupo->getId() == $credEmp->getEmprendedor()->getId()) {
                        $entity->addCreditoEmprendedor($credEmp);
                        $esta = true;
                    }
                }
                if (!$esta) {
                    /* agregar emprendedor */
                    $modif = true;
                    $nuevo = new CreditoEmprendedor();
                    $nuevo->setCredito($entity);
                    $nuevo->setEmprendedor($empGrupo);
                    $nuevo->setMontoCredito(0);
                    $entity->addCreditoEmprendedor($nuevo);
                }
            }
            if (count($grupo->getEmprendedores()) != count($old->getEmprendedores()) || $modif) {
                /* se modificaron los datos de emprendedores */
                $entity->setMontoCredito(0);
                $entity->setGastoAdministrativo(0);
            }
            //die;
        } else {
            /* individual */
            foreach ($old->getEmprendedores() as $credEmp) {
                $entity->addCreditoEmprendedor($credEmp);
            }
        }
        $entity->setNroCredito($old->getNroCredito());
        $entity->setNroRenovacion($old->getNroRenovacion() + 1);
        $entity->setNroCarpeta($old->getNroCarpeta());
        $entity->setPeriodo($old->getPeriodo());
        $entity->setPrograma($old->getPrograma());
        //   $gastoAdm = self::getGastoAdm($entity->getPeriodo(),$entity->getMontoCredito(), $em);
        //   $entity->setGastoAdministrativo($gastoAdm);

        $form = $this->createForm(CreditoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'Credito/creditoNew.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Nueva solicitud de credito.
     */
    public function createAction()
    {
        $credito = new Credito();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $credito->setFechaSolicitudCredito(new \DateTime());
        $emprendedor = $request->get('empId');
        $monto = $request->get('monto_credito');
        $typeCredito = $request->get('tipo_de_pago');
        $gasto = $request->get('gasto_administrativo');
        $coordinador = $request->get('coordinador');
        $garante = $request->get('garante');
        $garanteDni = $request->get('garanteDni');

        $garanteDireccion = $request->get('garanteDireccion');
        $garanteTelefono = $request->get('garanteTelefono');
        $garanteOcupacion = $request->get('garanteOcupacion');
        $garanteLugarTrabajo = $request->get('garanteLugarTrabajo');

        $garanteEmpleadorNombre = $request->get('garanteEmpleadorNombre');
        $garanteEmpleadorCuit = $request->get('garanteEmpleadorCuit');
        $garanteIngresos = $request->get('garanteIngresos');

        $form = $this->createForm(CreditoType::class, $credito);
        if ($request->isMethod('POST')) {

            $requestData = $request->get($form->getName());
            $form->submit($requestData);

            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                /* Verificar que sea valido para credito nuevo */
                if ($credito->getGrupo()) {
                    $valido = $em->getRepository('App:Credito')->esValidoParaCredito('G', $credito->getGrupo()->getId(), $credito->getNroRenovacion(), $credito->getId());
                    $msg = 'Existe un crédito o solicitud vigente para el grupo indicado. No es posible generar uno nuevo.';
                } else {
                    $valido = $em->getRepository('App:Credito')->esValidoParaCredito('E', $emprendedor, $credito->getNroRenovacion(), $credito->getId());
                    $msg = 'Existe un crédito o solicitud vigente para el emprendedor indicado. No es posible generar uno nuevo.';
                }

                if ($valido) {
                    $em->getConnection()->beginTransaction();
                    $credito->setAsesor($this->get('security.token_storage')->getToken()->getUser());
                    $credito->setFechaCredito($credito->getFechaSolicitudCredito());
                    try {
                        if (!$request->get('renov')) {
                            $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Credito');
                            $nro = $ultimo->getNumerico() + 1;
                            $nroCredito = str_pad($nro, 6, "0", STR_PAD_LEFT);
                            $ultimo->setNumerico($nro);
                            $credito->setNroCredito($nroCredito);
                            $em->persist($ultimo);
                        }

                        foreach ($emprendedor as $key => $id) {
                            $credito_emprendedor = new CreditoEmprendedor();
                            $credito_emprendedor->setCredito($credito);
                            $emp = $em->getRepository('App:Emprendedor')->find($id);
                            $credito_emprendedor->setEmprendedor($emp);
                            $credito_emprendedor->setMontoCredito($monto[$key]);
                            $credito_emprendedor->setIsCredito($typeCredito[$key]);
                            $credito_emprendedor->setGastoAdministrativo($gasto[$key]);
                            $credito_emprendedor->setCoordinador(isset($coordinador[$key]));
                            if ($garante) {
                                if (isset($garante[$key])) {
                                    $credito_emprendedor->setGarante($garante[$key]);
                                    $credito_emprendedor->setGaranteDni($garanteDni[$key]);
                                    $credito_emprendedor->setGaranteDireccion($garanteDireccion[$key]);
                                    $credito_emprendedor->setGaranteTelefono($garanteTelefono[$key]);
                                    $credito_emprendedor->setGaranteOcupacion($garanteOcupacion[$key]);
                                    $credito_emprendedor->setGaranteLugarTrabajo($garanteLugarTrabajo[$key]);

                                    $credito_emprendedor->setGaranteEmpleadorNombre($garanteEmpleadorNombre[$key]);
                                    $credito_emprendedor->setGaranteEmpleadorCuit($garanteEmpleadorCuit[$key]);
                                    $credito_emprendedor->setGaranteIngresos($garanteIngresos[$key]);
                                } else {
                                    $credito_emprendedor->setGarante($garante[0]);
                                    $credito_emprendedor->setGaranteDni($garanteDni[0]);
                                    $credito_emprendedor->setGaranteDireccion($garanteDireccion[0]);
                                    $credito_emprendedor->setGaranteTelefono($garanteTelefono[0]);
                                    $credito_emprendedor->setGaranteOcupacion($garanteOcupacion[0]);
                                    $credito_emprendedor->setGaranteLugarTrabajo($garanteLugarTrabajo[0]);

                                    $credito_emprendedor->setGaranteEmpleadorNombre($garanteEmpleadorNombre[0]);
                                    $credito_emprendedor->setGaranteEmpleadorCuit($garanteEmpleadorCuit[0]);
                                    $credito_emprendedor->setGaranteIngresos($garanteIngresos[0]);
                                }
                            }
                            $credito->addCreditoEmprendedor($credito_emprendedor);
                        }

                        if ($credito->getPrograma()->getIndividual()) {
                            $credito->setGrupo(null);
                        }

                        $em->persist($credito);
                        $em->flush();
                        $em->getConnection()->commit();
                        return $this->redirect($this->generateUrl('credito_show', array('id' => $credito->getId())));
                    } catch (\PDOException $e) {
                        $em->getConnection()->rollback();
                        $session = $request->getSession();
                        $session->getFlashBag()->set('error', $e->getMessage());
                        return $this->render(
                            'Credito/creditoNew.html.twig',
                            array(
                                'entity' => $credito,
                                'form' => $form->createView(),
                                'delete_form' => $form->createView()
                            )
                        );
                    }
                } else {
                    /* no es valido para nuevo credito */
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', $msg);
                    return $this->render(
                        'Credito/creditoNew.html.twig',
                        array(
                            'entity' => $credito,
                            'form' => $form->createView(),
                            'delete_form' => $form->createView()
                        )
                    );
                }
            }
        }
        return $this->render(
            'Credito/creditoNew.html.twig',
            array(
                'entity' => $credito,
                'form' => $form->createView(),
                'delete_form' => $form->createView()
            )
        );
    }

    public function simulacionCuotasAction()
    {
        //$calcRaro = $cap * pow(  ( ( 1- pow(  (1+$tasa/100)   ,-$cant) )  / ( $tasa/100 )  )  ,-1 );
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form = $request->get('credito');
        $emprendedor = $request->get('empId');
        $id = $request->get('creditoId');


        /* Controlar que sea válido el grupo o el emprendedor para sacar credito nuevo */
        if (isset($form['grupo']) && $form['grupo'] != '') {
            $valido = $em->getRepository('App:Credito')->esValidoParaCredito('G', $form['grupo'], $form['nro_renovacion'], $id);
        } else {
            $valido = $em->getRepository('App:Credito')->esValidoParaCredito('E', $emprendedor, $form['nro_renovacion'], $id);
        }
        $msg = 'VALIDO';

        $garanteDni = $request->get('garanteDni');
        $programa = $em->getRepository('App:Programa')->findOneById($form["programa"]);
        $creditosConProblemas = [];
        $cargarGarante = false;
        if (!$programa->getGrupoSolidario()) {
            if ($garanteDni[0] != '') {
                $creditosConProblemas = ($em->getRepository('App:Emprendedor')->getCreditosActivosByGaranteDni($garanteDni[0]));
            } else {
                $creditosConProblemas = ['El Grupo no puede generar un nuevocredito'];
                $cargarGarante = true;
            }
        }

        if ($valido and count($creditosConProblemas) == 0) {
            $monto = $request->get('monto_credito');
            $gastosAdmin = $request->get('gasto_administrativo');

            $tasa = ($form['porc_interes'] / 365) * $form['periodo'];
            $simulacionCuotas = array();
            $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
            //composicion de cuota

            //Calcula el total pedido
            $montoCapitalTotal = 0;
            $montoArrayEmprendedor = array();


            foreach ($emprendedor as $key => $id) {
                $emp = $em->getRepository('App:Emprendedor')->find($id);
                $capital = $monto[$key] / $form['cant_cuotas'];
                //$capital   = $monto[$key];
                $montoCapital = $montoCapital + $capital;
                $intereses = (isset($form['sin_intereses']) ? 0 : round(($monto[$key] * $tasa / 100), 2));
                $gastos = $gastosAdmin[$key] / $form['cant_cuotas'];
                $total = ceil($capital + $intereses + $gastos);
                $redondeo = $total - ($capital + $intereses + $gastos);
                $gastos = $gastos + $redondeo;

                $gastoAdm = $gastoAdm + $gastos;
                //$intereses = $total -( $capital + $gastos );
                $montoIntereses = $montoIntereses + $intereses;
                $montoTotal = $montoTotal + $total;
                $simCuota = array(
                    'emprendedor' => $emp,
                    'montoCapital' => $capital,
                    'montoIntereses' => $intereses,
                    'gastoAdministrativo' => $gastos,
                    'montoTotal' => $total
                );
                array_push($simulacionCuotas, $simCuota);
            }


            $cuota = array(
                'montoCapital' => $montoCapital,
                'montoIntereses' => $montoIntereses,
                'gastoAdministrativo' => $gastoAdm,
                'montoTotal' => $montoTotal
            );

            $partial = $this->renderView(
                'Credito/simulacion_credito_cuota.html.twig',
                array('simulacionCuotas' => $simulacionCuotas, 'cuota' => $cuota)
            );
        } else {
            $msg = 'INVALIDO';
            $mensaje = '';
            if ($cargarGarante) {
                $mensaje .= "Se debe cargar un garante. ";
            } else {
                if (!$valido) {
                    $mensaje .= "Existe un crédito o solicitud vigente. No es posible generar uno nuevo. ";
                }
                if (count($creditosConProblemas) > 0) {
                    $mensaje .= "Existe un problema con el garante, motivo: <br/> ";
                    foreach ($creditosConProblemas as $problema) {
                        $mensaje .= ' - ' . $problema . "<br/> ";
                    }
                }
            }

            $partial = '<div class="notification error png_bg" style="margin:15px 0"><div>' . $mensaje . '</div></div>';
        }
        $array = array('msg' => $msg, 'partial' => $partial);
        return new Response(json_encode($array));
    }

    public function getSimulacionCuotaAction()
    {
        //$calcRaro = $cap * pow(  ( ( 1- pow(  (1+$tasa/100)   ,-$cant) )  / ( $tasa/100 )  )  ,-1 );
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('empId');
        $credito = $em->getRepository('App:Credito')->find($id);

        $monto = $credito->getMontoCredito();
        $tasa = ($credito->getPorcInteres() / 365) * $credito->getPeriodo();
        $simulacionCuotas = array();
        $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
        //composicion de cuota

        //Calcula el total pedido
        $montoCapitalTotal = 0;
        $montoArrayEmprendedor = array();
        foreach ($credito->getEmprendedores() as $index => $credEmp) {
            $montoArrayEmprendedor[$index] = $credEmp->getMontoCredito();
            $montoCapitalTotal = $montoCapitalTotal + $montoArrayEmprendedor[$index];
        }
        $gastosAdminArrayEmprendedor = array();
        $programaGastosAdmin = doubleval($credito->getPrograma()->getPorcGastosAdm());
        $totalGastoAdmin = ($montoCapitalTotal * $programaGastosAdmin) / 100;

        foreach ($montoArrayEmprendedor as $index => $monto) {
            $gastosAdminArrayEmprendedor[$index] = ($montoArrayEmprendedor[$index] / $montoCapitalTotal) * $totalGastoAdmin;
        }

        foreach ($credito->getEmprendedores() as $index => $credEmp) {
            $capital = $credEmp->getMontoCredito() / $credito->getCantCuotas();
            $montoCapital = $montoCapital + $capital;

            $intereses = (($credito->getSinIntereses()) ? 0 : round(($credEmp->getMontoCredito() * $tasa / 100), 2));
            $gastos = $gastosAdminArrayEmprendedor[$index] / $credito->getCantCuotas();
            $total = ceil($capital + $intereses + $gastos);
            $redondeo = $total - ($capital + $intereses + $gastos);
            $gastos = $gastos + $redondeo;

            // $intereses = $total - ( $capital + $gastos );
            $gastoAdm = $gastoAdm + $gastos;
            $montoIntereses = $montoIntereses + $intereses;
            $montoTotal = $montoTotal + $total;
            $simCuota = array(
                'emprendedor' => $credEmp->getEmprendedor(),
                'montoCapital' => $capital,
                'montoIntereses' => $intereses,
                'gastoAdministrativo' => $gastos,
                'montoTotal' => $total
            );
            array_push($simulacionCuotas, $simCuota);
        }

        $cuota = array(
            'montoCapital' => $montoCapital,
            'montoIntereses' => $montoIntereses,
            'gastoAdministrativo' => $gastoAdm,
            'montoTotal' => $montoTotal
        );
        $partial = $this->renderView(
            'Credito/simulacion_credito_cuota.html.twig',
            array('simulacionCuotas' => $simulacionCuotas, 'cuota' => $cuota)
        );

        return new Response(json_encode($partial));
    }

    public function aprobacionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $credito = $em->getRepository('App:Credito')->find($id);

            $fecha = new \DateTime();
            $credito->setFechaActualizacion($fecha);

            /* Verificar que no este cerrado el resumen mensual */
            $reporteCerrado = $em->getRepository('App:ReporteMensual')->reporteCerrado($credito);
            if ($reporteCerrado) {
                $em->getConnection()->rollback();
                $request = $this->container->get('request_stack')->getCurrentRequest();
                $session = $request->getSession();
                $session->getFlashBag()->set('error', 'El Reporte Mensual ya fue cerrado. Ingrese una fecha posterior de entrega para el crédito');
                return $this->redirect($this->generateUrl('credito_show', array('id' => $id)));
            }
            $credito->setAprobado(true);
            $credito->setFechaAprobacion(new \Datetime());
            /* Generar las cuotas */
            $periodo = 86400 * $credito->getPeriodo();
            $tasa = ($credito->getPorcInteres() / 365) * $credito->getPeriodo();

            $programa = $credito->getPrograma();

            $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
            //composicion de cuota

            $montoCapitalTotal = 0;
            $cantidadEfectivo = 0;
            $montoArrayEmprendedor = array();
            foreach ($credito->getEmprendedores() as $index => $credEmp) {

                if($credEmp->getIsCredito() != 1) {
                    $cantidadEfectivo = $cantidadEfectivo + $credEmp->getMontoCredito();
                }

                $montoArrayEmprendedor[$index] = $credEmp->getMontoCredito();
                $montoCapitalTotal = $montoCapitalTotal + $montoArrayEmprendedor[$index];
            }

            if($cantidadEfectivo > 0) {
                if ($programa->getUtilizaCaja()) {
                    $em->getRepository('App:Caja')->guardarNuevoEgreso(
                        $cantidadEfectivo,
                        'Credito Entregado',
                        $this->get('security.token_storage')->getToken()->getUser()->getId(),
                        $credito,
                        null,
                        $em->getRepository('App:Parametro')->find(209)
                    );
                }
            }

            $gastosAdminArrayEmprendedor = array();
            $programaGastosAdmin = doubleval($credito->getPrograma()->getPorcGastosAdm());
            $totalGastoAdmin = ($montoCapitalTotal * $programaGastosAdmin) / 100;

            foreach ($montoArrayEmprendedor as $index => $monto) {
                $gastosAdminArrayEmprendedor[$index] = ($montoArrayEmprendedor[$index] / $montoCapitalTotal) * $totalGastoAdmin;
            }

            foreach ($credito->getEmprendedores() as $index => $credEmp) {
                $capital = $credEmp->getMontoCredito() / $credito->getCantCuotas();
                $montoCapital = $montoCapital + $capital;

                $intereses = (($credito->getSinIntereses()) ? 0 : round(($credEmp->getMontoCredito() * $tasa / 100), 2));

                $gastos = ($gastosAdminArrayEmprendedor[$index] / doubleval($credito->getCantCuotas()));

                $total = ceil($capital + $intereses + $gastos);
                $redondeo = $total - ($capital + $intereses + $gastos);
                $gastos = $gastos + $redondeo;
                $gastoAdm = $gastoAdm + $gastos;
                $montoIntereses = $montoIntereses + $intereses;
                $montoTotal = $montoTotal + $total;
                $credEmp->setMontoCuota($total);
            }

            $fechaVencimiento = strtotime($credito->getFechaVtoCuota1()->format('d-m-Y'));
            for ($i = 1; $i <= $credito->getCantCuotas(); $i++) {

                $cuota = new Cuota();
                $cuota->setCredito($credito);
                $cuota->setNroCuota($i);
                $cuota->setMontoCapital(round($montoCapital, 2));
                $cuota->setMontoIntereses(round($montoIntereses, 2));
                $cuota->setGastoAdministrativo($gastoAdm);
                $cuota->setMontoTotal($montoTotal);
                $cuota->setSaldo($montoTotal);
                $cuota->setFechaVencimiento(new \DateTime(date('d-m-Y', $fechaVencimiento)));
                $cuota->setFechaActualizacion(new \DateTime());
                $em->persist($cuota);
                $em->flush();

                $credito->addCuota($cuota);

                $fechaVencimiento = $fechaVencimiento + $periodo;
            }

            $em->persist($credito);
            $em->flush();
            $em->getConnection()->commit();
        } catch (\PDOException $e) {

            var_dump($e);
            exit;

            $em->getConnection()->rollback();
            return new Response(json_encode($e));
        }
        return $this->redirect($this->generateUrl('credito_show', array('id' => $id)));
    }

    /**
     * Finds and displays a Credito entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $entity = $em->getRepository('App:Credito')->find($id);

        if ($entity->getAprobado() == 0)
            $esRenovacion = $em->getRepository('App:Credito')->esRenovacion($entity);
        else
            $esMoroso = $em->getRepository('App:Credito')->esMoroso($entity);

        $esRenovacion = isset($esRenovacion) ? $esRenovacion : 0;
        $esMoroso = isset($esMoroso) ? $esMoroso : 0;
        $historico = $em->getRepository('App:Credito')->findHistorico($id);
        $cuotas = $em->getRepository('App:Cuota')->findCuotasByCredito($id);
        $mora = array();

        /** CORREGIR VTO SI CAE FERIADO O DÍA DE FIN DE SEMANA **/
        $datosFeriados = $em->getRepository('App:TablaFeriados')->findFechas();
        $feriados = array();
        foreach ($datosFeriados as $dia) {
            $feriados[] = $dia['fecha'];
        }

        if ($esMoroso) {
            $fecha = new \Datetime;

            foreach ($cuotas as $cuota) {
                $montoPunitorios = 0;
                $vencimiento = $cuota->getFechaVencimiento();
                while (in_array($vencimiento->format('Y-m-d'), $feriados) || in_array($vencimiento->format('N'), [6, 7])) {
                    $vencimiento->modify("+1 days");
                }
                if ($cuota->getPagado() > 0)
                    $fechaCuota = ($vencimiento > $cuota->getFechaCancelacion())
                        ? $vencimiento : $cuota->getFechaCancelacion();
                //$fechaCuota = $cuota->getFechaCancelacion();
                else
                    $fechaCuota = $vencimiento;

                $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fecha, $entity->getPrograma());
                array_push($mora, $array['montoPunitorios']);
            }
        }

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Crédito que está buscando.');
        }
        $deleteForm = $this->createDeleteForm($id);

        $pandiamiaDesde = new \DateTime('2020-03-20');
        $pandiamiaHasta = new \DateTime('2020-06-08');
        $registroPagoManualConError1 = new \DateTime('2020-04-01');
        $registroPagoManualConError2 = new \DateTime('2020-04-13');
        return $this->render(
            'Credito/creditoShow.html.twig',
            array(
                'entity' => $entity,
                'historico' => $historico,
                'esRenovacion' => $esRenovacion,
                'esMoroso' => $esMoroso,
                'cuotas' => $cuotas,
                'mora' => $mora,
                'pandiamiaDesde' => $pandiamiaDesde,
                'pandiamiaHasta' => $pandiamiaHasta,
                'fechaPagoConError1' => $registroPagoManualConError1,
                'fechaPagoConError2' => $registroPagoManualConError2,
                'delete_form' => $deleteForm->createView()
            )
        );
    }

    /**
     * Displays a form to edit an existing Credito entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Credito')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Credito entity.');
        }
        if ($entity->getAprobado() == 0)
            $esRenovacion = $em->getRepository('App:Credito')->esRenovacion($entity);
        $esRenovacion = isset($esRenovacion) ? $esRenovacion : 0;
        $editForm = $this->createForm(CreditoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'Credito/creditoEdit.html.twig',
            array(
                'entity' => $entity,
                'esRenovacion' => $esRenovacion,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Edits an existing Credito entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $aprobar = $request->get('aprobar');
        $emprendedor = $request->get('empId');
        $monto = $request->get('monto_credito');
        $typeCredito = $request->get('tipo_de_pago');
        $coordinador = $request->get('coordinador');
        $garante = $request->get('garante');
        $garanteDni = $request->get('garanteDni');
        $garanteDireccion = $request->get('garanteDireccion');
        $garanteTelefono = $request->get('garanteTelefono');
        $garanteOcupacion = $request->get('garanteOcupacion');
        $garanteLugarTrabajo = $request->get('garanteLugarTrabajo');

        $garanteEmpleadorNombre = $request->get('garanteEmpleadorNombre');
        $garanteEmpleadorCuit = $request->get('garanteEmpleadorCuit');
        $garanteIngresos = $request->get('garanteIngresos');

        $gasto = $request->get('gasto_administrativo');

        $credito = $em->getRepository('App:Credito')->find($id);

        if (!$credito) {
            throw $this->createNotFoundException('Unable to find Credito entity.');
        }

        $editForm = $this->createForm(CreditoType::class, $credito);
        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createForm(CreditoType::class, $credito);
        if ($request->isMethod('POST')) {

            $editForm->submit($request->get($editForm->getName()));

            if ($editForm->isValid()) {

                if ($credito->getPrograma()->getIndividual()) {
                    $credito->setGrupo(null);
                }

                $fecha = new \DateTime();
                $credito->setFechaActualizacion($fecha);

                $em->persist($credito);
                $em->flush();

                $em->refresh($credito);


                //Borrar los creditos emprendedores Anteriores
                foreach ($emprendedor as $key => $empId) {
                    foreach ($credito->getEmprendedores() as $credEmp) {
                        $em->remove($credEmp);
                        $em->flush();
                    }
                }

                //Crear los nuevos
                foreach ($emprendedor as $key => $id) {
                    $credito_emprendedor = new CreditoEmprendedor();
                    $credito_emprendedor->setCredito($credito);
                    $emp = $em->getRepository('App:Emprendedor')->find($id);
                    $credito_emprendedor->setEmprendedor($emp);
                    $credito_emprendedor->setMontoCredito($monto[$key]);
                    $credito_emprendedor->setIsCredito($typeCredito[$key]);
                    $credito_emprendedor->setGastoAdministrativo($gasto[$key]);
                    $credito_emprendedor->setCoordinador(isset($coordinador[$key]));
                    if ($garante) {
                        if (isset($garante[$key])) {
                            $credito_emprendedor->setGarante($garante[$key]);
                            $credito_emprendedor->setGaranteDni($garanteDni[$key]);
                            $credito_emprendedor->setGaranteDireccion($garanteDireccion[$key]);
                            $credito_emprendedor->setGaranteTelefono($garanteTelefono[$key]);
                            $credito_emprendedor->setGaranteOcupacion($garanteOcupacion[$key]);
                            $credito_emprendedor->setGaranteLugarTrabajo($garanteLugarTrabajo[$key]);

                            $credito_emprendedor->setGaranteEmpleadorNombre($garanteEmpleadorNombre[$key]);
                            $credito_emprendedor->setGaranteEmpleadorCuit($garanteEmpleadorCuit[$key]);
                            $credito_emprendedor->setGaranteIngresos($garanteIngresos[$key]);
                        } else {
                            $credito_emprendedor->setGarante($garante[0]);
                            $credito_emprendedor->setGaranteDni($garanteDni[0]);
                            $credito_emprendedor->setGaranteDireccion($garanteDireccion[0]);
                            $credito_emprendedor->setGaranteTelefono($garanteTelefono[0]);
                            $credito_emprendedor->setGaranteOcupacion($garanteOcupacion[0]);
                            $credito_emprendedor->setGaranteLugarTrabajo($garanteLugarTrabajo[0]);

                            $credito_emprendedor->setGaranteEmpleadorNombre($garanteEmpleadorNombre[0]);
                            $credito_emprendedor->setGaranteEmpleadorCuit($garanteEmpleadorCuit[0]);
                            $credito_emprendedor->setGaranteIngresos($garanteIngresos[0]);
                        }
                    }
                    $credito->addCreditoEmprendedor($credito_emprendedor);
                }
                $em->persist($credito);
                $em->flush();


                if ($credito->getAprobado()) {
                    $aprobar = 1;
                    foreach ($credito->getCuotas() as $cuota) {
                        $em->remove($cuota);
                    }
                    $credito->getCuotas()->clear();
                    $em->getRepository('App:Cuota')->delCuotasByCredito($credito);
                }

                $em->persist($credito);
                $em->flush();

                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');

                if ($aprobar)
                    return $this->redirect($this->generateUrl('credito_aprobacion', array('id' => $id)));
                else
                    return $this->redirect($this->generateUrl('credito_show', array('id' => $credito->getId())));
            }
        }

        return $this->render(
            'Credito/edit.html.twig',
            array(
                'entity' => $credito,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    public function reimputarPagoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $pago = $em->getRepository('App:Pago')->find($request->get('pago'));

        $cuota = $pago->getCuota();

        //Suma lo que se fue pagando en pagos anteriores
        $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
        foreach ($cuota->getPagos() as $pagoRealizdo) {
            if ($pago->getId() > $pagoRealizdo->getId()) {
                $porcCapital += $pagoRealizdo->getCapital();
                $porcInteres += $pagoRealizdo->getIntereses();
                $porcGasto += $pagoRealizdo->getGastoAdministrativo();
                $porPunitorios += $pagoRealizdo->getPunitorios();
                $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
            }
        }

        $totalReimputar = round($pago->getTotal(), 2);

        //RefPunitorios
        if ($totalReimputar > 0) {
            if (($cuota->getSaldoPunitorios() - $porRefPunitorios) > $totalReimputar) {

                $pago->setRefinPunitorios($totalReimputar);
                $totalReimputar = 0;
            } else {
                $pago->setRefinPunitorios($cuota->getSaldoPunitorios() - $porRefPunitorios);
                $totalReimputar = $totalReimputar - ($cuota->getSaldoPunitorios() - $porRefPunitorios);
            }
        } else {
            $pago->setRefinPunitorios(0);
        }

        //Punitorios
        $credito = $cuota->getCredito();
        if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
            if ($totalReimputar > 0) {
                if (($cuota->getMontoPunitorios() - $porPunitorios) > $totalReimputar) {
                    $pago->setPunitorios($totalReimputar);
                    $totalReimputar = 0;
                } else {
                    $pago->setPunitorios($cuota->getMontoPunitorios() - $porPunitorios);
                    $totalReimputar = $totalReimputar - ($cuota->getMontoPunitorios() - $porPunitorios);
                }
            } else {
                $pago->setPunitorios(0);
            }
        }

        //intereses
        if ($totalReimputar > 0) {
            if (($cuota->getMontoIntereses() - $porcInteres) > $totalReimputar) {
                $pago->setIntereses($totalReimputar);
                $totalReimputar = 0;
            } else {
                $pago->setIntereses($cuota->getMontoIntereses() - $porcInteres);
                $totalReimputar = $totalReimputar - ($cuota->getMontoIntereses() - $porcInteres);
            }
        } else {
            $pago->setIntereses(0);
        }


        //Gastos
        if ($totalReimputar > 0) {
            if (($cuota->getGastoAdministrativo() - $porcGasto) > $totalReimputar) {
                $pago->setGastoAdministrativo($totalReimputar);
                $totalReimputar = 0;
            } else {
                $pago->setGastoAdministrativo($cuota->getGastoAdministrativo() - $porcGasto);
                $totalReimputar = $totalReimputar - ($cuota->getGastoAdministrativo() - $porcGasto);
            }
        } else {
            $pago->setGastoAdministrativo(0);
        }

        //Capital
        if ($totalReimputar > 0) {
            if (($cuota->getMontoCapital() - $porcCapital) > $totalReimputar) {
                $pago->setCapital($totalReimputar);
                $totalReimputar = 0;
            } else {
                $pago->setCapital($cuota->getMontoCapital() - $porcCapital);
                $totalReimputar = $totalReimputar - ($cuota->getMontoCapital() - $porcCapital);
            }
        } else {
            $pago->setCapital(0);
        }

        $partial = $this->renderView(
            'Credito/_partial_reimputar.html.twig',
            array(
                'pago' => $pago,
                'nuevoCredito' => $credito->getPrograma()->getHabilitadoNuevoCredito(),
            )
        );
        return new Response($partial);
    }

    public function registrarReimputacionPagoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $pago = $em->getRepository('App:Pago')->find($request->get('pago'));

        $capital = $request->get('capital');
        $intereses = $request->get('intereses');
        $punitorios = $request->get('punitorios');
        $gastosAdmin = $request->get('gastosAdmin');
        $punitoriosRef = 0;

        if ($request->query->has('punitoriosRef')) {
            $punitoriosRef = $request->get('punitoriosRef');
        }
        if (
            (round($capital + $intereses + $punitorios + $gastosAdmin, 2)
                - round($pago->getTotal(), 2)
            ) != 0
        ) {
            $partial = '<style>.success {background: #ff6957; border-color: #df0007; color: #665252;}'
                . '.notification div {display:block; padding: 10px 10px 10px 36px;}</style>'
                . '<div class="notification success png_bg">'
                . '<div>Ocurrio un error. Los importes re-distribuidos no deben superar el total del pago. </div></div>';
            return new Response($partial);
        }

        $pago->setCapital($capital);
        $pago->setIntereses($intereses);
        $pago->setPunitorios($punitorios);
        $pago->setGastoAdministrativo($gastosAdmin);
        $pago->setRefinPunitorios($punitoriosRef);
        $pago->setObservacion($request->get('observacion'));


        $cuota = $pago->getCuota();
        $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
        foreach ($cuota->getPagos() as $pagoRealizdo) {
            $porcCapital += $pagoRealizdo->getCapital();
            $porcInteres += $pagoRealizdo->getIntereses();
            $porcGasto += $pagoRealizdo->getGastoAdministrativo();
            $porPunitorios += $pagoRealizdo->getPunitorios();
            $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
        }
        $totalPagado = $porcCapital + $porcInteres + $porcGasto + $porPunitorios + $porRefPunitorios;
        $saldo = $cuota->getMontoTotal() + $cuota->getMontoPunitorios() - $totalPagado;

        $cuota->setSaldo($saldo);

        $em->persist($pago);
        $em->persist($cuota);
        $em->flush();

        $partial = '<style>.success {background: #8bc5ff; border-color: #142fdf; color: #665252;}'
            . '.notification div {display:block; padding: 10px 10px 10px 36px;}</style>'
            . '<div class="notification success png_bg">'
            . '<div>Se actualizado correctamente. Puede cerrar esta ventana</div></div>';

        return new Response($partial);
    }

    public function ingresarPagoAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $fecha = new \Datetime;
        $fechaHoy = $fecha->format('d-m-Y');
        //$pagoAnterior = null;
        $data = explode('-', $id);

        $montoExcel = null; // Used when the pagoaction is invoked from an excel analysis
        $creditoId = $data[0];
        $cuotaNro = $data[1];

        if (count($data) === 3) {
            $montoExcel = $data[2];
        }

        $cuota = $em->getRepository('App:Cuota')->findByCreditoCuota($creditoId, $cuotaNro);
        $credito = $em->getRepository('App:Credito')->find($creditoId);
        $tasa = $credito->getPrograma()->getPorcPunitorio() / 30;
        $montoPun = 0;
        $band = $cuota->getPagado() > 0;

        $conPagoErroneo = false;

        $array = [];

        if ($credito->getSinPunitorios()) {
            // NO SE COBRAN PUNITORIOS
            $cuota->setMontoPunitorios(0);
        } else {


            $saldo = ($cuota->getMontoCapital() +
                $cuota->getMontoIntereses() +
                $cuota->getGastoAdministrativo() +
                $cuota->getSaldoPunitorios()
            );
            /** Se controla si se debe tener en cuenta los punitorios */
            if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
                $saldo += $cuota->getMontoPunitorios();
            }

            /** CORREGIR VTO SI CAE FERIADO O DÍA DE FIN DE SEMANA **/
            $datosFeriados = $em->getRepository('App:TablaFeriados')->findFechas();
            $feriados = array();
            foreach ($datosFeriados as $dia) {
                $feriados[] = $dia['fecha'];
            }
            //$vencimiento = new \DateTime( $fechavto)
            $vencimiento = $cuota->getFechaVencimiento();
            while (in_array($vencimiento->format('Y-m-d'), $feriados) || in_array($vencimiento->format('N'), [6, 7])) {
                $vencimiento->modify("+1 days");
            }
            $cuota->setFechaVencimiento($vencimiento);

            if ($cuota->getPagado() > 0) {
                $band = true;
                $fechaCuota = ($vencimiento > $cuota->getFechaCancelacion())
                    ? $vencimiento : $cuota->getFechaCancelacion();
                // $fechaCuota = $cuota->getFechaCancelacion();
            } else {
                $fechaCuota = $vencimiento;
                $band = false;
            }

            //Calcular los dias, sacando los dias exlcuidos
            $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fecha, $credito->getPrograma());

            $totalPago = 0;
            $punitoriosPagados = 0;
            foreach ($cuota->getPagos() as $pagoRealizdo) {
                $punitoriosPagados += $pagoRealizdo->getPunitorios();

                //Controla que los pagos esten bien inputados
                $pagoTotalRegistrado = $pagoRealizdo->getCapital();
                $pagoTotalRegistrado += $pagoRealizdo->getPunitorios();
                $pagoTotalRegistrado += $pagoRealizdo->getIntereses();
                $pagoTotalRegistrado += $pagoRealizdo->getGastoAdministrativo();
                $pagoTotalRegistrado += $pagoRealizdo->getRefinPunitorios();

                $cuota->setMontoCapital($cuota->getMontoCapital() - $pagoRealizdo->getCapital());
                $cuota->setMontoPunitorios($cuota->getMontoPunitorios() - $pagoRealizdo->getPunitorios());
                $cuota->setMontoIntereses($cuota->getMontoIntereses() - $pagoRealizdo->getIntereses());
                $cuota->setGastoAdministrativo($cuota->getGastoAdministrativo() - $pagoRealizdo->getGastoAdministrativo());
                $cuota->setSaldoPunitorios($cuota->getSaldoPunitorios() - $pagoRealizdo->getRefinPunitorios());


                if ($pagoTotalRegistrado != $pagoRealizdo->getTotal()) {
                    $conPagoErroneo = true;
                }

                $totalPago += $pagoTotalRegistrado;
            }


            if ($array['dias'] > 0 and $array['montoPunitorios'] > 0) {
                $cuota->setMontoPunitorios($montoPun + $array['montoPunitorios']);
            }

            $saldo = $saldo - $totalPago;

            if ($cuota->getSaldo() != $saldo) {
                $cuota->setSaldo($saldo - $montoPun);
            } else {
                $cuota->setSaldo($cuota->getSaldo() - $montoPun);
            }
        }



        $cuota->setFechaCancelacion($fecha);


        $partial = $this->renderView(
            'Credito/_partial_pago.html.twig',
            array(
                'cuota' => $cuota,
                'tasa' => $tasa,
                'montoPun' => $montoPun,
                'creditoId' => $creditoId,
                'band' => $band,
                'nuevoCredito' => $credito->getPrograma()->getHabilitadoNuevoCredito(),
                'conPagoErroneo' => $conPagoErroneo,
                'arrayPunitorio' => $array,
                'montoExcel' => $montoExcel
            )
        );
        return new Response($partial);
    }

    public function registrarPagoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $creditoId = $request->get('credito');
        $credito = $em->getRepository('App:Credito')->find($creditoId);

        if (is_null($request->get('fechaCancelacion'))) {
            $fechaPago = new \DateTime();
        } else {
            $fechaPago = new \DateTime($request->get('fechaCancelacion'));
        }

        $cuotaNro = $request->get('cuota');
        $observacion = $request->get('observacion');
        $pagoBancario = $request->get('bancario');


        /* Verificar que no este cerrado el resumen mensual */
        $reporteCerrado = $em->getRepository('App:ReporteMensual')->reporteCerrado($credito, $fechaPago);
        if ($reporteCerrado) {
            $partial = '<style>.error {background: #ffcece; border-color: #df8f8f; color: #665252;}'
                . '.notification div {display:block; padding: 10px 10px 10px 36px;}</style>'
                . '<div class="notification error png_bg">'
                . '<div>El Reporte Mensual ya fue cerrado. Debe ingresar una fecha posterior de pago para la cuota.</div></div>';
            return new Response($partial);
        }

        /* para recibo */
        $num2str = new Num2Str();
        $fecha = DefaultController::longDateSpanish($fechaPago);
        $nombre = ($credito->getGrupo()) ? 'Grupo ' . $credito->getGrupo() : $credito->getEmprendedor();
        /* si se cancelan mas de una cuota */
        $tasa = $credito->getPrograma()->getPorcPunitorio() / 30;
        /**/
        $pagado = $request->get('pagado');
        $punitorios = floatval($request->get('montoPunitorios'));

        $bandpunitorios = TRUE;
        /* si el importe pagado no alcanza para cancelar los punitorios nuevos */
        if ($punitorios > $pagado) {
            $punitorios = $pagado;
        }


        $det_pagos = array();


        /* ITERAR CUOTAS HASTA CUBRIR EL MONTO PAGADO */
        do {
            $cuota = $em->getRepository('App:Cuota')->findByCreditoCuota($creditoId, $cuotaNro);

            $totalPagado = 0;
            if ($cuota) {

                /** CORREGIR VTO SI CAE FERIADO O DÍA DE FIN DE SEMANA **/
                $datosFeriados = $em->getRepository('App:TablaFeriados')->findFechas();
                $feriados = array();
                foreach ($datosFeriados as $dia) {
                    $feriados[] = $dia['fecha'];
                }
                $vencimiento = $cuota->getFechaVencimiento();
                while (in_array($vencimiento->format('Y-m-d'), $feriados) || in_array($vencimiento->format('N'), [6, 7])) {
                    $vencimiento->modify("+1 days");
                }
                $cuota->setFechaVencimiento($vencimiento);


                $arrayReturn = $this->registrarPagoCredito($credito, $pagado, $cuota, $fechaPago, $bandpunitorios, $observacion);
                $pago = $arrayReturn['pago'];
                $montoPago = $arrayReturn['montoPago'];
                $pagado = $arrayReturn['pagado'];

                if (is_null($pagoBancario)) {
                    $em->getRepository('App:Caja')->guardarNuevoIngreso(
                        ($pago->getTotal()),
                        '',
                        $this->get('security.token_storage')->getToken()->getUser()->getId(),
                        $credito,
                        $pago,
                        $em->getRepository('App:Parametro')->find(210)
                    );
                } else {
                    $em->getRepository('App:PagoBancario')->guardarNuevoIngreso(
                        ($pago->getTotal()),
                        $observacion,
                        $this->get('security.token_storage')->getToken()->getUser()->getId(),
                        $credito,
                        $pago,
                        $em->getRepository('App:Parametro')->find(210)
                    );
                }

                /* Para recibo */
                $letras = $num2str->ValorEnLetras($pago->getTotal(), "Pesos");
                array_push($det_pagos, array('monto' => $pago->getTotal(), 'letras' => $letras, 'cuota' => $cuota->getNroCuota()));
                /**/
                /* para iterar mas cuotas */
                $cuotaNro = $cuotaNro + 1;
                $punitorios = 0;
                $bandpunitorios = TRUE;
            } else
                $pagado = 0;
        } while ($pagado > 0);

        // Controlar si se cancela el credito
        if ($em->getRepository('App:Credito')->estaPagado($creditoId)) {
            $credito->setCancelado(true);
            $credito->setFechaCancelacion(new \DateTime());
            $em->persist($credito);
            $em->flush();
        }

        $partial = $this->renderView(
            'Credito/reciboPago.html.twig',
            array(
                'pagos' => $det_pagos,
                'nombre' => $nombre,
                'fecha' => $fecha,
                'credito' => $credito->getNroCredito() . '-' . $credito->getNroRenovacion()
            )
        );

        return new Response($partial);
    }

    private function registrarPagoCredito($credito, $pagado, $cuota, $fechaPago, $bandpunitorios, $observacion)
    {

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());

        /** Obtiene el Saldo que falta de la Cuota */
        $saldoCuota = ($cuota->getMontoCapital() +
            $cuota->getMontoIntereses() +
            $cuota->getGastoAdministrativo() +
            $cuota->getSaldoPunitorios()
        );


        /** Se controla si se debe tener en cuenta los punitorios */
        //        if( $credito->getPrograma()->getHabilitadoNuevoCredito() ) {
        $saldoCuota += $cuota->getMontoPunitorios();
        //        }

        /** Obtiene el Pago parciales de la Cuota */
        $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
        foreach ($cuota->getPagos() as $pagoRealizdo) {
            $porcCapital += $pagoRealizdo->getCapital();
            $porcInteres += $pagoRealizdo->getIntereses();
            $porcGasto += $pagoRealizdo->getGastoAdministrativo();
            $porPunitorios += $pagoRealizdo->getPunitorios();
            $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
        }

        /** Calcula el saldo de la Cuota menos los Pagos realizados */
        $saldoCuota = $saldoCuota - ($porcCapital + $porcInteres + $porcGasto + $porPunitorios + $porRefPunitorios);

        /** Calcula los nuevos punitorios en base a la la fecha de vencimiento o ultimo pago de esta cuota  */
        $punitorios = 0;
        if ($bandpunitorios) {
            //Calcular los dias, sacando los dias exlcuidos
            $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fechaPago, $credito->getPrograma());
            $dias = $array['dias'];
            $punitoriosNuevos = $array['montoPunitorios'];
        }

        $montoPago = $pagado;

        /** Empieza a Registrar el Pago */
        $pago = new Pago();
        $pago->setUsuario($usuario);
        $pago->setFecha($fechaPago);

        /** Punitorios */
        $punitorios = $cuota->getMontoPunitorios() + $punitoriosNuevos - $porPunitorios;
        if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
            if (doubleval($pagado) >= round($punitorios, 2)) {
                $pago->setPunitorios(round($punitorios, 2));
                $pagado = round($pagado - $punitorios, 2);
            } else {
                $pago->setPunitorios(round(($pagado), 2));
                $pagado = 0;
            }
            $saldoCuota += $punitorios;
        } else {
            $pago->setPunitorios(0);
        }

        /** Intereses */
        $porcInteres = $cuota->getMontoIntereses() - $porcInteres;
        if ($pagado > 0) {
            if (doubleval($pagado) >= round($porcInteres, 2)) {
                $pago->setIntereses(round($porcInteres, 2));
                $pagado = round($pagado - $porcInteres, 2);
            } else {
                $pago->setIntereses(round(($pagado), 2));
                $pagado = 0;
            }
        } else {
            $pago->setIntereses(0);
        }

        /** Gastos Administrativo */
        if ($pagado > 0) {
            $porcGasto = $cuota->getGastoAdministrativo() - $porcGasto;
            if (doubleval($pagado) >= round($porcGasto, 2)) {
                $pago->setGastoAdministrativo(round($porcGasto, 2));
                $pagado = round($pagado - $porcGasto, 2);
            } else {
                $pago->setGastoAdministrativo(round(($pagado), 2));
                $pagado = 0;
            }
        } else {
            $pago->setGastoAdministrativo(0);
        }

        /** Controla si la cuota esta refinanciada */
        if ($cuota->getRefinanciada()) {
            /** Punitorios por Refinanciación */
            $porRefPunitorios = $cuota->getSaldoPunitorios() - $porRefPunitorios;
            if ($pagado > 0) {
                if (doubleval($pagado) >= round($porRefPunitorios, 2)) {
                    $pago->setRefinPunitorios(round($porRefPunitorios, 2));
                    $pagado = round($pagado - $porRefPunitorios, 2);
                } else {
                    $pago->setRefinPunitorios(round(($pagado), 2));
                    $pagado = 0;
                }
            } else {
                $pago->setRefinPunitorios(0);
            }
        } else {
            $pago->setRefinPunitorios(0);
        }

        /** Monto Capital */
        if ($pagado > 0) {
            $porcCapital = $cuota->getMontoCapital() - $porcCapital;
            if (doubleval($pagado) >= round($porcCapital, 2)) {
                $pago->setCapital(round($porcCapital, 2));
                $pagado = round($pagado - $porcCapital, 2);
            } else {
                $pago->setCapital(round($pagado, 2));
                $pagado = 0;
            }
        } else {
            $pago->setCapital(0);
        }

        $pago->setCuota($cuota);
        $pago->setObservacion($observacion);

        /** Calculo importe total pagado */
        $totalPagado = round($pago->getCapital() +
            $pago->getGastoAdministrativo() +
            $pago->getIntereses() +
            $pago->getRefinPunitorios() +
            $pago->getPunitorios(), 2);

        $pago->setTotal($totalPagado);

        /** Comienza a actualizar la Cuota */

        /** Calculo el Saldo y si esta finalizado */
        if ((round($totalPagado, 2) - round($saldoCuota, 2)) == 0) {
            /** cuota completa */
            //            if( !$credito->getPrograma()->getHabilitadoNuevoCredito() ){
            //                $cuota->setSaldo($punitorios);
            //                $cuota->setCancelado(true);
            //                $cuota->setFechaCancelacion($fechaPago);
            //            }else{
            $cuota->setSaldo(0);
            $cuota->setCancelado(true);
            $cuota->setFechaCancelacion($fechaPago);
            //            }
        } else {
            /** cuota parcial */
            $cuota->setSaldo($saldoCuota - $totalPagado);
        }

        /** Punitorio */
        $punitorios_cta = $cuota->getMontoPunitorios() + $punitoriosNuevos;
        $cuota->setMontoPunitorios($punitorios_cta);

        /** Total Pagado */
        $cuotaPagado = $cuota->getPagado() + $montoPago;
        $cuota->setPagado($cuotaPagado);

        /** Se agrega el Pago */
        $cuota->addPagos($pago);

        if ($pagado == -0.0) {
            $pagado = 0;
        }

        $em->persist($cuota);
        $em->flush();

        return ['pago' => $pago, 'montoPago' => $montoPago, 'pagado' => $pagado];
    }

    private function registrarPagoCreditoRefinanciado($credito, $pagado, $cuota, $fechaPago, $bandpunitorios, $observacion)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());


        //Suma lo que se fue pagando en pagos parciales
        $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
        foreach ($cuota->getPagos() as $pagoRealizdo) {
            $porcCapital += $pagoRealizdo->getCapital();
            $porcInteres += $pagoRealizdo->getIntereses();
            $porcGasto += $pagoRealizdo->getGastoAdministrativo();
            $porPunitorios += $pagoRealizdo->getPunitorios();
            $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
        }


        if ($bandpunitorios) {
            //Calcular los dias, sacando los dias exlcuidos
            $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fechaPago, $credito->getPrograma());
            $dias = $array['dias'];
            $punitorios = $array['montoPunitorios'];
        }


        $montoPago = $pagado;
        $punitpago = 0;

        //Si retorna False es un plan Anterior
        if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
            //Planes Nuevos

            /* descuento punitorios nuevos */
            if ($pagado >= $punitorios) {
                $pagado = $pagado - $punitorios;
            }
        }


        /* descuento del resto de la cuota */
        if ($pagado >= $cuota->getSaldo()) {
            /* cuota completa */
            if (!$credito->getPrograma()->getHabilitadoNuevoCredito()) {
                $cuota->setSaldo($punitorios);
            } else {
                $cuota->setSaldo(0);
            }
        } else {
            /* cuota parcial */
            if (!$credito->getPrograma()->getHabilitadoNuevoCredito()) {
                $cuota->setSaldo($cuota->getSaldo() - $pagado + $punitorios);
            } else {
                $cuota->setSaldo($cuota->getSaldo() - $pagado);
            }
        }

        $punitorios_cta = $cuota->getMontoPunitorios() + $punitorios;
        $cuota->setMontoPunitorios($punitorios_cta);

        if ($cuota->getSaldo() == 0) {
            $cuota->setCancelado(true);
            $cuota->setFechaCancelacion($fechaPago);
        }

        //Si retorna False es un plan Anterior
        if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
            //Plan Nuevos
            $pago_a_cuota = ($montoPago - $punitorios) < 0 ? 0 : ($montoPago - $punitorios);
        } else {
            //Planes Anteriores
            $pago_a_cuota = $montoPago;
        }

        //registrar el pago
        $pago = new Pago();
        $pago->setUsuario($usuario);
        $pago->setFecha($fechaPago);


        if ($credito->getPrograma()->getHabilitadoNuevoCredito()) {
            $pago->setPunitorios($punitorios);
        } else {
            $pago->setPunitorios(0);
        }

        /* descuento punitorios de refinanciacion */
        $porRefPunitorios = $cuota->getSaldoPunitorios() - $porRefPunitorios;
        if ($pagado > 0) {
            if ($pagado >= $porRefPunitorios) {
                $pago->setRefinPunitorios(round($porRefPunitorios, 2));
                $pagado = $pagado - $porRefPunitorios;
            } else {
                $pago->setRefinPunitorios(round(($porRefPunitorios - $pagado), 2));
                $pagado = 0;
            }
        } else {
            $pago->setRefinPunitorios(0);
        }


        $porcInteres = $cuota->getMontoIntereses() - $porcInteres;
        if ($pagado > 0) {
            if ($pagado >= $porcInteres) {
                $pago->setIntereses(round($porcInteres, 2));
                $pagado = $pagado - $porcInteres;
            } else {
                $pago->setIntereses(round(($porcInteres - $pagado), 2));
                $pagado = 0;
            }
        } else {
            $pago->setIntereses(0);
        }


        if ($pagado > 0) {
            $porcGasto = $cuota->getGastoAdministrativo() - $porcGasto;
            if ($pagado >= $porcGasto) {
                $pago->setGastoAdministrativo(round($porcGasto, 2));
                $pagado = $pagado - $porcGasto;
            } else {
                $pago->setGastoAdministrativo(round(($porcGasto - $pagado), 2));
                $pagado = 0;
            }
        } else {
            $pago->setGastoAdministrativo(0);
        }

        if ($pagado > 0) {
            $porcCapital = $cuota->getMontoCapital() - $porcCapital;
            if ($pagado >= $porcCapital) {
                $pago->setCapital(round($porcCapital, 2));
                $pagado = $pagado - $porcCapital;
            } else {
                $pago->setCapital(round($pagado, 2));
                $pagado = 0;
            }
        } else {
            $pago->setCapital(0);
        }

        $pago->setTotal(
            $pago->getCapital() + $pago->getGastoAdministrativo() + $pago->getIntereses() + $pago->getRefinPunitorios()
        );

        $pago->setCuota($cuota);
        $pago->setObservacion($observacion);

        $cuota->setPagado($cuota->getPagado() + $pago->getTotal());
        $cuota->addPagos($pago);

        $em->persist($cuota);
        $em->flush();

        return ['pago' => $pago, 'montoPago' => $montoPago, 'pagado' => $pagado];
    }

    public function ingresarPagoCondolacionAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $fecha = new \Datetime;
        $fechaHoy = $fecha->format('d-m-Y');
        //$pagoAnterior = null;
        $data = explode('-', $id);
        $creditoId = $data[0];
        $cuotaNro = $data[1];
        $cuota = $em->getRepository('App:Cuota')->findByCreditoCuota($creditoId, $cuotaNro);
        $credito = $em->getRepository('App:Credito')->find($creditoId);
        $tasa = $credito->getPrograma()->getPorcPunitorio() / 30;
        $montoPun = 0;
        $band = $cuota->getPagado() > 0;
        $porPunitorios = 0;

        if ($credito->getSinPunitorios()) {
            // NO SE COBRAN PUNITORIOS
            $cuota->setMontoPunitorios(0);
        } else {
            $vencimiento = $cuota->getFechaVencimiento();
            /** CORREGIR VTO SI CAE FERIADO O DÍA DE FIN DE SEMANA **/
            $datosFeriados = $em->getRepository('App:TablaFeriados')->findFechas();
            $feriados = array();
            foreach ($datosFeriados as $dia) {
                $feriados[] = $dia['fecha'];
            }
            //$vencimiento = new \DateTime( $fechavto);
            while (in_array($vencimiento->format('Y-m-d'), $feriados) || in_array($vencimiento->format('N'), [6, 7])) {
                $vencimiento->modify("+1 days");
            }
            if ($cuota->getPagado() > 0) {
                $band = true;
                $fechaCuota = ($vencimiento > $cuota->getFechaCancelacion())
                    ? $vencimiento : $cuota->getFechaCancelacion();
                // $fechaCuota = $cuota->getFechaCancelacion();
            } else {
                $fechaCuota = $vencimiento;
                $band = false;
            }

            //Calcular los dias, sacando los dias exlcuidos

            //Calcular los dias, sacando los dias exlcuidos
            $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fecha, $credito->getPrograma());
            $dias = $array['dias'];
            $montoPun = $array['montoPunitorios'];

            foreach ($cuota->getPagos() as $pagoRealizdo) {
                $porPunitorios += $pagoRealizdo->getPunitorios();
            }

            //            if($dias>0){
            //                $cuota->setMontoPunitorios($montoPun) ;
            //            }else{
            //                $cuota->setMontoPunitorios(0);
            //            }
        }
        $cuota->setFechaCancelacion($fecha);
        $partial = $this->renderView(
            'Credito/_partial_condolacion_punitorios.html.twig',
            array(
                'cuota' => $cuota,
                'tasa' => $tasa,
                'montoPun' => $montoPun,
                'punitoriosPagados' => $porPunitorios,
                'creditoId' => $creditoId,
                'band' => $band
            )
        );
        return new Response($partial);
    }

    public function registrarPagoCondolacionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $fechaPago = new \DateTime($request->get('fechaCancelacion'));
        $creditoId = $request->get('credito');
        $cuotaNro = $request->get('cuota');
        $observacion = $request->get('observacion');
        $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
        $credito = $em->getRepository('App:Credito')->find($creditoId);

        /* para recibo */
        $num2str = new Num2Str();
        $fecha = DefaultController::longDateSpanish($fechaPago);
        $nombre = ($credito->getGrupo()) ? 'Grupo ' . $credito->getGrupo() : $credito->getEmprendedor();
        /* si se cancelan mas de una cuota */
        $tasa = $credito->getPrograma()->getPorcPunitorio() / 30;
        /**/
        $pagado = $request->get('pagado');
        $punitorios = floatval($request->get('montoPunitorios'));

        $bandpunitorios = FALSE;
        /* si el importe pagado no alcanza para cancelar los punitorios nuevos */
        if ($punitorios > $pagado) {
            $punitorios = $pagado;
        }
        $det_pagos = array();
        /* ITERAR CUOTAS HASTA CUBRIR EL MONTO PAGADO */

        $cuota = $em->getRepository('App:Cuota')->findByCreditoCuota($creditoId, $cuotaNro);
        if ($cuota) {
            $cuota->setFechaCancelacion($fechaPago);

            //Suma lo que se fue pagando en pagos parciales
            $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
            foreach ($cuota->getPagos() as $pagoRealizdo) {
                $porcCapital += $pagoRealizdo->getCapital();
                $porcInteres += $pagoRealizdo->getIntereses();
                $porcGasto += $pagoRealizdo->getGastoAdministrativo();
                $porPunitorios += $pagoRealizdo->getPunitorios();
                $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
            }

            if ($bandpunitorios) {
                //Calcular los dias, sacando los dias exlcuidos
                $array = DefaultController::getPunitoriosDiasMora($em, $cuota, $fecha, $credito->getPrograma());
                $dias = $array['dias'];
                $punitorios = $array['montoPunitorios'];
                $cuota->setMontoPunitorios($cuota->getMontoPunitorios() + $punitorios);
            }

            /* descuento del resto de la cuota */
            if ($pagado >= ($cuota->getSaldo() + $punitorios)) {
                $cuota->setSaldo(0);
            } else {
                /* cuota parcial */
                $cuota->setSaldo($cuota->getSaldo() + $punitorios - $pagado);
            }

            $punitorios_cta = $cuota->getMontoPunitorios() + $punitorios;
            $cuota->setMontoPunitorios($punitorios_cta);
            $cuota->setPagado($cuota->getPagado() + $pagado);
            if ($cuota->getSaldo() == 0)
                $cuota->setCancelado(true);

            //registrar el pago
            $pago = new Pago();

            $pago->setUsuario($usuario);
            $pago->setCreadoPor($usuario);
            $pago->setActualizadoPor($usuario);

            $pago->setFecha($fechaPago);
            $pago->setFechaCreacion($fechaPago);
            $pago->setFechaActualizacion($fechaPago);

            $pago->setCapital(0);
            $pago->setIntereses(0);
            $pago->setGastoAdministrativo(0);

            $pago->setPunitorios($pagado);
            $pago->setRefinPunitorios(0);
            $pago->setTotal($pagado);
            $pago->setCuota($cuota);
            $pago->setObservacion($observacion);

            $pago->setMontoExclusion($pagado);

            $cuota->addPagos($pago);

            $em->persist($cuota);
            $em->flush();

            /* Para recibo */
            $letras = $num2str->ValorEnLetras($pagado, "Pesos");
            array_push($det_pagos, array('monto' => $pagado, 'letras' => $letras, 'cuota' => $cuota->getNroCuota()));
        } else
            $pagado = 0;

        // Controlar si se cancela el credito
        if ($em->getRepository('App:Credito')->estaPagado($creditoId)) {
            $credito->setCancelado(true);
            $credito->setFechaCancelacion(new \DateTime());
            $em->persist($credito);
            $em->flush();
        }

        $partial = $this->renderView(
            'Credito/reciboPagoCondolacion.html.twig',
            array(
                'pagos' => $det_pagos,
                'nombre' => $nombre,
                'fecha' => $fecha,
                'credito' => $credito->getNroCredito() . '-' . $credito->getNroRenovacion()
            )
        );

        return new Response($partial);
    }

    public function reciboPagoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        if ($request->get('pago')) {
            $pago = $em->getRepository('App:Pago')->find($request->get('pago'));
            $creditoId = $pago->getCuota()->getCredito()->getId();
            $cuotaNro = $pago->getCuota()->getId();
            $pagado = $pago->getTotal();
            $fecha = DefaultController::longDateSpanish($pago->getFecha());
        } else {
            $creditoId = $request->get('credito');
            $cuotaNro = $request->get('cuota');
            $pagado = $request->get('pagado');
            $fecha = DefaultController::longDateSpanish(new \DateTime($request->get('fecha')));
        }
        $num2str = new Num2Str();
        $letras = $num2str->ValorEnLetras($pagado, "Pesos");

        $cuota = $em->getRepository('App:Cuota')->findByCreditoCuota($creditoId, $cuotaNro);
        $credito = $em->getRepository('App:Credito')->find($creditoId);
        //credito = new Credito();
        if ($credito->getGrupo())
            $nombre = 'Grupo ' . $credito->getGrupo();
        else
            $nombre = $credito->getEmprendedor();

        $det_pagos = array();
        array_push($det_pagos, array('monto' => $pagado, 'letras' => $letras, 'cuota' => $cuota->getNroCuota()));
        $partial = $this->renderView(
            'Credito/reciboPago.html.twig',
            array(
                'pagos' => $det_pagos,
                'nombre' => $nombre,
                'fecha' => $fecha,
                'credito' => $credito->getNroCredito() . '-' . $credito->getNroRenovacion()
            )
        );

        /*
        $partial = $this->renderView('MicrocreditosBundle:Credito:reciboPago.html.twig',
                                array('nombre'=>$nombre,'monto'=>$pagado,'letras'=>$letras,'cuota'=>$cuota->getNroCuota(),
                                    'fecha'=>$fecha ,'credito'=>$credito->getNroCredito().'-'.$credito->getNroRenovacion())); 
        */

        return new Response($partial);
    }

    public function deletePagoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $pago = $em->getRepository('App:Pago')->find($request->get('pago'));
        $pagoBancario = $em->getRepository('App:PagoBancario')->findOneByPago($request->get('pago'));
        $totalPago = $pago->getTotal();
        $cuota = $pago->getCuota();

        $cajas = $em->getRepository('App:Caja')->getCajaByPago($pago->getId());

        $hoy = new \Datetime;
        $hasAccess = $this->isGranted('ROLE_ADMIN');

        if (!$hasAccess) {
            if (($pago->getFechaAlta()->format('d-m-Y') <> $hoy->format('d-m-Y')) and !$hasAccess) {
                $array = array('msg' => 'No se puede eliminar un pago de días anteriores!!', 'estado' => 'ERROR');
                return new Response(json_encode($array));
            }
        }

        $em->getConnection()->beginTransaction();
        try {
            if (is_null($pagoBancario)) {
                $descripcion = 'Pago # ' . $pago->getId() . ' de la Cuota ' . $cuota->getNroCuota();
                $em->getRepository('App:Caja')->guardarNuevoEgreso(
                    $totalPago,
                    $descripcion,
                    $this->get('security.token_storage')->getToken()->getUser()->getId(),
                    $cuota->getCredito(),
                    null,
                    $em->getRepository('App:Parametro')->find(211)
                );

                foreach ($cajas as $caja) {
                    $caja->setObservacion('Actualizado por Borrado. ' . $descripcion);
                    $caja->setPago(null);
                    $em->persist($caja);
                }
            } else {
                $pagoBancario->setActivo(false);
                $em->persist($pagoBancario);
            }

            $cuota = $pago->getCuota();
            $cuota->setSaldoPunitorios($cuota->getSaldoPunitorios() + $pago->getRefinPunitorios());
            $cuota->setSaldo($cuota->getSaldo() + $pago->getRefinPunitorios());

            //Suma lo que se fue pagando en pagos parciales
            $porcCapital = $porcInteres = $porcGasto = $porPunitorios = $porRefPunitorios = 0;
            foreach ($cuota->getPagos() as $pagoRealizdo) {
                if ($pago->getId() <> $pagoRealizdo->getId()) {
                    $porcCapital += $pagoRealizdo->getCapital();
                    $porcInteres += $pagoRealizdo->getIntereses();
                    $porcGasto += $pagoRealizdo->getGastoAdministrativo();
                    $porPunitorios += $pagoRealizdo->getPunitorios();
                    $porRefPunitorios += $pagoRealizdo->getRefinPunitorios();
                }
            }


            $cuota->setMontoPunitorios($porPunitorios);

            $totalPagado = $porcCapital + $porcInteres + $porcGasto + $porPunitorios + $porRefPunitorios;
            $totalCuota = $cuota->getMontoCapital() +
                $cuota->getMontoIntereses() +
                $cuota->getGastoAdministrativo() +
                $cuota->getSaldoPunitorios() +
                $porPunitorios;

            $cuota->setPagado($totalPagado);
            $cuota->setSaldo($totalCuota - $totalPagado);
            $cuota->setCancelado(0);

            $em->persist($cuota);
            $em->remove($pago);


            $em->flush();
            $em->getConnection()->commit();
            $estado = 'OK';
        } catch (\PDOException $e) {
            $em->getConnection()->rollback();
            $array = array('msg' => 'No se puede eliminar el pago en este momento. Motivo:' . $e->getMessage(), 'estado' => 'ERROR');
            return new Response(json_encode($array));
        }

        $array = array('msg' => 'Se ha eliminado el pago correctamente.', 'estado' => $estado);
        return new Response(json_encode($array));
    }


    /**
     * Deletes a Credito entity.
     *
     */
    public function deleteAction($id)
    {
        //        $form = $this->createDeleteForm($id);
        //        $em = $this->getDoctrine()->getManager();
        //        $request = $this->container->get('request_stack')->getCurrentRequest();
        //
        //        $form->bindRequest($request);
        //
        //        if ($form->isValid()) {
        //            $entity = $em->getRepository('App:Credito')->find($id);
        //
        //            if (!$entity) {
        //                throw $this->createNotFoundException('Unable to find Credito entity.');
        //            }
        //
        //            $em->remove($entity);
        //            $em->flush();
        //        }

        return $this->redirect($this->generateUrl('solicitud'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm();
    }

    public function getEmprendedoresByGrupoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $id = $request->get('id');
        $empId = $request->get('empId');
        $individual = false;

        if ($id != '') {
            $grupo = $em->getRepository('App:Grupo')->find($id);
            $emprendedores = $grupo->getEmprendedores();
        } else {
            $individual = true;
            if ($empId)
                $emprendedores = $em->getRepository('App:Emprendedor')->find($empId);
            else
                $emprendedores = new Emprendedor();
        }
        $partial = $this->renderView(
            'Credito/credito_emprendedor.html.twig',
            array('emprendedores' => $emprendedores, 'individual' => $individual)
        );
        return new Response(json_encode($partial));
    }

    public function imprimirContratoAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $credito = $em->getRepository('App:Credito')->find($id);
        $representante = $em->getRepository('App:Parametro')->getRepresentante();
        //$fileName = ($credito->getAprobado())?'CRE'.$credito:'SOL'.$credito; 
        $fileName = 'CRE' . $credito;
        $num2str = new Num2Str();
        if ($credito->getAprobado() == 0)
            $montoCuota = $this->getMontoCuota($credito);
        else
            $montoCuota = $credito->getCuotas()->first()->getMontoTotal();

        if (is_string($montoCuota))
            $montoCuota = floatval($montoCuota);

        $letras = array(
            'monto' => $num2str->ValorEnLetras($credito->getMontoCredito(), "Pesos"),
            'cuota' => $num2str->ValorEnLetras($montoCuota, "Pesos"),
            'dias' => $num2str->ValorEnLetras($credito->getPeriodo(), "")
        );

        $fechaFirma = DefaultController::longDateSpanish($credito->getFechaEntrega() ? $credito->getFechaEntrega() : $credito->getFechaAlta());
        if ($credito->getGrupo()) {
            $partial = $this->renderView(
                'Credito/contrato_grupoPdf.html.twig',
                array(
                    'credito' => $credito,
                    'letras' => $letras,
                    'fechaFirma' => $fechaFirma,
                    'montoCuota' => $montoCuota,
                    'representante' => $representante
                )
            );
        } else {
            $partial = $this->renderView(
                'Credito/contrato_individualPdf.html.twig',
                array(
                    'credito' => $credito,
                    'letras' => $letras,
                    'fechaFirma' => $fechaFirma,
                    'montoCuota' => $montoCuota,
                    'representante' => $representante
                )
            );
        }

        $options = [
            'margin-top' => 8,
            'margin-right' => 0,
            'margin-bottom' => 10,
            'margin-left' => 10,
        ];

        $response = new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($partial), $options),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );

        return $response;
    }

    private function getMontoCuota($credito)
    {
        $em = $this->getDoctrine()->getManager();

        $tasa = ($credito->getPorcInteres() / 365) * $credito->getPeriodo();
        //composicion de cuota
        $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
        foreach ($credito->getEmprendedores() as $emp) {
            $capital = $emp->getMontoCredito() / $credito->getCantCuotas();
            $montoCapital = $montoCapital + $capital;
            $intereses = (($credito->getSinIntereses()) ? 0 : round(($emp->getMontoCredito() * $tasa / 100), 2));
            $gastos = (($credito->getSinGastos()) ? 0 : self::getGastoAdm($credito->getPeriodo(), $emp->getMontoCredito(), $em));
            $total = ceil($capital + $intereses + $gastos);
            $redondeo = $total - ($capital + $intereses + $gastos);
            $gastos = $gastos + $redondeo;
            $gastoAdm = $gastoAdm + $gastos;
            $montoIntereses = $montoIntereses + $intereses;
            $montoTotal = $montoTotal + $total;
        }
        return number_format($montoTotal, 2, '.', '');
    }

    public function imprimirSolicitudAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $credito = $em->getRepository('App:Credito')->find($id);
        $fileName = 'SOL' . $credito;
        $num2str = new Num2Str();
        $letras = $num2str->ValorEnLetras($credito->getMontoCredito(), "Pesos");

        $partial = $this->renderView(
            'Credito/solicitudPdf.html.twig',
            array(
                'credito' => $credito,
                'letras' => $letras,
            )
        );

        $response = new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($partial), array()),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
        return $response;
    }

    public function imprimirCuotasAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $credito = $em->getRepository('App:Credito')->find($id);
        $fileName = 'Cuotas_CRE' . $credito;

        if ($credito->getAprobado() == 0)
            $this->setCuotasCredito($credito);
        $partial = $this->renderView(
            'Credito/cuotasPdf.html.twig',
            array('credito' => $credito)
        );
        //return new Response($partial);

        $response = new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($partial), array()),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '.pdf"'
            )
        );
        /* $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-word; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename="'.$fileName.'.doc"');
            $response->setContent($partial);*/
        return $response;
    }

    private function setCuotasCredito($credito)
    {

        $em = $this->getDoctrine()->getManager();

        $periodo = 86400 * $credito->getPeriodo();
        $tasa = ($credito->getPorcInteres() / 365) * $credito->getPeriodo();
        $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
        //composicion de cuota
        foreach ($credito->getEmprendedores() as $credEmp) {
            $capital = $credEmp->getMontoCredito() / $credito->getCantCuotas();
            $montoCapital = $montoCapital + $capital;
            $intereses = (($credito->getSinIntereses()) ? 0 : round(($credEmp->getMontoCredito() * $tasa / 100), 2));
            //            $gastos = ( ($credito->getSinGastos())?0: self::getGastoAdm($credito->getPeriodo(), $credEmp->getMontoCredito(), $em));
            $gastos = $credEmp->getGastoAdministrativo() / $credito->getCantCuotas();
            $total = ceil($capital + $intereses + $gastos);
            $redondeo = $total - ($capital + $intereses + $gastos);
            $gastos = $gastos + $redondeo;

            $gastoAdm = $gastoAdm + $gastos;
            $montoIntereses = $montoIntereses + $intereses;
            $montoTotal = $montoTotal + $total;
            $credEmp->setMontoCuota($total);
        }
        $fechaVencimiento = strtotime($credito->getFechaVtoCuota1()->format('d-m-Y'));
        for ($i = 1; $i <= $credito->getCantCuotas(); $i++) {
            $cuota = new Cuota();
            $cuota->setCredito($credito);
            $cuota->setNroCuota($i);
            $cuota->setMontoCapital(round($montoCapital, 2));
            $cuota->setMontoIntereses(round($montoIntereses, 2));
            $cuota->setGastoAdministrativo($gastoAdm);
            $cuota->setMontoTotal($montoTotal);
            $cuota->setSaldo($montoTotal);
            $cuota->setFechaVencimiento(new \DateTime(date('d-m-Y', $fechaVencimiento)));
            $credito->addCuota($cuota);
            $fechaVencimiento = $fechaVencimiento + $periodo;
        }
        return $credito;
    }

    public function getopListAction($estado)
    {
        $partial = $this->renderView('Credito/opList.html.twig', array('estado' => $estado));
        return new Response($partial);
    }

    public function pagoOpListAction()
    {
        $partial = $this->renderView('Credito/pagosOpList.html.twig');
        return new Response($partial);
    }

    public function getDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $option = $request->get('option');
        $format = $request->get('format');
        $estado = $request->get('estado');
        $filename = 'Creditos';
        $pdfOptions = array('footer-right' => "[page]/[toPage]", 'footer-line' => true);
        switch ($estado) {
            case 0:
                $varFiltro = 'filtro_solicitud';
                $filename = 'Solicitudes';
                break;
            case 1:
                $varFiltro = 'filtro_credito';
                break;
            case 2:
                $varFiltro = 'filtro_morosos';
                $filename = 'CreditosMorosos';
                break;
            case 3:
                $varFiltro = 'filtro_incobrables';
                $filename = 'CreditosIncobrables';
                $pdfOptions = array('orientation' => 'Landscape', 'footer-right' => "[page]/[toPage]", 'footer-line' => true);
                break;
        }
        $sessionFiltro = $session->get($varFiltro);
        isset($sessionFiltro['programa']) ? $programa = $em->getRepository('App:Programa')->find($sessionFiltro['programa']) : $programa = null;
        if ($estado == 2) {
            $creditos = $em->getRepository('App:Credito')->findMorososDQL($sessionFiltro)->getResult();
            foreach ($creditos as $entity) {
                $entity->setDeuda($this->getDeuda($entity));
            }
            $ord = usort($creditos, function ($a1, $a2) {
                $value1 = strtotime(($a1->getPrimerVtoImpago()) ? $a1->getPrimerVtoImpago()->format('Y-m-d') : NULL);
                $value2 = strtotime(($a2->getPrimerVtoImpago()) ? $a2->getPrimerVtoImpago()->format('Y-m-d') : NULL);
                return $value1 - $value2;
            });
        } elseif ($estado == 3) {
            $creditos = $em->getRepository('App:Credito')->findIncobrablesDQL($sessionFiltro)->getResult();
            foreach ($creditos as $entity) {
                $this->setDatos($entity, $em);
                $entity->setDeuda($this->getDeuda($entity));
            }
            $ord = usort($creditos, function ($a1, $a2) {
                $value1 = strtotime($a1->getPrimerVtoImpago()->format('Y-m-d'));
                $value2 = strtotime($a2->getPrimerVtoImpago()->format('Y-m-d'));
                return $value1 - $value2;
            });
        } else {
            echo ("--------------------------------------------");
            $creditos = $em->getRepository('App:Credito')->findByMyFiltroDQL($sessionFiltro, $estado);
        }

        //$creditos = $em->getRepository('App:credito')->findByMyFiltroDQL($sessionFiltro,$estado)->getResult();
        $creditos = self::setUltimaCuota($creditos, $em);


        if ($format == 'PDF') {
            $html = $this->renderView(
                'Credito/listPdf.html.twig',
                array(
                    'option' => $option,
                    'filtro' => ($sessionFiltro) ? $sessionFiltro : '',
                    'programa' => $programa,
                    'entities' => $creditos,
                    'estado' => $estado
                )
            );
            $response = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html), $pdfOptions),
                200,
                array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="file.pdf"'
                )
            );
        } else {
            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=' . $filename . '.xls');
            $html = $this->renderView(
                'Credito/listXls.html.twig',
                array(
                    'option' => $option,
                    'filtro' => ($sessionFiltro) ? $sessionFiltro : '',
                    'programa' => $programa,
                    'entities' => $creditos,
                    'estado' => $estado
                )
            );
            $response->setContent($html);
        }
        return $response;
    }

    public function expEntregadosAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entregados = $em->getRepository('App:Credito')->getEntregados();
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Entregados.xls');
        $html = $this->renderView('Credito/entregadosXls.html.twig', array('entregados' => $entregados));
        $response->setContent($html);
        return $response;
    }

    public function expEntregadosEfectivosAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entregados = $em->getRepository('App:Credito')->getEntregadosEfectivo();
        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'filename=Entregados.xls');
        $html = $this->renderView('Credito/entregadosXls.html.twig', array('entregados' => $entregados));
        $response->setContent($html);
        return $response;
    }


    public function pagosGetDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $option = $request->get('option');
        $format = $request->get('format');

        $sessionFiltro = $session->get('filtro_pagos');

        /*   $programas = $em->getRepository('App:Programa')->findByActivosDQL();*/
        isset($sessionFiltro['asesor']) ? $asesor = $em->getRepository('App:User')->find($sessionFiltro['asesor']) : $asesor = null;
        ($session->has("filtro_pagos")) ? $pagos = $em->getRepository('App:Pago')->findPagosByFiltroDQL($session->get("filtro_pagos"))->getResult() : $pagos = null;
        ($session->has("filtro_pagos")) ? $grupoAsesores = $em->getRepository('App:Pago')->getPagosGroupAsesores($session->get("filtro_pagos"))->getResult() : $grupoAsesores = null;
        ($session->has("filtro_pagos")) ? $grupoFechas = $em->getRepository('App:Pago')->getPagosGroupFecha($session->get("filtro_pagos"))->getResult() : $grupoFechas = null;
        $varArray = array(
            'option' => $option,
            'filtro' => ($sessionFiltro) ? $sessionFiltro : '',
            'pagos' => $pagos,
            'asesor' => $asesor,
            'grupoAsesores' => $grupoAsesores,
            'grupoFechas' => $grupoFechas
        );

        //Ordenos los pagos por asesor y fecha
        $pagos = array();
        foreach ($varArray['pagos'] as $pago) {
            $pagos[$pago->getUsuario()->getId()][$pago->getFecha()->format('Y/m/d')][] = $pago;
        }

        $varArray['pagos'] = $pagos;




        if ($format == 'PDF') {
            $html = $this->renderView('Credito/pagosPdf.html.twig', $varArray);
            $response = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html), array('footer-right' => "[page]/[toPage]", 'footer-line' => true)),
                200,
                array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="file.pdf"'
                )
            );
        } else {
            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Pagos.xls');
            $html = $this->renderView('Credito/pagosXls.html.twig', $varArray);
            $response->setContent($html);
        }

        return $response;
        //        return new Response($html);

    }

    public function listadoPagosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_pagos');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array(
                    'programa' => '',
                    'grupo' => '',
                    'grupoId' => '',
                    'asesor' => '',
                    'itemsperpage' => 20,
                    'emprendedor' => '',
                    'emprendedorId' => '',
                    'desde' => '',
                    'hasta' => '',
                    'desdeCarga' => '',
                    'hastaCarga' => '',
                    'condonacion' => false
                );
                break;
            case 'buscar':
                $filtro = array(
                    'itemsperpage' => $request->get('_itemsperpage'),
                    'programa' => $request->get('_programa'),
                    'grupo' => $request->get('_grupo'),
                    'grupoId' => $request->get('_grupoId'),
                    'asesor' => $request->get('_asesor'),
                    'emprendedor' => $request->get('_emprendedor'),
                    'emprendedorId' => $request->get('_emprendedorId'),
                    'desde' => $request->get('_desde'),
                    'hasta' => $request->get('_hasta'),
                    'desdeCarga' => $request->get('_desdeCarga'),
                    'hastaCarga' => $request->get('_hastaCarga'),
                    'condonacion' => $request->get('_condonacion')
                );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'itemsperpage' => $sessionFiltro['itemsperpage'],
                    'programa' => $sessionFiltro['programa'],
                    'grupo' => $sessionFiltro['grupo'],
                    'grupoId' => $sessionFiltro['grupoId'],
                    'asesor' => $sessionFiltro['asesor'],
                    'emprendedor' => $sessionFiltro['emprendedor'],
                    'emprendedorId' => $sessionFiltro['emprendedorId'],
                    'desde' => $sessionFiltro['desde'],
                    'hasta' => $sessionFiltro['hasta'],
                    'desdeCarga' => $sessionFiltro['desdeCarga'],
                    'hastaCarga' => $sessionFiltro['hastaCarga'],
                    'condonacion' => $sessionFiltro['condonacion']
                );
                break;
        }

        $session->set('filtro_pagos', $filtro);


        $paginator = $this->get('knp_paginator');
        $query = null;
        $varItems = ($filtro['itemsperpage'] == 0) ? 1000 : $filtro['itemsperpage'];

        $query = $em->getRepository('App:Pago')
            ->findPagosTotalesByFiltroDQL($session->get("filtro_pagos"));

        $totalRegistros = 0;
        $totalCapital = 0;
        $totalIntereses = 0;
        $totalRefinPunitorios = 0;
        $totalPunitorios = 0;
        $totalGastoAdministrativo = 0;
        $totalPago = 0;
        foreach ($query->getArrayResult() as $pagoArray) {
            $totalRegistros += $pagoArray['cantidad'];
            $totalCapital += $pagoArray['capital'];
            $totalIntereses += $pagoArray['intereses'];
            //            $totalRefinPunitorios += $pagoArray['refinPunitorios'];
            $totalPunitorios += $pagoArray['punitorios'];
            $totalGastoAdministrativo += $pagoArray['gastoAdministrativo'];
            $totalPago += $pagoArray['total'];
        }
        $totales = array(
            'totalRegistros' => $totalRegistros,
            'totalCapital' => $totalCapital,
            'totalIntereses' => $totalIntereses,
            'totalRefinPunitorios' => $totalRefinPunitorios,
            'totalPunitorios' => $totalPunitorios,
            'totalGastoAdministrativo' => $totalGastoAdministrativo,
            'totalPago' => $totalPago,
        );


        $query = $em->getRepository('App:Pago')
            ->findPagosByFiltroDQL($session->get("filtro_pagos"));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            /*page number*/
            10 /*limit per page*/
        );


        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $usuarios = $em->getRepository('App:User')->findByMyCriteria();
        $vars = array(
            'entityName' => 'credito_list_pagos',
            'filtro' => $session->get('filtro_pagos'),
            'programas' => $programas,
            'usuarios' => $usuarios,
            'entities' => $entities,
            'paginator' => $paginator,
            'totales' => $totales
        );

        return $this->render('Credito/pagosList.html.twig', $vars);
    }

    public function getGastosAdmAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $monto = $request->get('monto');
        $periodo = $request->get('periodo');
        $gasto = ($monto > 0) ? self::getGastoAdm($periodo, $monto, $em) : 0;
        return new Response($gasto);
    }

    public static function getGastoAdm($periodo, $monto, $em)
    {
        if ($monto == 0)
            return 0;
        $periodo = intval($periodo);
        $data = $em->getRepository('App:Credito')->getGastosAdministrativos($periodo, $monto);


        //busco el perdiodo mas alto
        $periodoBusqueda = 0;
        $ultimoPerdiodo = 0;

        foreach ($data as $row) {

            //corresponde al periodo mas chico
            if ($periodo <= $row->getPeriodo() and $periodoBusqueda == 0) {
                $periodoBusqueda = $row->getPeriodo();
                break;
            }
            $ultimoPerdiodo = $row->getPeriodo();
        }

        if ($periodoBusqueda == 0 and $ultimoPerdiodo != 0) {
            $periodoBusqueda = $ultimoPerdiodo;
        }

        foreach ($data as $row) {

            if ($periodoBusqueda == $row->getPeriodo()) {
                if ($row->getDesde() == 0 && $row->getHasta() == 0) {
                    return $row->getValor();
                } else {
                    if (($monto >= $row->getDesde()) && ($monto <= $row->getHasta())) {
                        return $row->getValor();
                    }
                }
            }
        }

        return $em->getRepository('App:Credito')->getMinGastoAdministrativo();
    }

    public static function setUltimaCuota($creditos, $em)
    {
        foreach ($creditos as $entity) {
            self::setDatos($entity, $em);
        }
        return $creditos;
    }

    public static function setDatos($entity, $em)
    {
        $datos = $em->getRepository('App:Credito')->getUltimoPago($entity);
        $entity->setPrimerVtoImpago($datos['vto'] ? $datos['vto'] : null);
        $entity->setUltimaCuotaPaga($datos['cuota'] ? $datos['cuota'] : 0);
        $entity->setUltimoPago($datos['pago'] ? $datos['pago'] : null);

        //$entity->setDeuda( getDeuda($entity) );
        /* if($entity->getCancelado()==0)*/
        $entity->setRenovado($datos['renovado']);
        /* else  $entity->setRenovado(0);*/
    }

    public function getDiasDifAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $creditoId = $request->get('credito');
        $fechavto = $request->get('fechavto');
        $fechapago = $request->get('fechapago');
        $subtotal = $request->get('subtotal');
        $tasa = $request->get('tasa');
        $montoPun = $request->get('montopun');
        $dif = 0;

        $credito = $em->getRepository('App:Credito')->find($creditoId);
        if ($credito->getSinPunitorios()) {
            $punitorio = 0;
        } else {

            /** CORREGIR VTO SI CAE FERIADO O DÍA DE FIN DE SEMANA * */
            $datosFeriados = $em->getRepository('App:TablaFeriados')->findFechas();
            $feriados = array();
            foreach ($datosFeriados as $dia) {
                $feriados[] = $dia['fecha'];
            }
            $vencimiento = new \DateTime($fechavto);
            while (in_array($vencimiento->format('Y-m-d'), $feriados) || in_array($vencimiento->format('N'), [6, 7])) {
                $vencimiento->modify("+1 days");
            }

            $dif = DefaultController::dateDiff($vencimiento->format('d-m-Y'), $fechapago);
            $dif = ($dif >= 0) ? $dif : 0;
            if ($montoPun > 0)
                $punitorio = $montoPun * $dif;
            else
                $punitorio = round((($subtotal * $tasa) / 100) * $dif);
        }

        $total = round($subtotal + $punitorio);

        $data = array('punitorio' => $punitorio, 'total' => $total, 'dias' => $dif);
        return new Response(json_encode($data));
    }

    /**
     *  CARGA MASIVA DE CREDITOS
     */

    public function cargaMasivaAction()
    {

        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Credito');
        $nro = $ultimo->getNumerico() + 1;
        $codigo = str_pad($nro, 6, "0", STR_PAD_LEFT);
        // $ultimo->setNumerico($nro);
        //$em->persist($ultimo);
        //$em->flush();
        $entity = new Credito();
        $entity->setNroCredito($codigo);
        // $entity->setNroRenovacion(NULL);
        $entity->setPrograma($em->getRepository('App:Programa')->find(5));
        $entity->setFechaCredito(new \Datetime);
        $asesor = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
        $entity->setAsesor($asesor);
        $form = $this->createForm(CreditoType::class, $entity);

        return $this->render(
            'Credito/cargaMasiva.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView()
            )
        );
    }

    public function generarCuotasCargaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form = $request->get('credito');
        $emprendedor = $request->get('empId');
        /* Controlar el nro de carpeta */
        if ($form['nro_carpeta']) {
            $existe = $em->getRepository('App:Credito')->esValidoNroCarpeta($form['nro_carpeta']);
            if ($existe) {
                $msg = 'INVALIDO';
                $partial = '<div class="notification error png_bg" style="margin:15px 0"><div> Ya existe el nro de carpeta indicada.</div></div>';
                $array = array('msg' => $msg, 'partial' => $partial);
                return new Response(json_encode($array));
            }
        }
        /* Controlar que sea válido el grupo o el emprendedor para sacar credito nuevo */
        if (isset($form['grupo'])) {
            $valido = $em->getRepository('App:Credito')->esValidoParaCargaCredito('G', $form['grupo']);
        } else {
            $valido = $em->getRepository('App:Credito')->esValidoParaCargaCredito('E', $emprendedor);
        }
        $msg = 'VALIDO';

        if ($valido) {
            //$monto = $request->get('monto_credito');
            $periodo = 86400 * $form['periodo'];
            $tasa = ($form['porc_interes'] / 365) * $form['periodo'];
            $fechaVencimiento = $form['fecha_vto_cuota1'];
            $simulacionCuotas = array();
            //composicion de cuota
            $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = 0;
            foreach ($request->get('monto_credito') as $montoEmp) {
                $capital = $montoEmp / $form['cant_cuotas'];
                $montoCapital = $montoCapital + $capital;

                $intereses = (isset($form['sin_intereses']) ? 0 : round(($montoEmp * $tasa / 100), 2));
                $gastos = (isset($form['sin_gastos']) ? 0 : self::getGastoAdm($form['periodo'], $montoEmp, $em));
                $total = ceil($capital + $intereses + $gastos);
                $redondeo = $total - ($capital + $intereses + $gastos);
                $gastos = $gastos + $redondeo;
                //$intereses = $total -( $capital + $gastos );
                $gastoAdm = $gastoAdm + $gastos;
                $montoIntereses = $montoIntereses + $intereses;
                $montoTotal = $montoTotal + $total;

                /*  $montoCapital = $montoCapital + ( $montoEmp/$form['cant_cuotas'] );
                $int = round(($montoEmp * $tasa / 100),2);
                $montoIntereses = $montoIntereses + $int;*/
            }

            // $gastoAdm = $form['gasto_administrativo'];
            //  $montoTotal = ceil($montoCapital+$montoIntereses+$gastoAdm);
            // $montoIntereses = $montoTotal - ( $montoCapital + $gastoAdm );

            for ($i = 1; $i <= $form['cant_cuotas']; $i++) {
                $cuota = array(
                    'montoCapital' => $montoCapital,
                    'montoIntereses' => $montoIntereses,
                    'gastoAdministrativo' => $gastoAdm,
                    'montoTotal' => $montoTotal,
                    'saldo' => $montoTotal,
                    'vencimiento' => $fechaVencimiento
                );
                array_push($simulacionCuotas, $cuota);
                $fechaVencimiento = date('d-m-Y', (strtotime($fechaVencimiento) + $periodo));
                //                $fechaVencimiento = date('d-m-Y', $fechaVencimiento);
            }
            $partial = $this->renderView(
                'Credito/_partial_carga_cuota.html.twig',
                array('cuotas' => $simulacionCuotas)
            );
        } else {
            $msg = 'INVALIDO';
            $partial = '<div class="notification error png_bg" style="margin:15px 0"><div> Existe un crédito o solicitud vigente. 
                No es posible generar uno nuevo en esta sección.</div></div>';
        }
        $array = array('msg' => $msg, 'partial' => $partial);
        return new Response(json_encode($array));
    }

    /**
     *  Nueva carga de credito.
     */
    public function createCargaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $credito = new Credito();
        $emprendedor = $request->get('empId');
        $monto = $request->get('monto_credito');
        $gasto = $request->get('gasto_administrativo');
        $coordinador = $request->get('coordinador');
        $garante = $request->get('garante');
        $garanteDni = $request->get('garanteDni');
        $garanteDireccion = $request->get('garanteDireccion');
        $garanteTelefono = $request->get('garanteTelefono');
        $garanteOcupacion = $request->get('garanteOcupacion');
        $garanteLugarTrabajo = $request->get('garanteLugarTrabajo');

        $garanteEmpleadorNombre = $request->get('garanteEmpleadorNombre');
        $garanteEmpleadorCuit = $request->get('garanteEmpleadorCuit');
        $garanteIngresos = $request->get('garanteIngresos');

        $form = $this->createForm(CreditoType::class, $credito);
        $form->bindRequest($request);

        if ($form->isValid()) {
            /* Verificar que sea valido para credito nuevo */
            if ($credito->getGrupo()) {
                $valido = $em->getRepository('App:Credito')->esValidoParaCredito('G', $credito->getGrupo()->getId(), $credito->getNroRenovacion(), $credito->getId());
                $msg = 'Existe un crédito o solicitud vigente para el grupo indicado. No es posible generar uno nuevo.';
            } else {
                $valido = $em->getRepository('App:Credito')->esValidoParaCredito('E', $emprendedor, $credito->getNroRenovacion(), $credito->getId());
                $msg = 'Existe un crédito o solicitud vigente para el emprendedor indicado. No es posible generar uno nuevo.';
            }
            if ($valido) {
                $em->getConnection()->beginTransaction();
                try {

                    $nroCuota = $request->get('nro_cuota');
                    $montoCapital = $request->get('capital');
                    $montoIntereses = $request->get('interes');
                    $montoGastos = $request->get('gasto');
                    $total = $request->get('total');
                    $vtos = $request->get('vencimiento');
                    $punitorios = $request->get('punitorios');

                    $pagado = $request->get('pagado');
                    $saldo = $request->get('saldo');
                    $pago = $request->get('fecha_pago');
                    if (!$request->get('renov')) {
                        $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Credito');
                        $nro = $ultimo->getNumerico() + 1;
                        $nroCredito = str_pad($nro, 6, "0", STR_PAD_LEFT);
                        $ultimo->setNumerico($nro);
                        $credito->setNroCredito($nroCredito);
                        $em->persist($ultimo);
                    }
                    // $gastoAdm = self::getGastoAdm($credito->getPeriodo(),$credito->getMontoCredito(), $em);
                    // $credito->setGastoAdministrativo($gastoAdm);
                    foreach ($emprendedor as $key => $id) {
                        $credito_emprendedor = new CreditoEmprendedor();
                        $credito_emprendedor->setCredito($credito);
                        $emp = $em->getRepository('App:Emprendedor')->find($id);
                        $credito_emprendedor->setEmprendedor($emp);
                        $credito_emprendedor->setMontoCredito($monto[$key]);
                        $credito_emprendedor->setGastoAdministrativo($gasto[$key]);
                        $credito_emprendedor->setCoordinador(isset($coordinador[$key]));
                        if ($garante) {
                            $credito_emprendedor->setGarante($garante[$key]);
                            $credito_emprendedor->setGaranteDni($garanteDni[$key]);
                            $credito_emprendedor->setGaranteDireccion($garanteDireccion[$key]);
                            $credito_emprendedor->setGaranteTelefono($garanteTelefono[$key]);
                            $credito_emprendedor->setGaranteOcupacion($garanteOcupacion[$key]);
                            $credito_emprendedor->setGaranteLugarTrabajo($garanteLugarTrabajo[$key]);

                            $credito_emprendedor->setGaranteEmpleadorNombre($garanteEmpleadorNombre[$key]);
                            $credito_emprendedor->setGaranteEmpleadorCuit($garanteEmpleadorCuit[$key]);
                            $credito_emprendedor->setGaranteIngresos($garanteIngresos[$key]);
                        }
                        $credito->addCreditoEmprendedor($credito_emprendedor);
                    }
                    $credito->setAprobado(true);
                    $credito->setFechaAprobacion(new \Datetime);
                    //   var_dump($nroCuota);
                    /* Generar las cuotas */
                    for ($i = 1; $i <= count($nroCuota); $i++) {
                        $n = $i - 1;
                        $cuota = new Cuota();
                        $cuota->setCredito($credito);
                        $cuota->setNroCuota($i);
                        $cuota->setFechaVencimiento(new \DateTime($vtos[$n]));
                        $cuota->setMontoCapital(round($montoCapital[$n], 2));
                        $cuota->setMontoIntereses(round($montoIntereses[$n], 2));
                        $cuota->setGastoAdministrativo($montoGastos[$n]);
                        $cuota->setMontoTotal($total[$n]);
                        $cuota->setMontoPunitorios($punitorios[$n]);
                        $cuota->setPagado($pagado[$n]);
                        $cuota->setSaldo($saldo[$n]);
                        $fecha = $pago[$n] ? new \DateTime($pago[$n]) : null;
                        $cuota->setFechaCancelacion($fecha);
                        if ($saldo[$n] == 0)
                            $cuota->setCancelado(true);
                        $credito->addCuota($cuota);
                    }

                    $em->persist($credito);
                    $em->flush();
                    $em->getConnection()->commit();

                    return $this->redirect($this->generateUrl('credito_show', array('id' => $credito->getId())));
                } catch (\PDOException $e) {
                    $em->getConnection()->rollback();
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', $e->getMessage());
                    return $this->render(
                        'Credito/cargaMasiva.html.twig',
                        array(
                            'entity' => $credito,
                            'form' => $form->createView(),
                            'delete_form' => $form->createView()
                        )
                    );
                }
            } else {
                /* no es valido para nuevo credito */
                $session = $request->getSession();
                $session->getFlashBag()->set('error', $msg);
                return $this->render(
                    'Credito/cargaMasiva.html.twig',
                    array(
                        'entity' => $credito,
                        'form' => $form->createView(),
                        'delete_form' => $form->createView()
                    )
                );
            }
        }
        return $this->render(
            'MicrocreditosBundle:Credito:cargaMasiva.html.twig',
            array(
                'entity' => $credito,
                'form' => $form->createView(),
                'delete_form' => $form->createView()
            )
        );
        //return $this->render('MicrocreditosBundle:Credito:creditoShow.html.twig', array( 'entity' => $credito ));
    }

    public function refinanciarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $credito = $em->getRepository('App:Credito')->find($id);
        $totalDeuda = $this->getDeuda($credito);
        $partial = $this->renderView(
            'Credito/_partial_refinanciar.html.twig',
            array('credito' => $id, 'deuda' => $totalDeuda['deuda'], 'punitorios' => $totalDeuda['punitorios'])
        );
        return new Response($partial);
    }

    public function incobrableAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $credito = $em->getRepository('App:Credito')->find($id);

        $partial = $this->renderView(
            'Credito/_partial_incobrable.html.twig',
            array('credito' => $credito)
        );
        return new Response($partial);
    }

    public function aplicarIncobrableAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $credito = $em->getRepository('App:Credito')->find($id);
        if ($credito->getIncobrable()) {
            $credito->setIncobrable(0);
            $credito->setFechaIncobrable(NULL);
        } else {
            $credito->setIncobrable(1);
            $credito->setFechaIncobrable(new \Datetime);
        }
        $em->persist($credito);
        $em->flush();
        return $this->redirect($this->generateUrl('credito_show', array('id' => $id)));
    }

    public function getSimulacionRefinanciacionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('idCredito');
        $punitorio = $request->get('punitorios');
        $cantCuotas = $request->get('getCantidadCuotaCredito');
        $credito = $em->getRepository('App:Credito')->find($id);

        $simulacionCuotas = array();
        $totalDeuda = $this->getDeuda($credito);
        $totCapital = $totalDeuda['capital'] / $cantCuotas;
        $totInteres = $totalDeuda['interes'] / $cantCuotas;
        $totGasto = $totalDeuda['gasto'] / $cantCuotas;
        $totPunitorios = $punitorio / $cantCuotas;
        $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = $montoPunitorios = 0;
        //composicion de cuota
        foreach ($credito->getEmprendedores() as $credEmp) {
            $porcMonto = ($credEmp->getMontoCredito() * 100) / $credito->getMontoCredito();
            $capital = ($totCapital * $porcMonto) / 100;
            $montoCapital += $capital;
            $interes = ($totInteres * $porcMonto) / 100;
            $gasto = ($totGasto * $porcMonto) / 100;
            $punitorios = ($totPunitorios * $porcMonto) / 100;
            $total = ceil($capital + $interes + $gasto + $punitorios);
            $redondeo = $total - ($capital + $interes + $gasto + $punitorios);
            $gastos = $gasto + $redondeo;
            $montoPunitorios += $punitorios;
            $gastoAdm += $gastos;
            $montoIntereses += $interes;
            $montoTotal += $total;
            $simCuota = array(
                'emprendedor' => $credEmp->getEmprendedor(),
                'montoCapital' => $capital,
                'montoIntereses' => $interes,
                'gastoAdministrativo' => $gastos,
                'montoPunitorios' => $punitorios,
                'montoTotal' => $total
            );
            array_push($simulacionCuotas, $simCuota);
        }
        $cuota = array(
            'montoCapital' => $montoCapital,
            'montoIntereses' => $montoIntereses,
            'gastoAdministrativo' => $gastoAdm,
            'montoPunitorios' => $montoPunitorios,
            'montoTotal' => $montoTotal
        );
        $partial = $this->renderView(
            'Credito/simulacion_refinanciacion_cuota.html.twig',
            array('simulacionCuotas' => $simulacionCuotas, 'cuota' => $cuota)
        );

        return new Response(json_encode($partial));
    }

    public function aplicarRefinanciacionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('idCredito');
        $credito = $em->getRepository('App:Credito')->find($id);

        $em->getConnection()->beginTransaction();
        $credito->setNroRenovacion($credito->getNroRenovacion() + 1);

        $refin = new CreditoRefinanciacion();
        $refin->setCredito($credito);
        $refin->setCantCuotas($request->get('getCantidadCuotaCredito'));
        $refin->setFechaRefinanciacion(new \Datetime);
        $refin->setMontoRefinanciado($request->get('total'));
        $refin->setPunitorios($request->get('punitorios'));
        $refin->setPeriodo($request->get('periodicidad'));
        $refin->setNuevoVencimiento(new \DateTime($request->get('nuevoVencimiento')));
        $em->persist($refin);
        $detalleDeuda = $this->getDeuda($credito);
        $primerCuota = 0;
        //$credito->getCantCuotas()+1;
        /*eliminar cuotas anteriores*/
        foreach ($credito->getCuotas() as $cuota) {
            if ($cuota->getSaldo() > 0) {
                if ($cuota->getPagado() > 0) {
                    /*actualizar la cuota con los datos de los pagos*/
                    $pcap = $pint = $pgas = $ppun = $ptot = 0;
                    foreach ($cuota->getPagos() as $pago) {
                        $pcap += $pago->getCapital();
                        $pint += $pago->getIntereses();
                        $pgas += $pago->getGastoAdministrativo();
                        $ppun += $pago->getPunitorios();
                        $ptot += $pago->getTotal();
                    }
                    $cuota->setMontoCapital($pcap);
                    $cuota->setMontoIntereses($pint);
                    $cuota->setGastoAdministrativo($pgas);
                    $cuota->setMontoPunitorios($ppun);
                    $cuota->setMontoTotal($ptot - $ppun);
                    $cuota->setPagado($ptot);
                    $cuota->setSaldo(0);
                    $cuota->setCancelado(1);
                    $em->persist($cuota);
                } else {
                    if ($primerCuota == 0)
                        $primerCuota = $cuota->getNroCuota();
                    $em->remove($cuota);
                }
            }
        }

        $em->getRepository('App:Cuota')->delCuotasImpagasByCredito($credito);
        $em->persist($credito);
        $em->flush();
        /*generar nuevas cuotas*/
        $periodo = 86400 * $refin->getPeriodo();
        $fechaVencimiento = strtotime($refin->getNuevoVencimiento()->format('d-m-Y'));
        $nroCuotaNueva = $primerCuota;
        $cantCuotas = $refin->getCantCuotas();
        $totCapital = $detalleDeuda['capital'] / $cantCuotas;
        $totInteres = $detalleDeuda['interes'] / $cantCuotas;
        $totGasto = $detalleDeuda['gasto'] / $cantCuotas;
        $totPunitorios = $refin->getPunitorios() / $cantCuotas;
        $montoCapital = $montoIntereses = $gastoAdm = $montoTotal = $montoPunitorios = $subTotal = 0;
        //composicion de cuota
        foreach ($credito->getEmprendedores() as $credEmp) {
            $porcMonto = ($credEmp->getMontoCredito() * 100) / $credito->getMontoCredito();
            $capital = ($totCapital * $porcMonto) / 100;
            $montoCapital += $capital;
            $interes = ($totInteres * $porcMonto) / 100;
            $gasto = ($totGasto * $porcMonto) / 100;
            $punitorios = ($totPunitorios * $porcMonto) / 100;
            $total = ceil($capital + $interes + $gasto + $punitorios);
            $redondeo = $total - ($capital + $interes + $gasto + $punitorios);
            $gastos = $gasto + $redondeo;
            $subTotal += ($capital + $interes + $gastos);
            $montoPunitorios += $punitorios;
            $gastoAdm += $gastos;
            $montoIntereses += $interes;
            $montoTotal += $total;
            /* Actualizar valor de cuota en emprendedores */
            $credEmp->setCuotaRefinanciada($total);
            $em->persist($credEmp);
        }
        for ($i = 1; $i <= $refin->getCantCuotas(); $i++) {
            $cuota = new Cuota();
            $cuota->setCredito($credito);
            $cuota->setNroCuota($nroCuotaNueva++);
            $cuota->setMontoCapital($montoCapital);
            $cuota->setMontoIntereses($montoIntereses);
            $cuota->setGastoAdministrativo($gastoAdm);
            $cuota->setSaldoPunitorios($montoPunitorios);
            $cuota->setMontoTotal($subTotal);
            $cuota->setSaldo($montoTotal);
            $cuota->setRefinanciada(1);
            $cuota->setFechaVencimiento(new \DateTime(date('d-m-Y', $fechaVencimiento)));
            $credito->addCuota($cuota);
            $fechaVencimiento = $fechaVencimiento + $periodo;
        }
        $refin->setMontoRefinanciado($montoTotal * $refin->getCantCuotas());
        $em->persist($refin);

        $em->persist($credito);
        $em->flush();
        $em->getConnection()->commit();

        /*  $j = 1;
        foreach ($credito->getCuotas() as $cuota) {            
            $cuota->setNroCuota($j++);
            //var_dump($cuota->getNroCuota());
        }          
        $em->persist($credito);
        $em->flush();
        */
        return $this->redirect($this->generateUrl('credito_show', array('id' => $id)));
    }

    /**
     *  Modificacion de fechas de vencimiento
     */
    public function modificarVencimientosAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $refin = $em->getRepository('App:CreditoRefinanciacion')->find($id);
        $partial = $this->renderView(
            'Credito/_partial_modifvtos.html.twig',
            array('refinanc' => $refin)
        );
        return new Response($partial);
    }

    public function aplicarModifVencimientosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $refinId = $request->get('refinId');
        $nuevoVto = new \DateTime($request->get('nuevoVencimiento'));
        $cuotaDesde = $request->get('cuotaDesde');


        $refin = $em->getRepository('App:CreditoRefinanciacion')->find($refinId);
        $periodo = 86400 * $refin->getPeriodo();
        $fechaVencimiento = strtotime($nuevoVto->format('d-m-Y'));
        $cuotas = $em->getRepository('App:Cuota')->findByCredito($refin->getCredito()->getId());
        foreach ($cuotas as $cuota) {
            if ($cuota->getNroCuota() >= $cuotaDesde) {
                $cuota->setFechaVencimiento(new \DateTime(date('d-m-Y', $fechaVencimiento)));
                $fechaVencimiento = $fechaVencimiento + $periodo;
                $em->persist($cuota);
            }
        }
        $em->flush();
        return $this->redirect($this->generateUrl('credito_show', array('id' => $refin->getCredito()->getId())));
    }

    private function getDeuda($credito)
    {
        $tasa = $credito->getPrograma()->getPorcPunitorio() / 30;
        $montoPun = $credito->getPrograma()->getMontoPunitorio();
        $fecha = new \Datetime;
        $fechaHoy = $fecha->format('d-m-Y');
        $deuda = $capital = $interes = $gasto = $punitorios = 0;
        foreach ($credito->getCuotas() as $cuota) {
            if ($cuota->getSaldo() > 0) {
                $deuda += $cuota->getSaldo();
                if ($cuota->getPagado() > 0) {
                    /*con pago parcial*/
                    $porcCapital = ($cuota->getMontoCapital() * 100) / $cuota->getMontoTotal();
                    $capital += round(($cuota->getSaldo() * $porcCapital) / 100, 2);

                    $porcInteres = ($cuota->getMontoIntereses() * 100) / $cuota->getMontoTotal();
                    $interes += round(($cuota->getSaldo() * $porcInteres) / 100, 2);

                    $porcGasto = ($cuota->getGastoAdministrativo() * 100) / $cuota->getMontoTotal();
                    $gasto += round(($cuota->getSaldo() * $porcGasto) / 100, 2);
                    $fechaCuota = ($cuota->getFechaVencimiento() > $cuota->getFechaCancelacion())
                        ? $cuota->getFechaVencimiento() : $cuota->getFechaCancelacion();
                } else {
                    /*cuota impaga*/
                    $capital += $cuota->getMontoCapital();
                    $interes += $cuota->getMontoIntereses();
                    $gasto += $cuota->getGastoAdministrativo();
                    $fechaCuota = $cuota->getFechaVencimiento();
                }
                $dias = DefaultController::dateDiff($fechaCuota->format('d-m-Y'), $fechaHoy);
                if ($dias > 0) {
                    if ($montoPun > 0)
                        $punitorios += ($montoPun * $dias);
                    else
                        $punitorios += (round((($cuota->getSaldo() * $tasa / 100) * $dias)));
                }
            }
        }
        return array('deuda' => $deuda, 'capital' => $capital, 'interes' => $interes, 'gasto' => $gasto, 'punitorios' => $punitorios);
    }

    public function IndexExportacion()
    {


        return $this->render('Credito/credito_pago_exportacion.html.twig');
    }

    public function creditoAnalyzer()
    {

        $em = $this->getDoctrine()->getManager();

        // the $entity it's actually never used. 
        $entity = new Credito();

        // Create form 
        $credFile = new CreditoFile();
        $form = $this->createForm(CreditoFileAnalyzerType::class, $credFile);

        // used when the user submits a form
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            if ($file) {
                /* ----------------- REFACTOR -------------- */
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileNameWithExtension = $originalFilename . '.' . $file->guessExtension();

                // Move the file and persist it. 
                try {
                    $file->move(
                        $this->getParameter('kernel.project_dir') . '/public/uploads/',
                        $fileNameWithExtension
                    );
                } catch (FileException $e) {
                    // If error, probabbly the uploads/ folder does not have the permissions for writing. 
                    // try running "chmod 777 /var/www/html/public/uploads" 
                    echo ($e);
                }
                /* ---------------------------------------- */

                // Lets analyze the file! 
                $cuotasEmisores = $credFile->getCuotasEmisores($fileNameWithExtension, $em);

                // Delete all files on uploads
                $folderPath = $this->getParameter('kernel.project_dir') . '/public/uploads/';
                $files = glob($folderPath . '*'); // if error, probabbly the 'uploads' fulder does not exist.
                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }


                return $this->render('Credito/creditoAnalyzer.html.twig', [
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'cuotasEmisores' => $cuotasEmisores
                ]);


            }

            // return $this->redirectToRoute('task_success');
        }

        return $this->render('Credito/creditoAnalyzer.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
}