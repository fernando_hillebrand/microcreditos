<?php

namespace App\Controller;

use App\Entity\Credito;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\TablaFeriados;
use App\Form\FeriadoType;

/**
 * Gastos controller.
 *
 */
class FeriadosController extends Controller
{
    /**
     * Lists all Gastos entities.
     *
     */
    public function indexAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:TablaFeriados')->findAll();

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        $vars = array(  
            'entityName'   => 'TablaFeriados',
            'entities'     => $entities,
            'paginator'    => $paginator);
       
        return $this->render('Feriados/index.html.twig', $vars );
    }

     /**
     * Displays a form to create a new Gastos entity.
     *
     */
    public function newAction()
    {
        $entity = new TablaFeriados();
        $form   = $this->createForm(FeriadoType::class, $entity);

        return $this->render('Feriados/form.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }
    /**
     * Creates a new Gastos entity.
     *
     */
    public function createAction()
    {
        $entity  = new TablaFeriados();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(FeriadoType::class, $entity);
        $session = $request->getSession();
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
                $entity->setCreadoPor($usuario);
                $entity->setActualizadoPor($usuario);
                $entity->setCreatedBy($usuario);
                $entity->setUpdatedBy($usuario);
                $entity->setFechaCreacion(new \DateTime( ));
                $entity->setFechaActualizacion(new \DateTime( ));
                $entity->setCreated(new \DateTime( ));
                $entity->setUpdated(new \DateTime( ));
                $em->persist($entity);
                $em->flush();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('feriadios_edit', array('id' => $entity->getId())));

            }
        }
        return $this->render('Feriados/form.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));               
    }

    /**
     * Displays a form to edit an existing Gastos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:TablaFeriados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Gasto que está buscando.');
        }

        $editForm = $this->createForm(FeriadoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Feriados/form.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Gastos entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:TablaFeriados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Gasto que está buscando.');
        }

        $editForm   = $this->createForm(FeriadoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $usuario = $em->getRepository('App:User')->find($this->get('security.token_storage')->getToken()->getUser()->getId());
                $entity->setActualizadoPor($usuario);
                $entity->setUpdatedBy($usuario);
                $entity->setFechaActualizacion(new \DateTime( ));
                $entity->setUpdated(new \DateTime( ));
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('feriadios_edit', array('id' => $id)));
            }
        }

        return $this->render('Feriados/form.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Gastos entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:TablaFeriados')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Feriado entity.');
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('feriadios'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
