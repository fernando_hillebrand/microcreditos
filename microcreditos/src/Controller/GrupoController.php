<?php
namespace App\Controller;

use App\Entity\GruposEmprendedores;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Controller\CreditoController;
use App\Entity\Grupo;
use App\Form\GrupoType;

/**
 * Grupo controller.
 *
 */
class GrupoController extends Controller
{
    /**
     * Lists all Grupo entities.
     *
     */
    public function indexAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_grupo');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','grupo'=>'','grupoId'=>'','desde'=>'','hasta'=>'','itemsperpage'=>20);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'  =>$request->get('_programa'),
                    'grupo'     =>$request->get('_grupo'),
                    'grupoId'   =>$request->get('_grupoId'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'itemsperpage' => 20
                    );
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'  =>$sessionFiltro['programa'],
                    'grupo'     =>$sessionFiltro['grupo'],
                    'grupoId'   =>$sessionFiltro['grupoId'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'itemsperpage' => 20
                );
                break;
        }
                   
        $session->set('filtro_grupo',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $varItems = ($filtro['itemsperpage']==0) ? 1000 : $filtro['itemsperpage'];
        $query =  $em->getRepository('App:Grupo')
            ->findByMyFiltroDQL($session->get('filtro_grupo'));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        foreach ($entities as $grupo) {
            CreditoController::setUltimaCuota($grupo->getCreditos(), $em);
        }

        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $vars = array(  
            'entityName'   => 'grupo',
            'filtro'       => $session->get('filtro_grupo'),
            'programas'    => $programas, 
            'entities'     => $entities,
            'paginator'    => $paginator);
       
        return $this->render('Grupo/grupoIndex.html.twig', $vars );
    }

    /**
     * Finds and displays a Grupo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Grupo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Grupo que está buscando.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(GrupoType::class, $entity);

        return $this->render('Grupo/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'form'   => $editForm->createView()

        ));
    }

    /**
     * Displays a form to create a new Grupo entity.
     *
     */
    public function newAction()
    {
        $entity = new Grupo();
        $em = $this->getDoctrine()->getManager();
        $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Grupo');
        $nro = $ultimo->getNumerico()+1;
        $codigo = $ultimo->getDescripcion().str_pad($nro,3,"0",STR_PAD_LEFT);
        $entity->setCodigo($codigo);
        $form   = $this->createForm(GrupoType::class, $entity);

        return $this->render('Grupo/grupoEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Grupo entity.
     *
     */
    public function createAction()
    {
        $entity  = new Grupo();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(GrupoType::class, $entity);

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Grupo');
                $nro = $ultimo->getNumerico()+1;
                $codigo = $ultimo->getDescripcion().str_pad($nro,3,"0",STR_PAD_LEFT);
                $ultimo->setNumerico($nro);
                $entity->setCodigo($codigo);
                $em->persist($entity);
                $em->persist($ultimo);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success','El Grupo se ha creado con éxito.');
                return $this->redirect($this->generateUrl('grupo_edit', array('id' => $entity->getId())));

            }
        }

        return $this->render('Grupo/grupoEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Grupo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:Grupo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Grupo que está buscando.');
        }

        $puedeModificarse = true;
        foreach($entity->getCreditos() as $credito){
            if($credito->getAprobado()){
                if(DefaultController::controlaCredititoActivo($credito->getStatus()) ==  false){
                    $puedeModificarse= false;
                }
            }
        }

        if($puedeModificarse == false){
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $session = $request->getSession();
            $session->getFlashBag()->set('error','No se pudo editar el Grupo por que tiene Creditos entregados');
            return $this->redirect($this->generateUrl('grupo_show', array('id' => $id)));
        }

       // $page = pathinfo($this->getRequest()->headers->get('referer'),PATHINFO_BASENAME);
        $editForm = $this->createForm(GrupoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Grupo/grupoEdit.html.twig', array(
            'entity'      => $entity,
            //'page'   => (strpos($page, 'page'))?$page:'', 
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Grupo entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Grupo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Grupo que está buscando.');
        }

        $editForm   = $this->createForm(GrupoType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success','Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('grupo_edit', array('id' => $id)));
            }
        }

        return $this->render('Grupo/grupoEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Grupo entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Grupo')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Grupo que está buscando.');
                }

                if(count($entity->getCreditos()) > 0){
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error','No se pudo borrar el Grupo por que tiene Creditos entregados');
                    return $this->redirect($this->generateUrl('grupo_edit', array('id' => $id)));
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('grupo'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
/* Suggest de grupos */
    public function getGrupoSuggestAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $term = $request->get('term');
        $em = $this->getDoctrine()->getManager();
        $programa = $em->getRepository('App:Programa')->find($request->get('programa'));
        $items = $em->getRepository('App:Grupo')->findAllByTerm($term,$programa);

        return new Response(json_encode($items)); 
    }
    
    public function getopListAction(){
        $partial = $this->renderView('Grupo/opList.html.twig');
        return new Response($partial);  
    }
    
    public function getDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $option = $request->get('option');
        $format = $request->get('format');
        $sessionFiltro = $session->get('filtro_grupo');
        $programa = $em->getRepository('App:Programa')->find($sessionFiltro['programa']);
        $grupos = $em->getRepository('App:Grupo')->findByMyFiltroDQL($sessionFiltro)->getResult();
        foreach ($grupos as $grupo) {
          CreditoController::setUltimaCuota($grupo->getCreditos(), $em);
        }
        if($format=='PDF'){
            $html = $this->renderView('Grupo/listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$grupos));
            $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="file.pdf"' )
                         ); 
        }else{
            $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Grupos.xls');
            $html= $this->renderView('Grupo/listXls.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$grupos));
            $response->setContent($html);
        }
       /*return $this->render('MicrocreditosBundle:Grupo:listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$emprendedores));*/
        return $response; 
    }
    
    public function addEmprendedorAction(){
        $msg = $partial = '';
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $empId = $request->get('empId');
        $grupoId = $request->get('grupoId');
        $em = $this->getDoctrine()->getManager();
        $emprendedor = $em->getRepository('App:Emprendedor')->findOneById($empId);
        $grupo = $em->getRepository('App:Grupo')->findOneById($grupoId);
        $creditosEmprendedor = $emprendedor->getCreditos();
        $gruposEmprendedor = $emprendedor->getGrupo();


        $permiteAgregarGrupo = true;
        foreach( $creditosEmprendedor as $creditoEmprendedor){
            $credito = $creditoEmprendedor->getCredito();
            if(DefaultController::controlaCredititoActivo($credito->getStatus()) ==  false){
                $permiteAgregarGrupo = false;
            }
        }
        if(!$permiteAgregarGrupo) {
            $msg = 'No se puede agregar el Empredendor, por que el Emprendedor cuenta con un credito activo';
            return new Response(json_encode(array('msg'=>$msg, 'partial'=>$partial)));
        }

        $permiteAgregarGrupo = true;
        foreach($gruposEmprendedor as $grupoPertenece){
            $creditosGrupos = $grupoPertenece->getCreditos();
            foreach( $creditosGrupos as $credito) {
                if(DefaultController::controlaCredititoActivo($credito->getStatus()) ==  false){
                    $permiteAgregarGrupo = false;
                }
            }
        }
        if(!$permiteAgregarGrupo) {
            $msg = 'No se puede agregar el Empredendor al Grupo, por que el Grupo cuenta con un credito activo';
            return new Response(json_encode(array('msg'=>$msg, 'partial'=>$partial)));
        }
        $fecha = new \DateTime();

        $grupoEmpredor = new GruposEmprendedores();
        $grupoEmpredor->setGrupo($grupo);
        $grupoEmpredor->setFechaCreacion($fecha);
        $grupoEmpredor->setFechaActualizacion($fecha);
        $grupoEmpredor->setCreadoPor($this->get('security.token_storage')->getToken()->getUser());
        $grupoEmpredor->setActualizadoPor($this->get('security.token_storage')->getToken()->getUser());
        $grupoEmpredor->setEmprendedor($emprendedor);
        $em->persist($grupoEmpredor);
        $em->flush();

        $partial = $this->renderView('Grupo/_partial_emprendedor.html.twig', array('entity'=>$grupo));


        return new Response(json_encode(array('msg'=>$msg, 'partial'=>$partial)));
    }
    
    function removeEmprendedorAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $empId = $request->get('empId');
        $grupoId = $request->get('grupoId');
        $em = $this->getDoctrine()->getManager();
        $partial ='';
        $grupo = $em->getRepository('App:Grupo')->find($grupoId);
        if($empId){

            $emprendedor = $em->getRepository('App:GruposEmprendedores')->find($empId);


            $puedeModificarse = true;
            foreach($grupo->getCreditos() as $credito){
                if($credito->getAprobado()){
                    if($credito->getStatus() == 'Solicitud'){
                        $credito->setActivo(false);
                    }

                    if(DefaultController::controlaCredititoActivo($credito->getStatus())==  false){
                        $puedeModificarse = false;
                    }
                }
            }

            if(!$puedeModificarse) {
                $msg = 'No se puede eliminar el Empredendor al Grupo, por que el Grupo cuenta con un credito dado';
                return new Response(json_encode(array('msg'=>$msg, 'partial'=>$partial)));
            }

            $grupoEmprendedor = $em->getRepository('App:GruposEmprendedores')->findOneBy([
                'grupo' => $grupoId,
                'emprendedor' => $empId,
            ]);

            $em->remove($grupoEmprendedor);
            $em->flush();

        }
        $grupo = $em->getRepository('App:Grupo')->find($grupoId);
        $partial = $this->renderView('Grupo/_partial_emprendedor.html.twig',
                array('entity'=>$grupo));
        return new Response(json_encode(array('msg'=>'OK', 'partial'=>$partial)));
    }
}