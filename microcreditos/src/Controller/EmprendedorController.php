<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\Emprendedor;
use App\Form\EmprendedorType;

/**
 * Emprendedor controller.
 *
 */
class EmprendedorController extends Controller
{
    /**
     * Lists all Emprendedor entities.
     *
     */
    public function indexAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_emprendedor');
        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('programa'=>'','grupo'=>'','grupoId'=>'','emprendedor'=>'','emprendedorId'=>'','desde'=>'','hasta'=>'','itemsperpage' => 20);
                break;
            case 'buscar':
                $filtro = array(
                    'programa'      =>$request->get('_programa'),
                    'grupo'         =>$request->get('_grupo'),
                    'grupoId'       =>$request->get('_grupoId'),
                    'emprendedor'   =>$request->get('_emprendedor'),
                    'emprendedorId' =>$request->get('_emprendedorId'),
                    'desde'         =>$request->get('_desde'),
                    'hasta'         =>$request->get('_hasta'),
                    'itemsperpage' => 20);
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'programa'      =>$sessionFiltro['programa'],
                    'grupo'         =>$sessionFiltro['grupo'],
                    'grupoId'       =>$sessionFiltro['grupoId'],
                    'emprendedor'   =>$sessionFiltro['emprendedor'],
                    'emprendedorId' =>$sessionFiltro['emprendedorId'],
                    'desde'         =>$sessionFiltro['desde'],
                    'hasta'         =>$sessionFiltro['hasta'],
                    'itemsperpage' => 20);
                break;
        }
                   
        $session->set('filtro_emprendedor',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $varItems = ($filtro['itemsperpage']==0) ? 1000 : $filtro['itemsperpage'];
        $query =  $em->getRepository('App:Emprendedor')
            ->findByMyFiltroDQL($session->get('filtro_emprendedor'));

        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $programas = $em->getRepository('App:Programa')->findByActivosDQL();
        $vars = array(  
            'entityName'   => 'emprendedor',
            'filtro'       => $session->get('filtro_emprendedor'),
            'entities'     => $entities,
            'paginator'    => $paginator,
            'programas'    => $programas);
       
        return $this->render('Emprendedor/emprendedorIndex.html.twig', $vars );
    }    

    /**
     * Finds and displays a Emprendedor entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Emprendedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Emprendedor que está buscando.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm( EmprendedorType::class, $entity);


        return $this->render('Emprendedor/show.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Emprendedor entity.
     *
     */
    public function newAction()
    {
        $entity = new Emprendedor();
        $em = $this->getDoctrine()->getManager();
        $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Emprendedor');
        $nro = $ultimo->getNumerico()+1;
        $codigo = $ultimo->getDescripcion().str_pad($nro,4,"0",STR_PAD_LEFT);
        $entity->setCodigo($codigo);
        $form   = $this->createForm( EmprendedorType::class, $entity);
        return $this->render('Emprendedor/emprendedorEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Emprendedor entity.
     *
     */
    public function createAction()
    {
        $entity  = new Emprendedor();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm( EmprendedorType::class, $entity);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $requestData = $request->get($form->getName());
            $form->submit($requestData);

            if( $em->getRepository('App:Emprendedor')->checkUniqueDNI($requestData['dni']) == 0){

                if ($form->isValid()) {

                    $ultimo = $em->getRepository('App:Parametro')->getUltimoNroDQL('Emprendedor');
                    $nro = $ultimo->getNumerico() + 1;
                    $codigo = $ultimo->getDescripcion() . str_pad($nro, 4, "0", STR_PAD_LEFT);
                    $ultimo->setNumerico($nro);
                    $entity->setCodigo($codigo);
                    $em->persist($entity);
                    $em->persist($ultimo);
                    $em->flush();
                    $session = $request->getSession();
                    $session->getFlashBag()->set('success', 'El Emprendedor se ha creado con éxito');
                    return $this->redirect($this->generateUrl('emprendedor_edit', array('id' => $entity->getId())));
                }
            }else{
                $session = $request->getSession();
                $session->getFlashBag()->set('error', 'Ya existe un Emprendedor con ese DNI');
            }
        }

        return $this->render('Emprendedor/emprendedorEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Emprendedor entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Emprendedor')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('No existe el Emprendedor que está buscando.');
        }

        $request = $this->container->get('request_stack')->getCurrentRequest();
        //controla si tiene algun credito dado
        $creditos = $entity->getCreditos();
        //if(count($creditos) > 0){
        //    $session = $request->getSession();
        //    $session->getFlashBag()->set('error','No se pudo Editar el Emprendedor por que tiene Creditos entregados');
        //    return $this->redirect($this->generateUrl('emprendedor_show', array('id' => $id)));
        //}

//        $grupo = $entity->getGrupo();
//        if(count($grupo->getCreditos()) > 0){
//            $session = $request->getSession();
//            $session->getFlashBag()->set('error','No se pudo Editar el Emprendedor por que tiene Creditos entregado en el grupo');
//            return $this->redirect($this->generateUrl('emprendedor_show', array('id' => $id)));
//        }

        $editForm = $this->createForm( EmprendedorType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('Emprendedor/emprendedorEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Edits an existing Emprendedor entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Emprendedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Emprendedor que está buscando.');
        }

        $editForm   = $this->createForm( EmprendedorType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $requestData = $request->get($editForm->getName());
            $editForm->submit($requestData);
            //var_dump($em->getRepository('App:Emprendedor')->checkUniqueDNI($requestData['dni'],$id));exit;
            if( $em->getRepository('App:Emprendedor')->checkUniqueDNI($requestData['dni'],$id) == 0) {
                if ($editForm->isValid()) {
                    $em->persist($entity);
                    $em->flush();
                    $session = $request->getSession();
                    $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                    return $this->redirect($this->generateUrl('emprendedor_edit', array('id' => $id)));
                }
            }else{
                $session = $request->getSession();
                $session->getFlashBag()->set('error', 'Ya existe un Emprendedor con ese DNI');
            }

        }

        return $this->render('Emprendedor/emprendedorEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function updateInfoAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Emprendedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Emprendedor que está buscando.');
        }

        $editForm   = $this->createForm( EmprendedorType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        $data = $request->get($editForm->getName());

        $entity->setAclaraciones($data['aclaraciones']);
        $em->persist($entity);
        $em->flush();
        $session = $request->getSession();
        $session->getFlashBag()->set('success','Los cambios se han guardado con éxito');
        return $this->redirect($this->generateUrl('emprendedor_edit', array('id' => $id)));

        return $this->render('Emprendedor/emprendedorEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a Emprendedor entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();


        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:Emprendedor')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Emprendedor que está buscando.');
                }
                
                //controla si tiene algun credito dado
                $creditos = $entity->getCreditos();
                if(count($creditos) > 0){
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error','No se pudo borrar el Emprendedor por que tiene Creditos entregados');
                    return $this->redirect($this->generateUrl('emprendedor_edit', array('id' => $id)));
                }

                $grupo = $entity->getGrupo();
                if(count($grupo->getCreditos()) > 0){
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error','No se pudo borrar el Emprendedor por que tiene Creditos entregado en el grupo');
                    return $this->redirect($this->generateUrl('emprendedor_edit', array('id' => $id)));
                }

                $em->remove($entity);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('emprendedor'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    /* Suggest */
    public function getEmprendedorSuggestAction(){
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $term = $request->get('term');

        $items = $em->getRepository('App:Emprendedor')->findAllByTerm($term);
        $array = array();
        if($items){
            foreach($items as $item){
                $nombre = $item['apellido'].', '.$item['nombres']. ' ('.$item['dni'].')';
                array_push($array,
                        array('id'=>$item['id'],'label'=> $nombre, 'dni'=>$item['dni'], 'cbu' =>$item['cbu']) );
            }
        }  
        return new Response(json_encode($array)); 
    }
    
    public function getopListAction(){
        $partial = $this->renderView('Emprendedor/opList.html.twig', array());
        return new Response($partial);  
    }
    
    public function getDataAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $option = $request->get('option');
        $format = $request->get('format');
        $sessionFiltro = $session->get('filtro_emprendedor');
        if(is_null($sessionFiltro['programa'])){
            $programa = null;
        }else {
            $programa = $em->getRepository('App:Programa')->find($sessionFiltro['programa']);
        }
        $emprendedores = $em->getRepository('App:Emprendedor')->findByMyFiltroDQL($sessionFiltro)->getResult();
        
        if($format=='PDF'){
            $html = $this->renderView('Emprendedor/listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$emprendedores));
            $response = new Response(
                        $this->get('knp_snappy.pdf')->getOutputFromHtml(utf8_decode($html),array('footer-right'=>"[page]/[toPage]",'footer-line'=>true)),
                        200,
                        array( 'Content-Type' => 'application/pdf',
                               'Content-Disposition' => 'attachment; filename="file.pdf"' )
                         ); 
        }else{
            $response = new Response();  
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=UTF-8');
            $response->headers->set('Content-Disposition', 'filename=Emprendedores.xls');
            $html= $this->renderView('Emprendedor/listXls.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$emprendedores));
            $response->setContent($html);
        }
       /*return $this->render('MicrocreditosBundle:Grupo:listPdf.html.twig',
                            array('option'=>$option, 'filtro'=>($sessionFiltro)?$sessionFiltro:'', 
                                  'programa'=>$programa, 'entities'=>$emprendedores));*/
        return $response;
    }    
    
    public function editarDatosAction(){
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $emprendedor = $em->getRepository('App:Emprendedor')->findOneById($id);
        $editForm   = $this->createForm(EmprendedorType::class, $emprendedor);
        $partial =
                $this->renderView('Emprendedor/_partial_datos_basicos.html.twig',
                                        array(
                                              'emprendedor'=>$emprendedor,
                                              'form'=>$editForm->createView()
                                        )
                                );
        return new Response($partial);
        
    }
    public function guardarDatosAction(){
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $id = $request->get('id');
        $form = $request->get('emprendedor');
        $emprendedor = $em->getRepository('App:Emprendedor')->find($id);

        $emprendedor->setApellido($form['apellido']);
        $emprendedor->setNombres($form['nombres']);
        $emprendedor->setDni($form['dni']);
        $emprendedor->setCbu($form['cbu']);
        $emprendedor->setDireccion($form['direccion']);
        $emprendedor->setUbicacion($form['ubicacion']);
        $emprendedor->setTelefono($form['telefono']);

        $em->persist($emprendedor);
        $em->flush();

        return new Response(json_encode(
            array('codigo'=>  $emprendedor->getCodigo(),
                  'nombre'=>$emprendedor->__toString(),
                  'dni'=>$emprendedor->getDni()))
        );
    }
}
