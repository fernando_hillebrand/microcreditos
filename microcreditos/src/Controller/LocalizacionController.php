<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\Parametro;
use App\Form\ParametroType;

/**
 * Localizacion controller.
 *
 */
class LocalizacionController extends Controller
{
    /**
     * Lists all Parametro entities.
     *
     */
    public function indexAction($agrupador = null, $padreId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $agrupadores = $em->getRepository('App:Parametro')->getAgrupadoresLocalizacion();
        if($agrupador == null ){
            //pais
            $agrupador = $agrupadores[0];
            $padres = null;
        }else{
            $agrupador = $em->getRepository('App:Parametro')->find($agrupador);
            if ($agrupador->getDescripcion() != 'pais') {
                //es provincia o localidad
                $padres = $em->getRepository('App:Parametro')->findPadres($agrupador->getPadre());
               /* if ($padreId) {
                    
                } else {
                    $padres
                }*/
            }
        }
        $paginator = $this->get('Ideup.simple_paginator');
        $entities  = $paginator->paginate($em->getRepository('App:Parametro')->findByAgrupadoryPadre($agrupador,$padreId))->getResult();

        return $this->render('MicrocreditosBundle:Parametro:localizacionIndex.html.twig', array(
                        'entityName'   => 'localizacion',
                        'agrupadorSlug'=> $agrupador->getDescripcion(), 
                        'entities'     => $entities,
                        'agrupadores'  => $agrupadores,
                        'padres'       => $padres,
                        'paginator'    => $paginator
        ));
    }

    /**
     * Finds and displays a Parametro entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MicrocreditosBundle:Parametro:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Parametro entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->find($this->getRequest()->get('agrupador'));

        $entity = new Parametro();
        $entity->setAgrupador($agrupador);
        
        $form   = $this->createForm(new ParametroType(), $entity);

        return $this->render('MicrocreditosBundle:Parametro:parametroEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Parametro entity.
     *
     */
    public function createAction()
    {
        $entity  = new Parametro();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(new ParametroType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $session = $request->getSession();
            $session->getFlashBag()->set('success','El nuevo parámetro se ha creado con éxito');
            return $this->redirect($this->generateUrl('parametro_edit', array('id' => $entity->getId())));
        }
        return $this->render('MicrocreditosBundle:Parametro:parametroEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Parametro entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm = $this->createForm(new ParametroType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MicrocreditosBundle:Parametro:parametroEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Parametro entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:Parametro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametro entity.');
        }

        $editForm   = $this->createForm(new ParametroType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $session = $request->getSession();
            $session->getFlashBag()->set('success','Los cambios se han guardado con éxito');
            return $this->redirect($this->generateUrl('parametro_edit', array('id' => $id)));
        }

        return $this->render('MicrocreditosBundle:Parametro:parametroEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Parametro entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('App:Parametro')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Parametro entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('parametro_back', array('agrupador' => $entity->getAgrupador()->getId())));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', Hidden::class)
            ->getForm()
        ;
    }
    
    public function localizacionByAgrupadorAction(){
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->find($this->getRequest()->get('agrupador'));

        $paginator = $this->get('Ideup.simple_paginator');
        $entities  = $paginator->paginate($em->getRepository('App:Parametro')->findByAgrupadorLocalizacion($agrupador))->getResult();

        $partialParametros = $this->renderView('MicrocreditosBundle:Parametro:localizacionList.html.twig', array(
            'entities'  =>  $entities,
            'entityName'   => 'localizacion',
            'paginator'    => $paginator
            ));
        return new Response(json_encode($partialParametros));  
    }


    /**
 * ACTIONS PARA LOCALIZACION
 */    
    /**
     * Lists all Parametro entities.
     *
     */
    public function localizacionIndexAction($agrupador = null)
    {
        $em = $this->getDoctrine()->getManager();
        $agrupadores = $em->getRepository('App:Parametro')->getAgrupadoresLocalizacion();
        if($agrupador == null ){
            $agrupador = $agrupadores[0];
        }else{
            $agrupador = $em->getRepository('App:Parametro')->find($agrupador);
        }
        $paginator = $this->get('Ideup.simple_paginator');
        $entities  = $paginator->paginate($em->getRepository('App:Parametro')->findByAgrupadorDQL($agrupador))->getResult();

        return $this->render('MicrocreditosBundle:Parametro:localizacionIndex.html.twig', array(
                        'entityName'   => 'parametro',
                        'agrupadorId'  => $agrupador->getId(), 
                        'entities'     => $entities,
                        'agrupadores'  => $agrupadores,
                        'paginator'    => $paginator
        ));        
    }
    
    /**
     * Displays a form to create a new Parametro entity.
     *
     */
    public function newLocalizacionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $agrupador = $em->getRepository('App:Parametro')->find($this->getRequest()->get('agrupador'));

        $entity = new Parametro();
        $entity->setAgrupador($agrupador);
        $entity->setBoleano(1);
        
        $form   = $this->createForm(new ParametroType(), $entity);
            return $this->render('MicrocreditosBundle:Parametro:localizacionEdit.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView()
            ));
    }    
}
