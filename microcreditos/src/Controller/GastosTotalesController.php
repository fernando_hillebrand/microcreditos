<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\GastosTotales;
use App\Form\GastosTotalesType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class GastosTotalesController extends Controller
{
    /**
     * @Route("/gastos/totales", name="app_gastos_totales")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();
        $sessionFiltro = $session->get('filtro_ingreso');

        switch ($request->get('_opFiltro')) {
            case 'limpiar':
                $filtro = array('concepto'=>'','desde'=>'','hasta'=>'','page' => 1);
                break;
            case 'buscar':
                $filtro = array(
                   
                    'concepto'  =>$request->get('_concepto'),
                    'desde'     =>$request->get('_desde'),
                    'hasta'     =>$request->get('_hasta'),
                    'page'      =>$request->get('page'));
                break;
            default:
                //desde paginacion, se usa session
                $filtro = array(
                    'concepto'  =>$sessionFiltro['concepto'],
                    'desde'     =>$sessionFiltro['desde'],
                    'hasta'     =>$sessionFiltro['hasta'],
                    'page'      => ( isset($sessionFiltro['page']) ? $sessionFiltro['page'] : 1 )
                    );
                break;
        }

        $session->set('filtro_ingreso',$filtro);
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $query = null;
        $query =  $em->getRepository('App:GastosTotales')->findByMyCriteriaDQL($session->get('filtro_ingreso'),'ingreso');

     
        $conceptos =  $em->getRepository('App:ConceptoGastos')->getAll();
        //dd($query);
        $entities = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('GastosTotales/index.html.twig', array(
                        'entityName'   => 'GastosTotales',
                        'entities'     => $entities,
                        'paginator'    => $paginator,
                        'filtro'       => $session->get('filtro_ingreso'),
                        'conceptos'    => $conceptos,
        ));
    }


    public function newAction()
    {
        $entity = new GastosTotales();
        $form   = $this->createForm(GastosTotalesType::class, $entity);

        return $this->render('GastosTotales/GastosTotalesEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        )); 
    }

    public function createAction()
    {
        $entity  = new GastosTotales();
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $form    = $this->createForm(GastosTotalesType::class, $entity);
        
        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El nuevo Gasto se ha creado con éxito');
                return $this->redirect($this->generateUrl('gastos_totales_index'));
            }
        }
        

        return $this->render('GastosTotales/GastosTotalesEdit.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }


    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:GastosTotales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el registro de gastos totales que está buscando.');
        }

        //dd($entity);

        $editForm = $this->createForm(GastosTotalesType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GastosTotales/GastosTotalesEdit.html.twig', array(
            'entity'      => $entity,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('App:GastosTotales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No existe el Gasto que está buscando.');
        }

        $editForm   = $this->createForm( GastosTotalesType::class, $entity);
        //$deleteForm = $this->createDeleteForm($id);

        $request = $this->container->get('request_stack')->getCurrentRequest();

     
        if ($request->isMethod('POST')) {
            $editForm->submit($request->get($editForm->getName()));
            if ($editForm->isValid()) {
                $em->persist($entity);
                $em->flush();
                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'Los cambios se han guardado con éxito');
                return $this->redirect($this->generateUrl('gastos_totales_index'));
            }
        }

        return $this->render('GastosTotales/GastosTotalesEdit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod('POST')) {
            $form->submit($request->get($form->getName()));
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('App:GastosTotales')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('No existe el Gasto total que está buscando.');
                }
                try {
                    $em->remove($entity);
                    $em->flush();
                } catch (\PDOException $e) {
                    $session = $request->getSession();
                    $session->getFlashBag()->set('error', 'El gasto no se puede eliminar');
                    //return $this->redirect($this->generateUrl('gastos_totales_edit', array('id' => $id)));
                    return $this->redirect($this->generateUrl('gastos_totales_index'));
                }

                $session = $request->getSession();
                $session->getFlashBag()->set('success', 'El gasto total se ha eliminado con éxito');
            }
        }
        return $this->redirect($this->generateUrl('gastos_totales_index'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
