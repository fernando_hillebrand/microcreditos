/*
 * Funciones Generales
 */
function isEmpty( inputStr ) { if ( null == inputStr || "" == inputStr ) { return true; } return false; }

function cambiarActivo(obj){
      var datos = obj.attr('id').split('-');
      var data = 'entidad='+datos[0]+'&id='+datos[1];
      var func = 'aplicarCambiarActivo(data,adic)';
      callAjax(obj.attr('url'),data,func,obj);
      /*
      $.ajax({
            url: url,
            data: 'entidad='+datos[0]+'&id='+datos[1],
            dataType: 'json',
            success: function(data){
                if(data.message=='OK'){
                    $('#loading').fadeOut();
                    obj.removeClass(obj.attr('class'));
                    obj.addClass('activo'+data.value);
                }   
            }
        }).fail(function() { 
            $('#loading').fadeOut();
            alert("No es posible cambiar el estado en este momento."); 
        });*/
}
function aplicarCambiarActivo(data,obj){
    if(data.message=='OK'){
        obj.removeClass(obj.attr('class'));
        obj.addClass('activo'+data.value);
    } 
}
function checkPrograma(obj){
    callAjax(obj.attr('url'),'id='+obj.val(),'returnCheckPrograma(data)');
}
function returnCheckPrograma(data){
    if (data){
        $('#grupoAutocomplete').attr('disabled','true');
        $("#grupoId").val('');
        $('#grupoAutocomplete').val('');
    }
    else {
        $('#grupoAutocomplete').removeAttr("disabled");
    }  
}
/*
 * Programa Ingreso
*/
/* Editar o Nuevo Ingreso */
function editProgramaIngreso(url,id){
     callAjax(url,'id='+id,'cargarEditProgramaIngreso(data)');
}
function cargarEditProgramaIngreso(data){
    if( data.id == null ){
        $("#ingresos").append('<tr id="tr_"></tr>');
        $("#tr_").append(data.partial);
    }else{
        $("#tr_"+data.id+" td").remove();
        $("#tr_"+data.id).append(data.partial);
    }
    mostrarActionsProgramaIngreso(false);
}
function aceptEditProgramaIngreso(url,id){
    var programa = $('#form_id').val();
    var monto = $('#microcreditos_microcreditosbundle_programaingresotype_monto').val();
    var fecha = $('#microcreditos_microcreditosbundle_programaingresotype_fecha_ingreso').val();
    var datos = 'programa='+programa+'&monto='+monto+'&fecha='+fecha+'&id='+id;
    callAjax(url,datos,'cargarUpdateProgramaIngreso(data)');
}
function cargarUpdateProgramaIngreso(data){
    if($("#tr_"+data.id).length == 0){
        //Nuevo
        $("#tr_").remove();
        $("#ingresos").append('<tr id="tr_'+data.id+'"></tr>');
        $("#tr_"+data.id).append(data.partial);
    }else{
        //Edicion
        $("#tr_"+data.id+" td").remove();
        $("#tr_"+data.id).append(data.partial);
    }
    mostrarActionsProgramaIngreso(true);   
    $('.align-right.total').html('Total: '+data.total);
}
/* Borrar Ingresos */
function deleteProgramaIngreso(url,id){
    if (confirm('Desea eliminar este elemento?'))
        callAjax(url,'id='+id,'aceptarDeleteProgramaIngreso(data)');
}
function aceptarDeleteProgramaIngreso(data){
    if(data.msg=="OK"){
        $('#tr_'+data.id).remove();
        mostrarActionsProgramaIngreso(true);
        $('.align-right.total').html('Total: '+data.total);
    }
}

function cancelProgramaIngreso(url,row){
    if(row){
        callAjax(url,'id='+row,'cargarEditProgramaIngreso(data)'); 
    }else
        $('#tr_'+row).remove();
    mostrarActionsProgramaIngreso(true);
}
function mostrarActionsProgramaIngreso(bool){
    if(bool){
        $('#programa_ingreso_new').show();
        $('.programa_ingreso_edit').show();
        $('.programa_ingreso_delete').show();
    }else{
        $('#programa_ingreso_new').hide();
        $('.programa_ingreso_edit').hide();
        $('.programa_ingreso_delete').hide();
    }
}


/*function aceptProgramaIngreso(url){
    var programa = $('#form_id').val();
    var monto = $('#microcreditos_microcreditosbundle_programaingresotype_monto').val();
    var fecha = $('#microcreditos_microcreditosbundle_programaingresotype_fecha_ingreso').val();
    callAjax(url,'programa='+programa+'&monto='+monto+'&fecha='+fecha,'cargarUpdateProgramaIngreso(data)');
}*/

/*function editProgramaIngreso(url,id){
    var monto = $('#microcreditos_microcreditosbundle_programaingresotype_monto').val();
    var fecha = $('#microcreditos_microcreditosbundle_programaingresotype_fecha_ingreso').val();
     callAjax(url,'id='+id+'&monto='+monto+'&fecha='+fecha,'cargarUpdateProgramaIngreso(data,paramAdic)',id);
}
function cargarEditProgramaIngreso(data,id){
    cancelProgramaIngreso(id);
    $("#ingresos").append(data);
}*/



/*
 * Parametros
 */
function newParametro(){
    agrupador = $('#select_agrupador').val();
    window.location.href='new?agrupador='+agrupador;
}
function cargarParametros(agrupador){
   // $('#loading').show();
    callAjax('parametrosByAgrupador','agrupador='+agrupador,'aplicarCargarParametros(data)');
   /* $.ajax({
        url: 'parametrosByAgrupador',
        data: 'agrupador='+agrupador,
        dataType: 'json',
        success: function(data){
            $('#parametrosTable thead').remove();
            $('#parametrosTable tbody').remove();
            $('#parametrosTable tfoot').remove();
            $('#parametrosTable').append(data );
            $('#loading').hide();
        }
    }).fail(function() { 
        $('#loading').hide();
        alert("No es posible cargar los datos en este momento."); 
    });*/
}
function aplicarCargarParametros(data){
    $('#parametrosTable thead').remove();
    $('#parametrosTable tbody').remove();
    $('#parametrosTable tfoot').remove();
    $('#parametrosTable').append(data );
}


/* llamada ajax */
function callAjax(url,varData,functionSuccess,adic){
    $('#loading').fadeIn();
    $.ajax({
        url: url,
        data: varData,
        async:false,
        dataType: 'json',
        success: function(data){
            eval(functionSuccess);
            $('#loading').fadeOut();
        }
    }).fail(function() { 
        $('#loading').fadeOut();
        alert("No es posible realizar esta operación en este momento.");
    });
}


/*
 * Localizacion
 */
function cargarLocalizacion(agrupador){
    $('#loading').show();
    $.ajax({
        url: 'localizacionByAgrupador',
        data: 'agrupador='+agrupador,
        dataType: 'json',
        success: function(data){
            $('#parametrosTable thead').remove();
            $('#parametrosTable tbody').remove();
            $('#parametrosTable tfoot').remove();
            $('#parametrosTable').append(data );
            $('#loading').hide();
        }
    }).fail(function() { 
        $('#loading').hide();
        alert("No es posible cargar los datos en este momento."); 
    });
}
function newLocalizacion(){
    agrupador = $('#select_agrupador').val();
    padre = $('#select_padre').val();
    window.location.href='new?agrupador='+agrupador+'&padre='+padre;
}
