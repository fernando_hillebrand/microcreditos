jQuery(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
        'Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: 'dd-mm-yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$(document).ready(function(){
    $('.rango').on('change',function(){
        if( $("#microcreditos_microcreditosbundle_tablagastostype_desde").val()!=='' && $("#microcreditos_microcreditosbundle_tablagastostype_hasta").val()!=='' ){            
            if( $("#microcreditos_microcreditosbundle_tablagastostype_desde").val()>$("#microcreditos_microcreditosbundle_tablagastostype_hasta").val() ){
                alert('El Importe Desde debe ser menor que el Hasta');
                
            }
        }else{
            
        }
            
        });
    
   // $.facebox.settings.opacity = 0.5;
    var date = new Date();
    $('#loading').hide();
  /*  $('.datepicker').datepicker({changeMonth: true,
        changeYear: true,
        minDate: new Date(1930, 0, 1)});   */    
    var dates = $( "#fecha_desde_filter, #fecha_hasta_filter" ).datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: date,
        showOptions: {
            direction: 'up'
        },
        onSelect: function( selectedDate ) {
            var option = this.id == "fecha_desde_filter" ? "minDate" : "maxDate",
            instance = $( this ).data( "datepicker" ),
            date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
        }
    }); 
    var dates = $( "#fechaCarga_desde_filter, #fechaCarga_hasta_filter" ).datepicker({
        changeMonth: true,
        changeYear: true,
        maxDate: date,
        showOptions: {
            direction: 'up'
        },
        onSelect: function( selectedDate ) {
            var option = this.id == "fechaCarga_desde_filter" ? "minDate" : "maxDate",
            instance = $( this ).data( "datepicker" ),
            date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
        }
    }); 
    //Control del menu
    var route = $("#route").val();
    // redireccionar cualquier ruta de parametros 
    //if( route.indexOf("parametro")>=0 ){  route='parametro';  }
    
    if($("#"+route).hasClass('hidden'))  $("#"+route).removeClass('hidden');
    $("#"+route).addClass("current");
    parent = $("#"+route).closest("ul");
    $("#"+route).closest("ul").siblings("a").addClass("current");
   
    //Sidebar Accordion Menu:
    $("#main-nav li ul").hide(); // Hide all sub menus
    $("#main-nav li a.current").parent().find("ul").slideToggle("slow"); // Slide down the current menu item's sub menu
    $("#main-nav li a.nav-top-item").click( // When a top menu item is clicked...
        function () {
            $(this).parent().siblings().find("ul").slideUp("normal"); // Slide up all sub menus except the one clicked
            $(this).next().slideToggle("normal"); // Slide down the clicked sub menu
            if($(this).hasClass('nolist')) return false;
        }
     );
            
    //confirm de botones delete
    $("[title=Eliminar]").click(
        function(){
            if (!confirm('Desea eliminar este elemento?'))
                return false;
            return true;
        }
        );
		
    $("#main-nav li a.no-submenu").click( // When a menu item with no sub menu is clicked...
        function () {
            window.location.href=(this.href); // Just open the link instead of a sub menu
            return false;
        }
        ); 

    // Sidebar Accordion Menu Hover Effect:
    $("#main-nav li .nav-top-item").hover(
        function () {
            $(this).stop().animate({  paddingRight: "25px"  }, 200);  
        }, 
        function () {
            $(this).stop().animate({  paddingRight: "15px"  });
        }
        );

    //Minimize Content Box
    //$(".content-box-header h3").css({  "cursor":"s-resize"  }); // Give the h3 in Content Box Header a different cursor
    $(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
    $(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"
    $(".content-box-header span.arrow").click( // When the h3 is clicked...
        function () {
            $(this).parent().next().toggle('slow'); // Toggle the Content Box
            $(this).parent().parent().toggleClass("closed-box"); // Toggle the class "closed-box" on the content box
            $(this).toggleClass("close-box"); // Toggle the class "closed-box" on the content box
            $(this).parent().find(".content-box-tabs").toggle(); // Toggle the tabs
        }
        );
    
    // Content box tabs:
    $('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
    $('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
    $('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"
    $('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
        function() { 
            $(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
            $(this).addClass('current'); // Add class "current" to clicked tab
            var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
            $(currentTab).siblings().hide(); // Hide all content divs
            $(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
            return false; 
        }
        );

    //Close button:
    $(".close").click(
        function () {
            $(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
                $(this).slideUp(400);
            });
            return false;
        }
        );

    // Alternating table rows:
    $('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    // Check all checkboxes when the one in a table head is checked:
    $('.check-all').click(
        function(){
            $(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));   
        }
        );

    // Initialise Facebox Modal window:
    $('a[rel*=modal]').facebox(); // Applies modal window to any link with attribute rel="modal"

    // Initialise jQuery WYSIWYG:
    //$(".wysiwyg").wysiwyg(); // Applies WYSIWYG editor to any textarea with the class "wysiwyg"

    $('#select_agrupador').change( function() {
        cargarParametros($(this).val());
    } );
    $('#addParametro').click( function() {
        newParametro();
    } );
/*    $('#agrupador_localizacion').change( function() {
        cargarLocalizacion($(this).val());
    } );*/
    $('#grupoAutocomplete').autocomplete({
        minLength: 2,
        source: function( request, response ) {
            var data = 'programa='+$('[id$="_programa"] option:selected').val()+'&term='+request.term;
            $.ajax({
                url: $('#grupoAutocomplete').attr('url'),
                data: data,
                dataType: 'json',
                success: function(data){
                    response(data);
                }
            });
        },
        search: function(event,ui){
            $("#grupoId").val('');
            $('[id$="type_grupo"]').val('');
           //* $("#microcreditos_microcreditosbundle_creditotype_grupo").val('');
        },
        select: function(event,ui) {
            //alert($('[id$="_programa"]').val());
            $("#grupoId").val(ui.item.id);
            $('[id$="type_grupo"]').val(ui.item.id);
           // $("#microcreditos_microcreditosbundle_emprendedortype_grupo").val(ui.item.id);
        }
    }); 
        
    $('#grupoAutocomplete').blur(function(){
        if( $("#grupoId").val()== '')
            $('#grupoAutocomplete').val('');
        if( $('[id$="type_grupo"]').val()== '')
            $('#grupoAutocomplete').val('');
    });    

    $('#emprendedorAutocomplete').autocomplete({
        minLength: 2,
        source: $('#emprendedorAutocomplete').attr('url'),
        search: function(event,ui){
            $("#emprendedorId").val('');
            $("#lbldni").html('');
        },
        select: function(event,ui) {
            $("#emprendedorId").val(ui.item.id);
            $("#lbldni").html(ui.item.dni);
        }
    }); 
        
    $('#emprendedorAutocomplete').blur(function(){
        if( $("#emprendedorId").val()== ''){
            $('#emprendedorAutocomplete').val('');
            $("#lbldni").html('');         }
    });    

    $('#opList').click(function(){
        jQuery.facebox({ ajax: $(this).attr('url') });
    });
    
    $('#select_programa').change(function(){
        $("#grupoId").val('');
        $('#grupoAutocomplete').val('');
        checkPrograma($(this));
    });
    $('#select_programa').change();
    
    
    $("#sol_tab3").click(function(){
        $(".fc-button-today").click();
        mostrarTurnos();
    });
    
});
$(document).bind('loading.facebox', function() {
    $(document).unbind('keydown.facebox');
    $('#facebox_overlay').unbind('click');
});
function mostrarTurnos(){
    $('#calendar').fullCalendar('removeEvents');
    var asesor = $("#select_asesor").val();
    var ini = $("#fecha_ini").val();
    var fin = $("#fecha_fin").val();
    $.ajax({
        url: 'turnos',
        data: 'asesor='+asesor+'&ini='+ini+'&fin='+fin,
        dataType: 'json',
        success: function(data){
            if( isEmpty($("#fecha_ini").val()) )  $("#fecha_ini").val(data.ini);
            if( isEmpty($("#fecha_fin").val()) )  $("#fecha_fin").val(data.fin);
            $('#calendar').fullCalendar( 'addEventSource', data.turnos );
        }
    });
}  