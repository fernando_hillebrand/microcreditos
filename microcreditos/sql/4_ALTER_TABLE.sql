ALTER TABLE programa
  ADD habilitado_nuevo_credito tinyint(1) DEFAULT 0 NOT NULL,
  ADD monto_tope DECIMAL(10,2) default NULL,
  ADD cuotas_tope_mensual INT(11) default NULL,
  ADD cuotas_mensuales tinyint(1) DEFAULT 0 NOT NULL,
  ADD cuotas_tope_semanales INT(11) default NULL,
  ADD cuotas_semanales tinyint(1) DEFAULT 0 NOT NULL,
  ADD utiliza_caja tinyint(1) DEFAULT 0 NOT NULL,
  ADD solo_empleados tinyint(1) DEFAULT 0 NOT NULL
;


CREATE TABLE caja
  (
    id INT AUTO_INCREMENT NOT NULL,
    programa_id INT DEFAULT NULL,
    gasto_id INT DEFAULT NULL,
    usuario_id INT DEFAULT NULL,
    concepto INT DEFAULT NULL,
    creado_por_id INT DEFAULT NULL,
    actualizado_por_id INT DEFAULT NULL,
    total NUMERIC(10, 2) NOT NULL,
    ingreso NUMERIC(10, 2) NOT NULL,
    egreso NUMERIC(10, 2) NOT NULL,
    fecha DATETIME NOT NULL,
    obserbacion varchar(255) DEFAULT NULL,
    activo TINYINT(1) NOT NULL,
    fecha_creacion DATETIME NOT NULL,
    fecha_actualizacion DATETIME NOT NULL,

    INDEX IDX_E465F405FD8A7328 (programa_id),
    INDEX IDX_E465F405E96CF38D (gasto_id),
    INDEX IDX_E465F405DB38439E (usuario_id),
    INDEX IDX_E465F405648388D0 (concepto),
    INDEX IDX_E465F405FE35D8C4 (creado_por_id),
    INDEX IDX_E465F405F6167A1C (actualizado_por_id),
    PRIMARY KEY(id)
  ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;

 ALTER TABLE caja ADD CONSTRAINT FK_E465F405FD8A7328 FOREIGN KEY (programa_id) REFERENCES programa (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405E96CF38D FOREIGN KEY (gasto_id) REFERENCES gastos (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405648388D0 FOREIGN KEY (concepto) REFERENCES parametro (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);


INSERT INTO `parametro` (`id`, `padre_id`, `agrupador_id`, `nombre`, `descripcion`, `numerico`, `boleano`, `activo`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(207,	NULL,	169,	'Ingreso de Caja',	'E',	0.00,	0,	1,	NULL,	NULL,	NULL,	NULL);

ALTER TABLE caja
  ADD obserbacion varchar(255) DEFAULT NULL;

ALTER TABLE caja
  ADD credito_id INT DEFAULT NULL,
  ADD pago_id INT DEFAULT NULL;

 ALTER TABLE caja ADD CONSTRAINT FK_E465F405PAGO FOREIGN KEY (pago_id) REFERENCES pago (id);
 ALTER TABLE caja ADD CONSTRAINT FK_E465F405CREDITO FOREIGN KEY (credito_id) REFERENCES credito (id);


INSERT INTO `parametro` (`padre_id`, `agrupador_id`, `nombre`, `descripcion`, `numerico`, `boleano`, `activo`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`)
VALUES (NULL, NULL, 'Automaticos del sistema', 'automaticos-sistema', NULL, '1', '1', NULL, NULL, NULL, NULL);

INSERT INTO `parametro` (`padre_id`, `agrupador_id`, `nombre`, `descripcion`, `numerico`, `boleano`, `activo`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`)
VALUES (NULL, '208', 'Crédito Entregado', NULL, NULL, '1', '1', NULL, NULL, NULL, NULL);

INSERT INTO `parametro` (`padre_id`, `agrupador_id`, `nombre`, `descripcion`, `numerico`, `boleano`, `activo`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`)
VALUES (NULL, '208', 'Cobro de Crédito', NULL, NULL, '1', '1', NULL, NULL, NULL, NULL);

INSERT INTO `programa` (`id`, `nombre`, `sigla`, `activo`, `usuario_id`, `fecha_alta`, `porc_interes`, `porc_punitorio`, `individual`, `caja`, `banco`, `mes`, `anio`, `monto_punitorio`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`, `habilitado_nuevo_credito`, `monto_tope`, `cuotas_tope_mensual`, `cuotas_mensuales`, `cuotas_tope_semanales`, `cuotas_semanales`, `utiliza_caja`, `solo_empleados`) VALUES
(7,	'MicroEmprendedores Grupales',	'MEG',	1,	NULL,	'2020-05-23 18:02:04',	24.00,	10.00,	0,	0.00,	0.00,	5,	2020,	70.00,	NULL,	NULL,	NULL,	NULL,	1,	25000.00,	NULL,	0,	24,	1,	1,	0),
(8,	'MicroEmprendedores Individuales',	'MEI',	1,	NULL,	'2020-06-04 14:28:04',	24.00,	10.00,	0,	0.00,	0.00,	6,	2020,	70.00,	NULL,	NULL,	NULL,	NULL,	1,	20000.00,	6,	1,	NULL,	0,	1,	0),
(9,	'Agentes Internos de la Asociación y la Cooperativa',	'AG',	1,	NULL,	'2020-06-04 14:29:58',	24.00,	10.00,	0,	0.00,	0.00,	6,	2020,	70.00,	NULL,	NULL,	NULL,	NULL,	1,	20000.00,	6,	1,	NULL,	0,	1,	1),
(10,	'Cartera General',	'CG',	1,	NULL,	'2020-06-04 14:31:13',	36.00,	10.00,	0,	0.00,	0.00,	6,	2020,	70.00,	NULL,	NULL,	NULL,	NULL,	1,	20000.00,	6,	1,	NULL,	0,	1,	0);


INSERT INTO `parametro` (`padre_id`, `agrupador_id`, `nombre`, `descripcion`, `numerico`, `boleano`, `activo`, `creado_por_id`, `actualizado_por_id`, `fecha_creacion`, `fecha_actualizacion`)
VALUES (NULL, '208', 'Eliminación del Cobro del Crédito', NULL, NULL, '1', '1', NULL, NULL, NULL, NULL);


ALTER TABLE `tabla_feriados`
DROP FOREIGN KEY `FK_1`;

ALTER TABLE `tabla_feriados`
DROP FOREIGN KEY `FK_2`;