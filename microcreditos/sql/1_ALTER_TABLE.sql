
--php bin/console d:s:u --dump-sql


DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1,	'admin',	'admin',	'admin@admin.com',	'admin@admin.com',	1,	'Y7hBziYxxRrEUd2l7RCHVmn2Ygh55ICWh8UmBXIBarQ',	'$2y$13$M25DoAlykefm7BogA3gZ.OLdoF6WLejYzpdp1Ibr5dtv0jNemqPBC',	'2020-03-11 14:35:29',	NULL,	NULL,	'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),
(2,	'quiroga.rocio',	'quiroga.rocio',	'mail@mail.com',	'mail@mail.com',	1,	'ptiA6yw1iz05vlaKN8IXo2F9Tca1yzB.fNMWprMwkeg',	'$2y$13$FnwNGbQnlI3viBYatcgACeoidgrnRJjB6926ezm87UZCuafuiszt2',	NULL,	NULL,	NULL,	'a:0:{}'),
(3,	'noguera.rocio',	'noguera.rocio',	'rocio@nomail.com',	'rocio@nomail.com',	1,	'yd3ubnrbC49PZ15sY8IKezOEE1zGYJcVRK3j247i4RA',	'$2y$13$BxIlnbRUywJc2vrj6hUFTO0dd8MXoK2bVhyPagJdtCtUwhZNMlABK',	NULL,	NULL,	NULL,	'a:0:{}'),
(4,	'miñosusana',	'miñosusana',	'susana@nomail.com',	'susana@nomail.com',	1,	'4y1nolGiOCnLAEcchEHsdOfuwx/XEB5IKybw3XgRP/o',	'$2y$13$bwIe54iDx7bEAsyWH8s.yuFFlIWTXgBM4TYtNxAqj4s3LBKVJsLcW',	NULL,	NULL,	NULL,	'a:0:{}'),
(5,	'grunn.tamara',	'grunn.tamara',	'tamara@nomail.com',	'tamara@nomail.com',	1,	'2k7fwu0aF4eEl7pY5lr.vbeKKtTMIRAhML8TweM/5.g',	'$2y$13$1t28ezxgs2nt0uJQXrOWZOkce3KpU9saUyVOkfR/5ypR5GzwSfChO',	NULL,	NULL,	NULL,	'a:0:{}'),
(6,	'pereyra.marisol',	'pereyra.marisol',	'elianemarisel@hotmail.com',	'elianemarisel@hotmail.com',	1,	'dwutu1dIJvPIolHO4djZqrZXmI351gRQAq0KQnroei8',	'$2y$13$w1aSum9N3GpGqdA3DCnfiuWVzqJ1cbJBCrK7yf4Csa7u3dxeXRXIu',	NULL,	NULL,	NULL,	'a:0:{}'),
(7,	'bogado.lorena',	'bogado.lorena',	'lorenacbogado@hotmail.com',	'lorenacbogado@hotmail.com',	1,	'Y9jcLYKYJaSgJnifZ4chdWjwUwupxrNWZfbX5wHIEWE',	'$2y$13$TOUF.9VswqCizIPuCyAFWOKJUEIrVbTBKbbdFSn7/qDZ18QGW1uH.',	NULL,	NULL,	NULL,	'a:0:{}'),
(8,	'pereyra.matias',	'pereyra.matias',	'matiasspereira777@gmail.com',	'matiasspereira777@gmail.com',	1,	'L9HbeKPqBr1M1YUnZVphhicvwJUkYOAjMgTxJxid9T0',	'$2y$13$PsxwAEsr6kUb0CnUjkCq5.4l/tghSA1suEMg9u081/mnyimcuiFtS',	NULL,	NULL,	NULL,	'a:0:{}'),
(9,	'coronel.daniel',	'coronel.daniel',	'jatcoronel@hotmail.com',	'jatcoronel@hotmail.com',	1,	'SRyOt7xp7iUOCos9ezVUVaF2Ur28c3lQqqzkRaO1plo',	'$2y$13$6G0oYPeqO6Wha9.GjZCTCeF8Qc3bMIt7.yVovNb6oMiZvmkv1TUlC',	NULL,	NULL,	NULL,	'a:1:{i:0;s:11:\"ROLE_GRUPOS\";}'),
(11,	'TEST2',	'test2',	'res2@as.com',	'res2@as.com',	1,	'BieXiJEBo.2onXQW8cPNCH9Dq55cLLa1YuJ3w6kayPw',	'$2y$13$bt6v0z9V3aE4GIq/yLS5rO.5rLCOVWtYlQpU0nt6haOAabNHYMyYm',	'2020-02-26 17:40:24',	NULL,	NULL,	'a:1:{i:0;s:10:\"ROLE_ADMIN\";}');


ALTER TABLE gastos DROP FOREIGN KEY FK_17A58ACDB38439E;
ALTER TABLE gastos ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE gastos ADD CONSTRAINT FK_17A58ACFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE gastos ADD CONSTRAINT FK_17A58ACF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE gastos ADD CONSTRAINT FK_17A58ACDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_17A58ACFE35D8C4 ON gastos (creado_por_id);
CREATE INDEX IDX_17A58ACF6167A1C ON gastos (actualizado_por_id);

ALTER TABLE grupo DROP FOREIGN KEY FK_8C0E9BD3DB38439E;
ALTER TABLE grupo ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE grupo ADD CONSTRAINT FK_8C0E9BD3FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE grupo ADD CONSTRAINT FK_8C0E9BD3F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE grupo ADD CONSTRAINT FK_8C0E9BD3DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_8C0E9BD3FE35D8C4 ON grupo (creado_por_id);
CREATE INDEX IDX_8C0E9BD3F6167A1C ON grupo (actualizado_por_id);

ALTER TABLE credito DROP FOREIGN KEY FK_55168D31DB38439E;
ALTER TABLE credito ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE credito ADD CONSTRAINT FK_55168D31FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE credito ADD CONSTRAINT FK_55168D31F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE credito ADD CONSTRAINT FK_55168D31DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_55168D31FE35D8C4 ON credito (creado_por_id);
CREATE INDEX IDX_55168D31F6167A1C ON credito (actualizado_por_id);

ALTER TABLE emprendedor_datos_adicionales ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE emprendedor_datos_adicionales ADD CONSTRAINT FK_6ADDBC40FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE emprendedor_datos_adicionales ADD CONSTRAINT FK_6ADDBC40F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_6ADDBC40FE35D8C4 ON emprendedor_datos_adicionales (creado_por_id);
CREATE INDEX IDX_6ADDBC40F6167A1C ON emprendedor_datos_adicionales (actualizado_por_id);

ALTER TABLE pago DROP FOREIGN KEY FK_F4DF5F3EDB38439E;
ALTER TABLE pago ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE pago ADD CONSTRAINT FK_F4DF5F3EFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE pago ADD CONSTRAINT FK_F4DF5F3EF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE pago ADD CONSTRAINT FK_F4DF5F3EDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_F4DF5F3EFE35D8C4 ON pago (creado_por_id);
CREATE INDEX IDX_F4DF5F3EF6167A1C ON pago (actualizado_por_id);

ALTER TABLE tabla_gastos ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE tabla_gastos ADD CONSTRAINT FK_DBE25395FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE tabla_gastos ADD CONSTRAINT FK_DBE25395F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_DBE25395FE35D8C4 ON tabla_gastos (creado_por_id);
CREATE INDEX IDX_DBE25395F6167A1C ON tabla_gastos (actualizado_por_id);

ALTER TABLE cuota ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE cuota ADD CONSTRAINT FK_763CCB0FFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE cuota ADD CONSTRAINT FK_763CCB0FF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_763CCB0FFE35D8C4 ON cuota (creado_por_id);
CREATE INDEX IDX_763CCB0FF6167A1C ON cuota (actualizado_por_id);

ALTER TABLE credito_refinanciacion ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE credito_refinanciacion ADD CONSTRAINT FK_5E5C3CECFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE credito_refinanciacion ADD CONSTRAINT FK_5E5C3CECF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_5E5C3CECFE35D8C4 ON credito_refinanciacion (creado_por_id);
CREATE INDEX IDX_5E5C3CECF6167A1C ON credito_refinanciacion (actualizado_por_id);

ALTER TABLE programa DROP FOREIGN KEY FK_2F0140DDB38439E;
ALTER TABLE programa ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE programa ADD CONSTRAINT FK_2F0140DFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE programa ADD CONSTRAINT FK_2F0140DF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE programa ADD CONSTRAINT FK_2F0140DDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_2F0140DFE35D8C4 ON programa (creado_por_id);
CREATE INDEX IDX_2F0140DF6167A1C ON programa (actualizado_por_id);

ALTER TABLE tabla_feriados ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE tabla_feriados ADD CONSTRAINT FK_69B863FDE12AB56 FOREIGN KEY (created_by) REFERENCES fos_user (id);
ALTER TABLE tabla_feriados ADD CONSTRAINT FK_69B863F16FE72E1 FOREIGN KEY (updated_by) REFERENCES fos_user (id);
ALTER TABLE tabla_feriados ADD CONSTRAINT FK_69B863FFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE tabla_feriados ADD CONSTRAINT FK_69B863FF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_69B863FFE35D8C4 ON tabla_feriados (creado_por_id);
CREATE INDEX IDX_69B863FF6167A1C ON tabla_feriados (actualizado_por_id);
ALTER TABLE tabla_feriados RENAME INDEX fk_1 TO IDX_69B863FDE12AB56;
ALTER TABLE tabla_feriados RENAME INDEX fk_2 TO IDX_69B863F16FE72E1;

ALTER TABLE programa_ingreso DROP FOREIGN KEY FK_DF152217DB38439E;
ALTER TABLE programa_ingreso ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE programa_ingreso ADD CONSTRAINT FK_DF152217FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE programa_ingreso ADD CONSTRAINT FK_DF152217F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE programa_ingreso ADD CONSTRAINT FK_DF152217DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_DF152217FE35D8C4 ON programa_ingreso (creado_por_id);
CREATE INDEX IDX_DF152217F6167A1C ON programa_ingreso (actualizado_por_id);

ALTER TABLE emprendedor DROP FOREIGN KEY FK_E8418E3EDB38439E;
ALTER TABLE emprendedor ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE emprendedor ADD CONSTRAINT FK_E8418E3EFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE emprendedor ADD CONSTRAINT FK_E8418E3EF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE emprendedor ADD CONSTRAINT FK_E8418E3EDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_E8418E3EFE35D8C4 ON emprendedor (creado_por_id);
CREATE INDEX IDX_E8418E3EF6167A1C ON emprendedor (actualizado_por_id);

ALTER TABLE reporte_mensual DROP FOREIGN KEY FK_D553458EDB38439E;
ALTER TABLE reporte_mensual ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE reporte_mensual ADD CONSTRAINT FK_D553458EFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE reporte_mensual ADD CONSTRAINT FK_D553458EF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE reporte_mensual ADD CONSTRAINT FK_D553458EDB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
CREATE INDEX IDX_D553458EFE35D8C4 ON reporte_mensual (creado_por_id);
CREATE INDEX IDX_D553458EF6167A1C ON reporte_mensual (actualizado_por_id);

ALTER TABLE credito_emprendedor ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE credito_emprendedor ADD CONSTRAINT FK_9770CBA5FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE credito_emprendedor ADD CONSTRAINT FK_9770CBA5F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_9770CBA5FE35D8C4 ON credito_emprendedor (creado_por_id);
CREATE INDEX IDX_9770CBA5F6167A1C ON credito_emprendedor (actualizado_por_id);

ALTER TABLE agenda DROP FOREIGN KEY FK_2CEDC87798A29FBE;
ALTER TABLE agenda ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD activo TINYINT(1) NOT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE agenda ADD CONSTRAINT FK_2CEDC877FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE agenda ADD CONSTRAINT FK_2CEDC877F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE agenda ADD CONSTRAINT FK_2CEDC87798A29FBE FOREIGN KEY (asesor_id) REFERENCES fos_user (id);
CREATE INDEX IDX_2CEDC877FE35D8C4 ON agenda (creado_por_id);
CREATE INDEX IDX_2CEDC877F6167A1C ON agenda (actualizado_por_id);

ALTER TABLE parametro ADD creado_por_id INT DEFAULT NULL, ADD actualizado_por_id INT DEFAULT NULL, ADD fecha_creacion DATETIME DEFAULT NULL, ADD fecha_actualizacion DATETIME DEFAULT NULL;
ALTER TABLE parametro ADD CONSTRAINT FK_4C12795FFE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
ALTER TABLE parametro ADD CONSTRAINT FK_4C12795FF6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
CREATE INDEX IDX_4C12795FFE35D8C4 ON parametro (creado_por_id);
CREATE INDEX IDX_4C12795FF6167A1C ON parametro (actualizado_por_id);


ALTER TABLE `credito` ADD INDEX `credito_idx_aprobado_programa_id_fecha_alta` (`aprobado`,`programa_id`,`fecha_alta`);
ALTER TABLE `credito` ADD INDEX `credito_idx_nro_credito` (`nro_credito`);
ALTER TABLE `cuota` ADD INDEX `cuota_idx_cancelado_credito_id` (`cancelado`,`credito_id`);




