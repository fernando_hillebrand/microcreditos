-- Borra la segunda entrega del credito para el mismo credito
DELETE FROM caja WHERE `caja`.`id` = 16149;
DELETE FROM caja WHERE `caja`.`id` = 17095;

-- Se borra de la caja los pagos registrados
DELETE FROM caja WHERE concepto = 210 AND  pago_id >=112370 AND pago_id <=112379;

-- Borra los pagos de las cuotas duplicadas
DELETE FROM pago WHERE cuota_id  >= 136015 AND cuota_id<= 136024;

-- Borra los cuotas duplicadas
DELETE FROM cuota WHERE id  >= 136015 AND id <= 136024;


-- Se quita los intereses de los pagos de las cuotas originales
UPDATE cuota  SET monto_punitorios= 0, pagado= 67078 WHERE id  >= 136005 AND id <= 136014;

-- Se resta los intereses de los pagos de las cuotas originales
UPDATE pago SET punitorios= 0, total=67078 WHERE cuota_id  >= 136005 AND cuota_id<= 136014;

-- se actualiza los pagos en la caja de los pagos de las cuotas originales
UPDATE caja SET ingreso = 67078 WHERE pago_id >=112360 and  pago_id <=112369 AND id >= 17072 AND id <= 17081;



-- se elmina de la caja el gasto Id 2626
DELETE FROM caja where gasto_id= 2626;

-- se elimna el gasto id 2626
DELETE FROM  gastos where id = 2626;


-- Ajuste por pago banco
DELETE FROM caja WHERE `caja`.`id` = 17072;
DELETE FROM caja WHERE `caja`.`id` = 17073;
DELETE FROM caja WHERE `caja`.`id` = 17074;
DELETE FROM caja WHERE `caja`.`id` = 17075;

INSERT INTO `pagos_bancarios` (`programa_id`, `usuario_id`, `concepto`, `creado_por_id`, `actualizado_por_id`, `ingreso`, `fecha`, `obserbacion`, `activo`, `fecha_creacion`, `fecha_actualizacion`, `credito_id`, `pago_id`)
VALUES ('7', '17', '210', '17', '17', '67078', '2023-04-27 10:53:08	', 'Pago Bancario (Ajuste Sistema)', '1', '2023-06-16 10:53:08	', '2023-06-16 10:53:08	', '11260', '112360');
INSERT INTO `pagos_bancarios` (`programa_id`, `usuario_id`, `concepto`, `creado_por_id`, `actualizado_por_id`, `ingreso`, `fecha`, `obserbacion`, `activo`, `fecha_creacion`, `fecha_actualizacion`, `credito_id`, `pago_id`)
VALUES ('7', '17', '210', '17', '17', '67078', '2023-04-27 10:53:08	', 'Pago Bancario (Ajuste Sistema)', '1', '2023-06-16 10:53:08	', '2023-06-16 10:53:08	', '11260', '112361');
INSERT INTO `pagos_bancarios` (`programa_id`, `usuario_id`, `concepto`, `creado_por_id`, `actualizado_por_id`, `ingreso`, `fecha`, `obserbacion`, `activo`, `fecha_creacion`, `fecha_actualizacion`, `credito_id`, `pago_id`)
VALUES ('7', '17', '210', '17', '17', '67078', '2023-04-27 10:53:08	', 'Pago Bancario (Ajuste Sistema)', '1', '2023-06-16 10:53:08	', '2023-06-16 10:53:08	', '11260', '112362');
INSERT INTO `pagos_bancarios` (`programa_id`, `usuario_id`, `concepto`, `creado_por_id`, `actualizado_por_id`, `ingreso`, `fecha`, `obserbacion`, `activo`, `fecha_creacion`, `fecha_actualizacion`, `credito_id`, `pago_id`)
VALUES ('7', '17', '210', '17', '17', '67078', '2023-04-27 10:53:08	', 'Pago Bancario (Ajuste Sistema)', '1', '2023-06-16 10:53:08	', '2023-06-16 10:53:08	', '11260', '112363');