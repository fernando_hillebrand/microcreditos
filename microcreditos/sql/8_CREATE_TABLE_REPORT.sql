/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `concepto_gastos`
--

DROP TABLE IF EXISTS `concepto_gastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `concepto_gastos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` TINYINT(1) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL,
  `fecha_actualizacion` DATETIME NOT NULL,
   `creado_por_id` INT DEFAULT NULL,
    `actualizado_por_id` INT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
 ALTER TABLE concepto_gastos ADD CONSTRAINT FK_concepto_gastos_creado_por_id FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
 ALTER TABLE concepto_gastos ADD CONSTRAINT FK_concepto_gastos_actualizado_por_id FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `creditos_admin`
--

DROP TABLE IF EXISTS `creditos_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `creditos_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gastos_totales`
--

DROP TABLE IF EXISTS `gastos_totales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gastos_totales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `id_concepto` int(11) NOT NULL,
  `activo` TINYINT(1) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL,
  `fecha_actualizacion` DATETIME NOT NULL,
  `num_comprobante` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
     `creado_por_id` INT DEFAULT NULL,
    `actualizado_por_id` INT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
 ALTER TABLE gastos_totales ADD CONSTRAINT FK_gastos_totales_creado_por_id FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
 ALTER TABLE gastos_totales ADD CONSTRAINT FK_gastos_totales_actualizado_por_id FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);
ALTER TABLE gastos_totales CHANGE id_concepto id_concepto INT DEFAULT NULL;
ALTER TABLE gastos_totales ADD CONSTRAINT FK_6D237EF934586445 FOREIGN KEY (id_concepto) REFERENCES concepto_gastos (id);
--
-- Table structure for table `registros_bancarios`
--

DROP TABLE IF EXISTS `registros_bancarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registros_bancarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `cuenta` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banco` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- FIX ONLY_FULL_GROUP_BY`
--
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));


--
-- Dumping data for table `concepto_gastos`
--

LOCK TABLES `concepto_gastos` WRITE;
/*!40000 ALTER TABLE `concepto_gastos` DISABLE KEYS */;
INSERT INTO `concepto_gastos` VALUES (1,'dgrfg'),(2,'Facturas'),(3,'Pagos'),(4,'Pagos'),(5,'otros pagos');
/*!40000 ALTER TABLE `concepto_gastos` ENABLE KEYS */;


--
-- Dumping data for table `creditos_admin`
--

LOCK TABLES `creditos_admin` WRITE;
/*!40000 ALTER TABLE `creditos_admin` DISABLE KEYS */;
INSERT INTO `creditos_admin` VALUES (1,'2022-12-14',34545.00),(2,'2022-10-05',122010.00),(3,'2022-12-01',41152.00);
/*!40000 ALTER TABLE `creditos_admin` ENABLE KEYS */;

--
-- Dumping data for table `gastos_totales`
--

LOCK TABLES `gastos_totales` WRITE;
/*!40000 ALTER TABLE `gastos_totales` DISABLE KEYS */;
INSERT INTO `gastos_totales` VALUES (1,3243.00,'2022-12-15',1,'3432'),(2,32432.00,'2022-12-07',2,'23432'),(3,121554.00,'2022-09-15',2,'7347547654');
/*!40000 ALTER TABLE `gastos_totales` ENABLE KEYS */;


--
-- Dumping data for table `registros_bancarios`
--

LOCK TABLES `registros_bancarios` WRITE;
/*!40000 ALTER TABLE `registros_bancarios` DISABLE KEYS */;
INSERT INTO `registros_bancarios` VALUES (1,'2022-12-08',545.00,'343254','Macro'),(2,'2022-12-08',234324.00,'43253','Galicia'),(3,'2022-11-10',15000.00,'343254','Macro'),(4,'2022-12-08',35435.00,'34545','Galicia');
/*!40000 ALTER TABLE `registros_bancarios` ENABLE KEYS */;


UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


