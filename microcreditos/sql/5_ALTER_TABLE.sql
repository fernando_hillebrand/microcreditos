ALTER TABLE programa
  ADD porc_gastos_adm DECIMAL(10,2) default NULL
;

CREATE TABLE pagos_bancarios
  (
    id INT AUTO_INCREMENT NOT NULL,
    programa_id INT DEFAULT NULL,
    usuario_id INT DEFAULT NULL,
    concepto INT DEFAULT NULL,
    creado_por_id INT DEFAULT NULL,
    actualizado_por_id INT DEFAULT NULL,
    ingreso NUMERIC(10, 2) NOT NULL,
    fecha DATETIME NOT NULL,
    obserbacion varchar(255) DEFAULT NULL,
    activo TINYINT(1) NOT NULL,
    fecha_creacion DATETIME NOT NULL,
    fecha_actualizacion DATETIME NOT NULL,

    INDEX IDX_E465F405FD8A7328 (programa_id),
    INDEX IDX_E465F405DB38439E (usuario_id),
    INDEX IDX_E465F405648388D0 (concepto),
    INDEX IDX_E465F405FE35D8C4 (creado_por_id),
    INDEX IDX_E465F405F6167A1C (actualizado_por_id),
    PRIMARY KEY(id)
  ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;

 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405FD8A7328 FOREIGN KEY (programa_id) REFERENCES programa (id);
 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405DB38439E FOREIGN KEY (usuario_id) REFERENCES fos_user (id);
 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405648388D0 FOREIGN KEY (concepto) REFERENCES parametro (id);
 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405FE35D8C4 FOREIGN KEY (creado_por_id) REFERENCES fos_user (id);
 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405F6167A1C FOREIGN KEY (actualizado_por_id) REFERENCES fos_user (id);


ALTER TABLE pagos_bancarios
  ADD credito_id INT DEFAULT NULL,
  ADD pago_id INT DEFAULT NULL;

 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405PAGO FOREIGN KEY (pago_id) REFERENCES pago (id);
 ALTER TABLE pagos_bancarios ADD CONSTRAINT FK_E565F405CREDITO FOREIGN KEY (credito_id) REFERENCES credito (id);