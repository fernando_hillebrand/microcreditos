ALTER TABLE `credito` ADD INDEX `credito_idx_aprobado_incobrable_programa` (`aprobado`,`incobrable`,`programa_id`);
ALTER TABLE `credito` ADD INDEX `credito_idx_nro_credito_nro_renovacion` (`nro_credito`,`nro_renovacion`);
ALTER TABLE `cuota` ADD INDEX `cuota_idx_credito_id_saldo` (`credito_id`,`saldo`);